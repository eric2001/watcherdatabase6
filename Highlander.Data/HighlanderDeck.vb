﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Linq

Public Class HighlanderDeck

    ''' <summary>
    ''' Contains a list of all cards in the current deck.
    ''' </summary>
    ''' <returns></returns>
    Public Property Deck As New List(Of Card)

    ''' <summary>
    ''' Contains a count of the total number of cards in the deck.
    ''' Note:  This returns total number of cards, not total records, so if a deck contains 6 copies of Generic Upper Left Attack, then this will return 6, not 1.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property DeckSize() As Integer
        Get
            Return Deck.Sum(Function(card) card.Quantity)
        End Get
    End Property

    ''' <summary>
    ''' Contains a list of all Pre-Game cards used with the deck.
    ''' </summary>
    Public PreGame As New List(Of Card)

    ''' <summary>
    ''' Contains a count of the total number of pre-game cards in the deck.
    ''' Note:  This returns total number of cards, not total records, so if a deck contains 6 copies of Generic Upper Left Attack, then this will return 6, not 1.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property PreGameSize() As Integer
        Get
            Return PreGame.Sum(Function(card) card.Quantity)
        End Get
    End Property
End Class
