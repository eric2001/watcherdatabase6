﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class TradeList

    ''' <summary>
    ''' List of all cards on the trade lists Haves list.
    ''' </summary>
    ''' <returns></returns>
    Public Property Haves As New List(Of Card)

    ''' <summary>
    ''' List of all cards on the trade lists Wants list.
    ''' </summary>
    ''' <returns></returns>
    Public Property Wants As New List(Of Card)

    ''' <summary>
    ''' Contains a count of the total number of cards in the Haves list.
    ''' Note:  This returns total number of cards, not total records, so if the list contains 6 copies of Generic Upper Left Attack, then this will return 6, not 1.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property TotalHaves() As Integer
        Get
            Return Haves.Sum(Function(card) card.Quantity)
        End Get
    End Property

    ''' <summary>
    ''' Contains a count of the total number of cards in the Wants list.
    ''' Note:  This returns total number of cards, not total records, so if the list contains 6 copies of Generic Upper Left Attack, then this will return 6, not 1.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property TotalWants() As Integer
        Get
            Return Wants.Sum(Function(card) card.Quantity)
        End Get
    End Property

End Class
