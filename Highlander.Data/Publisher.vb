﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class Publisher

    ''' <summary>
    ''' Contains the Unique Id for this publisher.
    ''' </summary>
    ''' <returns></returns>
    Public Property Id As Integer

    ''' <summary>
    ''' Contains the unique code for this publisher.
    ''' </summary>
    ''' <returns></returns>
    Public Property Code As String

    ''' <summary>
    ''' Contains the full name for this publisher.
    ''' </summary>
    ''' <returns></returns>
    Public Property Name As String

    ''' <summary>
    ''' Converts a data from from the HLSets.xml file to a Publisher object.
    ''' </summary>
    ''' <param name="record">DataRow from the HLSets.xml file.</param>
    ''' <returns>A Publisher object populated with the values from the passed in record.</returns>
    Public Shared Function DataRowToPublisher(ByVal record As DataRow) As Publisher
        Dim publisher As New Publisher

        publisher.Id = record.Item("ID").ToString()
        publisher.Code = record.Item("Short_Name").ToString()
        publisher.Name = record.Item("Long_Name").ToString()

        Return publisher
    End Function

    ''' <summary>
    ''' Returns a Publisher object that corresponds to the passed in ID value.
    ''' </summary>
    ''' <param name="SetId">The Unique ID number of a record from the HLSets.xml file.</param>
    ''' <returns>A Publisher object populated with the values from the corresponding HLSets.xml record or Nothing if the Id is not valid.</returns>
    Public Shared Function LoadPublisher(ByVal PublisherId As Integer) As Publisher
        Dim SelectedSet() As DataRow = SetsDB.Data.Tables("Publisher").Select("ID = '" & PublisherId.ToString() & "'")
        If SelectedSet.Length > 0 Then
            Return DataRowToPublisher(SelectedSet(0))
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Returns a Publisher object that corresponds to the passed in publisher name.
    ''' </summary>
    ''' <param name="SetId">The Publisher Name of a record from the HLSets.xml file.</param>
    ''' <returns>A Publisher object populated with the values from the corresponding HLSets.xml record or Nothing if the Publisher Name is not valid.</returns>
    Public Shared Function LoadPublisher(ByVal PublisherName As String) As Publisher
        Dim SelectedSet() As DataRow = SetsDB.Data.Tables("Publisher").Select("Long_Name = '" & PublisherName.Replace("'", "''") & "'")
        If SelectedSet.Length > 0 Then
            Return DataRowToPublisher(SelectedSet(0))
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Returns a list of Publisher objects that match the SeachQuery.
    ''' </summary>
    ''' <param name="SearchQuery">The raw search string to use to search the HLSets.xml file (ex: "Long_Name = 'Thunder Castle Games'").  Pass in an empty string value to return all publishers.</param>
    ''' <returns>A list of Publisher objects that match the passed in search query.</returns>
    Public Shared Function LoadPublishers(ByVal Optional SearchQuery As String = "") As List(Of Publisher)
        Return New List(Of Publisher)((From publisherRow As DataRow In SetsDB.Data.Tables("Publisher").Select(SearchQuery).AsEnumerable() Select (DataRowToPublisher(publisherRow))))
    End Function
End Class


