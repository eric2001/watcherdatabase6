﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class HLD

    ''' <summary>
    ''' Load a saved .hld file.
    ''' </summary>
    ''' <param name="FileName">Full file path and file name of the .hld file to load</param>
    ''' <returns>A HighlanderDeck object containing the details from the .hld file.</returns>
    Public Shared Function Load(ByVal FileName As String) As HighlanderDeck
        ' This function is responsible for opening a deck that has previously been
        '   saved in .hld format.
        Dim Deck As New HighlanderDeck

        Dim FileReader As New System.IO.StreamReader(FileName)
        Dim tempLine As String, newCard() As String

        Do
            ' Read the file, one line at a time.
            tempLine = FileReader.ReadLine
            If tempLine <> "" Then

                ' Split the current line up into an array of three elements.
                '   (0) is either DECK or PREGAME for haves or Wants
                '   (1) is the ID Number
                '   (2) is the Quantity
                newCard = tempLine.Split(",")
                Dim counter As Integer = 0

                ' Loop through each item in the array, remove "" characters
                '   from around text values.
                While counter < 3
                    If newCard(counter).Chars(0) = """" Then
                        newCard(counter) = newCard(counter).Substring(1)
                    End If
                    If newCard(counter).Chars(newCard(counter).Length - 1) = """" Then
                        newCard(counter) = newCard(counter).Substring(0, newCard(counter).Length - 1)
                    End If
                    counter = counter + 1
                End While

                ' Load the image and details of the current card.
                Dim SelectedCard As Card = Card.LoadCard(newCard(1), newCard(2))
                If SelectedCard Is Nothing Then
                    Throw New Exception(String.Format("Unable to Load Deck:  Card ID {0} Not Found.", newCard(1)))
                End If

                ' Add the current card to either the Deck or Pre-Game list, based 
                '   on newCard(0).
                If newCard(0) = "DECK" Then
                    Deck.Deck.Add(SelectedCard)
                ElseIf newCard(0) = "PREGAME" Then
                    Deck.PreGame.Add(SelectedCard)
                Else
                    Dim SelectedPreGameCard As Card = Deck.PreGame.Where(Function(c) c.Id = newCard(0)).FirstOrDefault()
                    If SelectedPreGameCard IsNot Nothing Then
                        SelectedPreGameCard.SubItems.Add(SelectedCard)
                    Else
                        Throw New Exception(String.Format("Error loading file: {0}: Pre-Game card {1} not found.", FileName, newCard(0)))
                    End If
                End If
            End If
        Loop Until tempLine Is Nothing

        FileReader.Close()
        FileReader.Dispose()
        FileReader = Nothing

        Return Deck
    End Function

    ''' <summary>
    ''' Save a HighlanderDeck object as a .hld file.
    ''' </summary>
    ''' <param name="FileName">The full path and file name for where the data should be saved to.</param>
    ''' <param name="Deck">The data to save.</param>
    ''' <returns>Returns True if the save is successful.</returns>
    Public Shared Function Save(ByVal FileName As String, ByVal Deck As HighlanderDeck) As Boolean
        ' This sub is responsible for saving the current deck to a .hld file.
        '   If the deck has already been saved once, the save as dialog is skipped.

        Dim FileWriter As New System.IO.StreamWriter(FileName)
        Dim tempCard As Card

        ' Save each card in the pre-game list to the file in HLD format.
        For Each tempCard In Deck.PreGame
            FileWriter.WriteLine(String.Format("""PREGAME"",""{0}"",{1}", tempCard.Id, tempCard.Quantity))
        Next

        ' Save each card in the deck to the file in HLD format.
        For Each tempCard In Deck.Deck
            FileWriter.WriteLine(String.Format("""DECK"",""{0}"",{1}", tempCard.Id, tempCard.Quantity))
        Next

        ' Save Sub-Cards
        For Each PreGameCard As Card In Deck.PreGame
            For Each tempCard In PreGameCard.SubItems
                FileWriter.WriteLine(String.Format("""{0}"",""{1}"",{2}", PreGameCard.Id, tempCard.Id, tempCard.Quantity))
            Next
        Next

        ' Close the file.
        FileWriter.Close()
        FileWriter.Dispose()
        FileWriter = Nothing

        Return True
    End Function
End Class
