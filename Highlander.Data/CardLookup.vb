﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class CardLookup
    Class ThunderCastleGames

        Enum PromotionalCards

            ''' <summary>
            ''' Darius Pre-Game (Restriction)
            ''' Play this card before the game begins to increase the restriction number on a card you may normally include in your deck. You are allowed 1 extra card. You must announce to your opponent what card Darius affects before play begins.
            ''' </summary>
            DariusPreGameRestriction = 1871

            ''' <summary>
            ''' Quickening Pre-Game (Xavier St. Cloud)
            ''' You may have twice the number of restricted Plots in your deck than normally allowed.
            ''' </summary>
            QuickeningXavierStCloud = 968

            ''' <summary>
            ''' Quickening Pre-Game (Methos)
            ''' You may use Master cards from any Persona.
            ''' </summary>
            QuickeningMethos = 959

            ''' <summary>
            ''' Quickening Pre-Game (Bob)
            ''' You may use one of any non-Reserved card from each Persona. Normal card restrictions still apply.
            ''' </summary>
            QuickeningBob = 970

        End Enum

        Enum TheWatchersChronicle

            ''' <summary>
            ''' Darius Pre-Game (Master)
            ''' This card allows you to increase the number of Master cards you may normally include in your deck. You are allowed 1 extra Master card. Darius will not allow you to increase the restriction number of any Master card. You must announce to your opponent which Master card Darius allows before play begins.
            ''' </summary>
            DariusPreGameMaster = 2309

        End Enum

        Enum DuncanCollection

            ''' <summary>
            ''' Duncan MacLeod Premium (2 additional Pre-Game Cards)
            ''' Play this card before the game begins in conjunction with the Duncan MacLeod Persona card. You may use up to 2 additional Pre-Game cards beyond the normal restriction of Pre-Game cards. This card modifies your persona card and may not be removed from the game. 
            ''' </summary>
            DuncanMacLeodPremiumAdditionalCards = 1899

        End Enum

        Enum MethosCollection

            ''' <summary>
            ''' Methos Persona
            ''' You may use Master cards from any Persona, even if Reserved. At the beginning of your turn you may discard up to two cards from your hand and replace them from your Endurance. Only Adam Pierson can be assigned as your Watcher. You may have up to 8 Master cards. 
            ''' </summary>
            MethosPersona = 886

            ''' <summary>
            ''' Adam Pierson Pre-Game
            ''' Adam Pierson may only be assigned to Methos. While Adam Pierson is assigned as your Watcher, your opponent must discard a Special card to play a Watcher card.
            ''' </summary>
            AdamPiersonPreGame = 885

        End Enum

        Enum Misprints

            ''' <summary>
            ''' Master's Prize Pre-Game (Iman Fasil MCBC Back)
            ''' This card allows you to increase the restriction number on one card normally restricted to 6. This card is now restricted to 7. You must announce which card you have increased to 7 before play begins.
            ''' </summary>
            MastersPrizePreGameMisprint1 = 3706

            ''' <summary>
            ''' Master's Prize Pre-Game (Iman Fasil MCBC Front)
            ''' This card allows you to increase the restriction number on one card normally restricted to 6. This card is now restricted to 7. You must announce which card you have increased to 7 before play begins.
            ''' </summary>
            MastersPrizePreGameMisprint2 = 2428

            ''' <summary>
            ''' Master's Prize Pre-Game (Kane +1 Text Back)
            ''' This card allows you to increase the restriction number on one card normally restricted to 6. This card is now restricted to 7. You must announce which card you have increased to 7 before play begins.
            ''' </summary>
            MastersPrizePreGameMisprint3 = 3705

            ''' <summary>
            ''' Master's Prize Pre-Game (Yung Dol Kim +1 Front)
            ''' This card allows you to increase the restriction number on one card normally restricted to 6. This card is now restricted to 7. You must announce which card you have increased to 7 before play begins.
            ''' </summary>
            MastersPrizePreGameMisprint4 = 3707

        End Enum

        Enum ArmsAndTactics

            ''' <summary>
            ''' Generic Immortal Persona (WoC)
            ''' If you are using a Weapon of Choice, you may increase the Restriction of all Weapon Specific cards for that Weapon of Choice by one. You may include up to 3 Master card in your deck. 
            ''' </summary>
            GenericImmortalPersonaWoC = 1460

            ''' <summary>
            ''' Generic Immortal Persona (A&D)
            ''' You do not have to include the Basic Attacks and Defenses, but you must include at least 9 attacks and 6 blocks in your deck. You may include up to 3 Master cards in your deck. 
            ''' </summary>
            GenericImmortalPersonaAD = 1459

            ''' <summary>
            ''' Generic Immortal Persona (Immortal Specific)
            ''' Instead of using the Generic Immortal deck construction rules, you may include any number of Immortal Specific cards from one Persona that are not Reserved or Signature in your deck. Normal restrictions still apply. You may include up to 3 Master cards in your deck. 
            ''' </summary>
            GenericImmortalPersonaImmortalSpecific = 1458

            ''' <summary>
            ''' Generic Immortal Persona (Res)
            ''' You may include up to 3 of any Immortal Specific Reserved cards in your deck. For each Reserved card you include, your Master card limitation is reduced by one. Restriction numbers still apply. You may include up to 3 Master cards in your deck. 
            ''' </summary>
            GenericImmortalPersonaRes = 1457

            ''' <summary>
            ''' Generic Big and Bad
            ''' During deck construction, your minimum deck size is 80 cards. For each Big and Bad in your deck, this minimum increases by 10 cards. You may play Big and Bad during your turn to draw 10 cards.
            ''' </summary>
            GenericBigAndBad = 1171

            ''' <summary>
            ''' Parrying Blade Pre-Game
            ''' You may use Parrying Blade if you are using another one-handed Weapon of Choice. Your opponent's first attack during his turn cannot be to an area that your last defense played blocked during your turn. If you make a Power Block, you must discard the top 3 cards from your Endurance. You can only have one Parrying Blade in play. 
            ''' </summary>
            ParryingBladePreGame = 1570

            ''' <summary>
            ''' Shield Pre-Game
            ''' You may use the Shield if you are using another one-handed (1 Hand Icon) Weapon of Choice in play. Play this card before the game begins. You can only attack as if disarmed if the only Weapon of Choice you are armed with is Shield. Any basic block you play may remain in play as a Guard and is considered to be a Standing Defense. At the beginning of your turn, you must discard the top card of your Endurance for each Guard you have in play. 
            ''' </summary>
            ShieldPreGame = 1589

        End Enum

        Enum SeriesEdition

            ''' <summary>
            ''' Xavier St. Cloud Persona
            ''' If you do not play a Special card, you may play an additional 1 point Middle Center Attack, that cannot be a Power Blow. You may include up to twice the normal number of Plot cards. You may include up to five Master cards. 
            ''' </summary>
            XavierStCloudPersona = 1414

            ''' <summary>
            ''' Generic Darius (Event)
            ''' You may include one card from another Persona in your deck. You may only play that card or put that card into play in conjunction with this card. You may play this card in conjunction with another Special card even if you are only allowed 1 Special card this turn. 
            ''' </summary>
            GenericDarius = 1266

            ''' <summary>
            ''' Richie Ryan Persona
            ''' You may use one non-Signature Immortal Specific card from each Persona. You may include up to five Master cards. 
            ''' </summary>
            RichieRyanPersona = 1396

        End Enum

        Enum TinSet

            ''' <summary>
            ''' Xavier St. Cloud Persona
            ''' If you do not play a Special card, you may play an additional 1 point Middle Center Attack, that cannot be a Power Blow. You may include up to twice the normal number of Plot cards. You may include up to five Master cards. 
            ''' </summary>
            XavierStCloudPersona = 2080

            ''' <summary>
            ''' Generic Darius (Event)
            ''' You may include one card from another Persona in your deck. You may only play that card or put that card into play in conjunction with this card. You may play this card in conjunction with another Special card even if you are only allowed 1 Special card this turn. 
            ''' </summary>
            GenericDarius = 2239

            ''' <summary>
            ''' Richie Ryan Persona
            ''' You may use one non-Signature Immortal Specific card from each Persona. You may include up to five Master cards. 
            ''' </summary>
            RichieRyanPersona = 2062
        End Enum

        Enum MovieEdition

            ''' <summary>
            ''' Generic Immortal Persona
            ''' Though this represents a Generic Immortal, with the use of this Persona card you may use up to 3 Master cards. 
            ''' </summary>
            GenericImmortalPersona = 1724

        End Enum

    End Class

    Class SaecGames

        Enum BlackRavenVol2

            ''' <summary>
            ''' Father Liam Riley Pre-Game
            ''' Play this card before the game begins. You may include one Non-Signature Master card from another Immortal in your deck. You must announce to your opponent what Master Card this card has allowed before play begins. 
            ''' </summary>
            FatherLiamRiley = 3621

        End Enum

        Enum BlackRavenVol3

            ''' <summary>
            ''' Rebecca Horne Pre-Game (Additional Powers) 
            ''' Play this card before the game begins. You may add 1 extra non-Master Dodge type card over the restriction. You must announce which Dodge card was added in conjunction with this Pre-game. 
            ''' </summary>
            RebeccaHorneAdditionalPowers = 3653

        End Enum

        Enum BlackRavenVol4

            ''' <summary>
            ''' Andre Korda Pre-Game (Additional Powers)
            ''' If you do not play a Special Card during your turn, you may remove this card from the game to remove one Location, Object, or Situation titled card from play. 
            ''' </summary>
            AndreKordaPreGameAdditionalPowers = 3684

            ''' <summary>
            ''' Black Phoenix Fan Pre-Game
            ''' You may use Black Phoenix Fan if you are using another one-handed Weapon of Choise. You may return any unused attacks from an Exertion you make to the bottom of your Endurance in any order. You cannot block multiple attacks with a single block. You may play with another one-handed weapon. 
            ''' </summary>
            BlackPhoenixFanPreGame = 3674

        End Enum

    End Class

    Class Mle

        Enum MissingLinkExpansion

            ''' <summary>
            ''' Cimeterre / Scimitar Pre-Game Front (US)
            ''' Any turn you do not play a Special Card, you may extend to one adjoining grid square any Basic Attack. This attack now covers two grid squares and may not be a Power Blow. 
            ''' </summary>
            ScimitarPreGameFrontUs = 491

            ''' <summary>
            ''' Cimeterre / Scimitar Pre-Game Front (French)
            ''' Any turn you do not play a Special Card, you may extend to one adjoining grid square any Basic Attack. This attack now covers two grid squares and may not be a Power Blow. 
            ''' </summary>
            ScimitarPreGameFrontFrench = 330

            ''' <summary>
            ''' Cimeterre / Scimitar Pre-Game Back
            ''' Any turn you do not play a Special Card, you may extend to one adjoining grid square any Basic Attack. This attack now covers two grid squares and may not be a Power Blow. 
            ''' </summary>
            ScimitarPreGameBack = 329

            ''' <summary>
            ''' Jacob Kell Persona Front
            ''' [Multiple card backs with different text]
            ''' </summary>
            JacobKellPersonaFront = 384

            ''' <summary>
            ''' Jacob Kell Persona Back (US)
            ''' For each Quickening you include in a PreGame slot, you may gain an additional Pregame slot, up to five. These additional slots may only be used for Quickenings. Should a Quickening in a regular slot be removed from the game, you must remove the added Quickening as well. You may use up to 6 Master cards. 
            ''' </summary>
            JacobKellPersonaBackUs = 383

            ''' <summary>
            ''' Jacob Kell Persona Back (French)
            ''' If you do not play a Special Card this turn, you may draw one card at random from your opponent's hand. You may play this card as if it was one of your own Immortal Reserved cards. You may not play Signature cards from your opponent's hand. You may use up to 6 Master cards. 
            ''' </summary>
            JacobKellPersonaBackFrench = 382

            ''' <summary>
            ''' Jacob Kell Persona Back (English)
            ''' If you do not play a Special Card this turn, you may draw one card at random from your opponent's hand. You may play this card as if it was one of your own Immortal Reserved cards. You may not play Signature cards from your opponent's hand. You may use up to 6 Master cards. 
            ''' </summary>
            JacobKellPersonaBackEnglish = 381

        End Enum

        Enum MacLeodChronicles

            ''' <summary>
            ''' Escutcheon Pre-Game Front
            ''' You may use the Escutcheon if you are using another one handed (1 Hand Icon) Weapon of Choice in play. Play this card before the game begins. You can only attack as if disarmed if the only Weapon of Choice you are armed with is Escutcheon. You may play basic blocks against Ranged attacks. You must discard the top 3 cards of your Endurance for each ranged attack you block in this manner. 
            ''' </summary>
            EscutcheonPreGameFront = 610

            ''' <summary>
            ''' Escutcheon Pre-Game Back
            ''' You may use the Escutcheon if you are using another one handed (1 Hand Icon) Weapon of Choice in play. Play this card before the game begins. You can only attack as if disarmed if the only Weapon of Choice you are armed with is Escutcheon. You may play basic blocks against Ranged attacks. You must discard the top 3 cards of your Endurance for each ranged attack you block in this manner. 
            ''' </summary>
            EscutcheonPreGameBack = 609

        End Enum

        Enum SoldiersOfImmortality

            ''' <summary>
            ''' Hand Axe Pre-Game Front
            ''' You may use Hand Axe with or without another one handed Weapon Of Choice. You may have two Hand Axe's in play. You may make a Power Blow with a 3 card Exertion. You must discard a defense from your hand for each Power Block you make. 
            ''' </summary>
            HandAxePreGameFront = 1070

            ''' <summary>
            ''' Hand Axe Pre-Game Back
            ''' You may use Hand Axe with or without another one handed Weapon Of Choice. You may have two Hand Axe's in play. You may make a Power Blow with a 3 card Exertion. You must discard a defense from your hand for each Power Block you make. 
            ''' </summary>
            HandAxePreGameBack = 1069

            ''' <summary>
            ''' Katar Pre-Game Back
            ''' You may use the Katar with or without another one handed Weapon Of Choice in Play. You may have two Katar's in play. All of your successful Basic Attacks do an additional 1 point of damage. This damage does not stack with damage from another Weapon of Choice. You may not play attacks that cover more than one grid square with this weapon. 
            ''' </summary>
            KatarPreGameBack = 1084

            ''' <summary>
            ''' Katar Pre-Game Front
            ''' You may use the Katar with or without another one handed Weapon Of Choice in Play. You may have two Katar's in play. All of your successful Basic Attacks do an additional 1 point of damage. This damage does not stack with damage from another Weapon of Choice. You may not play attacks that cover more than one grid square with this weapon. 
            ''' </summary>
            KatarPreGameFront = 1085

        End Enum

        Enum PromotionalCards

            ''' <summary>
            ''' Jacob Kell Persona Front (Alternative Version)
            ''' [Multiple card backs with different text]
            ''' </summary>
            JacobKellPersonaFront = 153

            ''' <summary>
            ''' Katar Pre-Game Front (Alternate Version)
            ''' You may use the Katar with or without another one handed Weapon Of Choice in Play. You may have two Katar's in play. All of your successful Basic Attacks do an additional 1 point of damage. This damage does not stack with damage from another Weapon of Choice. You may not play attacks that cover more than one grid square with this weapon. 
            ''' </summary>
            KatarPreGameAlternateFront = 2391

        End Enum

    End Class

    Class LeMontagnard

        Enum BetaPromotionalCards

            ''' <summary>
            ''' Swordmaster Pre-Game (Fallen Tree) (P1)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameFallenTree = 4329

            ''' <summary>
            ''' Swordmaster Pre-Game (Mansion) (P1)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameMansion = 3841

            ''' <summary>
            ''' Swordmaster Pre-Game (Sulphur Plant) (P1)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameSulphurPlant = 4327

            ''' <summary>
            ''' Swordmaster Pre-Game (Templar Shrine) (P1)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameTemplarShrine = 4328

        End Enum

        Enum PromotionalCards

            ''' <summary>
            ''' Swordmaster Pre-Game (Special Card) (4H-296)
            ''' You may remove this card from the game to counter a Special Card as it is played or put into play. If you do, tear this card in half. 
            ''' </summary>
            SwordmasterPreGameSpecialCard = 7713

            ''' <summary>
            ''' Swordmaster Pre-Game (Kyala) (HSFVP-05)
            ''' Remove this card from the game to counter any one Special Card played by your opponent. Tear this card in half after use. 
            ''' </summary>
            SwordmasterPreGameKyala = 6948

            ''' <summary>
            ''' Swordmaster Pre-Game (Citytop) (HS1R-001)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameCitytop = 6920

            ''' <summary>
            ''' Swordmaster Pre-Game (Fallen Tree) (HS1R-002)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameFallenTree = 6927

            ''' <summary>
            ''' Swordmaster Pre-Game (MacLeod Ruins) (HtGP-12)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameMacLeodRuins = 6978

            ''' <summary>
            ''' Swordmaster Pre-Game (Mansion) (HS1R-004)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameMansion = 6941

            ''' <summary>
            ''' Swordmaster Pre-Game (Mossy Rocks) (HS2-05P)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameMossyRocks = 6950

            ''' <summary>
            ''' Swordmaster Pre-Game (Mountain) (HtGP-10)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameMountain = 6972

            ''' <summary>
            ''' Swordmaster Pre-Game (Snowy House) (HtGP-11)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameSnowyHouse = 6976

            ''' <summary>
            ''' Swordmaster Pre-Game (Sulphur Plant) (HS1R-005)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameSulphurPlant = 6949

            ''' <summary>
            ''' Swordmaster Pre-Game (Templar Shrine) (HS1R-003)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameTemplarShrine = 6934

            ''' <summary>
            ''' Swordmaster Pre-Game (Valley) (HtGP-09)
            ''' Rip this card in half and remove both pieces from the game to ignore the text on one Special Card or Edge in play. 
            ''' </summary>
            SwordmasterPreGameValley = 6967

            ''' <summary>
            ''' Swordmaster Pre-Game (Tank) (HSFVP-06)
            ''' Remove this card from the game to nullify the effects of any one Special or Edge card in play. Tear this card in half after use.
            ''' </summary>
            SwordmasterPreGameTank = 6954

            ''' <summary>
            ''' Colin MacLeod Persona (1st Edition) (HSFVP-08)
            ''' Your opponent's maximum Hand Size is reduced by one for each Ally you control that has been removed from the game. You may play Basic Blocks to defend Ranged Attacks.
            ''' </summary>
            ColinMacLeodPersona1stEdition = 6964

            ''' <summary>
            ''' Felice Martin Persona (1st Edition) (HS1P-007)
            ''' If you do not play a Special Card on your turn, you may look at your opponent's hand once.
            ''' </summary>
            FeliceMartinPersona1stEdition = 6958

            ''' <summary>
            ''' Jin Ke Persona (1st Edition) (HM3-005PP)
            ''' You may play Evades against attacks that cannot be dodged. You may play an additional attack during your turn for each attack you played during your last turn.
            ''' </summary>
            JinKePersona1stEdition = 6917

            ''' <summary>
            ''' Kyala Persona (1st Edition) (HSFVP-07)
            ''' If you do not play a Special Card, you may make a 3-card Hard Exertion to remove an Ally from play. You may make that a 5-card Hard Exertion to remove that Ally from the game instead.
            ''' </summary>
            KyalaPersona1stEdition = 6959

            ''' <summary>
            ''' Marcus Octavius Persona (1st Edition) (HSFVP-09)
            ''' During your May Do / Must Do Phase, you may draw a card for each Object: Ally you have in play.
            ''' </summary>
            MarcusOctaviusPersona1stEdition = 6968

        End Enum

        Enum FourHorsemen

            ''' <summary>
            ''' Cutlass Pre-Game (Caspian) (4H-284)
            ''' You must be Caspian to use this Weapon of Choice. You may nullify any effects in play by your opponent that would prevent or restrict you from playing attacks. If you do not play more than one attack during your turn, you may not draw any cards during your Draw/Discard Phase that turn. This does not count towards your Pre-Game limit.
            ''' </summary>
            CutlassPreGameCaspian = 7685

            ''' <summary>
            ''' English Longsword Pre-Game (Methos) (4H-286)
            ''' You must be Methos to use this Weapon of Choice. Nullify the text on Evades that you play. You may not include Counters in your deck. This does not count towards your Pre-Game limit.
            ''' </summary>
            EnglishLongswordPreGameMethos = 7686

            ''' <summary>
            ''' Great Sword Pre-Game (Kronos) (4H-288)	
            ''' You must be Kronos to use this Weapon of Choice. After your opponent makes a Power Block, they must discard the top three cards of their Endurance. You may not play hidden attacks. This does not count towards your Pre-Game limit.
            ''' </summary>
            GreatSwordPreGameKronos = 7204

            ''' <summary>
            ''' War Axe Pre-Game (Silas) (4H-290)
            ''' You must be Silas to use this Weapon of Choice. Your Power Blows do an additional point of damage. Your Hard Exertions are increased by two. This does not count towards your Pre-Game limit. 
            ''' </summary>
            WarAxePreGameSilas = 7205

        End Enum

        Enum ConnorVsDuncan

            ''' <summary>
            ''' Katana Pre-Game (Connor) (CvD-059)
            ''' You may only use this Weapon of Choice if you are Connor MacLeod. This weapon does count against your Pre-Game limit. You may attack against to area you just blocked. Your Non-Special-Attacks that cover only one grid do one less damage. 
            ''' </summary>
            KatanaPreGameConnor = 6076

            ''' <summary>
            ''' Katana Pre-Game (Duncan) (CvD-060)	
            ''' You may only use this Weapon of Choice if you are Duncan MacLeod. This weapon does not count against your Pre-Game limit. You may Attack to areas you just Blocked. Your Non-Special Attacks that cover only one grid do one less damage. 
            ''' </summary>
            KatanaPreGameDuncan = 6077

            ''' <summary>
            ''' Connor MacLeod Persona (1st Edition) (CvD-056)
            ''' You may ignore the effects of any card played by your opponent that would force you to discard cards from your hand. You may increase or decrease the size of a Hard Exertion made by your opponent by one for every Ally you have in play (to a maximum of three).
            ''' </summary>
            ConnorMacLeodPersona1stEdition = 6073

            ''' <summary>
            ''' Duncan MacLeod Persona (1st Edition) (CvD-058)
            ''' You may play Basic Blocks against attacks that cannot be blocked. For every Ally you have in play, you may increase or decrease the size of a Hard Exertion you make by one (you must announce the size before performing the Exertion).
            ''' </summary>
            DuncanMacLeodPersona1stEdition = 6075

        End Enum

        Enum ConnorVsKurgan

            ''' <summary>
            ''' Katana Pre-Game (Connor) (TS2-037)
            ''' You must be Connor MacLeod to use this Weapon of Choice. You may attack to areas you just blocked. 
            ''' </summary>
            KatanaPreGame = 5380

            ''' <summary>
            ''' Connor MacLeod Slash (Upper) (TS2-026)
            ''' You may include this card in your deck in place of a Basic Attack. you do not need to include this card in your deck to satisfy your Basic Attack requirements. 
            ''' </summary>
            ConnorMacLeodSlashUpper = 5369

        End Enum

        Enum DuncanVsKanwulf

            ''' <summary>
            ''' Great Sword Pre-Game (DvK-056)
            ''' You may only play this Weapon of Choice if you are a MacLeod Persona. Whenever your opponent makes a Power Block, they must discard the top three cards of their Endurance. You may not play hidden attacks. If you make a Power Blow during your turn, your opponent's first attack during their next turn may not be a Power Blow. 
            ''' </summary>
            GreatSwordPreGame = 7278

        End Enum

        Enum TheGathering

            ''' <summary>
            ''' Ramirez Premium (Horse) (HtG-192)
            ''' Play in conjunction with the Ramirez Persona. You begin the game with +1 Ability. Your Master Cards cannot be countered or removed from the game by your opponent. 
            ''' </summary>
            RamirezPremiumHorse = 4634

            ''' <summary>
            ''' Ramirez Premium (Close-Up) (HtG-192)
            ''' Play in conjunction with the Ramirez Persona. You begin the game with +1 Ability. Your Master Cards cannot be countered or removed from the game by your opponent. 
            ''' </summary>
            RamirezPremiumCloseUp = 4635

            ''' <summary>
            ''' Ramirez Evade (Escape A) (HtG-134a)
            ''' You lose one attack this turn.
            ''' </summary>
            RamirezEvadeEscapeA = 4564

            ''' <summary>
            ''' Ramirez Evade (Escape B) (HtG-134b)
            ''' You lose one attack this turn.
            ''' </summary>
            RamirezEvadeEscapeB = 4565

            ''' <summary>
            ''' Ramirez Alertness (Block & Dodge A) (HtG-141a)
            ''' Play during your May Do / Must Do Phase. For the remainder of your turn, you may block attacks that cannot be blocked and avoid attacks that cannot be dodged. 
            ''' </summary>
            RamirezAlertnessBlockDodgeA = 4575

            ''' <summary>
            ''' Ramirez Alertness (Block & Dodge B) (HtG-141b)
            ''' Play during your May Do / Must Do Phase. For the remainder of your turn, you may block attacks that cannot be blocked and avoid attacks that cannot be dodged. 
            ''' </summary>
            RamirezAlertnessBlockDodgeB = 4576

            ''' <summary>
            ''' Connor MacLeod Slash (Upper) (HtG-099)
            ''' You may include this card in your deck in place of a Basic Attack. You do not need to include this card in your deck to satisfy your Basic Attack requirements. 
            ''' </summary>
            ConnorMacLeodSlashUpper = 4529

        End Enum

        Enum TheQuickening

            ''' <summary>
            ''' Corda and Reno Corda Persona (HtG-273)
            ''' You may play this Persona in conjunction with Reno. During your May Do / Must Do Phase, you may draw a card. When playing with Reno, each Persona keeps their own Ability and Hand. At the end of your Draw / Discard Phase, you may switch hands. Place the hand you are not using under its respective Persona. Corda begins the game with eight Ability. 
            ''' </summary>
            CordaPersona = 4997

            ''' <summary>
            ''' Corda and Reno Reno Persona (HtG-275)
            ''' You may play this Persona in conjunction with Corda. You may make a 3-card Hard Exertion to remove an Ally from play. When playing with Corda, each Persona keeps their own Ability and Hand. At the end of your Draw / Discard Phase, you may switch hands. Place the hand you are not using under its respective Persona. Reno begins the game with eight Ability. 
            ''' </summary>
            RenoPersona = 4999

        End Enum

        Enum SeasonFour

            ''' <summary>
            ''' Jacob Galati Sniper Shot (HS4-084)
            ''' This attack does one damage. You may include this card in your deck in place of a Basic Attack.
            ''' </summary>
            JacobGalatiSniperShot = 6163

        End Enum
    End Class

    Class LeMontagnardRevised

        Enum SummerSpecial2017

            ''' <summary>
            ''' Generic Immortal Persona (Standing Defenses) (2017SS-004)
            ''' You may begin the Game with one STANDING DEFENSE in play. You may include up to nine cards titled "Guard" in your deck.
            ''' You may include and use one non-Reserved, non-Signature card from another persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaStandingDefenses = 9120

            ''' <summary>
            ''' Generic Immortal Persona (Exertions) (2017SS-002)
            ''' Your opponent may not make Exertions from your Endurance. If you do not play a Special card during your turn, you may make two Hard Exertions that turn.
            ''' You may include and use one non-Reserved, non-Signature card from another persona in your deck for each Master card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaExertions = 9118

            ''' <summary>
            ''' Generic Immortal Persona (Draw / Discard) (2017SS-003)
            ''' DISCARD / DRAW: During your May Do / Must Do Phase, you may either Discard a card, or Draw a card.
            ''' You may include and use one non-Reserved, non-Signature card from another persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaDrawDiscard = 9119

            ''' <summary>
            ''' Generic Immortal Persona (A&D) (2017SS-001)
            ''' You are not required to include the 9 basic attacks and 6 basic blocks during deck construction, as long as you have 9 non-special attacks and 6 blocks.
            ''' You may include and use one non-Reserved, non-Signature card from another persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaAD = 9117

        End Enum

        Enum PromotionalCards

            ''' <summary>
            ''' Generic Big and Bad
            ''' You may only include this card in your deck if you have 75 or more cards in your deck.
            ''' Play during your May Do / Must Do Phase. You may place one card from an Exertion you make this turn into your Hand. 
            ''' </summary>
            GenericBigAndBad = 9028

            ''' <summary>
            ''' Generic Big and Bad (Discard)
            ''' You may only include this card in your deck if you have 75 or more cards in your deck. Play during your May Do/Must Do Phase. You may retrieve one generic attack or block from your Discard. 
            ''' </summary>
            GenericBigAndBadDiscard = 7208

            ''' <summary>
            ''' Generic Big and Bad (Endurance)
            ''' You may only include this card in your deck if you have 75 or more cards in your deck. Play during your May Do/Must Do Phase. You may retrieve one generic attack or block from your Endurance. 
            ''' </summary>
            GenericBigAndBadEndurance = 7724

            ''' <summary>
            ''' Broad Bladed Spear Pre-Game (PP2022-014)
            ''' The only Off-Hand Weapon of Choice you can include or use is the Shield.
            ''' UPSIDE: If your opponent played a Dodge last turn, you may attack normally regardless of effects in play, and your first non-Power Blow attack this turn cannot be dodged.
            ''' DOWNSIDE: You may not play or Reveal Hidden Attacks. 
            ''' </summary>
            BroadBladedSpearPreGame = 9785

            ''' <summary>
            ''' Two Handed Broadsword Pre-Game (Kurgan's Two Handed Broadsword) (HLGP2023-008)
            ''' Kurgan's Two Handed Broadsword
            ''' You may only use this Weapon of Choice if you are using the Kurgan Persona. This card does not count towards your Pre-game limit.
            ''' UPSIDE: You may make an additional Exertion during your turn if that Exertion is used for a Power Blow or Power Block. Your Exertions for Power Blows and Power Blocks may be 3 or 5 card Exertions.
            ''' DOWNSIDE: You lose an additional Ability when Exhausting. 
            ''' </summary>
            TwoHandedBroadswordPreGame = 9796

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 1) (RIP01)
            ''' You may tear this card in half to counter an Illusion as it is being played. 
            ''' </summary>
            SwordmasterPreGameRIP1 = 7074

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 2) (RIP02)
            ''' You may tear this card in half to counter any effect from one pre-game card as it is activated. The pre-game card is considered to have used its effect. 
            ''' </summary>
            SwordmasterPreGameRIP2 = 7075

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 3) (RIP03)
            ''' You may tear this card in half to counter any effect from one in-game card as it activated. This in-game card is considered to have used its effect. 
            ''' </summary>
            SwordmasterPreGameRIP3 = 7076

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 4) (RIP4)
            ''' You may include this card in your Pre-Game. You may rip this card in half and remove it from the game to nullify the text on one Persona Specific card as it is played. 
            ''' </summary>
            SwordmasterPreGameRIP4 = 7954

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 5) (RIP5)
            ''' You may include this card in your Pre-Game. You may rip this card in half and remove it from the game to nullify the text on one Weapon of Choice Specific card as it is played. 
            ''' </summary>
            SwordmasterPreGameRIP5 = 7955

            ''' <summary>
            ''' Swordmaster Pre-Game (RIP8)
            ''' Tear this card in half. Your opponent cannot play Edge or Special cards outside of their May Do / Must Do Phase next turn. This effect cannot be duplicated by Divine Intervention. 
            ''' </summary>
            SwordmasterPreGameRIP8 = 9528

            ''' <summary>
            ''' Swordmaster Pre-Game (PP-2021)
            ''' You may tear this card in half and remove from the game to counter one Edge Card, Special Card, Illusion, or Attack the moment it is played or put into play. 
            ''' </summary>
            SwordmasterPreGame2021 = 9520

            ''' <summary>
            ''' Hugh Fitzcairn Persona (Type 1) (R2016-036)
            ''' REVEAL: During your May Do / Must Do Phase, Reveal the top five cards of your Endurance. For each Special Card Revealed, gain an additional attack (max 3). Place the Revealed cards on the bottom of your Endurance.
            ''' </summary>
            HughFitzcairnPersonaType1 = 9387

            ''' <summary>
            ''' Kiem Sun Persona (Type 1) (17PP01)
            ''' If you Discard an Edge from your Hand, you may Ignore a Location you have in play. If your opponent plays a Location, it does not bump your Location from play. You may only have one Location in play.
            ''' </summary>
            KiemSunPersonaType1 = 9524

            ''' <summary>
            ''' Katherine Scorned Blade (PP2024-002)
            ''' You may only include this card in your Deck if you are using the Swiss Long Sword Weapon of Choice.
            ''' During your turn, you may make one attack a Power Blow without an Exertion.
            ''' DRAW: When you make a Power Blow, you must immediately Draw a card. 
            ''' </summary>
            KatherineScornedBlade = 10055

            ''' <summary>
            ''' Kamir Talwar (PP2024-003)
            ''' You may only include this card in your Deck if you are using the Sabre Weapon of Choice.
            ''' RESTRICTION: If your opponent was Prone at any time during their last turn, your opponent cannot play a Block from their Hand to defend your first attack. 
            ''' </summary>
            KamirTalwar = 10056

            ''' <summary>
            ''' Generic Immortal Persona (Nullify If Successful) (PP2024-013)
            ''' If you do not play a Special Card during your turn, you may Nullify the "if this attack is successful" text on one attack not titled "Dirty Trick" you play this turn.
            ''' This Persona cannot be used with the Dark Quickening Pregame.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaNullifyIfSuccessful = 10066

            ''' <summary>
            ''' Generic Lean and Mean (Counter Hard Exertion) (PP2024-009)
            ''' Your Minimum Deck Size is reduced by one.
            ''' DRAW: Draw one card from the bottom of your Endurance.
            ''' Show this card from your Hand to your opponent and place it in your Discard to counter an attempt by your opponent to make a Hard Exertion from your Endurance. 
            ''' </summary>
            GenericLeanAndMeanCounterHardExertion = 10062

            ''' <summary>
            ''' Arms and Tactics Pre-Game (Hilt) (PP-2021)
            ''' Place a compatible Hilt with a Weapon of Choice in your Pre-Game. The Hilt chosen is not an In-Game card any longer but an extension of the Weapon of Choice.. Both cards are considered one Pre-Game. 
            ''' </summary>
            ArmsAndTacticsPreGameHilt = 9519

        End Enum

        Enum BrianCullenVsNicholasWard

            ''' <summary>
            ''' Cane Sword Pre-Game (BVN077)
            ''' Nicholas Ward's Cane Sword
            ''' UPSIDE: Once per turn, during your May Do / Must Do Phase, you may put one block from your Hand or one Dirty Trick from your Discard under this card. Once per turn, you may play a block or Dirty Trick from under this card as if it were from a Hard Exertion.
            ''' DOWNSIDE: You may not play blocks from your Hand. 
            ''' </summary>
            CaneSwordPreGame = 8658

        End Enum

        Enum CrystaVanPeltVsKhabulKhan

            ''' <summary>
            ''' Chinese Dao Pre-Game (CVK-033)
            ''' Crysta Van Pelt: Korda's Gift
            ''' UPSIDE: RETRIEVE: Once per turn, you may Retrieve a Chinese Dao specific attack or defense from your Discard Pile or Endurance.
            ''' DOWNSIDE: BURN: You must Burn one card for each Chinese Dao specific card you play. 
            ''' </summary>
            ChineseDaoPreGame = 7904

            ''' <summary>
            ''' Mongolian Broadsword Pre-Game (CVK-065)
            ''' Khabul Khan: Raider's Blade
            ''' UPSIDE: You may play attacks from under this card as if they were from your hand. Once per turn, you gain an additional attack if you have an Object in play.
            ''' CAPTURE: During your May Do/Must Do Phase you may place one attack from your Hand under Mongolian Broadsword.
            ''' DOWNSIDE: RESTRICTION: You cannot play attacks from an Exertion. 
            ''' </summary>
            MongolianBroadswordPreGame = 7888

        End Enum

        Enum UrsaVsGiovani

            ''' <summary>
            ''' Cortana Pre-Game (GVU037)
            ''' Cardinal Giovanni: God's Mercy
            ''' UPSIDE: You may include and use up to nine cards titled "Counter" in your deck. You may nullify any Cost to play cards titled "Counter". Once per turn you may place one Counter found in a Hard Exertion on the bottom of your Endurance.
            ''' DOWNSIDE: You may not include or use cards titled "Evade".
            ''' </summary>
            CortanaPreGame = 8772

        End Enum

        Enum ReggieWellerVsRaphael

            ''' <summary>
            ''' Cutlass Pre-Game (Methos' Gift) (RVR077)
            ''' Reggie Weller -- Methos' Gift
            ''' You may nullify any effects in play by your opponent that would prevent or restrict you from playing attacks. If you do not play a Hidden attack during your turn, you may not draw any cards during your Draw / Discard Phase that turn. 
            ''' </summary>
            CutlassPreGame = 8079

        End Enum

        Enum CarlRobinsonVsMatthewMcCormick

            ''' <summary>
            ''' Falchion Pre-Game (Blade of Freedom) (CVM036)
            ''' Carl Robinson -- Blade of Freedom
            ''' UPSIDE: One Basic Attack or Basic Block played from a Hard Exertion may be a Power Blow or Power Block without an Exertion. Once per turn you may place one non-gridded card from a Hard Exertion in your Hand.
            ''' DOWNSIDE: You cannot use any effect to alter the size of your Hard Exertions. 
            ''' </summary>
            FalchionPreGame = 8370

        End Enum

        Enum OldCarlVsThomasSullivan

            ''' <summary>
            ''' Hand Axe Pre-Game (CVT-003)
            ''' Old Carl's Hand Axe
            ''' You may use this with any other One-Handed Weapon of Choice.
            ''' UPSIDE: You may ignore Hand Icon requirements for one card you play during your turn.
            ''' DOWNSIDE: You cannot increase the damage of any card you play. 
            ''' </summary>
            HandAxePreGame = 8850

        End Enum

        Enum DarkDuncan

            ''' <summary>
            ''' Katana Pre-Game (DD-020)
            ''' Dark Duncan's Katana
            ''' UPSIDE: Your first attack this turn may be to areas you just defended regardless of restrictions on the defense played.
            ''' DOWNSIDE: You cannot take control of your opponent's cards. 
            ''' </summary>
            KatanaPreGame = 8511

        End Enum

        Enum TakNeVsTheBedoin

            ''' <summary>
            ''' Katana 	Pre-Game (TVB037)
            ''' Tak Ne's Masamune Katana
            ''' UPSIDE: You may attack to areas you just blocked.
            ''' COST: If you play only one attack, your opponent must Discard a dodge to play a dodge from their Hand.
            ''' DOWNSIDE: Your Non-Special Attacks that cover only one grid do one less damage. 
            ''' </summary>
            KatanaPreGame = 8792

            ''' <summary>
            ''' Nimcha Pre-Game (TVB075)
            ''' The Bedouin's Blade
            ''' UPSIDE: DISCARD / REVEAL: You may Discard an [A] (or in Type One, Discard a Special card) to Reveal a Hidden attack.
            ''' DOWNSIDE: If you played a generic dodge as your last defense this turn, you may not make a Power Blow this turn. 
            ''' </summary>
            NimchaPreGame = 8797

        End Enum

        Enum KiemSunVsVictorHansen

            ''' <summary>
            ''' Kris Pre-Game (SVH077)	
            ''' Victor Hansen's Kris
            ''' UPSIDE: During your Attack Phase, you may make a zero card Hard Exertion to remove one Standing Defense from play.
            ''' DOWNSIDE: You may not include attacks in your deck which cover more than one grid. 
            ''' </summary>
            KrisPreGame = 8527

        End Enum

        Enum MichaelMooreQuentinBarnesVsByron

            ''' <summary>
            ''' Sabre Pre-Game (BVM035)	
            ''' SABRE: BYRON
            ''' UPSIDE: If your opponent plays an unsuccessful defense against one of your Hidden attacks that is not a Power Blow, they cannot make an Exertion for a defense to defend that attack.
            ''' DOWNSIDE: You must Discard an attack to play a Special Attack from your Hand. 
            ''' </summary>
            SabrePreGame = 9428

        End Enum

        Enum HamzaElKahirVsMarcusConstantine

            ''' <summary>
            ''' Scimitar Pre-Game (HVM027)
            ''' SCIMITAR: Hamza El Kahir
            ''' If you play an unsuccessful block against a hidden attack, you may play a new block from your hand to defend that attack. Your dodges won't avoid multiple attacks.
            ''' Add one Toughness to any Attribute Check or Attribute Challenge. 
            ''' </summary>
            ScimitarPreGame = 7104

            ''' <summary>
            ''' Short Sword Pre-Game (HVM058)
            ''' Roman Short Sword: Marcus Constantine
            ''' Your opponent must discard a block to play a non-basic block against your attacks. You cannot include attacks in your deck that cover more than two grids. You may only play one attack per turn. You may substitute three Short Sword specific attacks for three of your required Basic Attacks during deck construction. 
            ''' </summary>
            ShortSwordPreGame = 7135

        End Enum

        Enum KatherineVsBartholomew

            ''' <summary>
            ''' Shield Pre-Game (Crusade Shield) (BVK041)
            ''' Bartholomew -- Crusade Shield
            ''' You may play this Weapon of Choice with another One-Handed Weapon of Choice.
            ''' UPSIDE: You may play blocks against Ranged Attacks.
            ''' DOWNSIDE: Your first attack cannot be played to areas you blocked this turn, regardless of any other effects, unless they are Shield specific attacks. 
            ''' </summary>
            ShieldPreGame = 8112

            ''' <summary>
            ''' Swiss Long Sword Pre-Game (Mercenary Blade) (BVK079)
            ''' Katherine -- Mercenary Blade
            ''' UPSIDE: You may make unaltered 3 or 5 card Exertions. You must announce the size before making the Exertion. After making a Hard Exertion, you may place one attack found in the Exertion on top of your Endurance.
            ''' DOWNSIDE: You cannot play Illusions from an Exertion. 
            ''' </summary>
            SwissLongSwordPreGame = 8128

        End Enum

        Enum AlexRavenVsDamonCase

            ''' <summary>
            ''' Single-Handed Broadsword Pre-Game (Pious Man's Blade) (AVD074)
            ''' Pious Man's Blade -- Damon Case
            ''' UPSIDE: If your opponent played a Power Blow last turn, you may play an additional attack during your turn.
            ''' DOWNSIDE: You take one damage when Power Blocking a Power Blow. 
            ''' </summary>
            SingleHandedBroadswordPreGame = 8034

        End Enum

        Enum CalebColeVsMarcusKorolus

            ''' <summary>
            ''' War Axe Pre-Game (Caleb Cole's Custom War Axe) (CVM-003)
            ''' Caleb Cole's Custom War Axe
            ''' UPSIDE: Your Power Blows do an additional point of damage. Once per turn, you may make a 0-card Hard Exertion for a Power Blow or Power Block.
            ''' DOWNSIDE: BURN: Your Hard Exertions are increased by two. 
            ''' </summary>
            WarAxePreGame = 9032

        End Enum

    End Class

    Class OneShotGames

        Enum Legacy3

            ''' <summary>
            ''' Great Sword Pre-Game (Slan Quince Great Sword) (LG3PR-053)
            ''' SLAN QUINCE GREAT SWORD
            ''' Choose one:
            ''' - This weapon does not count towards your Pre-Game limit.
            ''' - During deck construction, your Agility, Empathy, & Reason attributes are increased by one each.
            ''' UPSIDE: After your opponent makes a Power Block, they must discard the top three cards of their Endurance.
            ''' DOWNSIDE: You may not play hidden attacks. 
            ''' </summary>
            GreatSwordPreGame = 9710

            ''' <summary>
            ''' Sabre Pre-Game (Walter Reinhardt) (LG3-097)
            ''' SABRE: WALTER REINHARDT
            ''' UPSIDE: If your opponent plays an unsuccessful defense against one of your Hidden attacks that is not a Power Blow, they cannot make an Exertion for a defense to defend that attack.
            ''' DOWNSIDE: You must Discard an attack to play a Special Attack from your Hand. 
            ''' </summary>
            SabrePreGame = 9639

            ''' <summary>
            ''' Single-Handed Broadsword Pre-Game (Iman Fasil Toledo Salamanca) (LG3-104)
            ''' IMAN FASIL TOLEDO SALAMANCA -- SINGLE-HANDED BROADSWORD
            ''' Choose one:
            ''' - This weapon does not count towards your Pre-Game limit.
            ''' - During deck construction, your Toughness attribute is increased by three.
            ''' UPSIDE: If your opponent made a Power Blow or Power Block last turn, you may play an additional attack during your turn.
            ''' DOWNSIDE: You take three damage when blocking a Power Blow and one damage when Power Blocking a Power Blow. 
            ''' </summary>
            SingleHandedBroadswordPreGame = 9646

            ''' <summary>
            ''' Generic Immortal Persona (Steve Rice) (LG3G1-005)
            ''' If an effect controlled by your opponent forces you to discard cards from your Hand, you may randomly discard one card from your opponent's Hand for each card you discarded (Max. 3).
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaSteveRice = 9956

            ''' <summary>
            ''' Generic Immortal Persona (Shane Robbins) (LG3G1-004)
            ''' If your opponent used a retrieval effect during their turn, they must Burn 3 cards from the top of their Endurance to play a defense from their Hand against your first attack.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaShaneRobbins = 9959

            ''' <summary>
            ''' Generic Immortal Persona (Nigel Keates) (LG3G1-003)	
            ''' You may begin the game with one Location in play.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaNigelKeates = 9958

            ''' <summary>
            ''' Generic Immortal Persona (Jim Black) (LG3G1-006)
            ''' You may Nullify any effects your opponent has in play that prevent you from playing a dodge from your Hand. This does not include undodgeable attacks. You may not put this card under Dark Quickening.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaJimBlack = 9960

            ''' <summary>
            ''' Generic Immortal Persona (Jeff Smorey) (LG3G1-002)
            ''' During one Phase of your turn, you may make an unaltered 3 card Hard Exertion to Nullify any Prone effect your opponent controls targeting you. You may not put this card under Dark Quickening.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaJeffSmorey = 9957

            ''' <summary>
            ''' Generic Immortal Persona (Jason Hasis) (LG3G1-008)
            ''' If you are using the Cutlass Weapon of Choice, it does not count towards your Pre-Game limit.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaJasonHasis = 9963

            ''' <summary>
            ''' Generic Immortal Persona (David Robbins) (LG3G1-007)
            ''' You may make a 3 card Hard Exertion. For each [T] found, you may Ignore one Location or Object in play until the beginning of your next turn.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaDavidRobbins = 9962

            ''' <summary>
            ''' Generic Immortal Persona (David Detlefs) (LG3G1-006)
            ''' Your opponent must Burn 3 cards from the top of their Endurance to play a Head Shot. You may not put this card under Dark Quickening.
            ''' You may include and use one non-Reserved, non-Signature card from another Persona in your deck for each Master Card you are allowed. 
            ''' </summary>
            GenericImmortalPersonaDavidDetlefs = 9961

            ''' <summary>
            ''' Xavier St. Cloud Different Strokes (Plot) (LG3-099)
            ''' You may include this card in your deck in place of a Basic Attack.
            ''' Choose one area on the grid for each Plot you have in play to a maximum of three. 
            ''' </summary>
            XavierStCloudDifferentStrokesPlot = 9641

            ''' <summary>
            ''' Xavier St. Cloud Different Strokes (Additional Damage) (LG3-098)
            ''' You may include this card in your deck in place of a Basic Attack.
            ''' Choose one area on the grid. This attack does an additional point of damage for each Plot you have in play to a maximum of three. 
            ''' </summary>
            XavierStCloudDifferentStrokesAdditionalDamage = 9640

            ''' <summary>
            ''' Roland Kantos Illusory Strike (LG3PR-045)
            ''' You may include this card in your deck in place of a Basic Attack.
            ''' Choose a grid.
            ''' This attack does an additional point of damage for each Illusion you discard when using your Persona Power this turn. 
            ''' </summary>
            RolandKantosIllusoryStrike = 9702

            ''' <summary>
            ''' Walter Reinhardt Manipulator of Many (LG3-094)
            ''' ALLOWANCE: If you are using the Walter Reinhardt Persona, you may include and use one non-Signature, Mortal Ally from another Persona in your deck. You may only play or put that card into play in conjunction with this card.
            ''' </summary>
            WalterReinhardtManipulatorOfMany = 9636

            ''' <summary>
            ''' Walter Reinhardt Basket Hilt Sabre (LG3-088)
            ''' You may only include this card in your deck if you are using the Sabre Weapon of Choice. You may play one attack from under Rebecca Lorde as an additional attack each turn. This attack may not be a Power Blow.
            ''' </summary>
            WalterReinhardtBasketHiltSabre = 9630

            ''' <summary>
            ''' Slan Quince Brute Strength (LG3-080)
            ''' This card's Attributes do not count towards your deck construction limit.
            ''' Play this card in conjunction with a Basic Block. That Block may be a Power Block without making an Exertion.
            ''' </summary>
            SlanQuinceBruteStrength = 9622

            ''' <summary>
            ''' Shield Pre-Game (LG3PR-002)
            ''' You may play this Weapon of Choice with another One-Handed Weapon of Choice. You must use two Weapon of Choice. You may play blocks against Ranged Attacks. Your first attack cannot be played to areas you blocked this turn, regardless of any other effects.
            ''' </summary>
            ShieldPreGame = 9682

            ''' <summary>
            ''' Saif al-Rashid Master's Dodge (LG3PR-049)
            ''' You may include this card in your deck in place of a Basic Block.
            ''' This card satisfies any costs required to play a defense from your Hand. You may not Retrieve this card from your Discard.
            ''' </summary>
            SaifAlRashidMastersDodge = 9706

            ''' <summary>
            ''' Rita Luce Pre-Game (LG3-103)
            ''' You may make a three-card Hard Exertion to ignore your opponent's Persona Power for the remainder of your turn. You may include Hunter cards in your Deck. If you are using the Michael Christian Persona, this card does not count towards your Pre-Game limit.
            ''' </summary>
            RitaLucePreGame = 9645

            ''' <summary>
            ''' Rebecca Horne Elegant Strike (LG3PR-048)
            ''' This card's attributes do not count towards your deck requirements.
            ''' If you are using the Rebecca Horne Persona and if this is the only attack you play this turn, it does one additional point of damage for every 3 Crystals in your Pre-Game.
            ''' </summary>
            RebeccaHorneElegantStrike = 9705

        End Enum

        Enum StevenKeaneCollection

            ''' <summary>
            ''' Arms and Tactics Pre-Game (Hilt) (PP-2021)
            ''' Place a compatible Hilt with a Weapon of Choice in your Pre-Game. The Hilt chosen is not an In-Game card any longer but an extension of the Weapon of Choice.. Both cards are considered one Pre-Game. 
            ''' </summary>
            ArmsAndTacticsPreGameHilt = 9861

            ''' <summary>
            ''' Steven Keane Prussian Cavalry Sword (SKC-018)
            ''' You may only include this card in your Deck if you are using the Sabre Weapon of Choice. You do not have to Discard an attack from your Hand due to the Downside of the Sabre when playing a non-Basic Attack if that attack is played Hidden.
            ''' </summary>
            StevenKeanePrussianCavalrySword = 9845

        End Enum

        Enum KassimCollection

            ''' <summary>
            ''' Scimitar Pre-Game (Kassim's Damascus Scimitar) (KPC-003)
            ''' Kassim's Damascus Scimitar
            ''' UPSIDE:
            ''' RETRIEVE: If you play a successful Block against a Hidden attack you may put one attack from your Discard Pile into play as an additional attack.
            ''' DOWNSIDE:
            ''' Your dodges won't avoid multiple attacks. 
            ''' </summary>
            ScimitarPreGame = 9806

        End Enum

        Enum ConnorMacLeodVsSlanQuince

            ''' <summary>
            ''' Great Sword Pre-Game (CVS-022)
            ''' Slan Quince Great Sword
            ''' Choose one:
            ''' - This weapon does not count towards your Pre-Game limit.
            ''' - During deck construction, your Agility, Empathy, & Reason attributes are increased by one each.
            ''' UPSIDE: After your opponent makes a Power Block, they must discard the top three cards of their Endurance.
            ''' DOWNSIDE: You may not play hidden attacks. 
            ''' </summary>
            GreatSwordPreGame = 9883

            ''' <summary>
            ''' Katana Pre-Game (CVS-003)
            ''' Connor MacLeod's Rosewood Tachi
            ''' You must be Connor MacLeod to use this Weapon of Choice. This Weapon of Choice does not count against your Pre-Game limit.
            ''' UPSIDE: You may attack to areas you just blocked.
            ''' DOWNSIDE: Your Non-Special Attacks that cover only one grid do one less damage. 
            ''' </summary>
            KatanaPreGame = 9864

            ''' <summary>
            ''' Quickening Pre-Game (Dark Quickening) (Q2018-999)
            ''' Before the Game begins, place a Persona Card you are not playing as under this card. At the end of your Draw / Discard Phase you may choose to use the Persona Power under this card instead of the Persona Power you are playing as. This effect lasts until your next Draw / Discard Phase. This card is not a Capture Effect.
            ''' This card does not affect your Ability Score. This card cannot be used with Corda and/or Reno. 
            ''' </summary>
            QuickeningPreGameDarkQuickening = 9929

            ''' <summary>
            ''' Connor MacLeod Slash (Upper) (CVS-004)	
            ''' You may include this card in your deck in place of a Basic Attack. You do not need to include this card in your deck to satisfy deck requirements. 
            ''' </summary>
            ConnorMacLeodSlashUpper = 9865

        End Enum

        Enum GregorPowersCollection

            ''' <summary>
            ''' Single-Handed Broadsword Pre-Game (El Cid Tizona) (GPC-003)
            ''' GREGOR POWERS EL CID TIZONA
            ''' SINGLE-HANDED BROADSWORD
            ''' UPSIDE
            ''' REMOVAL: Once per turn, if you lose an Attribute Challenge you initiated, you may Remove the last card your opponent revealed in that Attribute Challenge from the Game.
            ''' DOWNSIDE
            ''' You take three damage when blocking a Power Blow and one damage when making a Power Block. 
            ''' </summary>
            SingleHandedBroadswordPreGame = 10103

        End Enum

        Enum MichaelKentCollection

            ''' <summary>
            ''' Michael Kent's Muramasa Katana
            ''' UPSIDE: You may attack to areas you just blocked.
            ''' If you successfully defend an attack by playing a card with "Slash" in the title as a block, that card becomes your first attack during your Attack Phase.
            ''' DOWNSIDE: Your non-Special Attacks that cover only one grid square do one less damage. 
            ''' </summary>
            KatanaPreGame = 10071

            ''' <summary>
            ''' Michael Kent Persona (MKC-001)
            ''' Once per turn, you may play any gridded card with "Slash" in the title as a block.
            ''' ALLOWANCE: You may include and use one non-Signature Immortal Specific Slash from each Immortal in your Deck for each Master Card you are allowed in your Deck. You may include up to 8 cards titled Slash in your Deck. 
            ''' </summary>
            MichaelKentPersona = 10069

            ''' <summary>
            ''' Connor MacLeod Slash (Upper) (CVS-004)
            ''' You may include this card in your deck in place of a Basic Attack. You do not need to include this card in your deck to satisfy deck requirements. 
            ''' </summary>
            ConnorMacLeodSlashUpper = 10096

        End Enum

    End Class

    Class ParadoxPublishing

        Enum PromotionalCards

            ''' <summary>
            ''' Generic Lean and Mean (Can't Play) (PRIZE2011-003)
            ''' You may not play this card or put it into play. 
            ''' </summary>
            LeanAndMeanCantPlay = 6897

        End Enum

        Enum TheProphecy

            ''' <summary>
            ''' English Longsword Pre-Game (Cassandra) (V01-002)
            ''' ENGLISH LONGSWORD - CASSANDRA
            ''' Nullify the text of Evades that you play. You may not include Counters in your deck.
            ''' DRAW: At the beginning of your turn, you may reveal the top three cards of your Endurance and take one Defense found there to your hand. Put the remaining cards on the bottom of your Endurance in any order. 
            ''' </summary>
            EnglishLongswordPreGameCassandra = 6627

            ''' <summary>
            ''' English Longsword Pre-Game (Roland Kantos) (V01-039)
            ''' ENGLISH LONGSWORD - ROLAND
            ''' Nullify the text on Evades that you play. You may not include Counters in your deck.
            ''' DRAW: At the beginning of your turn, you may reveal the top three cards of your Endurance and take one Defense found there to your hand. Put the remaining cards on the bottom of your Endurance in any order. 
            ''' </summary>
            EnglishLongswordPreGameRolandKantos = 6664

        End Enum

    End Class

    Class WorldOfGameDesign

        Enum PromotionalCards

            ''' <summary>
            ''' Princes of the Universe (CHAMP002)
            ''' This does not count towards your Pregame Limit.
            ''' REVEAL:  Once during your May Do/Must Do Phase, you may look at the top card of your Endurance.
            ''' </summary>
            PrincesOfTheUniversePreGame = 10043

            ''' <summary>
            ''' Princes of the Universe Lean and Mean (CHAMP003)
            ''' Your minimum Deck Size is reduced by 1.
            ''' DRAW:  Place the top card of your Endurance on the bottom of your Endurance.
            ''' </summary>
            PrincesOfTheUniverseLeanAndMean = 9977

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 1) (PROMO001)
            ''' TRIGGER: You may remove this card from the game and tear it in half to remove one Edge, Special, Defense or Attack Card from play. 
            ''' </summary>
            SwordmasterPreGame1 = 9980

            ''' <summary>
            ''' Swordmaster Pre-Game (Version 2) (PROMO001)
            ''' TRIGGER: You may remove this card from the game and tear it in half to remove one Edge, Special, Defense or Attack Card from play. 
            ''' </summary>
            SwordmasterPreGame2 = 9981

        End Enum

    End Class

End Class
