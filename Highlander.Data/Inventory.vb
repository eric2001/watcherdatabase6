﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class Inventory

    ''' <summary>
    ''' Contains the Unique Id for an individual card in the cards.xml file.
    ''' </summary>
    ''' <returns></returns>
    Public Property Id As Integer

    ''' <summary>
    ''' Contains the number of copies in the inventory for the card identified by the Id property.
    ''' </summary>
    ''' <returns></returns>
    Public Property Quantity As Integer = 0

    ''' <summary>
    ''' Contains either a file name or full file path and file name to the image file to display for the front of the card.
    ''' This will override the default FileNameFront value from the cards.xml file.
    ''' If the property only contains a file name, the file is expected to be in the default location located in CardsDB.HighlanderImagePath and in a sub folder based on the card's publisher and expansion set.
    ''' </summary>
    ''' <returns></returns>
    Public Property FileNameFront As String = String.Empty

    ''' <summary>
    ''' Contains either a file name or full file path and file name to the image file to display for the back of the card.
    ''' This will override the default FileNameBack value from the cards.xml file.
    ''' If the property only contains a file name, the file is expected to be in the default location located in CardsDB.HighlanderImagePath and in a sub folder based on the card's publisher and expansion set.
    ''' </summary>
    ''' <returns></returns>
    Public Property FileNameBack As String = String.Empty

    ''' <summary>
    ''' Contains the URL to where the image file for this card can be downloaded from.
    ''' </summary>
    ''' <returns></returns>
    Public Property MleUrl As String = String.Empty

    ''' <summary>
    ''' Contains any additional notes the user has about this card.
    ''' </summary>
    ''' <returns></returns>
    Public Property Notes As String = String.Empty

    ''' <summary>
    ''' Returns the Quantity for a specific card from the loaded inventory.
    ''' Note:  If the Card Id is not found in the current inventory, it will return 0.
    ''' </summary>
    ''' <param name="CardId">The unique Id to search the inventory for.</param>
    ''' <returns>The Quantity value associated with the supplied Card Id.</returns>
    Shared Function GetCardQuantity(ByVal CardId As Integer) As Integer
        Dim Quantity As Integer = 0
        Dim resultsInventory() As DataRow
        resultsInventory = InventoryDB.Data.Tables("Inventory").Select("ID = " & CardId)
        If resultsInventory.Length > 0 Then
            Quantity = resultsInventory(0).Item("Quantity")
        End If

        Return Quantity
    End Function

    ''' <summary>
    ''' Returns the record from the loaded inventory file that corresponds to the passed in CardId.
    ''' If no match is found, it will return a new Inventory object with Id set to CardId and all other fields set to blank / 0.
    ''' </summary>
    ''' <param name="CardId">The Id of the card to search the inventory file for.</param>
    ''' <returns>An Inventory object with the details for the requested Card Id.</returns>
    Shared Function GetInventoryForCard(ByVal CardId As Integer) As Inventory

        Dim InventoryRecord As Inventory = New Inventory()
        InventoryRecord.Id = CardId

        Dim SelectedCard() As DataRow = InventoryDB.Data.Tables("Inventory").Select("ID = '" & InventoryRecord.Id.ToString() & "'")
        If SelectedCard.Length > 0 Then
            InventoryRecord.Quantity = SelectedCard(0).Item("Quantity").ToString()
            InventoryRecord.FileNameBack = SelectedCard(0).Item("FileNameBack").ToString()
            InventoryRecord.FileNameFront = SelectedCard(0).Item("FileNameFront").ToString()
            InventoryRecord.MleUrl = SelectedCard(0).Item("MLEURL").ToString()
            InventoryRecord.Notes = SelectedCard(0).Item("Notes").ToString()
        End If

        Return InventoryRecord
    End Function

    ''' <summary>
    ''' Add the details for the current card to the inventory file currently loaded into memory.
    ''' Note:  This only loads the data into the InventoryDB object.  InventoryDB.Save() must be called separately to save the data to disk.
    ''' </summary>
    ''' <param name="InventoryRecord">The Inventory details to save.</param>
    ''' <returns>Returns True if a change was made to the Inventory data, returns false if no changes were found.</returns>
    Shared Function SaveInventoryForCard(ByVal InventoryRecord As Inventory) As Boolean
        Dim result As Boolean = False ' default to no changes made.

        Dim SelectedCard() As DataRow = InventoryDB.Data.Tables("Inventory").Select("ID = '" & InventoryRecord.Id.ToString() & "'")
        If SelectedCard.Length > 0 Then ' If a record already exists in the current inventory for this card, then update it.
            If DoesInventoryHaveValues(InventoryRecord) Then ' If at least one field is set
                If (InventoryRecord.Quantity <> SelectedCard(0).Item("Quantity")) Or
                    (InventoryRecord.FileNameBack <> SelectedCard(0).Item("FileNameBack").ToString()) Or
                    (InventoryRecord.FileNameFront <> SelectedCard(0).Item("FileNameFront").ToString()) Or
                    (InventoryRecord.MleUrl <> SelectedCard(0).Item("MLEURL").ToString()) Or
                    (InventoryRecord.Notes <> SelectedCard(0).Item("Notes").ToString()) Then
                    ' If at least one field is different from what's in the existing record, then update the record, otherwise do nothing

                    SelectedCard(0).Item("Quantity") = InventoryRecord.Quantity
                    SelectedCard(0).Item("FileNameBack") = InventoryRecord.FileNameBack
                    SelectedCard(0).Item("FileNameFront") = InventoryRecord.FileNameFront
                    SelectedCard(0).Item("MLEURL") = InventoryRecord.MleUrl
                    SelectedCard(0).Item("Notes") = InventoryRecord.Notes

                    result = True
                End If
            Else ' If a record already exists and none of the passed in fields are set, then delete the record
                SelectedCard(0).Delete()

                result = True
            End If
        Else ' If a record does not exist in the current inventory for this card, then create a new one.
            If DoesInventoryHaveValues(InventoryRecord) Then ' If at least one field is set then add a new row
                Dim newRow As DataRow = InventoryDB.Data.Tables("Inventory").NewRow
                With newRow
                    .Item("ID") = InventoryRecord.Id
                    .Item("Quantity") = InventoryRecord.Quantity
                    .Item("FileNameFront") = InventoryRecord.FileNameFront
                    .Item("FileNameBack") = InventoryRecord.FileNameBack
                    .Item("MLEURL") = InventoryRecord.MleUrl
                    .Item("Notes") = InventoryRecord.Notes
                End With
                InventoryDB.Data.Tables("Inventory").Rows.Add(newRow)

                result = True
            End If
        End If

        InventoryDB.Data.AcceptChanges()

        Return result
    End Function

    ''' <summary>
    ''' Checks to see if any of the fields on the inventory object are set.
    ''' </summary>
    ''' <param name="InventoryRecord">The Inventory object to check.</param>
    ''' <returns>Returns true if at least one of the fields on the Inventory object are set to a valid value, otherwise returns false.</returns>
    Private Shared Function DoesInventoryHaveValues(ByVal InventoryRecord As Inventory) As Boolean

        If (InventoryRecord.Quantity > 0) Or
                (Not String.IsNullOrEmpty(InventoryRecord.FileNameBack)) Or
                (Not String.IsNullOrEmpty(InventoryRecord.FileNameFront)) Or
                (Not String.IsNullOrEmpty(InventoryRecord.MleUrl)) Or
                (Not String.IsNullOrEmpty(InventoryRecord.Notes)) Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Add the details for the current card to the inventory file currently loaded into memory.
    ''' Note:  This only loads the data into the InventoryDB object.  InventoryDB.Save() must be called separately to save the data to disk.
    ''' </summary>
    ''' <param name="CardId">The Id of the card from the cards.xml file.</param>
    ''' <param name="Quantity">The current quantity of the card.</param>
    ''' <param name="FileNameFront">The file name or file path and file name to the image to display for the front of the card.</param>
    ''' <param name="FileNameBack">The file name or file path and file name to the image to display for the back of the card.</param>
    ''' <param name="MleUrl">The URL for where the card image can be found online.</param>
    ''' <param name="Notes">Any additional notes about the card.</param>
    ''' <returns>Returns True if a change was made to the Inventory data, returns false if no changes were found.</returns>
    Shared Function SaveInventoryForCard(ByVal CardId As Integer, ByVal Quantity As Integer, ByVal FileNameFront As String, ByVal FileNameBack As String, ByVal MleUrl As String, ByVal Notes As String) As Boolean
        Dim InventoryRecord As Inventory = New Inventory()
        InventoryRecord.Id = CardId
        InventoryRecord.Quantity = Quantity
        InventoryRecord.FileNameFront = FileNameFront
        InventoryRecord.FileNameBack = FileNameBack
        InventoryRecord.MleUrl = MleUrl
        InventoryRecord.Notes = Notes

        Return SaveInventoryForCard(InventoryRecord)
    End Function
End Class
