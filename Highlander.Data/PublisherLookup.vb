﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class PublisherLookup

    ''' <summary>
    ''' Indicates what publisher a card was printed by.
    ''' </summary>
    Enum Publisher

        ''' <summary>
        ''' Thunder Castle Games (First Edition)
        ''' </summary>
        ThunderCastleGames = 1

        ''' <summary>
        ''' The Missing Link Expansion Team (First Edition)
        ''' </summary>
        MleTeam = 2

        ''' <summary>
        ''' SAEC Games (First Edition)
        ''' </summary>
        SaecGames = 3

        ''' <summary>
        ''' Le Montagnard Inc (Second Edition / Second Edition Revised)
        ''' </summary>
        LeMontagnard = 4

        ''' <summary>
        ''' Paradox Publishing (Third Edition)
        ''' </summary>
        ParadoxPublishing = 5

        ''' <summary>
        ''' World of Game Design (Highlander Pro / Fourth Edition)
        ''' </summary>
        WorldOfGameDesign = 6

        ''' <summary>
        ''' OneShot Games (Second Edition Revised)
        ''' </summary>
        OneShotGames = 7
    End Enum
End Class
