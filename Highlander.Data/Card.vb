﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Threading
Imports System.Data

Public Class Card
    ''' <summary>
    ''' Contains the ID for this card's publisher.  See PublisherLookup for valid values.
    ''' </summary>
    Private _PublisherID As Integer

    ''' <summary>
    ''' Contains the two to three character code for this cards expansion set.
    ''' </summary>
    Private _Set As String

    ''' <summary>
    ''' Contains the Unique Id number for this card.
    ''' </summary>
    ''' <returns></returns>
    Public Property Id As Integer

    ''' <summary>
    ''' Contains the default image file image for the front of this card.
    ''' </summary>
    ''' <returns></returns>
    Public Property FileNameFront As String

    ''' <summary>
    ''' Contains the default image file name for the back of this card.
    ''' </summary>
    ''' <returns></returns>
    Public Property FileNameBack As String

    ''' <summary>
    ''' Contains the rarity of this card (Common, Uncommon, Rare, etc).
    ''' </summary>
    ''' <returns></returns>
    Public Property Rarity As String

    ''' <summary>
    ''' Contains the type of this card (Attack, Block, etc).
    ''' </summary>
    ''' <returns></returns>
    Public Property Type As String

    ''' <summary>
    ''' Contains any restricted information for this card (R = Reserved, S = Signatured, # = restriction number, ex: S2 = Signatured to 2).
    ''' For Persona cards, this will contain the Master card limit.
    ''' </summary>
    ''' <returns></returns>
    Public Property Restricted As String

    ''' <summary>
    ''' Contains the hand indicators for this card (1 or 2 for one or two hands, o1 for off-hand)
    ''' Can also be # / # for multiple options, ex: 1 / 2, one or two hands, 1 / o1 one or offhand.
    ''' </summary>
    ''' <returns></returns>
    Public Property Hands As String

    ''' <summary>
    ''' Contains the Immortal / Weapon of Choice / etc that this card is reserved to (or Generic for generic cards).
    ''' </summary>
    ''' <returns></returns>
    Public Property Immortal As String

    ''' <summary>
    ''' Contains the original card title (ex: Back Away).
    ''' </summary>
    ''' <returns></returns>
    Public Property Title As String

    ''' <summary>
    ''' Contains the full text of this card.
    ''' </summary>
    ''' <returns></returns>
    Public Property Text As String

    ''' <summary>
    ''' Contains Grid information for Attacks/Blocks/Dodges (ex: x-------- is Upper Left grid)
    ''' </summary>
    ''' <returns></returns>
    Public Property Grid As String

    ''' <summary>
    ''' Contains any Gems that are printed on this card.
    ''' </summary>
    ''' <returns></returns>
    Public Property Gems As String

    ''' <summary>
    ''' Indicates if this card has the "second edition only" symbol printed on it.
    ''' </summary>
    ''' <returns></returns>
    Public Property SecondEditionOnly As Boolean

    ''' <summary>
    ''' Contains the card number printed on the card, if any.
    ''' </summary>
    ''' <returns></returns>
    Public Property CardNumber As String

    ''' <summary>
    ''' Indicates if this card has the "uniqueness marker" symbol printed on it.
    ''' </summary>
    ''' <returns></returns>
    Public Property UniquenessMarker As Boolean

    ''' <summary>
    ''' Indicates if this card has the "double diamond" symbol printed on it.
    ''' </summary>
    ''' <returns></returns>
    Public Property DoubleDiamond As Boolean

    ''' <summary>
    ''' Contains the original sub-type of this card, if any.
    ''' </summary>
    ''' <returns></returns>
    Public Property SubType As String

    ''' <summary>
    ''' Indicates the number of copies of this card to display.
    ''' This can be used to indicate number of copies from the inventory, or in a deck, or on a trade list, etc.
    ''' If Quantity is not passed in when this object is initialized, it will default to 0.
    ''' </summary>
    ''' <returns></returns>
    Public Property Quantity As Integer = 0

    ''' <summary>
    ''' List of cards placed under the current card.
    ''' </summary>
    ''' <returns></returns>
    Public Property SubItems As List(Of Card) = New List(Of Card)

    ''' <summary>
    ''' A prefix used when adding multiple copies of the same card to a list (Ex: Haves / Wants List would set this to H or W to indicate which list the card came from).
    ''' </summary>
    ''' <returns></returns>
    Public Property DisplayNamePrefix As String = String.Empty

    ''' <summary>
    ''' Used to cache the calculated display name ([Immortal] [Original Card Title]).
    ''' </summary>
    Private _OriginalDisplayName As String = String.Empty

    ''' <summary>
    ''' Used to cache the calculated display name ([Immortal] [Card Title]).
    ''' Note:  Some first edition cards (ex: Back Away) have been errata'd to have a different title when playing in type 1 (ex: Evade (Back Away)).  This will use the CardsDB.CardDisplayMode value to determine which card title to use.
    ''' </summary>
    Private _DisplayName As String = String.Empty

    ''' <summary>
    ''' Returns the Printed Display Name of the card ([Immortal] [Original Card Title]).
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property OriginalDisplayName() As String
        Get
            If String.IsNullOrEmpty(_OriginalDisplayName) Then
                ' If "Immortal" is null, just use title
                _OriginalDisplayName = Immortal.Trim()
                If Not String.IsNullOrEmpty(_OriginalDisplayName) Then
                    _OriginalDisplayName &= " "
                End If

                _OriginalDisplayName &= Title
            End If

            Return _OriginalDisplayName
        End Get
    End Property

    ''' <summary>
    ''' Returns the Display Name of the card ([Immortal] [Card Title]).
    ''' Note:  Some first edition cards (ex: Back Away) have been errata'd to have a different title when playing in type 1 (ex: Evade (Back Away)).  This will use the CardsDB.CardDisplayMode value to determine which card title to use.
    ''' Note 2:  The Display Name will be calculated the first time this property is read and then cached in Memory and returned during additional requests.  It will not recalculate in the event that the CardsDB.CardDisplayMode value changes.
    ''' Note 3:  The Display Name can be manually.  If this happens the value that DisplayName was set to will take precedence over the calculated value.
    ''' Note 4:  If Display Name is cleared out (set to Empty String) then it'll re-calculate the next time the property is read.
    ''' </summary>
    ''' <returns></returns>
    Public Property DisplayName() As String
        Get
            If String.IsNullOrEmpty(_DisplayName) Then
                ' If "Immortal" is null, just use title
                _DisplayName = Immortal.Trim()
                If Not String.IsNullOrEmpty(_DisplayName) Then
                    _DisplayName &= " "
                End If

                ' If display mode is type 1, and publisher is a 1st edition publisher, then check for type 1 errata, and if found, use it instead.
                If CardsDB.CardDisplayMode = "type_one" And _PublisherID < 4 Then
                    _DisplayName &= SecondEditionTitle
                Else
                    _DisplayName &= Title
                End If
            End If

            Return _DisplayName
        End Get
        Set(value As String)
            _DisplayName = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the Title of the card.
    ''' Note:  Some first edition cards (ex: Back Away) have been errata'd to have a different title when playing in type 1 (ex: Evade (Back Away)).  This will use the CardsDB.CardDisplayMode value to determine which card title to use.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property DisplayTitle As String
        Get
            If CardsDB.CardDisplayMode = "type_one" And _PublisherID < 4 Then
                Return SecondEditionTitle
            Else
                Return Title
            End If
        End Get
    End Property

    ''' <summary>
    ''' Used to cache the expansion set information for the current card.
    ''' The Expansion Set record will be automatically loaded once based on the value in _Set the first time this property is accessed.
    ''' </summary>
    Private _SetData As ExpansionSet = Nothing

    ''' <summary>
    ''' Contains information about the cards Expansion Set (Expansion Set Name / Code, Edition and Release Date).
    ''' This property will be automatically set the first time it's read based on the value in _Set.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property ExpansionSet As ExpansionSet
        Get
            If _SetData Is Nothing Then
                _SetData = ExpansionSet.LoadSet(_Set)
            End If

            Return _SetData
        End Get
    End Property

    ''' <summary>
    ''' Used to cache the publisher information for the current card.
    ''' The Publisher record will be automatically loaded once based on the value in _PublisherID the first time this property is accessed.
    ''' </summary>
    Private _PublisherData As Publisher = Nothing

    ''' <summary>
    ''' Contains information about the cards Publisher (Publisher Name and Code).
    ''' This property will be automatically set the first time it's read based on the value in _PublisherID.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Publisher() As Publisher
        Get
            If _PublisherData Is Nothing Then
                _PublisherData = Publisher.LoadPublisher(_PublisherID)
            End If

            Return _PublisherData
        End Get
    End Property

    ''' <summary>
    ''' Contains the full path to the image of the front of the card.
    ''' This will first check the loaded inventory for a FileNameFront and if not found will then use the default FileNameFront value.
    ''' It will then Check to see if a file path is specified in the value and if not, will prefix it with the default file path located in CardsDB.HighlanderImagePath based on the card's publisher and expansion set.
    ''' If a image file path can't be found or it doesn't exist, it'll instead return the path to the back.jpg file that corresponds to the current cards edition.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property FilePathFront() As String
        Get
            ' Check the loaded inventory for the file name
            Dim resultsInventory As Inventory = Inventory.GetInventoryForCard(Id)
            Dim CardJPEG As String = resultsInventory.FileNameFront

            ' If the current card isn't in the inventory the grab the default file name from the database instead.
            If (CardJPEG = "") Then
                CardJPEG = FileNameFront
            End If

            Return GetFullFilePath(CardJPEG)
        End Get
    End Property

    ''' <summary>
    ''' Contains the full path to the image of the back of the card.
    ''' This will first check the loaded inventory for a FileNameBack and if not found will then use the default FileNameBack value.
    ''' It will then Check to see if a file path is specified in the value and if not, will prefix it with the default file path located in CardsDB.HighlanderImagePath based on the card's publisher and expansion set.
    ''' If a image file path can't be found or it doesn't exist, it'll instead return the path to the back.jpg file that corresponds to the current cards edition.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property FilePathBack() As String
        Get
            ' Check the loaded inventory for the file name
            Dim resultsInventory As Inventory = Inventory.GetInventoryForCard(Id)
            Dim CardJPEG As String = resultsInventory.FileNameBack

            ' If the current card isn't in the inventory the grab the default file name from the database instead.
            If (CardJPEG = "") Then
                CardJPEG = FileNameBack
            End If

            Return GetFullFilePath(CardJPEG)
        End Get
    End Property

    ''' <summary>
    ''' This will return the second edition version of the card's title.
    ''' For Second / Third Edition card, this will be the CardTitle value.
    ''' For First Edition cards, this will be the errata'd value if available, or else the CardTitle value (ex: A first edition "Back Away" will return as "Evade (Back Away)" to match the latest errata).
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property SecondEditionTitle() As String
        Get
            If _PublisherID = PublisherLookup.Publisher.ThunderCastleGames Or
                _PublisherID = PublisherLookup.Publisher.MleTeam Or
                _PublisherID = PublisherLookup.Publisher.SaecGames Then
                If Title = "Dodge" Then
                    Return "Evade (Escape)" 'All 1ed non-master dodges titled Dodge now have the title Evade and the sub-type is now Dodge : Escape.
                ElseIf Title = "Back Away" Then
                    Return "Evade (Back Away)" ' All 1ed non-master dodges titled Back Away now have the title Evade and the sub-title is now Dodge: Back Away.
                ElseIf Title = "Duck" Then
                    Return "Counter (Duck)" ' All 1ed non-master dodges titled Duck now have the title Counter and the sub-title is now Dodge: Duck.
                ElseIf Title = "Jump" Then
                    Return "Counter (Jump)" ' All 1ed non-master dodges titled Jump have the title Counter and the sub-title is now Dodge: Jump.
                ElseIf Title.Contains("Side Step") Then
                    If Title.Contains("Left") Then
                        Return "Counter (Left Sidestep)" ' All 1ed non-master dodges titled Side Step now have the title Counter and the sub-title is now Dodge: Side Step.
                    ElseIf Title.Contains("Right") Then
                        Return "Counter (Right Sidestep)" ' All 1ed non-master dodges titled Side Step now have the title Counter and the sub-title is now Dodge: Side Step.
                    End If

                ElseIf IsGeneric Then

                    ' All 1ed Generic Guard cards (Upper Guard, Lower Guard, Right Guard, Left Guard) are now titled Guard.
                    If Title = "Upper Guard" Then
                        Return "Guard (Upper)"
                    ElseIf Title = "Lower Guard" Then
                        Return "Guard (Lower)"
                    ElseIf Title = "Right Guard" Then
                        Return "Guard (Right)"
                    ElseIf Title = "Left Guard" Then
                        Return "Guard (Left)"
                    End If
                End If
            End If

            Return Title
        End Get
    End Property

    ''' <summary>
    ''' This will return the second edition version of the card's sub-type.
    ''' For Second / Third Edition card, this will be the SubType value.
    ''' For First Edition cards, this will be the errata'd value if available, or else the SubType value (ex: A first edition "Back Away" will return as "Back Away" to match the latest errata).
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property SecondEditionSubType() As String
        Get
            If _PublisherID = PublisherLookup.Publisher.ThunderCastleGames Or
                _PublisherID = PublisherLookup.Publisher.MleTeam Or
                _PublisherID = PublisherLookup.Publisher.SaecGames Then
                If Title = "Dodge" Then
                    Return "Escape" 'All 1ed non-master dodges titled Dodge now have the title Evade and the sub-type is now Dodge : Escape.
                ElseIf Title = "Back Away" Then
                    Return "Back Away" ' All 1ed non-master dodges titled Back Away now have the title Evade and the sub-title is now Dodge: Back Away.
                ElseIf Title = "Duck" Then
                    Return "Duck" ' All 1ed non-master dodges titled Duck now have the title Counter and the sub-title is now Dodge: Duck.
                ElseIf Title = "Jump" Then
                    Return "Jump" ' All 1ed non-master dodges titled Jump have the title Counter and the sub-title is now Dodge: Jump.
                ElseIf Title.Contains("Side Step") Then
                    Return "Side Step" ' All 1ed non-master dodges titled Side Step now have the title Counter and the sub-title is now Dodge: Side Step.
                End If

            End If
            Return SubType
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the current card is a Master card.
    ''' If the current card either has a Master Gem on it, or contains Master in the title, then it's a master card.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsMasterCard() As Boolean
        Get
            If IsPreGame() Then ' Only In-Game cards can be Master Cards.
                Return False
            Else
                If Gems.Length > 0 Then ' If card has Gems, then see if it has a Master Gem.
                    If Gems.ToLower.Contains("m") Then
                        Return True
                    End If
                ElseIf Title.ToLower.Contains("master") Then ' If not, see if Master is in the title.
                    Return True
                End If
            End If

            ' This is not a master card.
            Return False
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the card is a pre-game card (Ex: Persona, Weapon of Choice, etc).
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsPreGame() As Boolean
        Get
            If (Type = "Persona") Or
                (Type = "Place") Or
                (Type = "Pre-Game") Or
                (Type = "Quickening") Or
                (Type = "Watcher") Or
                (Type = "Hunter") Or
                (Type = "Weapon of Choice") Or
                (Type = "Faction") Or
                (Type = "Skill") Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the card is a Generic card.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsGeneric() As Boolean
        Get
            If Immortal.ToLower() = "generic" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the card is an Immortal Specific card (Ex:  if Immortal is Generic or Katana, it's not Immortal Specific.  If Immortal is Duncan MacLeod, it is Immortal Specific).
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsImmortalSpecific() As Boolean
        Get
            If Type = "Persona" Then
                Return False
            Else

                ' There isn't a persona for these immortals in first edition, so these first edition cards are not 
                '   immortal specific.  However there are personas in second edition and / or MLE, which causes the 
                '   database search for a persona to flag them as immortal specific incorrectly.
                If Id = CardLookup.ThunderCastleGames.PromotionalCards.DariusPreGameRestriction Or
                    Id = CardLookup.ThunderCastleGames.TheWatchersChronicle.DariusPreGameMaster Or
                    Id = CardLookup.SaecGames.BlackRavenVol4.AndreKordaPreGameAdditionalPowers Or
                    Id = CardLookup.SaecGames.BlackRavenVol3.RebeccaHorneAdditionalPowers Or
                    Id = CardLookup.SaecGames.BlackRavenVol2.FatherLiamRiley Then
                    Return False
                Else

                    Dim isPersonaSpecificCard() As DataRow = CardsDB.Data.Tables("Card").Select("Immortal = '" & Immortal.Replace("'", "''") & "' AND Type = 'Persona'")
                    If isPersonaSpecificCard.Length > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the card is Faction specific.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsFactionSpecific() As Boolean
        Get
            ' 4th Edition has Faction Pre-Games, earlier editions do not
            If ExpansionSet.Edition = Edition.FourthEdition Then
                Dim isFactionSpecificCard() As DataRow = CardsDB.Data.Tables("Card").Select("Immortal = '" & Immortal.Replace("'", "''") & "' AND Type = 'Faction'")
                If isFactionSpecificCard.Length > 0 Then
                    Return True
                Else
                    Return False
                End If

            Else
                If Immortal.ToLower() <> "clan macleod" AndAlso
                    Immortal <> "the four horsemen" AndAlso
                    Immortal <> "warriors of zeist" AndAlso
                    Immortal <> "rebecca's students" AndAlso
                    Immortal <> "highlanders" AndAlso
                    Immortal <> "Romans" Then
                    Return False
                Else
                    Return True
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the card is Weapon of Choice specific.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsWeaponSpecific() As Boolean
        Get
            If Type = "Weapon of Choice" Then
                Return False
            Else
                Dim IsWeaponSpecificCard() As DataRow = CardsDB.Data.Tables("Card").Select("Immortal = '" & Immortal.Replace("'", "''") & "' AND Type = 'Weapon of Choice'")
                If IsWeaponSpecificCard.Length > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' This indicates if the card is Skill specific.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property IsSkillSpecific() As Boolean
        Get
            If Type = "Skill" Then
                Return False
            Else
                Dim IsSkillSpecificCard() As DataRow = CardsDB.Data.Tables("Card").Select("Immortal = '" & Immortal.Replace("'", "''") & "' AND Type = 'Skill'")
                If IsSkillSpecificCard.Length > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Convert the supplied data row from the cards.xml file to a Card object.  Optionally set the Quantity property on the object.
    ''' </summary>
    ''' <param name="record">The data row from the cards.xml to convert to a Card Object.</param>
    ''' <param name="quantity">The Quantity value to set on the Card object.  If blank, Quantity will be set to 0.</param>
    ''' <returns></returns>
    Public Shared Function DataRowToCard(ByVal record As DataRow, ByVal Optional quantity As Integer = 0) As Card
        Dim card As New Card

        card.Id = record.Item("ID").ToString()
        card._Set = record.Item("Set").ToString()
        card.FileNameFront = record.Item("File Name Front").ToString()
        card.FileNameBack = record.Item("File Name Back").ToString()
        card.Rarity = record.Item("Rarity").ToString()
        card.Type = record.Item("Type").ToString()
        card.Restricted = record.Item("Restricted").ToString()
        card.Hands = record.Item("Hands").ToString()
        card.Immortal = record.Item("Immortal").ToString()
        card.Title = record.Item("Title").ToString()
        card.Text = record.Item("Text").ToString()
        card.Grid = record.Item("Grid").ToString()
        card.Gems = record.Item("Gems").ToString()
        card.SecondEditionOnly = record.Item("2E Only").ToString()
        card._PublisherID = record.Item("PublisherID").ToString()
        card.CardNumber = record.Item("CardNumber").ToString()
        card.UniquenessMarker = record.Item("UniquenessMarker").ToString()
        card.DoubleDiamond = record.Item("DoubleDiamond").ToString()
        card.SubType = record.Item("Sub Type").ToString()
        card.Quantity = quantity

        Return card
    End Function

    ''' <summary>
    ''' Load a card from the cards.xml by card Id and return a Card object.  Optionally set the Quantity property on the object.
    ''' </summary>
    ''' <param name="CardId">The Id of the card to load.  If the Id is not valid, Nothing will be returned.</param>
    ''' <param name="quantity">The Quantity value to set on the Card object.  If blank, Quantity will be set to 0.</param>
    ''' <returns>Returns a Card object with the details of the requested card, or Nothing if the Card Id wasn't found.</returns>
    Public Shared Function LoadCard(ByVal CardId As Integer, ByVal Optional CardQuantity As Integer = 0) As Card
        Dim SelectedCard() As DataRow = CardsDB.Data.Tables("Card").Select("ID = '" & CardId.ToString() & "'")
        If SelectedCard.Length > 0 Then
            Return DataRowToCard(SelectedCard(0), CardQuantity)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Load multiple cards from the cards.xml and return a list of Card objects.
    ''' </summary>
    ''' <param name="SearchQuery">The search criteria to use to determine which cards to load (Ex: "Set = 'SE'" will load all Series Edition cards).</param>
    ''' <returns></returns>
    Public Shared Function LoadCards(ByVal SearchQuery As String) As List(Of Card)
        Return New List(Of Card)((From cardRow As DataRow In CardsDB.Data.Tables("Card").Select(SearchQuery).AsEnumerable() Select (DataRowToCard(cardRow))))
    End Function

    ''' <summary>
    ''' Get the full path to the passed in filename for the front / back of the card (if it exists)
    ''' or else get the default default back for the card.
    ''' </summary>
    ''' <param name="FileName">The file name for the card (front or back).</param>
    ''' <returns>The full path and file name to use for displaying the card.</returns>
    Private Function GetFullFilePath(ByVal FileName As String) As String
        Dim strJPEGPath As String = CardsDB.HighlanderImagePath

        If FileName <> "" Then
            ' If the file name doesn't have a path, add the default path for the expansion set the card belongs to.
            If FileName.Chars(1) <> ":" Then
                If _Set = "MAO" Then
                    strJPEGPath = IO.Path.Combine(strJPEGPath, "Misprints and Oddities")
                Else
                    strJPEGPath = IO.Path.Combine(strJPEGPath, Publisher.Name, ExpansionSet.Name.Replace("/", "and"))
                End If
                FileName = IO.Path.Combine(strJPEGPath, FileName)
            End If

            ' If the file exists, use it, otherwise move onto the next section and return the card back.
            If System.IO.File.Exists(FileName) Then
                Return FileName
            End If
        End If

        ' If file doesn't exist or a path isn't specified, return the card back instead
        If _PublisherID = PublisherLookup.Publisher.LeMontagnard Then ' Second Edition Back
            Return IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "2eback.jpg")
        ElseIf _PublisherID = PublisherLookup.Publisher.OneShotGames Then  ' Second Edition Back
            Return IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "2eback.jpg")
        ElseIf _PublisherID = PublisherLookup.Publisher.ParadoxPublishing Then ' Third Edition Back
            Return IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "3eback.jpg")
        ElseIf _PublisherID = PublisherLookup.Publisher.WorldOfGameDesign Then ' Fourth Edition Back
            Return IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "4eback.jpg")
        Else
            Return IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "1eback.jpg") ' First Edition Back for everything else
        End If
    End Function

    ''' <summary>
    ''' Some cards are identical other then the picture and have a number in the title to distinguish them.
    ''' As these cards are functionally the same, we don't need this number and need to strip it out to compare with other cards.
    ''' </summary>
    ''' <param name="CardTitle"></param>
    ''' <returns></returns>
    Public Shared Function RemoveUnnecessaryVersion(ByVal CardTitle As String, ByVal CardId As Integer) As String

        Dim ReturnValue As String = CardTitle

        If CardId = CardLookup.LeMontagnard.TheGathering.RamirezPremiumCloseUp OrElse
            CardId = CardLookup.LeMontagnard.TheGathering.RamirezPremiumHorse Then
            ReturnValue = "Premium"

        ElseIf CardId = CardLookup.LeMontagnard.TheGathering.RamirezEvadeEscapeA OrElse
                CardId = CardLookup.LeMontagnard.TheGathering.RamirezEvadeEscapeB Then
            ReturnValue = "Evade (Escape)"

        ElseIf CardId = CardLookup.LeMontagnard.TheGathering.RamirezAlertnessBlockDodgeA OrElse
                CardId = CardLookup.LeMontagnard.TheGathering.RamirezAlertnessBlockDodgeB Then
            ReturnValue = "Alertness (Block & Dodge)"

        Else
            ReturnValue = CardTitle.Replace("(Version 1)", "").Replace("(Version 2)", "").Replace("(Version 3)", "").Replace("(1)", "").Replace("(2)", "").Replace("(3)", "").Replace("(A)", "").Replace("(B)", "").Trim()
        End If

        Return ReturnValue
    End Function

End Class


