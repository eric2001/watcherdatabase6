﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

''' <summary>
''' Indicates what edition an expansion set is.
''' </summary>
Public Enum Edition
    ''' <summary>
    ''' Used for special sets like MAO that span multiple publishers
    ''' </summary>
    All = 0

    ''' <summary>
    ''' First Edition expansion set printed by Thunder Castle Games, SAEC Games or the MLE Team.
    ''' </summary>
    FirstEdition = 1

    ''' <summary>
    ''' Second Edition expansion set printed by Le Montagnard Inc.
    ''' </summary>
    SecondEdition = 2

    ''' <summary>
    ''' Third Edition expansion set printed by Paradox Publishing.
    ''' </summary>
    ThirdEdition = 3

    ''' <summary>
    ''' Second Edition Revised expansion set printed by Le Montagnard Inc.
    ''' Note:  This is number 4 because Second Edition Revised was released after Third Edition.
    ''' </summary>
    SecondEditionRevised = 4

    ''' <summary>
    ''' Highlander Pro / Fourth Edition expansion set printed by World of Game Design.
    ''' </summary>
    FourthEdition = 5
End Enum

