﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class InventoryDB

    ''' <summary>
    ''' Contains the raw data from the current Inventory file (if any) or a new DataSet to populate with new Inventory data.
    ''' </summary>
    Public Shared Data As New DataSet("Inventory")

    ''' <summary>
    ''' Contains the full file path and name of the current inventory file (if any).
    ''' </summary>
    Private Shared _fileName As String = String.Empty

    ''' <summary>
    ''' Contains the full file path and name of the current inventory file.
    ''' Note:  This will be blank for new / unsaved inventory data.
    ''' </summary>
    ''' <returns></returns>
    Public Shared ReadOnly Property FileName() As String
        Get
            Return _fileName
        End Get
    End Property

    ''' <summary>
    ''' Initialize a new inventory data object.
    ''' </summary>
    Shared Sub New()
        Data.ReadXmlSchema(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\Inventory.xsd"))
    End Sub

    ''' <summary>
    ''' Clears all Inventory data in memory.  Use when creating a new inventory.
    ''' </summary>
    Shared Sub ClearFile()
        Data.Tables("Inventory").Rows.Clear()
        Data.Tables("Settings").Rows.Clear()
        _fileName = String.Empty
    End Sub

    ''' <summary>
    ''' Load an existing Inventory file into memory.  This will clear any existing data that was previously loaded.
    ''' </summary>
    ''' <param name="FileName">The full file path and name of the .hli file to load.</param>
    Shared Sub LoadFile(ByVal FileName As String)
        ' Clear any existing data
        ClearFile()

        If Not String.IsNullOrEmpty(FileName) Then
            ' Load the new file into memory
            Data.ReadXml(FileName)

            ' Store the location of the file that was just loaded to use when saving changes.
            _fileName = FileName
        End If

    End Sub

    ''' <summary>
    ''' Save the current data to a .hli file.
    ''' </summary>
    ''' <param name="FileName">Optional:  The full file path and name to save the data to.  If blank the file name that was used when LoadFile() was called will be used.  If a file name cannot be determined then an exception will be thrown.</param>
    Shared Sub SaveFile(Optional ByVal FileName As String = "")
        If FileName <> String.Empty Then
            _fileName = FileName
        End If

        If _fileName = String.Empty Then
            Throw New Exception("Unable to Save Inventory:  No File Name")
        End If

        Data.WriteXml(_fileName)
    End Sub

    ''' <summary>
    ''' Returns the number of records in the open inventory file.
    ''' </summary>
    ''' <returns>A count of the number of records that are loaded in memory for the current inventory file.</returns>
    Shared Function GetInventoryRecordCount() As Integer
        Return Data.Tables("Inventory").Rows.Count
    End Function

End Class
