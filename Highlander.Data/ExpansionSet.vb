﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class ExpansionSet

    ''' <summary>
    ''' Contains the unique ID number for this set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Id As Integer

    ''' <summary>
    ''' Contains the unique Code for this set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Code As String

    ''' <summary>
    ''' Contains the full name for this set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Name As String

    ''' <summary>
    ''' Contains the release date for this set (format: yyyy.mm or yyyy.mm.dd)
    ''' </summary>
    ''' <returns></returns>
    Public Property ReleaseDate As String

    ''' <summary>
    ''' Contains the edition for this set (First Edition / Second Edition / Third Edition).
    ''' </summary>
    ''' <returns></returns>
    Public Property Edition As Edition

    ''' <summary>
    ''' Contains the ID of the publisher for this set.
    ''' </summary>
    Private _PublisherId As Integer

    ''' <summary>
    ''' Contains the details of the publisher for this set.
    ''' </summary>
    Private _PublisherData As Publisher = Nothing

    ''' <summary>
    ''' Contains the details of the publisher for this set (loaded dynamically when requested).
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Publisher() As Publisher
        Get
            If _PublisherData Is Nothing Then
                _PublisherData = Publisher.LoadPublisher(_PublisherId)
            End If

            Return _PublisherData
        End Get
    End Property

    ''' <summary>
    ''' Converts a data from from the HLSets.xml file to an ExpansionSet object.
    ''' </summary>
    ''' <param name="record">DataRow from the HLSets.xml file.</param>
    ''' <returns>An ExpansionSet object populated with the values from the passed in record.</returns>
    Public Shared Function DataRowToSet(ByVal record As DataRow) As ExpansionSet
        Dim expansionSet As New ExpansionSet

        expansionSet.Id = record.Item("set_ID").ToString()
        expansionSet.Code = record.Item("set_short").ToString()
        expansionSet.Name = record.Item("set_long").ToString()
        expansionSet.ReleaseDate = record.Item("set_date").ToString()
        expansionSet._PublisherId = record.Item("set_Publisher").ToString()

        Select Case record.Item("set_Edition").ToString().ToUpper()
            Case "1E"
                expansionSet.Edition = Edition.FirstEdition
            Case "2E"
                expansionSet.Edition = Edition.SecondEdition
            Case "3E"
                expansionSet.Edition = Edition.ThirdEdition
            Case "2ER"
                expansionSet.Edition = Edition.SecondEditionRevised
            Case "4E"
                expansionSet.Edition = Edition.FourthEdition
            Case Else
                ' MAO Cards
                expansionSet.Edition = Edition.All
        End Select

        Return expansionSet
    End Function

    ''' <summary>
    ''' Returns an ExpansionSet object that corresponds to the passed in ID value.
    ''' </summary>
    ''' <param name="SetId">The Unique ID number of a record from the HLSets.xml file.</param>
    ''' <returns>An ExpansionSet object populated with the values from the corresponding HLSets.xml record or Nothing if the Id is not valid.</returns>
    Public Shared Function LoadSet(ByVal SetId As Integer) As ExpansionSet
        Dim SelectedSet() As DataRow = SetsDB.Data.Tables("set").Select("set_ID = '" & SetId.ToString() & "'")
        If SelectedSet.Length > 0 Then
            Return DataRowToSet(SelectedSet(0))
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Returns an ExpansionSet object that corresponds to the passed in Set Code
    ''' </summary>
    ''' <param name="SetCode">The unique Code of a record from the HLSets.xml file</param>
    ''' <returns>An ExpansionSet object populated with the values from the corresponding HLSets.xml record or Nothing if the Code is not valid.</returns>
    Public Shared Function LoadSet(ByVal SetCode As String) As ExpansionSet
        Dim SelectedSet() As DataRow = SetsDB.Data.Tables("set").Select("set_short = '" & SetCode & "'")
        If SelectedSet.Length > 0 Then
            Return DataRowToSet(SelectedSet(0))
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Returns a list of ExpansionSet objects that match the SeachQuery, sorted based on the SortQuery value.
    ''' </summary>
    ''' <param name="SearchQuery">The raw search string to use to search the HLSets.xml file (ex: "set_log = 'Series Edition'", "set_Publisher = 1", etc).  Pass in an empty string value to return all sets.</param>
    ''' <param name="SortQuery">The raw sort string to use to sort the results of the search (ex: "set_long ASC" will sort by Set Name in Ascending order).</param>
    ''' <returns>A list of ExpansionSet objects that match the passed in search query.</returns>
    Public Shared Function LoadSets(ByVal SearchQuery As String, Optional ByVal SortQuery As String = "set_long ASC") As List(Of ExpansionSet)
        Return New List(Of ExpansionSet)((From setRow As DataRow In SetsDB.Data.Tables("set").Select(SearchQuery, SortQuery).AsEnumerable() Select (DataRowToSet(setRow))))
    End Function

End Class
