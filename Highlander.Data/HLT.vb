﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class HLT

    ''' <summary>
    ''' Load a previously saved .hlt file.
    ''' </summary>
    ''' <param name="FileName">The full path and file name to the .hlt file to load.</param>
    ''' <returns>A TradeList object containing the details from the .hlt file.</returns>
    Public Shared Function Load(ByVal FileName As String) As TradeList
        ' This function is responsible for opening a trade list that has previously been
        '   saved in .hlt format.
        Dim list As New TradeList

        Dim FileReader As New System.IO.StreamReader(FileName)
        Dim tempLine As String, newCard() As String

        Do
            ' Read the file, one line at a time.
            tempLine = FileReader.ReadLine
            If tempLine <> "" Then

                ' Split the current line up into an array of three elements.
                '   (0) is either H or W for haves or Wants
                '   (1) is the quantity
                '   (2) is the ID Number
                newCard = tempLine.Split(",")
                Dim counter As Integer = 0

                ' Loop through each item in the array, remove "" characters
                '   from around text values.
                While counter < 3
                    If newCard(counter).Chars(0) = """" Then
                        newCard(counter) = newCard(counter).Substring(1)
                    End If
                    If newCard(counter).Chars(newCard(counter).Length - 1) = """" Then
                        newCard(counter) = newCard(counter).Substring(0, newCard(counter).Length - 1)
                    End If
                    counter = counter + 1
                End While

                ' Load the image and details of the current card.
                Dim SelectedCard As Card = Card.LoadCard(newCard(2), newCard(1))
                If SelectedCard Is Nothing Then
                    Throw New Exception(String.Format("Unable to Load Deck:  Card ID {0} Not Found.", newCard(2)))
                End If

                ' Add the current card to either the Haves or Wants list, based 
                '   on newCard(0).
                If newCard(0) = "H" Then
                    list.Haves.Add(SelectedCard)
                Else
                    list.Wants.Add(SelectedCard)
                End If
            End If
        Loop Until tempLine Is Nothing

        FileReader.Close()
        FileReader.Dispose()
        FileReader = Nothing

        Return list
    End Function

    ''' <summary>
    ''' Save a TradeList object as a .hlt file.
    ''' </summary>
    ''' <param name="FileName">The full path and file name for where the data should be saved to.</param>
    ''' <param name="list">The data to save.</param>
    ''' <returns>Returns True if the save is successful.</returns>
    Public Shared Function Save(ByVal FileName As String, ByVal list As TradeList) As Boolean
        ' Save the current Haves and Wants list to a .hlt file.
        '   If the list has already been saved, update the existing file.

        Dim FileWriter As New System.IO.StreamWriter(FileName)
        Dim tempCard As Card

        ' Save each card in the Haves list to the file in HLT format.
        For Each tempCard In list.Haves
            FileWriter.WriteLine(String.Format("""H"",{0},{1}", tempCard.Quantity, tempCard.Id))
        Next

        ' Save each card in the Wants to the file in HLT format.
        For Each tempCard In list.Wants
            FileWriter.WriteLine(String.Format("""W"",{0},{1}", tempCard.Quantity, tempCard.Id))
        Next

        ' Close the file.
        FileWriter.Close()
        FileWriter.Dispose()
        FileWriter = Nothing

        Return True
    End Function

End Class
