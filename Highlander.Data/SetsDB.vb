﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class SetsDB

    ''' Contains raw data from the HLSets.xml database.  Data access should be done through the ExpansionSet and Publisher classes where possible rather then accessing directly.
    Public Shared Data As New DataSet("Set")

    ''' <summary>
    ''' Full file path to the HLSets.xml file which contains details on all Highlander expansion sets and publishers.
    ''' </summary>
    Private Shared XmlFilePath As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\HLSets.xml")

    ''' <summary>
    ''' Full file path to the HLSets.xsd file which contains details on the HLSets.xml data structure.
    ''' </summary>
    Private Shared XmlSchemaFilePath As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\HLSets.xsd")

    ''' <summary>
    ''' Load existing Expansion Set and Publisher data into memory.
    ''' </summary>
    Shared Sub New()
        Data.ReadXmlSchema(XmlSchemaFilePath)
        Data.ReadXml(XmlFilePath)
    End Sub

    ''' <summary>
    ''' Save current contents of the Data property to the HLSets.xml file.
    ''' </summary>
    Public Shared Sub Save(Optional ByVal FilePath As String = "")
        Dim DataFilePath As String = XmlFilePath
        If Not String.IsNullOrEmpty(FilePath) Then
            DataFilePath = FilePath
        End If

        Data.WriteXml(DataFilePath)
    End Sub

End Class
