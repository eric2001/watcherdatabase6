﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class CardsDB
    ''' <summary>
    ''' Contains raw data from the cards.xml database.  Data access should be done through Card class where possible rather then accessing directly.
    ''' </summary>
    Public Shared Data As New DataSet("Card")

    ''' <summary>
    ''' Full file path for the root image folder for card scans.
    ''' </summary>
    Public Shared Property HighlanderImagePath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) & "\Highlander CCG"

    ''' <summary>
    ''' Sort order for expansion sets lists.  Either "set_long" for expansion set title, 
    ''' or "set_date" for release date.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Property ExpansionSortOrder As String = "set_long"

    ''' <summary>
    ''' Display mode to use for first edition cards.  "first_edition" means use original errata, "type_one" means use newer errata.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Property CardDisplayMode As String = "first_edition"

    ''' <summary>
    ''' Full file path to the cards.xml file which contains details on all Highlander cards.
    ''' </summary>
    Private Shared XmlFilePath As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\cards.xml")

    ''' <summary>
    ''' Full file path to the cards.xsd file which contains details on the cards.xml data structure.
    ''' </summary>
    Private Shared XmlSchemaFilePath As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\cards.xsd")

    ''' <summary>
    ''' Load existing cards.xml data into memory.
    ''' </summary>
    Shared Sub New()
        ' Load the cards.xml data into a shared property for later use
        Data.ReadXmlSchema(XmlSchemaFilePath)
        Data.ReadXml(XmlFilePath)
    End Sub

    ''' <summary>
    ''' Save the current contents of the Data property to the cards.xml file.
    ''' </summary>
    Public Shared Sub Save(Optional ByVal FilePath As String = "")
        Dim DataFilePath As String = XmlFilePath
        If Not String.IsNullOrEmpty(FilePath) Then
            DataFilePath = FilePath
        End If

        Data.WriteXml(DataFilePath)
    End Sub

End Class
