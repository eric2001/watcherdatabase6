﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports System.Data
Imports Highlander.Reporting
Imports Xunit

Namespace Highlander.Reporting.Tests

    ''' <summary>
    ''' Some of these tests involve changing global settings, so this needs to be tested Sequentially.
    ''' </summary>
    <Collection("Sequential")>
    Public Class CollectionSetDataTests

        <Theory>
        <InlineData("", "Series Edition", 169, 76, 4, 0)>
        <InlineData("", "Season 1", 299, 79, 17, 0)>
        <InlineData("TestData\SeriesEditionTestInventory.hli", "Series Edition", 169, 76, 4, 5)>
        <InlineData("", "ASDF", 0, 0, 0, 0)> ' Test processing a bad set name
        Sub RefreshDataTest(ByVal InventoryFileName As String, ByVal SetName As String, ByVal TotalRows As Integer, ByVal TotalGenericCards As Integer, ByVal TotalInserts As Integer, ByVal TotalQuantity As Integer)
            ' Clear any previously loaded inventory data.
            InventoryDB.ClearFile()

            ' If an inventory file was specified for this test, load it
            If Not String.IsNullOrEmpty(InventoryFileName) Then
                InventoryDB.LoadFile(InventoryFileName)
            End If

            ' Generate data for the set from cards and inventory
            Dim ReportData As New CollectionSetData()
            ReportData.RefreshData(SetName)

            ' Confirm the data for the set matches expected values
            Assert.Equal(TotalRows, ReportData.Data.Tables("Card").Rows.Count)
            Assert.Equal(TotalQuantity, If(ReportData.Data.Tables("Card").Rows.Count = 0, 0, ReportData.Data.Tables("Card").Compute("SUM(Quantity)", String.Empty)))
            Assert.Equal(TotalGenericCards, ReportData.Data.Tables("Card").Select("CardSet = 'Y'").Count) ' Generic Count
            Assert.Equal(TotalInserts, ReportData.Data.Tables("Card").Select("CardSet = 'Z'").Count) ' Inserts Count
        End Sub

    End Class
End Namespace

