' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports System.Data
Imports Highlander.Reporting
Imports Xunit

Namespace Highlander.Reporting.Tests

    ''' <summary>
    ''' Some of these tests involve changing global settings, so this needs to be tested Sequentially.
    ''' </summary>
    <Collection("Sequential")>
    Public Class CollectionOverviewDataTests

        <Theory>
        <InlineData("", 0)>
        <InlineData("TestData\SeriesEditionTestInventory.hli", 5)>
        Sub RefreshDataTest(ByVal InventoryFileName As String, ByVal TotalQuantity As Integer)
            ' Clear any previously loaded inventory data.
            InventoryDB.ClearFile()

            ' If an inventory file was specified for this test, load it
            If Not String.IsNullOrEmpty(InventoryFileName) Then
                InventoryDB.LoadFile(InventoryFileName)
            End If

            ' Generate data from cards and inventory
            Dim ReportData As New CollectionOverviewData()
            ReportData.RefreshData()

            ' Confirm System records are not included in the generated data.
            Assert.Equal(CardsDB.Data.Tables("Card").Rows.Count - 1, ReportData.Data.Tables("Card").Rows.Count)
            Assert.Equal(TotalQuantity, ReportData.Data.Tables("Card").Compute("SUM(Quantity)", String.Empty))
        End Sub

    End Class
End Namespace

