﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports System.Data
Imports Highlander.Reporting
Imports Xunit

Namespace Highlander.Reporting.Tests

    Public Class TradeListDataTests

        <Theory>
        <InlineData("TestData\TradeLists\EmptyTradeList.hlt")>
        <InlineData("TestData\TradeLists\TradeListHavesOnly.hlt")>
        <InlineData("TestData\TradeLists\TradeListHavesWants.hlt")>
        <InlineData("TestData\TradeLists\TradeListWantsOnly.hlt")>
        <InlineData("TestData\TradeLists\FirstEditionSecondEditionTradeList.hlt")>
        Sub RefreshDataTest(ByVal TradeListFileName As String)

            ' Load the deck
            Dim tradeList As TradeList = HLT.Load(TradeListFileName)

            ' Generate data from the deck
            Dim ReportData As New TradeListData()

            ' Confirm data matches expected values when processing Haves and Wants
            ReportData.RefreshData(tradeList, "B")
            Assert.Equal(tradeList.Haves.Count, ReportData.Data.Tables("Card").Select("Immortal = 'Haves (" & tradeList.TotalHaves.ToString() & ")'").Count)
            Assert.Equal(tradeList.Wants.Count, ReportData.Data.Tables("Card").Select("Immortal = 'Wants (" & tradeList.TotalWants.ToString() & ")'").Count)

            ' Confirm data matches expected values when processing Haves
            ReportData.RefreshData(tradeList, "H")
            Assert.Equal(tradeList.Haves.Count, ReportData.Data.Tables("Card").Select("Immortal = 'Haves (" & tradeList.TotalHaves.ToString() & ")'").Count)
            Assert.Equal(0, ReportData.Data.Tables("Card").Select("Immortal = 'Wants (" & tradeList.TotalWants.ToString() & ")'").Count)

            ' Confirm data matches expected values when processing Wants
            ReportData.RefreshData(tradeList, "W")
            Assert.Equal(0, ReportData.Data.Tables("Card").Select("Immortal = 'Haves (" & tradeList.TotalHaves.ToString() & ")'").Count)
            Assert.Equal(tradeList.Wants.Count, ReportData.Data.Tables("Card").Select("Immortal = 'Wants (" & tradeList.TotalWants.ToString() & ")'").Count)
        End Sub

    End Class
End Namespace

