﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports System.Data
Imports Highlander.Reporting
Imports Xunit

Namespace Highlander.Reporting.Tests

    Public Class DeckListDataTests

        <Theory>
        <InlineData("TestData\Decks\Amanda1EOnlyDeckSimple.hld")>
        <InlineData("TestData\Decks\Amanda2EOnlyDeckWeaponGems.hld")>
        <InlineData("TestData\Decks\BasicTCGGenericImmortalDeck.hld")>
        Sub RefreshDataTest(ByVal DeckFileName As String)

            ' Load the deck
            Dim deck As HighlanderDeck = HLD.Load(DeckFileName)

            ' Generate data from the deck
            Dim ReportData As New DeckListData()
            ReportData.RefreshData(deck)

            ' Confirm data matches expected values
            Assert.Equal(deck.Deck.Count, ReportData.Data.Tables("Card").Select("Immortal = 'Deck (" & deck.DeckSize.ToString() & ")'").Count)
            Assert.Equal(deck.PreGame.Count, ReportData.Data.Tables("Card").Select("Immortal = 'Pre-Game (" & deck.PreGameSize.ToString() & ")'").Count)
        End Sub

    End Class
End Namespace

