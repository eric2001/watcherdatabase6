﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewGame))
        Me.txtPlayerName = New System.Windows.Forms.TextBox()
        Me.lblPlayerName = New System.Windows.Forms.Label()
        Me.txtOpponentName = New System.Windows.Forms.TextBox()
        Me.lblOpponentName = New System.Windows.Forms.Label()
        Me.RadioButtonYouPlayFirst = New System.Windows.Forms.RadioButton()
        Me.RadioButtonOpponentPlaysFirst = New System.Windows.Forms.RadioButton()
        Me.lblDeckPath = New System.Windows.Forms.Label()
        Me.txtDeckPath = New System.Windows.Forms.TextBox()
        Me.ButtonBrowse = New System.Windows.Forms.Button()
        Me.ButtonStartGame = New System.Windows.Forms.Button()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtPlayerName
        '
        Me.txtPlayerName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPlayerName.Location = New System.Drawing.Point(113, 6)
        Me.txtPlayerName.Name = "txtPlayerName"
        Me.txtPlayerName.Size = New System.Drawing.Size(246, 20)
        Me.txtPlayerName.TabIndex = 17
        '
        'lblPlayerName
        '
        Me.lblPlayerName.AutoSize = True
        Me.lblPlayerName.Location = New System.Drawing.Point(12, 9)
        Me.lblPlayerName.Name = "lblPlayerName"
        Me.lblPlayerName.Size = New System.Drawing.Size(63, 13)
        Me.lblPlayerName.TabIndex = 16
        Me.lblPlayerName.Text = "Your Name:"
        '
        'txtOpponentName
        '
        Me.txtOpponentName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOpponentName.Location = New System.Drawing.Point(113, 34)
        Me.txtOpponentName.Name = "txtOpponentName"
        Me.txtOpponentName.Size = New System.Drawing.Size(246, 20)
        Me.txtOpponentName.TabIndex = 15
        '
        'lblOpponentName
        '
        Me.lblOpponentName.AutoSize = True
        Me.lblOpponentName.Location = New System.Drawing.Point(12, 37)
        Me.lblOpponentName.Name = "lblOpponentName"
        Me.lblOpponentName.Size = New System.Drawing.Size(95, 13)
        Me.lblOpponentName.TabIndex = 14
        Me.lblOpponentName.Text = "Opponent's Name:"
        '
        'RadioButtonYouPlayFirst
        '
        Me.RadioButtonYouPlayFirst.AutoSize = True
        Me.RadioButtonYouPlayFirst.Location = New System.Drawing.Point(67, 92)
        Me.RadioButtonYouPlayFirst.Name = "RadioButtonYouPlayFirst"
        Me.RadioButtonYouPlayFirst.Size = New System.Drawing.Size(89, 17)
        Me.RadioButtonYouPlayFirst.TabIndex = 18
        Me.RadioButtonYouPlayFirst.TabStop = True
        Me.RadioButtonYouPlayFirst.Text = "You Play First"
        Me.RadioButtonYouPlayFirst.UseVisualStyleBackColor = True
        '
        'RadioButtonOpponentPlaysFirst
        '
        Me.RadioButtonOpponentPlaysFirst.AutoSize = True
        Me.RadioButtonOpponentPlaysFirst.Location = New System.Drawing.Point(179, 92)
        Me.RadioButtonOpponentPlaysFirst.Name = "RadioButtonOpponentPlaysFirst"
        Me.RadioButtonOpponentPlaysFirst.Size = New System.Drawing.Size(124, 17)
        Me.RadioButtonOpponentPlaysFirst.TabIndex = 19
        Me.RadioButtonOpponentPlaysFirst.TabStop = True
        Me.RadioButtonOpponentPlaysFirst.Text = "Opponent Play's First"
        Me.RadioButtonOpponentPlaysFirst.UseVisualStyleBackColor = True
        '
        'lblDeckPath
        '
        Me.lblDeckPath.AutoSize = True
        Me.lblDeckPath.Location = New System.Drawing.Point(12, 64)
        Me.lblDeckPath.Name = "lblDeckPath"
        Me.lblDeckPath.Size = New System.Drawing.Size(61, 13)
        Me.lblDeckPath.TabIndex = 20
        Me.lblDeckPath.Text = "Your Deck:"
        '
        'txtDeckPath
        '
        Me.txtDeckPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDeckPath.Location = New System.Drawing.Point(113, 61)
        Me.txtDeckPath.Name = "txtDeckPath"
        Me.txtDeckPath.Size = New System.Drawing.Size(170, 20)
        Me.txtDeckPath.TabIndex = 21
        '
        'ButtonBrowse
        '
        Me.ButtonBrowse.Location = New System.Drawing.Point(284, 59)
        Me.ButtonBrowse.Name = "ButtonBrowse"
        Me.ButtonBrowse.Size = New System.Drawing.Size(75, 23)
        Me.ButtonBrowse.TabIndex = 22
        Me.ButtonBrowse.Text = "Browse..."
        Me.ButtonBrowse.UseVisualStyleBackColor = True
        '
        'ButtonStartGame
        '
        Me.ButtonStartGame.Location = New System.Drawing.Point(261, 115)
        Me.ButtonStartGame.Name = "ButtonStartGame"
        Me.ButtonStartGame.Size = New System.Drawing.Size(75, 23)
        Me.ButtonStartGame.TabIndex = 23
        Me.ButtonStartGame.Text = "Start Game"
        Me.ButtonStartGame.UseVisualStyleBackColor = True
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(179, 115)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
        Me.ButtonCancel.TabIndex = 24
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'frmNewGame
        '
        Me.AcceptButton = Me.ButtonStartGame
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(371, 149)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.ButtonStartGame)
        Me.Controls.Add(Me.ButtonBrowse)
        Me.Controls.Add(Me.txtDeckPath)
        Me.Controls.Add(Me.lblDeckPath)
        Me.Controls.Add(Me.RadioButtonOpponentPlaysFirst)
        Me.Controls.Add(Me.RadioButtonYouPlayFirst)
        Me.Controls.Add(Me.txtPlayerName)
        Me.Controls.Add(Me.lblPlayerName)
        Me.Controls.Add(Me.txtOpponentName)
        Me.Controls.Add(Me.lblOpponentName)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmNewGame"
        Me.Text = "New Game..."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPlayerName As System.Windows.Forms.TextBox
    Friend WithEvents lblPlayerName As System.Windows.Forms.Label
    Friend WithEvents txtOpponentName As System.Windows.Forms.TextBox
    Friend WithEvents lblOpponentName As System.Windows.Forms.Label
    Friend WithEvents RadioButtonYouPlayFirst As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonOpponentPlaysFirst As System.Windows.Forms.RadioButton
    Friend WithEvents lblDeckPath As System.Windows.Forms.Label
    Friend WithEvents txtDeckPath As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBrowse As System.Windows.Forms.Button
    Friend WithEvents ButtonStartGame As System.Windows.Forms.Button
    Friend WithEvents ButtonCancel As System.Windows.Forms.Button
End Class
