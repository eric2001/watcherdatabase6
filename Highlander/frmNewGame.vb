﻿Public Class frmNewGame

    Private Sub ButtonStartGame_Click(sender As System.Object, e As System.EventArgs) Handles ButtonStartGame.Click
        If txtPlayerName.Text = "" Then
            MessageBox.Show("Please enter your name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If txtOpponentName.Text = "" Then
            MessageBox.Show("Please enter your opponent's name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If txtDeckPath.Text = "" Then
            MessageBox.Show("Please specify the deck you wish to play with.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If RadioButtonYouPlayFirst.Checked = False And RadioButtonOpponentPlaysFirst.Checked = False Then
            MessageBox.Show("Please determine who should go first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If RadioButtonYouPlayFirst.Checked = True And RadioButtonOpponentPlaysFirst.Checked = True Then
            MessageBox.Show("Please determine who should go first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub ButtonBrowse_Click(sender As System.Object, e As System.EventArgs) Handles ButtonBrowse.Click
        Dim fileDialog As New OpenFileDialog
        fileDialog.DefaultExt = "hld"
        fileDialog.Filter = "Highlander Decks (*.hld)|*.hld"
        If fileDialog.ShowDialog() = DialogResult.OK Then
            txtDeckPath.Text = fileDialog.FileName
        End If
    End Sub
End Class