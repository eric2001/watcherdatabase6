Public Class cardsImageList
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents panelImages As System.Windows.Forms.Panel
    Friend WithEvents listCardDetails As System.Windows.Forms.ListView
    Friend WithEvents columnImmortal As System.Windows.Forms.ColumnHeader
    Friend WithEvents columnTitle As System.Windows.Forms.ColumnHeader
    Friend WithEvents columnType As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.panelImages = New System.Windows.Forms.Panel
        Me.listCardDetails = New System.Windows.Forms.ListView
        Me.columnImmortal = New System.Windows.Forms.ColumnHeader
        Me.columnTitle = New System.Windows.Forms.ColumnHeader
        Me.columnType = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'panelImages
        '
        Me.panelImages.AutoScroll = True
        Me.panelImages.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelImages.Location = New System.Drawing.Point(0, 0)
        Me.panelImages.Name = "panelImages"
        Me.panelImages.Size = New System.Drawing.Size(360, 150)
        Me.panelImages.TabIndex = 0
        '
        'listCardDetails
        '
        Me.listCardDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.listCardDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnImmortal, Me.columnTitle, Me.columnType})
        Me.listCardDetails.Location = New System.Drawing.Point(0, 0)
        Me.listCardDetails.Name = "listCardDetails"
        Me.listCardDetails.Size = New System.Drawing.Size(360, 152)
        Me.listCardDetails.TabIndex = 2
        Me.listCardDetails.View = System.Windows.Forms.View.Details
        Me.listCardDetails.Visible = False
        '
        'columnImmortal
        '
        Me.columnImmortal.Text = "Immortal"
        Me.columnImmortal.Width = 129
        '
        'columnTitle
        '
        Me.columnTitle.Text = "Title"
        Me.columnTitle.Width = 112
        '
        'columnType
        '
        Me.columnType.Text = "Type"
        Me.columnType.Width = 112
        '
        'cardsImageList
        '
        Me.Controls.Add(Me.listCardDetails)
        Me.Controls.Add(Me.panelImages)
        Me.Name = "cardsImageList"
        Me.Size = New System.Drawing.Size(360, 150)
        Me.ResumeLayout(False)

    End Sub

#End Region

    'array(*, 1) = id, array(*, 2) = random number, array(*, 4) = Immortal, 
    '  array(*, 4) = Title, array(*, 5) = Type
    '* = record number
    Public cards(500, 5)

    Public Sub addCard(ByVal cardID As Integer, ByVal randomNum As Integer, ByVal cardImmortal As String, ByVal cardTitle As String, ByVal cardType As String, ByVal cardImage As String, ByVal cardText As String, ByVal cardContextMenu As ContextMenuStrip, ByVal AddressOfdefaultAction As System.EventHandler)
        Dim newCard As New PictureBox
        Dim point As New System.Drawing.Size
        Dim newCardCaption As New ToolTip

        Try
            ' Add the new card's image to the image pannel.
            newCard.Image = System.Drawing.Bitmap.FromFile(cardImage)
            newCard.SizeMode = PictureBoxSizeMode.StretchImage
            newCard.ContextMenuStrip = cardContextMenu
            point.Width = 73
            point.Height = 101
            newCard.Size = point
            newCardCaption.SetToolTip(newCard, cardText)
            newCard.Tag = cardID
            newCard.BorderStyle = BorderStyle.Fixed3D
            'AddHandler newCard.DoubleClick, AddressOf defaultAction
            AddHandler newCard.DoubleClick, AddressOfdefaultAction
            newCard.Parent = panelImages

            ' Add the details of the new card to the list view.
            Dim newCardItem As New ListViewItem
            newCardItem.Text = cardImmortal
            newCardItem.SubItems.Add(cardTitle)
            newCardItem.SubItems.Add(cardType)
            newCardItem.Tag = cardID
            listCardDetails.ContextMenuStrip = cardContextMenu
            listCardDetails.Items.Add(newCardItem)

            refreshImageList()

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub

    Public Sub deleteCard(ByVal cardID As Integer)
        ' Remove one copy of card cardID from this control.
        Dim counter As Integer = 0
        While counter < listCardDetails.Items.Count
            If listCardDetails.Items(counter).Tag = cardID Then

                panelImages.Controls(counter).Dispose()
                listCardDetails.Items(counter).Remove()
                Exit While
            End If
            counter = counter + 1
        End While
        refreshImageList()
    End Sub

    Public Sub toggleImageView()
        ' Toggle between Image View and List View.
        listCardDetails.Visible = (listCardDetails.Visible = False)
    End Sub

    Public Sub setImageView(ByVal view As Boolean)
        ' Manually switch between Image View and List View.
        If view = True Then
            listCardDetails.Visible = False
        Else
            listCardDetails.Visible = True
        End If
    End Sub

    Private Sub refreshImageList()
        ' Auto Align all card images within this control.
        Dim oneCard As Windows.Forms.Control
        Dim newpoint As System.Drawing.Point
        newpoint.X = 0
        newpoint.Y = 0

        ' Loop through each item in panelImages, adjust positions as needed.
        For Each oneCard In panelImages.Controls
            oneCard.Location = newpoint
            newpoint.X = newpoint.X + oneCard.Size.Width + 10
            If oneCard.Size.Width + newpoint.X > panelImages.Size.Width Then
                newpoint.X = 0
                newpoint.Y = newpoint.Y + oneCard.Size.Height + 10
            End If
        Next
    End Sub

    Private Sub panelImages_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles panelImages.Resize
        ' Update Image Positions when panel size is changed.
        refreshImageList()
    End Sub
End Class
