Imports System.Data

Public Class frmPlayGame
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents panelGameInfo As System.Windows.Forms.Panel
    Friend WithEvents PanelGame As System.Windows.Forms.Panel
    Friend WithEvents groupChat As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents groupOpponent As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter3 As System.Windows.Forms.Splitter
    Friend WithEvents groupYou As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter4 As System.Windows.Forms.Splitter
    Friend WithEvents panelYouTop As System.Windows.Forms.Panel
    Friend WithEvents panelYouBottom As System.Windows.Forms.Panel
    Friend WithEvents Splitter5 As System.Windows.Forms.Splitter
    Friend WithEvents tabYourGameCards As System.Windows.Forms.TabControl
    Friend WithEvents groupYourPreGame As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter6 As System.Windows.Forms.Splitter
    Friend WithEvents tabYourInPlay As System.Windows.Forms.TabPage
    Friend WithEvents tabYourDiscard As System.Windows.Forms.TabPage
    Friend WithEvents tabYourXedFromGame As System.Windows.Forms.TabPage
    Friend WithEvents tabYourDojo As System.Windows.Forms.TabPage
    Friend WithEvents menuMainPlayGame As System.Windows.Forms.MenuStrip
    Friend WithEvents groupYourHidden As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter7 As System.Windows.Forms.Splitter
    Friend WithEvents panelYourInPlayCards As System.Windows.Forms.Panel
    Friend WithEvents menuYourDeckOptions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents menuDeckDrawUpAbility As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuYouDraw1Card As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuYouShuffleEndurance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents groupYourDeck As System.Windows.Forms.GroupBox
    Friend WithEvents buttonYouMakeExertion As System.Windows.Forms.Button
    Friend WithEvents buttonShuffleDiscardToEndurance As System.Windows.Forms.Button
    Friend WithEvents pictureYourDeck As System.Windows.Forms.PictureBox
    Friend WithEvents Splitter8 As System.Windows.Forms.Splitter
    Friend WithEvents groupYourTopXCards As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter9 As System.Windows.Forms.Splitter
    Friend WithEvents panelYourHand As System.Windows.Forms.Panel
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents Splitter11 As System.Windows.Forms.Splitter
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabGameDetails As System.Windows.Forms.TabPage
    Friend WithEvents txtOppName As System.Windows.Forms.TextBox
    Friend WithEvents lblOppName As System.Windows.Forms.Label
    Friend WithEvents lblYourOpponent As System.Windows.Forms.Label
    Friend WithEvents lblYour As System.Windows.Forms.Label
    Friend WithEvents txtOpponentsAbility As System.Windows.Forms.TextBox
    Friend WithEvents lblOpponentsTurnCounter As System.Windows.Forms.Label
    Friend WithEvents lblOpponentAbility As System.Windows.Forms.Label
    Friend WithEvents lblOpponentTurn As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtYourAbility As System.Windows.Forms.TextBox
    Friend WithEvents lblYourTurnCounter As System.Windows.Forms.Label
    Friend WithEvents lblYourAbility As System.Windows.Forms.Label
    Friend WithEvents lblYourTurnNum As System.Windows.Forms.Label
    Friend WithEvents lblDivide As System.Windows.Forms.Label
    Friend WithEvents tabYourDeck As System.Windows.Forms.TabPage
    Friend WithEvents tabCardPicture As System.Windows.Forms.TabPage
    Friend WithEvents tabCardDetails As System.Windows.Forms.TabPage
    Friend WithEvents tabAllCardsList As System.Windows.Forms.TabPage
    Friend WithEvents lblCardName As System.Windows.Forms.Label
    Friend WithEvents lblRarity As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents lblOther As System.Windows.Forms.Label
    Friend WithEvents lblCardText As System.Windows.Forms.Label
    Friend WithEvents txtCardName As System.Windows.Forms.TextBox
    Friend WithEvents txtCardType As System.Windows.Forms.TextBox
    Friend WithEvents txtCardRarity As System.Windows.Forms.TextBox
    Friend WithEvents txtCardSet As System.Windows.Forms.TextBox
    Friend WithEvents txtCardOther As System.Windows.Forms.TextBox
    Friend WithEvents txtCardText As System.Windows.Forms.TextBox
    Friend WithEvents lstAllCards As System.Windows.Forms.ListBox
    Friend WithEvents txtCardFileName As System.Windows.Forms.TextBox
    Friend WithEvents pictureSelectedHLCard As System.Windows.Forms.PictureBox
    Friend WithEvents buttonSelectPhase As System.Windows.Forms.Button
    Friend WithEvents comboTurnPhase As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrPhase As System.Windows.Forms.Label
    Friend WithEvents VScrollBar1 As System.Windows.Forms.VScrollBar
    Friend WithEvents MenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents groupOppPreGame As System.Windows.Forms.GroupBox
    Friend WithEvents tabOppGameCards As System.Windows.Forms.TabControl
    Friend WithEvents tabOppInPlay As System.Windows.Forms.TabPage
    Friend WithEvents tabOppDiscard As System.Windows.Forms.TabPage
    Friend WithEvents tabOppXedFromGame As System.Windows.Forms.TabPage
    Friend WithEvents opponentInPlayCards As cardsImageList
    Friend WithEvents opponentDiscardCards As cardsImageList
    Friend WithEvents opponentXedFromGameCards As cardsImageList
    Friend WithEvents opponentPreGameCards As cardsImageList
    Friend WithEvents toolTipAllCardsPic As System.Windows.Forms.ToolTip
    Friend WithEvents oppInPlayOptions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents menuOppDiscardFromPlay As System.Windows.Forms.ToolStripMenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPlayGame))
        Me.panelGameInfo = New System.Windows.Forms.Panel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabGameDetails = New System.Windows.Forms.TabPage()
        Me.lblCurrPhase = New System.Windows.Forms.Label()
        Me.comboTurnPhase = New System.Windows.Forms.ComboBox()
        Me.txtOppName = New System.Windows.Forms.TextBox()
        Me.lblOppName = New System.Windows.Forms.Label()
        Me.lblYourOpponent = New System.Windows.Forms.Label()
        Me.lblYour = New System.Windows.Forms.Label()
        Me.txtOpponentsAbility = New System.Windows.Forms.TextBox()
        Me.lblOpponentsTurnCounter = New System.Windows.Forms.Label()
        Me.lblOpponentAbility = New System.Windows.Forms.Label()
        Me.lblOpponentTurn = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.buttonSelectPhase = New System.Windows.Forms.Button()
        Me.txtYourAbility = New System.Windows.Forms.TextBox()
        Me.lblYourTurnCounter = New System.Windows.Forms.Label()
        Me.lblYourAbility = New System.Windows.Forms.Label()
        Me.lblYourTurnNum = New System.Windows.Forms.Label()
        Me.lblDivide = New System.Windows.Forms.Label()
        Me.tabYourDeck = New System.Windows.Forms.TabPage()
        Me.Splitter11 = New System.Windows.Forms.Splitter()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.tabCardDetails = New System.Windows.Forms.TabPage()
        Me.txtCardText = New System.Windows.Forms.TextBox()
        Me.txtCardOther = New System.Windows.Forms.TextBox()
        Me.txtCardSet = New System.Windows.Forms.TextBox()
        Me.txtCardRarity = New System.Windows.Forms.TextBox()
        Me.txtCardType = New System.Windows.Forms.TextBox()
        Me.txtCardName = New System.Windows.Forms.TextBox()
        Me.lblCardText = New System.Windows.Forms.Label()
        Me.lblOther = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.lblRarity = New System.Windows.Forms.Label()
        Me.lblCardName = New System.Windows.Forms.Label()
        Me.tabAllCardsList = New System.Windows.Forms.TabPage()
        Me.txtCardFileName = New System.Windows.Forms.TextBox()
        Me.lstAllCards = New System.Windows.Forms.ListBox()
        Me.tabCardPicture = New System.Windows.Forms.TabPage()
        Me.pictureSelectedHLCard = New System.Windows.Forms.PictureBox()
        Me.menuYourDeckOptions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.menuDeckDrawUpAbility = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuYouDraw1Card = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuYouShuffleEndurance = New System.Windows.Forms.ToolStripMenuItem()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.PanelGame = New System.Windows.Forms.Panel()
        Me.groupYou = New System.Windows.Forms.GroupBox()
        Me.Splitter5 = New System.Windows.Forms.Splitter()
        Me.panelYouBottom = New System.Windows.Forms.Panel()
        Me.Splitter8 = New System.Windows.Forms.Splitter()
        Me.groupYourDeck = New System.Windows.Forms.GroupBox()
        Me.buttonYouMakeExertion = New System.Windows.Forms.Button()
        Me.buttonShuffleDiscardToEndurance = New System.Windows.Forms.Button()
        Me.pictureYourDeck = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.panelYourHand = New System.Windows.Forms.Panel()
        Me.VScrollBar1 = New System.Windows.Forms.VScrollBar()
        Me.Splitter9 = New System.Windows.Forms.Splitter()
        Me.groupYourTopXCards = New System.Windows.Forms.GroupBox()
        Me.panelYouTop = New System.Windows.Forms.Panel()
        Me.Splitter6 = New System.Windows.Forms.Splitter()
        Me.groupYourPreGame = New System.Windows.Forms.GroupBox()
        Me.tabYourGameCards = New System.Windows.Forms.TabControl()
        Me.tabYourInPlay = New System.Windows.Forms.TabPage()
        Me.panelYourInPlayCards = New System.Windows.Forms.Panel()
        Me.Splitter7 = New System.Windows.Forms.Splitter()
        Me.groupYourHidden = New System.Windows.Forms.GroupBox()
        Me.tabYourDiscard = New System.Windows.Forms.TabPage()
        Me.tabYourXedFromGame = New System.Windows.Forms.TabPage()
        Me.tabYourDojo = New System.Windows.Forms.TabPage()
        Me.Splitter3 = New System.Windows.Forms.Splitter()
        Me.groupOpponent = New System.Windows.Forms.GroupBox()
        Me.tabOppGameCards = New System.Windows.Forms.TabControl()
        Me.tabOppInPlay = New System.Windows.Forms.TabPage()
        Me.opponentInPlayCards = New Highlander.cardsImageList()
        Me.tabOppDiscard = New System.Windows.Forms.TabPage()
        Me.opponentDiscardCards = New Highlander.cardsImageList()
        Me.tabOppXedFromGame = New System.Windows.Forms.TabPage()
        Me.opponentXedFromGameCards = New Highlander.cardsImageList()
        Me.Splitter4 = New System.Windows.Forms.Splitter()
        Me.groupOppPreGame = New System.Windows.Forms.GroupBox()
        Me.opponentPreGameCards = New Highlander.cardsImageList()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.groupChat = New System.Windows.Forms.GroupBox()
        Me.menuMainPlayGame = New System.Windows.Forms.MenuStrip()
        Me.MenuItem13 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem14 = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolTipAllCardsPic = New System.Windows.Forms.ToolTip(Me.components)
        Me.oppInPlayOptions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.menuOppDiscardFromPlay = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelGameInfo.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabGameDetails.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.tabCardDetails.SuspendLayout()
        Me.tabAllCardsList.SuspendLayout()
        Me.tabCardPicture.SuspendLayout()
        CType(Me.pictureSelectedHLCard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.menuYourDeckOptions.SuspendLayout()
        Me.PanelGame.SuspendLayout()
        Me.groupYou.SuspendLayout()
        Me.panelYouBottom.SuspendLayout()
        Me.groupYourDeck.SuspendLayout()
        CType(Me.pictureYourDeck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.panelYourHand.SuspendLayout()
        Me.panelYouTop.SuspendLayout()
        Me.tabYourGameCards.SuspendLayout()
        Me.tabYourInPlay.SuspendLayout()
        Me.groupOpponent.SuspendLayout()
        Me.tabOppGameCards.SuspendLayout()
        Me.tabOppInPlay.SuspendLayout()
        Me.tabOppDiscard.SuspendLayout()
        Me.tabOppXedFromGame.SuspendLayout()
        Me.groupOppPreGame.SuspendLayout()
        Me.menuMainPlayGame.SuspendLayout()
        Me.oppInPlayOptions.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelGameInfo
        '
        Me.panelGameInfo.BackColor = System.Drawing.SystemColors.Control
        Me.panelGameInfo.Controls.Add(Me.TabControl1)
        Me.panelGameInfo.Controls.Add(Me.Splitter11)
        Me.panelGameInfo.Controls.Add(Me.TabControl2)
        Me.panelGameInfo.Dock = System.Windows.Forms.DockStyle.Left
        Me.panelGameInfo.Location = New System.Drawing.Point(0, 24)
        Me.panelGameInfo.Name = "panelGameInfo"
        Me.panelGameInfo.Size = New System.Drawing.Size(228, 532)
        Me.panelGameInfo.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabGameDetails)
        Me.TabControl1.Controls.Add(Me.tabYourDeck)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 358)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(228, 174)
        Me.TabControl1.TabIndex = 5
        '
        'tabGameDetails
        '
        Me.tabGameDetails.Controls.Add(Me.lblCurrPhase)
        Me.tabGameDetails.Controls.Add(Me.comboTurnPhase)
        Me.tabGameDetails.Controls.Add(Me.txtOppName)
        Me.tabGameDetails.Controls.Add(Me.lblOppName)
        Me.tabGameDetails.Controls.Add(Me.lblYourOpponent)
        Me.tabGameDetails.Controls.Add(Me.lblYour)
        Me.tabGameDetails.Controls.Add(Me.txtOpponentsAbility)
        Me.tabGameDetails.Controls.Add(Me.lblOpponentsTurnCounter)
        Me.tabGameDetails.Controls.Add(Me.lblOpponentAbility)
        Me.tabGameDetails.Controls.Add(Me.lblOpponentTurn)
        Me.tabGameDetails.Controls.Add(Me.Label1)
        Me.tabGameDetails.Controls.Add(Me.buttonSelectPhase)
        Me.tabGameDetails.Controls.Add(Me.txtYourAbility)
        Me.tabGameDetails.Controls.Add(Me.lblYourTurnCounter)
        Me.tabGameDetails.Controls.Add(Me.lblYourAbility)
        Me.tabGameDetails.Controls.Add(Me.lblYourTurnNum)
        Me.tabGameDetails.Controls.Add(Me.lblDivide)
        Me.tabGameDetails.Location = New System.Drawing.Point(4, 24)
        Me.tabGameDetails.Name = "tabGameDetails"
        Me.tabGameDetails.Size = New System.Drawing.Size(220, 146)
        Me.tabGameDetails.TabIndex = 0
        Me.tabGameDetails.Text = "Game Details"
        '
        'lblCurrPhase
        '
        Me.lblCurrPhase.AutoSize = True
        Me.lblCurrPhase.Location = New System.Drawing.Point(10, 10)
        Me.lblCurrPhase.Name = "lblCurrPhase"
        Me.lblCurrPhase.Size = New System.Drawing.Size(107, 15)
        Me.lblCurrPhase.TabIndex = 47
        Me.lblCurrPhase.Text = "Your Discard Phase"
        '
        'comboTurnPhase
        '
        Me.comboTurnPhase.Items.AddRange(New Object() {"Begin Turn", "Sweep Phase", "Defense Phase", "Attack Phase", "Ability Adjustment Phase", "Draw / Discard Phase", "End Turn"})
        Me.comboTurnPhase.Location = New System.Drawing.Point(10, 39)
        Me.comboTurnPhase.Name = "comboTurnPhase"
        Me.comboTurnPhase.Size = New System.Drawing.Size(115, 23)
        Me.comboTurnPhase.TabIndex = 46
        Me.comboTurnPhase.Text = "ComboBox1"
        '
        'txtOppName
        '
        Me.txtOppName.Location = New System.Drawing.Point(86, 246)
        Me.txtOppName.Name = "txtOppName"
        Me.txtOppName.Size = New System.Drawing.Size(125, 23)
        Me.txtOppName.TabIndex = 45
        '
        'lblOppName
        '
        Me.lblOppName.AutoSize = True
        Me.lblOppName.Location = New System.Drawing.Point(19, 246)
        Me.lblOppName.Name = "lblOppName"
        Me.lblOppName.Size = New System.Drawing.Size(42, 15)
        Me.lblOppName.TabIndex = 44
        Me.lblOppName.Text = "Name:"
        '
        'lblYourOpponent
        '
        Me.lblYourOpponent.AutoSize = True
        Me.lblYourOpponent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point)
        Me.lblYourOpponent.Location = New System.Drawing.Point(10, 167)
        Me.lblYourOpponent.Name = "lblYourOpponent"
        Me.lblYourOpponent.Size = New System.Drawing.Size(89, 13)
        Me.lblYourOpponent.TabIndex = 43
        Me.lblYourOpponent.Text = "Your Opponent's:"
        '
        'lblYour
        '
        Me.lblYour.AutoSize = True
        Me.lblYour.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point)
        Me.lblYour.Location = New System.Drawing.Point(10, 89)
        Me.lblYour.Name = "lblYour"
        Me.lblYour.Size = New System.Drawing.Size(32, 13)
        Me.lblYour.TabIndex = 42
        Me.lblYour.Text = "Your:"
        '
        'txtOpponentsAbility
        '
        Me.txtOpponentsAbility.Location = New System.Drawing.Point(86, 207)
        Me.txtOpponentsAbility.Name = "txtOpponentsAbility"
        Me.txtOpponentsAbility.Size = New System.Drawing.Size(68, 23)
        Me.txtOpponentsAbility.TabIndex = 41
        Me.txtOpponentsAbility.Text = "15"
        Me.txtOpponentsAbility.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblOpponentsTurnCounter
        '
        Me.lblOpponentsTurnCounter.AutoSize = True
        Me.lblOpponentsTurnCounter.Location = New System.Drawing.Point(86, 187)
        Me.lblOpponentsTurnCounter.Name = "lblOpponentsTurnCounter"
        Me.lblOpponentsTurnCounter.Size = New System.Drawing.Size(26, 15)
        Me.lblOpponentsTurnCounter.TabIndex = 40
        Me.lblOpponentsTurnCounter.Text = "10B"
        '
        'lblOpponentAbility
        '
        Me.lblOpponentAbility.AutoSize = True
        Me.lblOpponentAbility.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblOpponentAbility.Location = New System.Drawing.Point(19, 217)
        Me.lblOpponentAbility.Name = "lblOpponentAbility"
        Me.lblOpponentAbility.Size = New System.Drawing.Size(37, 13)
        Me.lblOpponentAbility.TabIndex = 39
        Me.lblOpponentAbility.Text = "Ability:"
        '
        'lblOpponentTurn
        '
        Me.lblOpponentTurn.AutoSize = True
        Me.lblOpponentTurn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblOpponentTurn.Location = New System.Drawing.Point(19, 187)
        Me.lblOpponentTurn.Name = "lblOpponentTurn"
        Me.lblOpponentTurn.Size = New System.Drawing.Size(42, 13)
        Me.lblOpponentTurn.TabIndex = 38
        Me.lblOpponentTurn.Text = "Turn #:"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(10, 158)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(199, 2)
        Me.Label1.TabIndex = 37
        '
        'buttonSelectPhase
        '
        Me.buttonSelectPhase.Location = New System.Drawing.Point(125, 39)
        Me.buttonSelectPhase.Name = "buttonSelectPhase"
        Me.buttonSelectPhase.Size = New System.Drawing.Size(86, 30)
        Me.buttonSelectPhase.TabIndex = 36
        Me.buttonSelectPhase.Text = "Select"
        '
        'txtYourAbility
        '
        Me.txtYourAbility.Location = New System.Drawing.Point(86, 128)
        Me.txtYourAbility.Name = "txtYourAbility"
        Me.txtYourAbility.Size = New System.Drawing.Size(68, 23)
        Me.txtYourAbility.TabIndex = 34
        Me.txtYourAbility.Text = "15"
        Me.txtYourAbility.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblYourTurnCounter
        '
        Me.lblYourTurnCounter.AutoSize = True
        Me.lblYourTurnCounter.Location = New System.Drawing.Point(86, 108)
        Me.lblYourTurnCounter.Name = "lblYourTurnCounter"
        Me.lblYourTurnCounter.Size = New System.Drawing.Size(27, 15)
        Me.lblYourTurnCounter.TabIndex = 33
        Me.lblYourTurnCounter.Text = "10A"
        '
        'lblYourAbility
        '
        Me.lblYourAbility.AutoSize = True
        Me.lblYourAbility.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblYourAbility.Location = New System.Drawing.Point(19, 138)
        Me.lblYourAbility.Name = "lblYourAbility"
        Me.lblYourAbility.Size = New System.Drawing.Size(37, 13)
        Me.lblYourAbility.TabIndex = 32
        Me.lblYourAbility.Text = "Ability:"
        '
        'lblYourTurnNum
        '
        Me.lblYourTurnNum.AutoSize = True
        Me.lblYourTurnNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblYourTurnNum.Location = New System.Drawing.Point(19, 108)
        Me.lblYourTurnNum.Name = "lblYourTurnNum"
        Me.lblYourTurnNum.Size = New System.Drawing.Size(42, 13)
        Me.lblYourTurnNum.TabIndex = 31
        Me.lblYourTurnNum.Text = "Turn #:"
        '
        'lblDivide
        '
        Me.lblDivide.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDivide.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDivide.Location = New System.Drawing.Point(10, 79)
        Me.lblDivide.Name = "lblDivide"
        Me.lblDivide.Size = New System.Drawing.Size(199, 2)
        Me.lblDivide.TabIndex = 30
        '
        'tabYourDeck
        '
        Me.tabYourDeck.Location = New System.Drawing.Point(4, 24)
        Me.tabYourDeck.Name = "tabYourDeck"
        Me.tabYourDeck.Size = New System.Drawing.Size(220, 269)
        Me.tabYourDeck.TabIndex = 1
        Me.tabYourDeck.Text = "Deck"
        Me.tabYourDeck.Visible = False
        '
        'Splitter11
        '
        Me.Splitter11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter11.Location = New System.Drawing.Point(0, 354)
        Me.Splitter11.Name = "Splitter11"
        Me.Splitter11.Size = New System.Drawing.Size(228, 4)
        Me.Splitter11.TabIndex = 4
        Me.Splitter11.TabStop = False
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.tabCardDetails)
        Me.TabControl2.Controls.Add(Me.tabAllCardsList)
        Me.TabControl2.Controls.Add(Me.tabCardPicture)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.TabControl2.Location = New System.Drawing.Point(0, 0)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(228, 354)
        Me.TabControl2.TabIndex = 3
        '
        'tabCardDetails
        '
        Me.tabCardDetails.AutoScroll = True
        Me.tabCardDetails.Controls.Add(Me.txtCardText)
        Me.tabCardDetails.Controls.Add(Me.txtCardOther)
        Me.tabCardDetails.Controls.Add(Me.txtCardSet)
        Me.tabCardDetails.Controls.Add(Me.txtCardRarity)
        Me.tabCardDetails.Controls.Add(Me.txtCardType)
        Me.tabCardDetails.Controls.Add(Me.txtCardName)
        Me.tabCardDetails.Controls.Add(Me.lblCardText)
        Me.tabCardDetails.Controls.Add(Me.lblOther)
        Me.tabCardDetails.Controls.Add(Me.lblSet)
        Me.tabCardDetails.Controls.Add(Me.lblType)
        Me.tabCardDetails.Controls.Add(Me.lblRarity)
        Me.tabCardDetails.Controls.Add(Me.lblCardName)
        Me.tabCardDetails.Location = New System.Drawing.Point(4, 24)
        Me.tabCardDetails.Name = "tabCardDetails"
        Me.tabCardDetails.Size = New System.Drawing.Size(220, 326)
        Me.tabCardDetails.TabIndex = 1
        Me.tabCardDetails.Text = "Details"
        '
        'txtCardText
        '
        Me.txtCardText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardText.Location = New System.Drawing.Point(67, 158)
        Me.txtCardText.Multiline = True
        Me.txtCardText.Name = "txtCardText"
        Me.txtCardText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCardText.Size = New System.Drawing.Size(145, 162)
        Me.txtCardText.TabIndex = 33
        Me.txtCardText.Text = "TextBox1"
        '
        'txtCardOther
        '
        Me.txtCardOther.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardOther.Location = New System.Drawing.Point(67, 128)
        Me.txtCardOther.Name = "txtCardOther"
        Me.txtCardOther.Size = New System.Drawing.Size(145, 23)
        Me.txtCardOther.TabIndex = 32
        Me.txtCardOther.Text = "TextBox1"
        '
        'txtCardSet
        '
        Me.txtCardSet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardSet.Location = New System.Drawing.Point(67, 98)
        Me.txtCardSet.Name = "txtCardSet"
        Me.txtCardSet.Size = New System.Drawing.Size(145, 23)
        Me.txtCardSet.TabIndex = 31
        Me.txtCardSet.Text = "TextBox1"
        '
        'txtCardRarity
        '
        Me.txtCardRarity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardRarity.Location = New System.Drawing.Point(67, 69)
        Me.txtCardRarity.Name = "txtCardRarity"
        Me.txtCardRarity.Size = New System.Drawing.Size(145, 23)
        Me.txtCardRarity.TabIndex = 30
        Me.txtCardRarity.Text = "TextBox1"
        '
        'txtCardType
        '
        Me.txtCardType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardType.Location = New System.Drawing.Point(67, 39)
        Me.txtCardType.Name = "txtCardType"
        Me.txtCardType.Size = New System.Drawing.Size(145, 23)
        Me.txtCardType.TabIndex = 29
        Me.txtCardType.Text = "TextBox1"
        '
        'txtCardName
        '
        Me.txtCardName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardName.Location = New System.Drawing.Point(67, 10)
        Me.txtCardName.Name = "txtCardName"
        Me.txtCardName.Size = New System.Drawing.Size(145, 23)
        Me.txtCardName.TabIndex = 28
        Me.txtCardName.Text = "TextBox1"
        '
        'lblCardText
        '
        Me.lblCardText.AutoSize = True
        Me.lblCardText.Location = New System.Drawing.Point(10, 167)
        Me.lblCardText.Name = "lblCardText"
        Me.lblCardText.Size = New System.Drawing.Size(31, 15)
        Me.lblCardText.TabIndex = 27
        Me.lblCardText.Text = "Text:"
        '
        'lblOther
        '
        Me.lblOther.AutoSize = True
        Me.lblOther.Location = New System.Drawing.Point(10, 138)
        Me.lblOther.Name = "lblOther"
        Me.lblOther.Size = New System.Drawing.Size(40, 15)
        Me.lblOther.TabIndex = 26
        Me.lblOther.Text = "Other:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Location = New System.Drawing.Point(10, 108)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(26, 15)
        Me.lblSet.TabIndex = 25
        Me.lblSet.Text = "Set:"
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Location = New System.Drawing.Point(10, 49)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(35, 15)
        Me.lblType.TabIndex = 24
        Me.lblType.Text = "Type:"
        '
        'lblRarity
        '
        Me.lblRarity.AutoSize = True
        Me.lblRarity.Location = New System.Drawing.Point(10, 79)
        Me.lblRarity.Name = "lblRarity"
        Me.lblRarity.Size = New System.Drawing.Size(40, 15)
        Me.lblRarity.TabIndex = 23
        Me.lblRarity.Text = "Rarity:"
        '
        'lblCardName
        '
        Me.lblCardName.AutoSize = True
        Me.lblCardName.Location = New System.Drawing.Point(10, 20)
        Me.lblCardName.Name = "lblCardName"
        Me.lblCardName.Size = New System.Drawing.Size(42, 15)
        Me.lblCardName.TabIndex = 22
        Me.lblCardName.Text = "Name:"
        '
        'tabAllCardsList
        '
        Me.tabAllCardsList.Controls.Add(Me.txtCardFileName)
        Me.tabAllCardsList.Controls.Add(Me.lstAllCards)
        Me.tabAllCardsList.Location = New System.Drawing.Point(4, 24)
        Me.tabAllCardsList.Name = "tabAllCardsList"
        Me.tabAllCardsList.Size = New System.Drawing.Size(220, 326)
        Me.tabAllCardsList.TabIndex = 2
        Me.tabAllCardsList.Text = "All Cards"
        '
        'txtCardFileName
        '
        Me.txtCardFileName.Location = New System.Drawing.Point(86, 79)
        Me.txtCardFileName.Name = "txtCardFileName"
        Me.txtCardFileName.Size = New System.Drawing.Size(120, 23)
        Me.txtCardFileName.TabIndex = 1
        Me.txtCardFileName.Visible = False
        '
        'lstAllCards
        '
        Me.lstAllCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstAllCards.ItemHeight = 15
        Me.lstAllCards.Location = New System.Drawing.Point(0, 0)
        Me.lstAllCards.Name = "lstAllCards"
        Me.lstAllCards.Size = New System.Drawing.Size(220, 326)
        Me.lstAllCards.TabIndex = 0
        '
        'tabCardPicture
        '
        Me.tabCardPicture.Controls.Add(Me.pictureSelectedHLCard)
        Me.tabCardPicture.Location = New System.Drawing.Point(4, 24)
        Me.tabCardPicture.Name = "tabCardPicture"
        Me.tabCardPicture.Size = New System.Drawing.Size(220, 326)
        Me.tabCardPicture.TabIndex = 0
        Me.tabCardPicture.Text = "Picture"
        '
        'pictureSelectedHLCard
        '
        Me.pictureSelectedHLCard.Image = CType(resources.GetObject("pictureSelectedHLCard.Image"), System.Drawing.Image)
        Me.pictureSelectedHLCard.Location = New System.Drawing.Point(38, 10)
        Me.pictureSelectedHLCard.Name = "pictureSelectedHLCard"
        Me.pictureSelectedHLCard.Size = New System.Drawing.Size(131, 186)
        Me.pictureSelectedHLCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureSelectedHLCard.TabIndex = 20
        Me.pictureSelectedHLCard.TabStop = False
        Me.pictureSelectedHLCard.Tag = ""
        '
        'menuYourDeckOptions
        '
        Me.menuYourDeckOptions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuDeckDrawUpAbility, Me.menuYouDraw1Card, Me.menuYouShuffleEndurance})
        Me.menuYourDeckOptions.Name = "menuYourDeckOptions"
        Me.menuYourDeckOptions.Size = New System.Drawing.Size(173, 70)
        '
        'menuDeckDrawUpAbility
        '
        Me.menuDeckDrawUpAbility.Name = "menuDeckDrawUpAbility"
        Me.menuDeckDrawUpAbility.Size = New System.Drawing.Size(172, 22)
        Me.menuDeckDrawUpAbility.Text = "Draw Up To Ability"
        '
        'menuYouDraw1Card
        '
        Me.menuYouDraw1Card.Name = "menuYouDraw1Card"
        Me.menuYouDraw1Card.Size = New System.Drawing.Size(172, 22)
        Me.menuYouDraw1Card.Text = "Draw 1 Card"
        '
        'menuYouShuffleEndurance
        '
        Me.menuYouShuffleEndurance.Name = "menuYouShuffleEndurance"
        Me.menuYouShuffleEndurance.Size = New System.Drawing.Size(172, 22)
        Me.menuYouShuffleEndurance.Text = "Shuffle Endurance"
        '
        'Splitter1
        '
        Me.Splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter1.Location = New System.Drawing.Point(228, 24)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(4, 532)
        Me.Splitter1.TabIndex = 1
        Me.Splitter1.TabStop = False
        '
        'PanelGame
        '
        Me.PanelGame.Controls.Add(Me.groupYou)
        Me.PanelGame.Controls.Add(Me.Splitter3)
        Me.PanelGame.Controls.Add(Me.groupOpponent)
        Me.PanelGame.Controls.Add(Me.Splitter2)
        Me.PanelGame.Controls.Add(Me.groupChat)
        Me.PanelGame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelGame.Location = New System.Drawing.Point(232, 24)
        Me.PanelGame.Name = "PanelGame"
        Me.PanelGame.Size = New System.Drawing.Size(560, 532)
        Me.PanelGame.TabIndex = 2
        '
        'groupYou
        '
        Me.groupYou.Controls.Add(Me.Splitter5)
        Me.groupYou.Controls.Add(Me.panelYouBottom)
        Me.groupYou.Controls.Add(Me.panelYouTop)
        Me.groupYou.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupYou.Location = New System.Drawing.Point(0, 240)
        Me.groupYou.Name = "groupYou"
        Me.groupYou.Size = New System.Drawing.Size(560, 219)
        Me.groupYou.TabIndex = 4
        Me.groupYou.TabStop = False
        Me.groupYou.Text = "Your Cards"
        '
        'Splitter5
        '
        Me.Splitter5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Splitter5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter5.Location = New System.Drawing.Point(3, 167)
        Me.Splitter5.Name = "Splitter5"
        Me.Splitter5.Size = New System.Drawing.Size(554, 4)
        Me.Splitter5.TabIndex = 2
        Me.Splitter5.TabStop = False
        '
        'panelYouBottom
        '
        Me.panelYouBottom.Controls.Add(Me.Splitter8)
        Me.panelYouBottom.Controls.Add(Me.groupYourDeck)
        Me.panelYouBottom.Controls.Add(Me.Panel1)
        Me.panelYouBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelYouBottom.Location = New System.Drawing.Point(3, 167)
        Me.panelYouBottom.Name = "panelYouBottom"
        Me.panelYouBottom.Size = New System.Drawing.Size(554, 49)
        Me.panelYouBottom.TabIndex = 1
        '
        'Splitter8
        '
        Me.Splitter8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter8.Dock = System.Windows.Forms.DockStyle.Right
        Me.Splitter8.Location = New System.Drawing.Point(339, 0)
        Me.Splitter8.Name = "Splitter8"
        Me.Splitter8.Size = New System.Drawing.Size(3, 49)
        Me.Splitter8.TabIndex = 2
        Me.Splitter8.TabStop = False
        '
        'groupYourDeck
        '
        Me.groupYourDeck.Controls.Add(Me.buttonYouMakeExertion)
        Me.groupYourDeck.Controls.Add(Me.buttonShuffleDiscardToEndurance)
        Me.groupYourDeck.Controls.Add(Me.pictureYourDeck)
        Me.groupYourDeck.Dock = System.Windows.Forms.DockStyle.Right
        Me.groupYourDeck.Location = New System.Drawing.Point(342, 0)
        Me.groupYourDeck.Name = "groupYourDeck"
        Me.groupYourDeck.Size = New System.Drawing.Size(212, 49)
        Me.groupYourDeck.TabIndex = 1
        Me.groupYourDeck.TabStop = False
        Me.groupYourDeck.Text = "Your Deck (60 Cards)"
        '
        'buttonYouMakeExertion
        '
        Me.buttonYouMakeExertion.Location = New System.Drawing.Point(114, 107)
        Me.buttonYouMakeExertion.Name = "buttonYouMakeExertion"
        Me.buttonYouMakeExertion.Size = New System.Drawing.Size(90, 49)
        Me.buttonYouMakeExertion.TabIndex = 21
        Me.buttonYouMakeExertion.Text = "Make an Exertion"
        '
        'buttonShuffleDiscardToEndurance
        '
        Me.buttonShuffleDiscardToEndurance.Location = New System.Drawing.Point(114, 38)
        Me.buttonShuffleDiscardToEndurance.Name = "buttonShuffleDiscardToEndurance"
        Me.buttonShuffleDiscardToEndurance.Size = New System.Drawing.Size(90, 59)
        Me.buttonShuffleDiscardToEndurance.TabIndex = 20
        Me.buttonShuffleDiscardToEndurance.Text = "Shuffle Discard Into Endurance"
        '
        'pictureYourDeck
        '
        Me.pictureYourDeck.ContextMenuStrip = Me.menuYourDeckOptions
        Me.pictureYourDeck.Image = CType(resources.GetObject("pictureYourDeck.Image"), System.Drawing.Image)
        Me.pictureYourDeck.Location = New System.Drawing.Point(8, 38)
        Me.pictureYourDeck.Name = "pictureYourDeck"
        Me.pictureYourDeck.Size = New System.Drawing.Size(88, 124)
        Me.pictureYourDeck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureYourDeck.TabIndex = 19
        Me.pictureYourDeck.TabStop = False
        Me.pictureYourDeck.Tag = ""
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.panelYourHand)
        Me.Panel1.Controls.Add(Me.Splitter9)
        Me.Panel1.Controls.Add(Me.groupYourTopXCards)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(554, 49)
        Me.Panel1.TabIndex = 0
        '
        'panelYourHand
        '
        Me.panelYourHand.AutoScroll = True
        Me.panelYourHand.Controls.Add(Me.VScrollBar1)
        Me.panelYourHand.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelYourHand.Location = New System.Drawing.Point(167, 0)
        Me.panelYourHand.Name = "panelYourHand"
        Me.panelYourHand.Size = New System.Drawing.Size(387, 49)
        Me.panelYourHand.TabIndex = 2
        '
        'VScrollBar1
        '
        Me.VScrollBar1.Location = New System.Drawing.Point(0, 0)
        Me.VScrollBar1.Name = "VScrollBar1"
        Me.VScrollBar1.Size = New System.Drawing.Size(20, 201)
        Me.VScrollBar1.TabIndex = 0
        '
        'Splitter9
        '
        Me.Splitter9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter9.Location = New System.Drawing.Point(163, 0)
        Me.Splitter9.Name = "Splitter9"
        Me.Splitter9.Size = New System.Drawing.Size(4, 49)
        Me.Splitter9.TabIndex = 1
        Me.Splitter9.TabStop = False
        '
        'groupYourTopXCards
        '
        Me.groupYourTopXCards.Dock = System.Windows.Forms.DockStyle.Left
        Me.groupYourTopXCards.Location = New System.Drawing.Point(0, 0)
        Me.groupYourTopXCards.Name = "groupYourTopXCards"
        Me.groupYourTopXCards.Size = New System.Drawing.Size(163, 49)
        Me.groupYourTopXCards.TabIndex = 0
        Me.groupYourTopXCards.TabStop = False
        Me.groupYourTopXCards.Text = "Top X Cards"
        Me.groupYourTopXCards.Visible = False
        '
        'panelYouTop
        '
        Me.panelYouTop.Controls.Add(Me.Splitter6)
        Me.panelYouTop.Controls.Add(Me.groupYourPreGame)
        Me.panelYouTop.Controls.Add(Me.tabYourGameCards)
        Me.panelYouTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelYouTop.Location = New System.Drawing.Point(3, 19)
        Me.panelYouTop.Name = "panelYouTop"
        Me.panelYouTop.Size = New System.Drawing.Size(554, 148)
        Me.panelYouTop.TabIndex = 0
        '
        'Splitter6
        '
        Me.Splitter6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter6.Dock = System.Windows.Forms.DockStyle.Right
        Me.Splitter6.Location = New System.Drawing.Point(339, 0)
        Me.Splitter6.Name = "Splitter6"
        Me.Splitter6.Size = New System.Drawing.Size(3, 148)
        Me.Splitter6.TabIndex = 2
        Me.Splitter6.TabStop = False
        '
        'groupYourPreGame
        '
        Me.groupYourPreGame.Dock = System.Windows.Forms.DockStyle.Right
        Me.groupYourPreGame.Location = New System.Drawing.Point(342, 0)
        Me.groupYourPreGame.Name = "groupYourPreGame"
        Me.groupYourPreGame.Size = New System.Drawing.Size(212, 148)
        Me.groupYourPreGame.TabIndex = 1
        Me.groupYourPreGame.TabStop = False
        Me.groupYourPreGame.Text = "Pre-Game"
        '
        'tabYourGameCards
        '
        Me.tabYourGameCards.Controls.Add(Me.tabYourInPlay)
        Me.tabYourGameCards.Controls.Add(Me.tabYourDiscard)
        Me.tabYourGameCards.Controls.Add(Me.tabYourXedFromGame)
        Me.tabYourGameCards.Controls.Add(Me.tabYourDojo)
        Me.tabYourGameCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabYourGameCards.Location = New System.Drawing.Point(0, 0)
        Me.tabYourGameCards.Name = "tabYourGameCards"
        Me.tabYourGameCards.SelectedIndex = 0
        Me.tabYourGameCards.Size = New System.Drawing.Size(554, 148)
        Me.tabYourGameCards.TabIndex = 0
        '
        'tabYourInPlay
        '
        Me.tabYourInPlay.Controls.Add(Me.panelYourInPlayCards)
        Me.tabYourInPlay.Controls.Add(Me.Splitter7)
        Me.tabYourInPlay.Controls.Add(Me.groupYourHidden)
        Me.tabYourInPlay.Location = New System.Drawing.Point(4, 24)
        Me.tabYourInPlay.Name = "tabYourInPlay"
        Me.tabYourInPlay.Size = New System.Drawing.Size(546, 120)
        Me.tabYourInPlay.TabIndex = 0
        Me.tabYourInPlay.Text = "In Play"
        '
        'panelYourInPlayCards
        '
        Me.panelYourInPlayCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelYourInPlayCards.Location = New System.Drawing.Point(157, 0)
        Me.panelYourInPlayCards.Name = "panelYourInPlayCards"
        Me.panelYourInPlayCards.Size = New System.Drawing.Size(389, 120)
        Me.panelYourInPlayCards.TabIndex = 2
        '
        'Splitter7
        '
        Me.Splitter7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter7.Location = New System.Drawing.Point(154, 0)
        Me.Splitter7.Name = "Splitter7"
        Me.Splitter7.Size = New System.Drawing.Size(3, 120)
        Me.Splitter7.TabIndex = 1
        Me.Splitter7.TabStop = False
        '
        'groupYourHidden
        '
        Me.groupYourHidden.Dock = System.Windows.Forms.DockStyle.Left
        Me.groupYourHidden.Location = New System.Drawing.Point(0, 0)
        Me.groupYourHidden.Name = "groupYourHidden"
        Me.groupYourHidden.Size = New System.Drawing.Size(154, 120)
        Me.groupYourHidden.TabIndex = 0
        Me.groupYourHidden.TabStop = False
        Me.groupYourHidden.Text = "Hidden Cards"
        Me.groupYourHidden.Visible = False
        '
        'tabYourDiscard
        '
        Me.tabYourDiscard.Location = New System.Drawing.Point(4, 24)
        Me.tabYourDiscard.Name = "tabYourDiscard"
        Me.tabYourDiscard.Size = New System.Drawing.Size(704, 120)
        Me.tabYourDiscard.TabIndex = 1
        Me.tabYourDiscard.Text = "Discard"
        '
        'tabYourXedFromGame
        '
        Me.tabYourXedFromGame.Location = New System.Drawing.Point(4, 24)
        Me.tabYourXedFromGame.Name = "tabYourXedFromGame"
        Me.tabYourXedFromGame.Size = New System.Drawing.Size(704, 120)
        Me.tabYourXedFromGame.TabIndex = 2
        Me.tabYourXedFromGame.Text = "X-ed From Game"
        '
        'tabYourDojo
        '
        Me.tabYourDojo.Location = New System.Drawing.Point(4, 24)
        Me.tabYourDojo.Name = "tabYourDojo"
        Me.tabYourDojo.Size = New System.Drawing.Size(704, 120)
        Me.tabYourDojo.TabIndex = 3
        Me.tabYourDojo.Text = "Dojo"
        '
        'Splitter3
        '
        Me.Splitter3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter3.Location = New System.Drawing.Point(0, 236)
        Me.Splitter3.Name = "Splitter3"
        Me.Splitter3.Size = New System.Drawing.Size(560, 4)
        Me.Splitter3.TabIndex = 3
        Me.Splitter3.TabStop = False
        '
        'groupOpponent
        '
        Me.groupOpponent.Controls.Add(Me.tabOppGameCards)
        Me.groupOpponent.Controls.Add(Me.Splitter4)
        Me.groupOpponent.Controls.Add(Me.groupOppPreGame)
        Me.groupOpponent.Dock = System.Windows.Forms.DockStyle.Top
        Me.groupOpponent.Location = New System.Drawing.Point(0, 0)
        Me.groupOpponent.Name = "groupOpponent"
        Me.groupOpponent.Size = New System.Drawing.Size(560, 236)
        Me.groupOpponent.TabIndex = 2
        Me.groupOpponent.TabStop = False
        Me.groupOpponent.Text = "Opponent"
        '
        'tabOppGameCards
        '
        Me.tabOppGameCards.Controls.Add(Me.tabOppInPlay)
        Me.tabOppGameCards.Controls.Add(Me.tabOppDiscard)
        Me.tabOppGameCards.Controls.Add(Me.tabOppXedFromGame)
        Me.tabOppGameCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabOppGameCards.Location = New System.Drawing.Point(3, 19)
        Me.tabOppGameCards.Name = "tabOppGameCards"
        Me.tabOppGameCards.SelectedIndex = 0
        Me.tabOppGameCards.Size = New System.Drawing.Size(359, 214)
        Me.tabOppGameCards.TabIndex = 3
        '
        'tabOppInPlay
        '
        Me.tabOppInPlay.AutoScroll = True
        Me.tabOppInPlay.Controls.Add(Me.opponentInPlayCards)
        Me.tabOppInPlay.Location = New System.Drawing.Point(4, 24)
        Me.tabOppInPlay.Name = "tabOppInPlay"
        Me.tabOppInPlay.Size = New System.Drawing.Size(351, 186)
        Me.tabOppInPlay.TabIndex = 0
        Me.tabOppInPlay.Text = "In Play"
        '
        'opponentInPlayCards
        '
        Me.opponentInPlayCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opponentInPlayCards.Location = New System.Drawing.Point(0, 0)
        Me.opponentInPlayCards.Name = "opponentInPlayCards"
        Me.opponentInPlayCards.Size = New System.Drawing.Size(351, 186)
        Me.opponentInPlayCards.TabIndex = 0
        '
        'tabOppDiscard
        '
        Me.tabOppDiscard.Controls.Add(Me.opponentDiscardCards)
        Me.tabOppDiscard.Location = New System.Drawing.Point(4, 24)
        Me.tabOppDiscard.Name = "tabOppDiscard"
        Me.tabOppDiscard.Size = New System.Drawing.Size(508, 182)
        Me.tabOppDiscard.TabIndex = 1
        Me.tabOppDiscard.Text = "Discard"
        Me.tabOppDiscard.Visible = False
        '
        'opponentDiscardCards
        '
        Me.opponentDiscardCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opponentDiscardCards.Location = New System.Drawing.Point(0, 0)
        Me.opponentDiscardCards.Name = "opponentDiscardCards"
        Me.opponentDiscardCards.Size = New System.Drawing.Size(508, 182)
        Me.opponentDiscardCards.TabIndex = 0
        '
        'tabOppXedFromGame
        '
        Me.tabOppXedFromGame.Controls.Add(Me.opponentXedFromGameCards)
        Me.tabOppXedFromGame.Location = New System.Drawing.Point(4, 24)
        Me.tabOppXedFromGame.Name = "tabOppXedFromGame"
        Me.tabOppXedFromGame.Size = New System.Drawing.Size(508, 182)
        Me.tabOppXedFromGame.TabIndex = 2
        Me.tabOppXedFromGame.Text = "X-ed From Game"
        Me.tabOppXedFromGame.Visible = False
        '
        'opponentXedFromGameCards
        '
        Me.opponentXedFromGameCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opponentXedFromGameCards.Location = New System.Drawing.Point(0, 0)
        Me.opponentXedFromGameCards.Name = "opponentXedFromGameCards"
        Me.opponentXedFromGameCards.Size = New System.Drawing.Size(508, 182)
        Me.opponentXedFromGameCards.TabIndex = 0
        '
        'Splitter4
        '
        Me.Splitter4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Splitter4.Location = New System.Drawing.Point(362, 19)
        Me.Splitter4.Name = "Splitter4"
        Me.Splitter4.Size = New System.Drawing.Size(3, 214)
        Me.Splitter4.TabIndex = 2
        Me.Splitter4.TabStop = False
        '
        'groupOppPreGame
        '
        Me.groupOppPreGame.Controls.Add(Me.opponentPreGameCards)
        Me.groupOppPreGame.Dock = System.Windows.Forms.DockStyle.Right
        Me.groupOppPreGame.Location = New System.Drawing.Point(365, 19)
        Me.groupOppPreGame.Name = "groupOppPreGame"
        Me.groupOppPreGame.Size = New System.Drawing.Size(192, 214)
        Me.groupOppPreGame.TabIndex = 1
        Me.groupOppPreGame.TabStop = False
        Me.groupOppPreGame.Text = "Pre-Game"
        '
        'opponentPreGameCards
        '
        Me.opponentPreGameCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opponentPreGameCards.Location = New System.Drawing.Point(3, 19)
        Me.opponentPreGameCards.Name = "opponentPreGameCards"
        Me.opponentPreGameCards.Size = New System.Drawing.Size(186, 192)
        Me.opponentPreGameCards.TabIndex = 0
        '
        'Splitter2
        '
        Me.Splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Splitter2.Location = New System.Drawing.Point(0, 459)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(560, 4)
        Me.Splitter2.TabIndex = 1
        Me.Splitter2.TabStop = False
        '
        'groupChat
        '
        Me.groupChat.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.groupChat.Location = New System.Drawing.Point(0, 463)
        Me.groupChat.Name = "groupChat"
        Me.groupChat.Size = New System.Drawing.Size(560, 69)
        Me.groupChat.TabIndex = 0
        Me.groupChat.TabStop = False
        Me.groupChat.Text = "Chat"
        Me.groupChat.Visible = False
        '
        'menuMainPlayGame
        '
        Me.menuMainPlayGame.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuItem13, Me.MenuItem14})
        Me.menuMainPlayGame.Location = New System.Drawing.Point(0, 0)
        Me.menuMainPlayGame.Name = "menuMainPlayGame"
        Me.menuMainPlayGame.Size = New System.Drawing.Size(792, 24)
        Me.menuMainPlayGame.TabIndex = 3
        '
        'MenuItem13
        '
        Me.MenuItem13.Name = "MenuItem13"
        Me.MenuItem13.Size = New System.Drawing.Size(59, 20)
        Me.MenuItem13.Text = "Macro3"
        '
        'MenuItem14
        '
        Me.MenuItem14.Name = "MenuItem14"
        Me.MenuItem14.Size = New System.Drawing.Size(59, 20)
        Me.MenuItem14.Text = "Macro4"
        '
        'oppInPlayOptions
        '
        Me.oppInPlayOptions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuOppDiscardFromPlay})
        Me.oppInPlayOptions.Name = "oppInPlayOptions"
        Me.oppInPlayOptions.Size = New System.Drawing.Size(114, 26)
        '
        'menuOppDiscardFromPlay
        '
        Me.menuOppDiscardFromPlay.Name = "menuOppDiscardFromPlay"
        Me.menuOppDiscardFromPlay.Size = New System.Drawing.Size(113, 22)
        Me.menuOppDiscardFromPlay.Text = "Discard"
        '
        'frmPlayGame
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(792, 556)
        Me.Controls.Add(Me.PanelGame)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.panelGameInfo)
        Me.Controls.Add(Me.menuMainPlayGame)
        Me.MainMenuStrip = Me.menuMainPlayGame
        Me.Name = "frmPlayGame"
        Me.Text = "Play Game"
        Me.panelGameInfo.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.tabGameDetails.ResumeLayout(False)
        Me.tabGameDetails.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.tabCardDetails.ResumeLayout(False)
        Me.tabCardDetails.PerformLayout()
        Me.tabAllCardsList.ResumeLayout(False)
        Me.tabAllCardsList.PerformLayout()
        Me.tabCardPicture.ResumeLayout(False)
        CType(Me.pictureSelectedHLCard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.menuYourDeckOptions.ResumeLayout(False)
        Me.PanelGame.ResumeLayout(False)
        Me.groupYou.ResumeLayout(False)
        Me.panelYouBottom.ResumeLayout(False)
        Me.groupYourDeck.ResumeLayout(False)
        CType(Me.pictureYourDeck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.panelYourHand.ResumeLayout(False)
        Me.panelYouTop.ResumeLayout(False)
        Me.tabYourGameCards.ResumeLayout(False)
        Me.tabYourInPlay.ResumeLayout(False)
        Me.groupOpponent.ResumeLayout(False)
        Me.tabOppGameCards.ResumeLayout(False)
        Me.tabOppInPlay.ResumeLayout(False)
        Me.tabOppDiscard.ResumeLayout(False)
        Me.tabOppXedFromGame.ResumeLayout(False)
        Me.groupOppPreGame.ResumeLayout(False)
        Me.menuMainPlayGame.ResumeLayout(False)
        Me.menuMainPlayGame.PerformLayout()
        Me.oppInPlayOptions.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Dim allHLCards As New DataSet

    Private Sub pictureYourDeck_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles pictureYourDeck.DoubleClick
        MessageBox.Show("Draw Up to Ability Code Goes Here")
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabGameDetails.Hide()
    End Sub

    Function getAllCards() As DataSet
        Dim connection As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\Documents and Settings\Eric\My Documents\highlander\cards.mdb;Persist Security Info=False")
        Dim queryCommand As New System.Data.OleDb.OleDbCommand("select * from cards ORDER BY Set, Name", connection)
        Dim myspaceData As New System.Data.OleDb.OleDbDataAdapter
        myspaceData.SelectCommand = queryCommand
        Dim messages As New DataSet
        myspaceData.Fill(messages, "cards")
        myspaceData.Dispose()
        queryCommand.Dispose()
        connection.Dispose()
        Return messages
    End Function

    Private Sub frmPlayGame_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        allHLCards = getAllCards()
        lstAllCards.DataSource = allHLCards.Tables("cards")
        lstAllCards.DisplayMember = "Name"
        txtCardName.DataBindings.Add("Text", allHLCards.Tables("cards"), "Name")
        txtCardName.Refresh()
        txtCardType.DataBindings.Add("Text", allHLCards.Tables("cards"), "Type")
        txtCardSet.DataBindings.Add("Text", allHLCards.Tables("cards"), "Set")
        txtCardSet.Refresh()

        txtCardRarity.DataBindings.Add("Text", allHLCards.Tables("cards"), "Rarity")
        txtCardOther.DataBindings.Add("Text", allHLCards.Tables("cards"), "Other")
        txtCardText.DataBindings.Add("Text", allHLCards.Tables("cards"), "Text")
        txtCardFileName.DataBindings.Add("Text", allHLCards.Tables("cards"), "File Name")
    End Sub

    Private Sub txtCardFileName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCardFileName.TextChanged
        Dim baseDir As String = "C:\Documents and Settings\Eric\My Documents\highlander\"
        Console.WriteLine(baseDir & txtCardSet.Text & "\" & txtCardFileName.Text)
        toolTipAllCardsPic.SetToolTip(pictureSelectedHLCard, txtCardName.Text & vbCrLf & txtCardRarity.Text & vbCrLf & txtCardOther.Text & vbCrLf & vbCrLf & txtCardText.Text)

        Try
            pictureSelectedHLCard.Image = System.Drawing.Bitmap.FromFile(baseDir & txtCardSet.Text & "\" & txtCardFileName.Text)
        Catch ex As Exception
            Console.WriteLine(ex)

            pictureSelectedHLCard.Image = System.Drawing.Bitmap.FromFile(baseDir & "1eback.jpg")
        End Try

    End Sub

    Private Sub lstAllCards_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstAllCards.SelectedIndexChanged
        Dim baseDir As String = "C:\Documents and Settings\Eric\My Documents\highlander\"

        toolTipAllCardsPic.SetToolTip(pictureSelectedHLCard,
            lstAllCards.SelectedItem.item("Name").ToString & vbCrLf &
            lstAllCards.SelectedItem.item("Rarity").ToString & vbCrLf &
            lstAllCards.SelectedItem.item("Other").ToString & vbCrLf & vbCrLf &
            lstAllCards.SelectedItem.item("Text").ToString)

        Try
            pictureSelectedHLCard.Image = System.Drawing.Bitmap.FromFile(baseDir &
                lstAllCards.SelectedItem.item("Set").ToString &
                "\" & lstAllCards.SelectedItem.item("File Name").ToString)
        Catch ex As Exception
            Console.WriteLine(ex)

            pictureSelectedHLCard.Image = System.Drawing.Bitmap.FromFile(baseDir & "1eback.jpg")
        End Try
    End Sub

    Private Sub MenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem13.Click
        opponentInPlayCards.addCard(1, 0, "Generic", "Thrust", "Attack", "C:\Documents and Settings\Eric\My Documents\highlander\tin\Thrust.jpg", "GRID: ---/-X-/---" & vbCrLf & "If Thrust is successful, it does an additional point of damage.", oppInPlayOptions, AddressOf oppDiscardFromPlay)
        opponentInPlayCards.addCard(2, 0, "Generic", "Quality Blade", "Object", "C:\Documents and Settings\Eric\My Documents\highlander\tin\Quality Blade.jpg", "GRID: ---/-X-/---" & vbCrLf & "Card Text", oppInPlayOptions, AddressOf oppDiscardFromPlay)
        opponentInPlayCards.addCard(3, 0, "Slan Quince", "Persona", "Object", "C:\Documents and Settings\Eric\My Documents\highlander\tin\Slan Quince.jpg", "GRID: ---/-X-/---" & vbCrLf & "Card Text", oppInPlayOptions, AddressOf oppDiscardFromPlay)
        opponentPreGameCards.addCard(3, 0, "Slan Quince", "Persona", "Object", "C:\Documents and Settings\Eric\My Documents\highlander\tin\Slan Quince.jpg", "GRID: ---/-X-/---" & vbCrLf & "Card Text", oppInPlayOptions, AddressOf oppDiscardFromPlay)
    End Sub

    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem14.Click
        opponentInPlayCards.toggleImageView()
    End Sub

    Private Sub menuDeckDrawUpAbility_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuDeckDrawUpAbility.Click
        ' Draw Up to Your Ability
        MessageBox.Show(sender.parent.ToString)

    End Sub

    Private Sub menuYouDraw1Card_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuYouDraw1Card.Click
        ' Draw 1 Card.
    End Sub

    Private Sub menuYouShuffleEndurance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuYouShuffleEndurance.Click
        ' Shuffle Your Endurance
    End Sub

    Private Sub menuOppDiscardFromPlay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuOppDiscardFromPlay.Click
    End Sub

    Private Sub oppDiscardFromPlay(ByVal sender As System.Object, ByVal e As System.EventArgs)
        opponentInPlayCards.deleteCard(sender.Tag)
        sender.dispose()
    End Sub

    Private Sub VScrollBar1_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VScrollBar1.Scroll

    End Sub
End Class
