﻿Imports System.Data

Public Class frmPlayGame2
    Public PlayerTurn As Boolean = True

    Private Sub ButtonCardDetails_Click(sender As System.Object, e As System.EventArgs) Handles ButtonCardDetails.Click
        Dim results() As DataRow = Data.CardsDB.Data.Tables("Card").Select("ID = " & CurrentCardImage.getCurrentID().ToString())
        If results.Length > 0 Then
            Dim detailsWindow As New frmCardDetails
            detailsWindow.CardDetails1.LoadCard(CurrentCardImage.getCurrentID())
            detailsWindow.Text = results(0).Item("Immortal").ToString() & " " & results(0).Item("Title").ToString()
            Me.ParentForm.AddOwnedForm(detailsWindow)
            detailsWindow.MdiParent = Me.ParentForm
            detailsWindow.Show()
        End If
    End Sub

    Private Sub ComboBoxPhase_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxPhase.SelectedIndexChanged
        txtLogBox.Text &= vbCrLf & " ** " & ComboBoxPhase.SelectedItem.ToString() & " **"
        If ComboBoxPhase.SelectedItem.ToString() = "End Turn" Then
            Dim strTurn As String = ""
            If PlayerTurn = True Then
                strTurn = txtPlayerTurnNum.Text
            Else
                strTurn = txtOpponentTurnNum.Text
            End If
            strTurn = (Convert.ToInt32(strTurn.Substring(0, strTurn.Length - 1)) + 1).ToString() & strTurn.Substring(strTurn.Length - 1)
            If PlayerTurn = True Then
                txtPlayerTurnNum.Text = strTurn
                strTurn = txtOpponentTurnNum.Text
            Else
                txtOpponentTurnNum.Text = strTurn
                strTurn = txtPlayerTurnNum.Text
            End If

            PlayerTurn = (PlayerTurn = False)
            txtLogBox.Text &= vbCrLf & vbCrLf & " ** Begin Turn " & strTurn & " **"
            ComboBoxPhase.SelectedIndex = 0
        End If
        If PlayerTurn = True Then
            GroupGameStatus.Text = "Game Details -- Your Turn (" & ComboBoxPhase.SelectedItem.ToString() & ")"
        Else
            GroupGameStatus.Text = "Game Details -- " & txtOpponentName.Text & "'s Turn (" & ComboBoxPhase.SelectedItem.ToString() & ")"
        End If
    End Sub

    Private Sub ButtonSend_Click(sender As System.Object, e As System.EventArgs) Handles ButtonSend.Click
        txtLogBox.Text &= vbCrLf & txtPlayerName.Text & ":  " & txtChatText.Text
        txtChatText.Text = ""
    End Sub

    Public Sub AddCardToList(ByVal CardID As Integer, CardList As System.Windows.Forms.ListView)
        Dim newCard As New ListViewItem
        Dim results() As DataRow = Data.CardsDB.Data.Tables("Card").Select("ID = " & CardID.ToString())
        If results.Length > 0 Then
            Dim currentCard As Data.Card = Highlander.Data.Card.LoadCard(CardID)
            newCard.Text = currentCard.DisplayName()
            newCard.Tag = currentCard.Id.ToString()
            Dim strImagePath As String = currentCard.FilePathFront()
            ImageListCards.Images.Add(CardID, classAspectedImage.CreateImage(strImagePath, ImageListCards.ImageSize.Height, ImageListCards.ImageSize.Width))
            newCard.ImageKey = CardID

            Dim strCardText As String = ""
            strCardText = "Type: " & currentCard.Type & vbCrLf & currentCard.Text
            If currentCard.Grid <> "" Then
                strCardText = "GRID: " & currentCard.Grid & vbCrLf & strCardText
            End If
            newCard.ToolTipText = strCardText
            CardList.Items.Add(newCard)
        End If
    End Sub

    Private Sub frmPlayGame2_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Set up the control responsible for displaying all available cards.
        CardsAllCards.controlSetup(New EventHandler(AddressOf allCardsSelectChanged), New EventHandler(AddressOf allCardsDoubleClick))
        ' Load Publisher Names.
        For Each onePublisher As DataRow In Data.SetsDB.Data.Tables("Publisher").Rows
            comboPublisher.Items.Add(onePublisher.Item("Long_Name"))
        Next
        comboPublisher.SelectedIndex = 1

        ' Set up Handlers for card Drag/Drop.
        '  I didn't feel like naming every list view on this screen, so we're recurssively looping through every object to find them.
        AddDragDropHandlers(Me.Controls)
    End Sub

    Private Sub AddDragDropHandlers(ByRef ControlsList As Control.ControlCollection)
        For Each c As Control In ControlsList
            If TypeOf c Is ListView Then
                With CType(c, ListView)
                    AddHandler .ItemDrag, AddressOf ListView_ItemDrag
                    AddHandler .DragEnter, AddressOf ListView_DragEnter
                    AddHandler .DragDrop, AddressOf ListView_DragDrop
                End With
            ElseIf TypeOf c Is SplitContainer Or
                TypeOf c Is SplitterPanel Or
                TypeOf c Is GroupBox Or
                TypeOf c Is TabControl Or
                TypeOf c Is TabPage Then
                AddDragDropHandlers(c.Controls)
            End If
        Next
    End Sub

    Private Sub ListView_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs)
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then
            Exit Sub
        End If

        With CType(sender, ListView)
            .DoDragDrop(e.Item, DragDropEffects.Move)
        End With
    End Sub

    Private Sub ListView_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then
            Exit Sub
        End If

        If e.Data.GetDataPresent(GetType(ListViewItem)) Then
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    Private Sub ListView_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then
            Exit Sub
        End If

        ' Remove the selected cards from the current list and add them to the dropped list.
        If e.Data.GetDataPresent(GetType(ListViewItem)) Then
            Dim draggedCard As ListViewItem = CType(e.Data.GetData(GetType(ListViewItem)), ListViewItem)
            Dim SendingListView As ListView = draggedCard.ListView ' Record this before the loop, because it'll change as soon as we move the item to a new list.
            For Each OneCard As ListViewItem In SendingListView.SelectedItems
                AddCardToList(OneCard.Tag, sender)
                SendingListView.Items.Remove(OneCard)
                ' *** Log box code should go here. ***
            Next
            If SendingListView.Name = "ListPlayerHand" Or sender.name = "ListPlayerHand" Then
                GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"

            End If
        End If

    End Sub

    Public Sub allCardsSelectChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When the selected card title is changed, display the card's image.
        If CardsAllCards.getSelectedCards().Count > 0 Then
            CurrentCardImage.LoadCard(CardsAllCards.getSelectedCards()(CardsAllCards.getSelectedCards().Count - 1).Tag)
        End If
    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If CardsAllCards.getSelectedCards().Count > 0 Then
            CurrentCardImage.LoadCard(CardsAllCards.getSelectedCards()(0).Tag)
            AddCardToList(CardsAllCards.getSelectedCards()(0).Tag, ListOpponentInPlay)
            txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " plays " & CardsAllCards.getSelectedCards()(0).Text
        End If
    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()

        Dim SelectedPublisher As DataRow() = Data.SetsDB.Data.Tables("Publisher").Select("Long_Name = '" & comboPublisher.SelectedItem.ToString() & "'")
        If SelectedPublisher.Length > 0 Then
            If SelectedPublisher(0).Item("ID").ToString() = "0" Then
                Dim CardsView As DataView = Data.SetsDB.Data.Tables("set").DefaultView
                CardsView.Sort = Data.CardsDB.ExpansionSortOrder & " ASC"
                For Each oneSet As DataRowView In CardsView
                    comboSets.Items.Add(oneSet.Item("set_long"))
                Next
            Else
                For Each oneSet As DataRow In Data.SetsDB.Data.Tables("set").Select("set_Publisher = " & SelectedPublisher(0).Item("ID").ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Item("set_long"))
                Next
            End If
        End If
        comboSets.SelectedIndex = 1
    End Sub

    Private Sub comboSets_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Update the list of cards whenever the selected set is changed.
        generateSearchQuery()
    End Sub

    Private Sub txtKeywordSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' Re-query the database whenever the user enters or changes
        '   the keyword search text.
        generateSearchQuery()
    End Sub

    Private Sub generateSearchQuery()
        CardsAllCards.filterCardsList(comboSets.SelectedItem, comboPublisher.SelectedItem, txtKeywordSearch.Text, 0, False)
    End Sub

    Private Sub ButtonOpponentAddCards_Click(sender As System.Object, e As System.EventArgs) Handles ButtonOpponentAddCards.Click
        Dim SelectedListView As System.Windows.Forms.ListView
        Dim strLogText As String = ""
        If ComboBoxAddCardsAction.SelectedItem.ToString() = "Opponent's In Play" Then
            SelectedListView = ListOpponentInPlay
            strLogText = "  " & txtOpponentName.Text & " plays "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Opponent's Discard" Then
            SelectedListView = ListOpponentDiscard
            strLogText = "  " & txtOpponentName.Text & " discards "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Opponent's X-ed From Game" Then
            SelectedListView = ListOpponentRemovedFromGame
            strLogText = "  " & txtOpponentName.Text & " removes "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Opponent's Pre-Game" Then
            SelectedListView = ListOpponentPreGame
            strLogText = "  " & txtOpponentName.Text & " adds to pre-game: "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Your In Play" Then
            SelectedListView = ListPlayerInPlay
            strLogText = "  " & txtPlayerName.Text & " adds to play "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Your Discard" Then
            SelectedListView = ListPlayerDiscard
            strLogText = "  " & txtPlayerName.Text & " adds to discard "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Your X-ed From Game" Then
            SelectedListView = ListPlayerRemovedFromGame
            strLogText = "  " & txtPlayerName.Text & " adds to x-ed from game "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Your Pre-Game" Then
            SelectedListView = ListPlayerPreGame
            strLogText = "  " & txtPlayerName.Text & " adds to pre-game: "
        ElseIf ComboBoxAddCardsAction.SelectedItem.ToString() = "Your Hand" Then
            SelectedListView = ListPlayerHand
            strLogText = "  " & txtPlayerName.Text & " adds to hand: "
        Else
            Exit Sub
        End If

        For Each OneCard As ListViewItem In CardsAllCards.getSelectedCards()
            AddCardToList(OneCard.Tag, SelectedListView)
            txtLogBox.Text &= vbCrLf & strLogText & OneCard.Text
        Next
        UpdatePlayUnderneathMenu()
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub ListOpponentInPlay_DoubleClick(sender As Object, e As System.EventArgs) Handles ListOpponentInPlay.DoubleClick
        If ListOpponentInPlay.SelectedItems.Count > 0 Then
            AddCardToList(ListOpponentInPlay.SelectedItems(0).Tag, ListOpponentDiscard)
            txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " discards " &
                ListOpponentInPlay.SelectedItems(0).Text
            ListOpponentInPlay.SelectedItems(0).Remove()
        End If
    End Sub

    Private Sub ListOpponentInPlay_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListOpponentInPlay.SelectedIndexChanged
        If ListOpponentInPlay.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListOpponentInPlay.SelectedItems(ListOpponentInPlay.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListOpponentDiscard_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListOpponentDiscard.SelectedIndexChanged
        If ListOpponentDiscard.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListOpponentDiscard.SelectedItems(ListOpponentDiscard.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListOpponentRemovedFromGame_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListOpponentRemovedFromGame.SelectedIndexChanged
        If ListOpponentRemovedFromGame.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListOpponentRemovedFromGame.SelectedItems(ListOpponentRemovedFromGame.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListOpponentPreGame_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListOpponentPreGame.SelectedIndexChanged
        If ListOpponentPreGame.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListOpponentPreGame.SelectedItems(ListOpponentPreGame.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListPlayerInPlay_DoubleClick(sender As Object, e As System.EventArgs) Handles ListPlayerInPlay.DoubleClick
        If ListPlayerInPlay.SelectedItems.Count > 0 Then
            AddCardToList(ListPlayerInPlay.SelectedItems(0).Tag, ListPlayerDiscard)
            txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " discards " &
                ListPlayerInPlay.SelectedItems(0).Text
            If ListPlayerInPlay.SelectedItems(0).SubItems.Count > 1 Then
                Dim UnderneathCounter As Integer = 1
                While UnderneathCounter < ListPlayerInPlay.SelectedItems(0).SubItems.Count
                    AddCardToList(ListPlayerInPlay.SelectedItems(0).SubItems(UnderneathCounter).Text, ListPlayerDiscard)
                    UnderneathCounter += 1
                End While
                txtLogBox.Text &= vbCrLf & "  All cards underneath " &
                    ListPlayerInPlay.SelectedItems(0).Text & " have been discarded."
            End If
            ListPlayerInPlay.SelectedItems(0).Remove()
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub ListPlayerInPlay_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListPlayerInPlay.SelectedIndexChanged
        If ListPlayerInPlay.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListPlayerInPlay.SelectedItems(ListPlayerInPlay.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListPlayerDiscard_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListPlayerDiscard.SelectedIndexChanged
        If ListPlayerDiscard.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListPlayerDiscard.SelectedItems(ListPlayerDiscard.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListPlayerRemovedFromGame_DoubleClick(sender As Object, e As System.EventArgs) Handles ListPlayerRemovedFromGame.DoubleClick
        If ListPlayerRemovedFromGame.SelectedItems.Count > 0 Then
            AddCardToList(ListPlayerRemovedFromGame.SelectedItems(0).Tag, ListPlayerInPlay)
            txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " returns " &
                ListPlayerRemovedFromGame.SelectedItems(0).Text & " to play"
            ListPlayerRemovedFromGame.SelectedItems(0).Remove()
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub ListPlayerRemovedFromGame_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListPlayerRemovedFromGame.SelectedIndexChanged
        If ListPlayerRemovedFromGame.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListPlayerRemovedFromGame.SelectedItems(ListPlayerRemovedFromGame.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListPlayerPreGame_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListPlayerPreGame.SelectedIndexChanged
        If ListPlayerPreGame.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListPlayerPreGame.SelectedItems(ListPlayerPreGame.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Private Sub ListPlayerHand_DoubleClick(sender As Object, e As System.EventArgs) Handles ListPlayerHand.DoubleClick
        If ListPlayerHand.SelectedItems.Count > 0 Then
            AddCardToList(ListPlayerHand.SelectedItems(0).Tag, ListPlayerInPlay)
            txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " plays " &
                ListPlayerHand.SelectedItems(0).Text
            ListPlayerHand.SelectedItems(0).Remove()
        End If
        UpdatePlayUnderneathMenu()
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub ListPlayerHand_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ListPlayerHand.SelectedIndexChanged
        If ListPlayerHand.SelectedItems.Count > 0 Then
            CurrentCardImage.LoadCard(ListPlayerHand.SelectedItems(ListPlayerHand.SelectedItems.Count - 1).Tag)
        End If
    End Sub

    Public Sub openHLDFile(ByVal fileName As String)
        If Not System.IO.File.Exists(fileName) Then
            Exit Sub
        End If

        Dim deckFile As New System.IO.StreamReader(fileName)
        Dim tempLine As String, newCard() As String
        Dim randNum As New Random
        Do
            ' Read the file, one line at a time.
            tempLine = deckFile.ReadLine
            If tempLine <> "" Then

                ' Split the current line up into an array of three elements.
                '   (0) is either DECK or PREGAME for haves or Wants
                '   (1) is the ID Number
                '   (2) is the Quantity
                newCard = tempLine.Split(",")
                Dim counter As Integer = 0

                ' Loop through each item in the array, remove "" characters
                '   from around text values.
                While counter < 3
                    If newCard(counter).Chars(0) = """" Then
                        newCard(counter) = newCard(counter).Substring(1)
                    End If
                    If newCard(counter).Chars(newCard(counter).Length - 1) = """" Then
                        newCard(counter) = newCard(counter).Substring(0, newCard(counter).Length - 1)
                    End If
                    counter = counter + 1
                End While

                ' Load the image and details of the current card.
                Dim SelectedCard() As DataRow = Data.CardsDB.Data.Tables("Card").Select("ID = '" & newCard(1) & "'")
                If SelectedCard.Length = 0 Then
                    Exit Sub ' This should never happen.
                End If

                ' Add the current card to either the Deck or Pre-Game list, based 
                '   on newCard(0).
                If newCard(0) = "DECK" Then
                    Dim intCardCounter As Integer = 0
                    While intCardCounter < newCard(2)
                        Dim NewCardItem As New ListViewItem
                        NewCardItem.Text = randNum.NextDouble
                        NewCardItem.SubItems.Add(newCard(1))
                        NewCardItem.SubItems.Add(SelectedCard(0).Item("Immortal").ToString() & " " & SelectedCard(0).Item("Title").ToString())
                        ListPlayerDeck.Items.Add(NewCardItem)
                        intCardCounter += 1
                    End While
                Else
                    Dim intCardCounter As Integer = 0
                    While intCardCounter < newCard(2)
                        AddCardToList(newCard(1), ListPlayerPreGame)
                        intCardCounter += 1
                    End While
                End If
            End If
        Loop Until tempLine Is Nothing

        deckFile.Close()
        deckFile.Dispose()
        deckFile = Nothing

        ListPlayerDeck.Sort()
        GroupPlayerDeck.Text = "Deck (" & ListPlayerDeck.Items.Count.ToString() & ")"
    End Sub

    Private Sub PictureDeck_DoubleClick(sender As Object, e As System.EventArgs) Handles PictureDeck.DoubleClick
        If ListPlayerDeck.Items.Count > 0 Then
            AddCardToList(ListPlayerDeck.Items(0).SubItems(1).Text, ListPlayerHand)
            ListPlayerDeck.Items(0).Remove()
            txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " draws a card"
            GroupPlayerDeck.Text = "Deck (" & ListPlayerDeck.Items.Count.ToString() & ")"
        End If
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub DrawUpToAbilityToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DrawUpToAbilityToolStripMenuItem.Click
        Dim intDrawCounter As Integer = 0
        While (ListPlayerHand.Items.Count < txtPlayerAbility.Text) And (ListPlayerDeck.Items.Count > 0)
            AddCardToList(ListPlayerDeck.Items(0).SubItems(1).Text, ListPlayerHand)
            ListPlayerDeck.Items(0).Remove()
            intDrawCounter += 1
        End While
        GroupPlayerDeck.Text = "Deck (" & ListPlayerDeck.Items.Count.ToString() & ")"
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
        txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " draws up to ability (" & intDrawCounter.ToString() & " cards)"
    End Sub

    Private Sub ShuffleDeckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ShuffleDeckToolStripMenuItem.Click
        Dim rand As New Random
        For Each OneCard As ListViewItem In ListPlayerDeck.Items
            OneCard.Text = rand.NextDouble()
        Next
        ListPlayerDeck.Sort()
        txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " shuffles his deck"
    End Sub

    Private Sub ShuffleDiscardIntoDeckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ShuffleDiscardIntoDeckToolStripMenuItem.Click
        Dim rand As New Random
        For Each OneCard As ListViewItem In ListPlayerDiscard.Items
            Dim NewCardItem As New ListViewItem
            NewCardItem.Text = rand.NextDouble
            NewCardItem.SubItems.Add(OneCard.Tag)
            NewCardItem.SubItems.Add(OneCard.Text)
            ListPlayerDeck.Items.Add(NewCardItem)
        Next
        ListPlayerDiscard.Items.Clear()

        For Each OneCard As ListViewItem In ListPlayerDeck.Items
            OneCard.Text = rand.NextDouble()
        Next
        ListPlayerDeck.Sort()
        GroupPlayerDeck.Text = "Deck (" & ListPlayerDeck.Items.Count.ToString() & ")"
        txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " shuffles his discard into his deck"
    End Sub

    Private Sub PlayCardsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PlayCardsToolStripMenuItem.Click
        If ListPlayerHand.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerHand.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " plays " & OneCard.Text
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub DiscardCardsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DiscardCardsToolStripMenuItem.Click
        If ListPlayerHand.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerHand.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerDiscard)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " discards " & OneCard.Text
                OneCard.Remove()
            Next
        End If
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub RemoveFromGameToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveFromGameToolStripMenuItem.Click
        If ListPlayerHand.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerHand.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " removes " & OneCard.Text & " from the game"
                OneCard.Remove()
            Next
        End If
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub DiscardCardsToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles DiscardCardsToolStripMenuItem1.Click
        If ListPlayerInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerDiscard)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " discards " & OneCard.Text
                If OneCard.SubItems.Count > 1 Then
                    Dim UnderneathCounter As Integer = 1
                    While UnderneathCounter < OneCard.SubItems.Count
                        AddCardToList(OneCard.SubItems(UnderneathCounter).Text, ListPlayerDiscard)
                        UnderneathCounter += 1
                    End While
                    txtLogBox.Text &= vbCrLf & "  All cards underneath " &
                        OneCard.Text & " have been discarded."
                End If
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub RemoveCardsFromGameToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsFromGameToolStripMenuItem.Click
        If ListPlayerInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " removes " & OneCard.Text & " from the game"
                If OneCard.SubItems.Count > 1 Then
                    Dim UnderneathCounter As Integer = 1
                    While UnderneathCounter < OneCard.SubItems.Count
                        AddCardToList(OneCard.SubItems(UnderneathCounter).Text, ListPlayerDiscard)
                        UnderneathCounter += 1
                    End While
                    txtLogBox.Text &= vbCrLf & "  All cards underneath " &
                        OneCard.Text & " have been discarded."
                End If
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub ReturnCardsToHandToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ReturnCardsToHandToolStripMenuItem.Click
        If ListPlayerInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerHand)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " returns " & OneCard.Text & " to his hand"
                If OneCard.SubItems.Count > 1 Then
                    Dim UnderneathCounter As Integer = 1
                    While UnderneathCounter < OneCard.SubItems.Count
                        AddCardToList(OneCard.SubItems(UnderneathCounter).Text, ListPlayerDiscard)
                        UnderneathCounter += 1
                    End While
                    txtLogBox.Text &= vbCrLf & "  All cards underneath " &
                        OneCard.Text & " have been discarded."
                End If
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub DiscardToTopOfDeckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DiscardToTopOfDeckToolStripMenuItem.Click
        If ListPlayerHand.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerHand.SelectedItems
                Dim NewCardItem As New ListViewItem
                NewCardItem.Text = (Convert.ToDouble(ListPlayerDeck.Items(0).Text) - 0.000001).ToString()
                NewCardItem.SubItems.Add(OneCard.Tag)
                NewCardItem.SubItems.Add(OneCard.Text)
                ListPlayerDeck.Items.Add(NewCardItem)

                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " discards " & OneCard.Text & " to the top of his deck"
                OneCard.Remove()
            Next
        End If
        ListPlayerDeck.Sort()
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub DiscardToBottomOfDeckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DiscardToBottomOfDeckToolStripMenuItem.Click
        If ListPlayerHand.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerHand.SelectedItems
                Dim NewCardItem As New ListViewItem
                NewCardItem.Text = (Convert.ToDouble(ListPlayerDeck.Items(ListPlayerDeck.Items.Count - 1).Text) + 0.000001).ToString()
                NewCardItem.SubItems.Add(OneCard.Tag)
                NewCardItem.SubItems.Add(OneCard.Text)
                ListPlayerDeck.Items.Add(NewCardItem)

                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " discards " & OneCard.Text & " to the top of his deck"
                OneCard.Remove()
            Next
        End If
        ListPlayerDeck.Sort()
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub ReturnCardsToPlayToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ReturnCardsToPlayToolStripMenuItem.Click
        If ListPlayerDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerDiscard.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " plays " & OneCard.Text & " from discard"
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub ReturnCardsToHandToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ReturnCardsToHandToolStripMenuItem1.Click
        If ListPlayerDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerDiscard.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerHand)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " returns " & OneCard.Text & " from discard to his hand"
                OneCard.Remove()
            Next
        End If
        GroupPlayerHand.Text = "Your Hand (" & ListPlayerHand.Items.Count.ToString() & ")"
    End Sub

    Private Sub RemoveCardsFromGameToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsFromGameToolStripMenuItem1.Click
        If ListPlayerDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerDiscard.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " removes " & OneCard.Text & " in discard from the game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub PlaceCardsOnTopOfDeckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PlaceCardsOnTopOfDeckToolStripMenuItem.Click
        If ListPlayerDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerDiscard.SelectedItems
                Dim NewCardItem As New ListViewItem
                NewCardItem.Text = (Convert.ToDouble(ListPlayerDeck.Items(0).Text) - 0.000001).ToString()
                NewCardItem.SubItems.Add(OneCard.Tag)
                NewCardItem.SubItems.Add(OneCard.Text)
                ListPlayerDeck.Items.Add(NewCardItem)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " moves " & OneCard.Text & " from discard to the top of his deck"
                OneCard.Remove()
            Next
        End If
        ListPlayerDeck.Sort()

    End Sub

    Private Sub PlaceCardsOnBottomOfDeckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PlaceCardsOnBottomOfDeckToolStripMenuItem.Click
        If ListPlayerDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerDiscard.SelectedItems
                Dim NewCardItem As New ListViewItem
                NewCardItem.Text = (Convert.ToDouble(ListPlayerDeck.Items(ListPlayerDeck.Items.Count - 1).Text) + 0.000001).ToString()
                NewCardItem.SubItems.Add(OneCard.Tag)
                NewCardItem.SubItems.Add(OneCard.Text)
                ListPlayerDeck.Items.Add(NewCardItem)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " moves " & OneCard.Text & " from discard to the top of his deck"
                OneCard.Remove()
            Next
        End If
        ListPlayerDeck.Sort()
    End Sub

    Private Sub PlayCardsToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles PlayCardsToolStripMenuItem1.Click
        If ListPlayerPreGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerPreGame.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " plays " & OneCard.Text & " from pre-game"
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub RemoveCardsFromTheGameToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsFromTheGameToolStripMenuItem.Click
        If ListPlayerPreGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerPreGame.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " removes " & OneCard.Text & " from the game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub ReturnToPlayToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ReturnToPlayToolStripMenuItem.Click
        If ListPlayerRemovedFromGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerRemovedFromGame.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " returns " & OneCard.Text & " to play"
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub ReturnToPreGameToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ReturnToPreGameToolStripMenuItem.Click
        If ListPlayerRemovedFromGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerRemovedFromGame.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerPreGame)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " returns " & OneCard.Text & " to pre-game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub DiscardCardsToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles DiscardCardsToolStripMenuItem2.Click
        If ListOpponentInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentDiscard)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " discards " & OneCard.Text
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsFromTheGameToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsFromTheGameToolStripMenuItem1.Click
        If ListOpponentInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text & " from the game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsPermanentlyToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsPermanentlyToolStripMenuItem.Click
        If ListOpponentInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentInPlay.SelectedItems
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub StealCardsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles StealCardsToolStripMenuItem.Click
        If ListOpponentInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListPlayerInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " steals " & OneCard.Text
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub ReturnCardsToPlayToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles ReturnCardsToPlayToolStripMenuItem1.Click
        If ListOpponentDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentDiscard.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " returns " & OneCard.Text & " to play"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsFromTheGameToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsFromTheGameToolStripMenuItem2.Click
        If ListOpponentDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentDiscard.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text & " from the game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsPermanentlyToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsPermanentlyToolStripMenuItem1.Click
        If ListOpponentDiscard.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentDiscard.SelectedItems
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveAllCardsPermanentlyToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RemoveAllCardsPermanentlyToolStripMenuItem.Click
        ListOpponentDiscard.Items.Clear()
        txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes all cards from discard."
    End Sub

    Private Sub ReturnCardsToPlayToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles ReturnCardsToPlayToolStripMenuItem2.Click
        If ListOpponentRemovedFromGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentRemovedFromGame.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " returns " & OneCard.Text & " to play"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub ReturnCardsToPreGameToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ReturnCardsToPreGameToolStripMenuItem.Click
        If ListOpponentRemovedFromGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentRemovedFromGame.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentPreGame)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " returns " & OneCard.Text & " to pre-game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsPermanentlyToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsPermanentlyToolStripMenuItem2.Click
        If ListOpponentRemovedFromGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentRemovedFromGame.SelectedItems
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub PlayCardsToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles PlayCardsToolStripMenuItem2.Click
        If ListOpponentPreGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentPreGame.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " plays " & OneCard.Text & " from pre-game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsFromTheGameToolStripMenuItem3_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsFromTheGameToolStripMenuItem3.Click
        If ListOpponentPreGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentPreGame.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentRemovedFromGame)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text & " from the game"
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub RemoveCardsPermanentlyToolStripMenuItem3_Click(sender As System.Object, e As System.EventArgs) Handles RemoveCardsPermanentlyToolStripMenuItem3.Click
        If ListOpponentPreGame.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListOpponentPreGame.SelectedItems
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " removes " & OneCard.Text
                OneCard.Remove()
            Next
        End If
    End Sub

    Private Sub StealCardToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles StealCardToolStripMenuItem.Click
        If ListPlayerInPlay.SelectedItems.Count > 0 Then
            For Each OneCard As ListViewItem In ListPlayerInPlay.SelectedItems
                AddCardToList(OneCard.Tag, ListOpponentInPlay)
                txtLogBox.Text &= vbCrLf & "  " & txtOpponentName.Text & " steals " & OneCard.Text
                If OneCard.SubItems.Count > 1 Then
                    Dim UnderneathCounter As Integer = 1
                    While UnderneathCounter < OneCard.SubItems.Count
                        AddCardToList(OneCard.SubItems(UnderneathCounter).Text, ListPlayerDiscard)
                        UnderneathCounter += 1
                    End While
                    txtLogBox.Text &= vbCrLf & "  All cards underneath " &
                        OneCard.Text & " have been discarded."
                End If
                OneCard.Remove()
            Next
        End If
        UpdatePlayUnderneathMenu()
    End Sub

    Private Sub UpdatePlayUnderneathMenu()
        PlayUnderneathToolStripMenuItem.DropDownItems.Clear()
        Dim counter As Integer = 0
        While counter < ListPlayerInPlay.Items.Count
            Dim NewMenuItem As System.Windows.Forms.ToolStripItem = PlayUnderneathToolStripMenuItem.DropDownItems.Add(ListPlayerInPlay.Items(counter).Text)
            NewMenuItem.Tag = counter.ToString()
            AddHandler NewMenuItem.Click, New EventHandler(AddressOf PlayCardsUnderneathClick)
            counter += 1
        End While

        If PlayUnderneathToolStripMenuItem.DropDownItems.Count = 0 Then
            PlayUnderneathToolStripMenuItem.Enabled = False
        Else
            PlayUnderneathToolStripMenuItem.Enabled = True
        End If
    End Sub

    Public Sub PlayCardsUnderneathClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim SelectedCard() As DataRow = Data.CardsDB.Data.Tables("Card").Select("ID = " & ListPlayerInPlay.Items(Convert.ToInt32(sender.Tag)).Tag)
        For Each OneCard As ListViewItem In ListPlayerHand.SelectedItems
            ListPlayerInPlay.Items(Convert.ToInt32(sender.Tag)).SubItems.Add(OneCard.Tag)
            txtLogBox.Text &= vbCrLf & "  " & txtPlayerName.Text & " plays 1 card underneath " & SelectedCard(0).Item("Immortal").ToString() & " " & SelectedCard(0).Item("Title").ToString()
            OneCard.Remove()
        Next
        ListPlayerInPlay.Items(Convert.ToInt32(sender.Tag)).Text = "{" & (ListPlayerInPlay.Items(Convert.ToInt32(sender.Tag)).SubItems.Count - 1).ToString() & "} " & SelectedCard(0).Item("Immortal").ToString() & " " & SelectedCard(0).Item("Title").ToString()
    End Sub

    Private Sub TabControl2_DragOver(sender As Object, e As System.Windows.Forms.DragEventArgs) Handles TabControl2.DragOver
        Dim MousePoint As Point = TabControl2.PointToClient(Control.MousePosition)
        For counter As Integer = 0 To TabControl2.TabCount - 1 Step 1
            If (TabControl2.GetTabRect(counter).Contains(MousePoint)) Then
                TabControl2.SelectedIndex = counter
                Exit For
            End If
        Next
    End Sub

    Private Sub TabControl1_DragOver(sender As Object, e As System.Windows.Forms.DragEventArgs) Handles TabControl1.DragOver
        Dim MousePoint As Point = TabControl1.PointToClient(Control.MousePosition)
        For counter As Integer = 0 To TabControl1.TabCount - 1 Step 1
            If (TabControl1.GetTabRect(counter).Contains(MousePoint)) Then
                TabControl1.SelectedIndex = counter
                Exit For
            End If
        Next
    End Sub
End Class