﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMDBImporter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMDBImporter))
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.lblCardsMDBPath = New System.Windows.Forms.Label()
        Me.txtCardsMDB = New System.Windows.Forms.TextBox()
        Me.ButtonBrowse = New System.Windows.Forms.Button()
        Me.ListMissingCards = New System.Windows.Forms.ListView()
        Me.ID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CardName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Expansion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuErrorList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SaveListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ButtonImport = New System.Windows.Forms.Button()
        Me.ProgressImport = New System.Windows.Forms.ProgressBar()
        Me.ContextMenuErrorList.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblInfo
        '
        Me.lblInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblInfo.Location = New System.Drawing.Point(12, 9)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(560, 141)
        Me.lblInfo.TabIndex = 0
        Me.lblInfo.Text = resources.GetString("lblInfo.Text")
        '
        'lblCardsMDBPath
        '
        Me.lblCardsMDBPath.AutoSize = True
        Me.lblCardsMDBPath.Location = New System.Drawing.Point(12, 159)
        Me.lblCardsMDBPath.Name = "lblCardsMDBPath"
        Me.lblCardsMDBPath.Size = New System.Drawing.Size(59, 13)
        Me.lblCardsMDBPath.TabIndex = 2
        Me.lblCardsMDBPath.Text = "cards.mdb:"
        '
        'txtCardsMDB
        '
        Me.txtCardsMDB.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardsMDB.Location = New System.Drawing.Point(77, 156)
        Me.txtCardsMDB.Name = "txtCardsMDB"
        Me.txtCardsMDB.Size = New System.Drawing.Size(414, 20)
        Me.txtCardsMDB.TabIndex = 3
        '
        'ButtonBrowse
        '
        Me.ButtonBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonBrowse.Location = New System.Drawing.Point(497, 154)
        Me.ButtonBrowse.Name = "ButtonBrowse"
        Me.ButtonBrowse.Size = New System.Drawing.Size(75, 23)
        Me.ButtonBrowse.TabIndex = 4
        Me.ButtonBrowse.Text = "Browse..."
        Me.ButtonBrowse.UseVisualStyleBackColor = True
        '
        'ListMissingCards
        '
        Me.ListMissingCards.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListMissingCards.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ID, Me.CardName, Me.Expansion})
        Me.ListMissingCards.ContextMenuStrip = Me.ContextMenuErrorList
        Me.ListMissingCards.Location = New System.Drawing.Point(15, 212)
        Me.ListMissingCards.Name = "ListMissingCards"
        Me.ListMissingCards.Size = New System.Drawing.Size(557, 118)
        Me.ListMissingCards.TabIndex = 5
        Me.ListMissingCards.UseCompatibleStateImageBehavior = False
        Me.ListMissingCards.View = System.Windows.Forms.View.Details
        '
        'ID
        '
        Me.ID.Text = "ID"
        Me.ID.Width = 75
        '
        'CardName
        '
        Me.CardName.Text = "Name"
        Me.CardName.Width = 368
        '
        'Expansion
        '
        Me.Expansion.Text = "Set"
        Me.Expansion.Width = 82
        '
        'ContextMenuErrorList
        '
        Me.ContextMenuErrorList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveListToolStripMenuItem})
        Me.ContextMenuErrorList.Name = "ContextMenuErrorList"
        Me.ContextMenuErrorList.Size = New System.Drawing.Size(129, 26)
        '
        'SaveListToolStripMenuItem
        '
        Me.SaveListToolStripMenuItem.Name = "SaveListToolStripMenuItem"
        Me.SaveListToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveListToolStripMenuItem.Text = "Save List..."
        '
        'ButtonImport
        '
        Me.ButtonImport.Location = New System.Drawing.Point(486, 183)
        Me.ButtonImport.Name = "ButtonImport"
        Me.ButtonImport.Size = New System.Drawing.Size(86, 23)
        Me.ButtonImport.TabIndex = 6
        Me.ButtonImport.Text = "Begin Import"
        Me.ButtonImport.UseVisualStyleBackColor = True
        '
        'ProgressImport
        '
        Me.ProgressImport.Location = New System.Drawing.Point(12, 183)
        Me.ProgressImport.Name = "ProgressImport"
        Me.ProgressImport.Size = New System.Drawing.Size(468, 23)
        Me.ProgressImport.TabIndex = 7
        '
        'FormMDBImporter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 342)
        Me.Controls.Add(Me.ProgressImport)
        Me.Controls.Add(Me.ButtonImport)
        Me.Controls.Add(Me.ListMissingCards)
        Me.Controls.Add(Me.ButtonBrowse)
        Me.Controls.Add(Me.txtCardsMDB)
        Me.Controls.Add(Me.lblCardsMDBPath)
        Me.Controls.Add(Me.lblInfo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormMDBImporter"
        Me.Text = "Watcher Database Version 5 Import"
        Me.ContextMenuErrorList.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents lblCardsMDBPath As System.Windows.Forms.Label
    Friend WithEvents txtCardsMDB As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBrowse As System.Windows.Forms.Button
    Friend WithEvents ListMissingCards As System.Windows.Forms.ListView
    Friend WithEvents ID As System.Windows.Forms.ColumnHeader
    Friend WithEvents CardName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Expansion As System.Windows.Forms.ColumnHeader
    Friend WithEvents ButtonImport As System.Windows.Forms.Button
    Friend WithEvents ProgressImport As System.Windows.Forms.ProgressBar
    Friend WithEvents ContextMenuErrorList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SaveListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
