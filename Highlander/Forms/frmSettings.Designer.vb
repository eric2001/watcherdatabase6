﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettings))
        Me.lblImageFilesFolder = New System.Windows.Forms.Label()
        Me.txtImageFolderPath = New System.Windows.Forms.TextBox()
        Me.buttonSave = New System.Windows.Forms.Button()
        Me.ButtonBrowseImages = New System.Windows.Forms.Button()
        Me.lblAutoLoadInventory = New System.Windows.Forms.Label()
        Me.txtDefaultInventory = New System.Windows.Forms.TextBox()
        Me.buttonBrowseInventory = New System.Windows.Forms.Button()
        Me.lblExpansionSort = New System.Windows.Forms.Label()
        Me.RadioButtonSet = New System.Windows.Forms.RadioButton()
        Me.RadioButtonDate = New System.Windows.Forms.RadioButton()
        Me.PanelCardSort = New System.Windows.Forms.Panel()
        Me.PanelCardDisplayMode = New System.Windows.Forms.Panel()
        Me.RadioButton1stEdition = New System.Windows.Forms.RadioButton()
        Me.RadioButtonType1 = New System.Windows.Forms.RadioButton()
        Me.lblCardDisplayMode = New System.Windows.Forms.Label()
        Me.PanelCardSort.SuspendLayout()
        Me.PanelCardDisplayMode.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblImageFilesFolder
        '
        Me.lblImageFilesFolder.AutoSize = True
        Me.lblImageFilesFolder.Location = New System.Drawing.Point(15, 17)
        Me.lblImageFilesFolder.Name = "lblImageFilesFolder"
        Me.lblImageFilesFolder.Size = New System.Drawing.Size(153, 13)
        Me.lblImageFilesFolder.TabIndex = 0
        Me.lblImageFilesFolder.Text = "Default Folder for Card Images:"
        '
        'txtImageFolderPath
        '
        Me.txtImageFolderPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImageFolderPath.Location = New System.Drawing.Point(174, 14)
        Me.txtImageFolderPath.Name = "txtImageFolderPath"
        Me.txtImageFolderPath.Size = New System.Drawing.Size(225, 20)
        Me.txtImageFolderPath.TabIndex = 1
        '
        'buttonSave
        '
        Me.buttonSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonSave.Location = New System.Drawing.Point(393, 123)
        Me.buttonSave.Name = "buttonSave"
        Me.buttonSave.Size = New System.Drawing.Size(75, 23)
        Me.buttonSave.TabIndex = 2
        Me.buttonSave.Text = "Save"
        Me.buttonSave.UseVisualStyleBackColor = True
        '
        'ButtonBrowseImages
        '
        Me.ButtonBrowseImages.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonBrowseImages.Location = New System.Drawing.Point(407, 12)
        Me.ButtonBrowseImages.Name = "ButtonBrowseImages"
        Me.ButtonBrowseImages.Size = New System.Drawing.Size(75, 23)
        Me.ButtonBrowseImages.TabIndex = 3
        Me.ButtonBrowseImages.Text = "Browse..."
        Me.ButtonBrowseImages.UseVisualStyleBackColor = True
        '
        'lblAutoLoadInventory
        '
        Me.lblAutoLoadInventory.AutoSize = True
        Me.lblAutoLoadInventory.Location = New System.Drawing.Point(15, 50)
        Me.lblAutoLoadInventory.Name = "lblAutoLoadInventory"
        Me.lblAutoLoadInventory.Size = New System.Drawing.Size(152, 13)
        Me.lblAutoLoadInventory.TabIndex = 4
        Me.lblAutoLoadInventory.Text = "Load Inventory File on Startup:"
        '
        'txtDefaultInventory
        '
        Me.txtDefaultInventory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDefaultInventory.Location = New System.Drawing.Point(174, 47)
        Me.txtDefaultInventory.Name = "txtDefaultInventory"
        Me.txtDefaultInventory.Size = New System.Drawing.Size(225, 20)
        Me.txtDefaultInventory.TabIndex = 5
        '
        'buttonBrowseInventory
        '
        Me.buttonBrowseInventory.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonBrowseInventory.Location = New System.Drawing.Point(407, 45)
        Me.buttonBrowseInventory.Name = "buttonBrowseInventory"
        Me.buttonBrowseInventory.Size = New System.Drawing.Size(75, 23)
        Me.buttonBrowseInventory.TabIndex = 6
        Me.buttonBrowseInventory.Text = "Browse..."
        Me.buttonBrowseInventory.UseVisualStyleBackColor = True
        '
        'lblExpansionSort
        '
        Me.lblExpansionSort.AutoSize = True
        Me.lblExpansionSort.Location = New System.Drawing.Point(15, 76)
        Me.lblExpansionSort.Name = "lblExpansionSort"
        Me.lblExpansionSort.Size = New System.Drawing.Size(120, 13)
        Me.lblExpansionSort.TabIndex = 7
        Me.lblExpansionSort.Text = "Sort Expansion Sets By:"
        '
        'RadioButtonSet
        '
        Me.RadioButtonSet.AutoSize = True
        Me.RadioButtonSet.Location = New System.Drawing.Point(1, 1)
        Me.RadioButtonSet.Name = "RadioButtonSet"
        Me.RadioButtonSet.Size = New System.Drawing.Size(72, 17)
        Me.RadioButtonSet.TabIndex = 8
        Me.RadioButtonSet.TabStop = True
        Me.RadioButtonSet.Text = "Set Name"
        Me.RadioButtonSet.UseVisualStyleBackColor = True
        '
        'RadioButtonDate
        '
        Me.RadioButtonDate.AutoSize = True
        Me.RadioButtonDate.Location = New System.Drawing.Point(79, 1)
        Me.RadioButtonDate.Name = "RadioButtonDate"
        Me.RadioButtonDate.Size = New System.Drawing.Size(90, 17)
        Me.RadioButtonDate.TabIndex = 9
        Me.RadioButtonDate.TabStop = True
        Me.RadioButtonDate.Text = "Release Date"
        Me.RadioButtonDate.UseVisualStyleBackColor = True
        '
        'PanelCardSort
        '
        Me.PanelCardSort.Controls.Add(Me.RadioButtonSet)
        Me.PanelCardSort.Controls.Add(Me.RadioButtonDate)
        Me.PanelCardSort.Location = New System.Drawing.Point(174, 74)
        Me.PanelCardSort.Name = "PanelCardSort"
        Me.PanelCardSort.Size = New System.Drawing.Size(200, 20)
        Me.PanelCardSort.TabIndex = 11
        '
        'PanelCardDisplayMode
        '
        Me.PanelCardDisplayMode.Controls.Add(Me.RadioButton1stEdition)
        Me.PanelCardDisplayMode.Controls.Add(Me.RadioButtonType1)
        Me.PanelCardDisplayMode.Location = New System.Drawing.Point(174, 100)
        Me.PanelCardDisplayMode.Name = "PanelCardDisplayMode"
        Me.PanelCardDisplayMode.Size = New System.Drawing.Size(200, 20)
        Me.PanelCardDisplayMode.TabIndex = 13
        '
        'RadioButton1stEdition
        '
        Me.RadioButton1stEdition.AutoSize = True
        Me.RadioButton1stEdition.Location = New System.Drawing.Point(1, 1)
        Me.RadioButton1stEdition.Name = "RadioButton1stEdition"
        Me.RadioButton1stEdition.Size = New System.Drawing.Size(74, 17)
        Me.RadioButton1stEdition.TabIndex = 8
        Me.RadioButton1stEdition.TabStop = True
        Me.RadioButton1stEdition.Text = "1st Edition"
        Me.RadioButton1stEdition.UseVisualStyleBackColor = True
        '
        'RadioButtonType1
        '
        Me.RadioButtonType1.AutoSize = True
        Me.RadioButtonType1.Location = New System.Drawing.Point(79, 1)
        Me.RadioButtonType1.Name = "RadioButtonType1"
        Me.RadioButtonType1.Size = New System.Drawing.Size(58, 17)
        Me.RadioButtonType1.TabIndex = 9
        Me.RadioButtonType1.TabStop = True
        Me.RadioButtonType1.Text = "Type 1"
        Me.RadioButtonType1.UseVisualStyleBackColor = True
        '
        'lblCardDisplayMode
        '
        Me.lblCardDisplayMode.AutoSize = True
        Me.lblCardDisplayMode.Location = New System.Drawing.Point(15, 102)
        Me.lblCardDisplayMode.Name = "lblCardDisplayMode"
        Me.lblCardDisplayMode.Size = New System.Drawing.Size(74, 13)
        Me.lblCardDisplayMode.TabIndex = 12
        Me.lblCardDisplayMode.Text = "Display Mode:"
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(494, 158)
        Me.Controls.Add(Me.PanelCardDisplayMode)
        Me.Controls.Add(Me.lblCardDisplayMode)
        Me.Controls.Add(Me.PanelCardSort)
        Me.Controls.Add(Me.lblExpansionSort)
        Me.Controls.Add(Me.buttonBrowseInventory)
        Me.Controls.Add(Me.txtDefaultInventory)
        Me.Controls.Add(Me.lblAutoLoadInventory)
        Me.Controls.Add(Me.ButtonBrowseImages)
        Me.Controls.Add(Me.buttonSave)
        Me.Controls.Add(Me.txtImageFolderPath)
        Me.Controls.Add(Me.lblImageFilesFolder)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSettings"
        Me.Text = "Settings"
        Me.PanelCardSort.ResumeLayout(False)
        Me.PanelCardSort.PerformLayout()
        Me.PanelCardDisplayMode.ResumeLayout(False)
        Me.PanelCardDisplayMode.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblImageFilesFolder As System.Windows.Forms.Label
    Friend WithEvents txtImageFolderPath As System.Windows.Forms.TextBox
    Friend WithEvents buttonSave As System.Windows.Forms.Button
    Friend WithEvents ButtonBrowseImages As System.Windows.Forms.Button
    Friend WithEvents lblAutoLoadInventory As System.Windows.Forms.Label
    Friend WithEvents txtDefaultInventory As System.Windows.Forms.TextBox
    Friend WithEvents buttonBrowseInventory As System.Windows.Forms.Button
    Friend WithEvents lblExpansionSort As System.Windows.Forms.Label
    Friend WithEvents RadioButtonSet As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonDate As System.Windows.Forms.RadioButton
    Friend WithEvents PanelCardSort As System.Windows.Forms.Panel
    Friend WithEvents PanelCardDisplayMode As System.Windows.Forms.Panel
    Friend WithEvents RadioButton1stEdition As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonType1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblCardDisplayMode As System.Windows.Forms.Label
End Class
