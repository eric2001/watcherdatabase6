﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class frmReportViewer

    Private Sub frmReportViewer_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Make sure page orientation is set to Portrait, and that paper type is set to Letter.
        Dim pageSettingsPortrait As New System.Drawing.Printing.PageSettings
        pageSettingsPortrait.Landscape = False
        Dim intPaperCounter As Integer = 0
        While intPaperCounter < ReportViewer1.PrinterSettings.PaperSizes.Count
            If ReportViewer1.PrinterSettings.PaperSizes(intPaperCounter).Kind = System.Drawing.Printing.PaperKind.Letter Then
                pageSettingsPortrait.PaperSize = ReportViewer1.PrinterSettings.PaperSizes(intPaperCounter)
                Exit While
            End If
            intPaperCounter = intPaperCounter + 1
        End While
        ReportViewer1.SetPageSettings(pageSettingsPortrait)

        Me.ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class