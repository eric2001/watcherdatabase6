﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreateImageFolders
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCreateImageFolders))
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.lblStartingFolder = New System.Windows.Forms.Label()
        Me.txtImageFolder = New System.Windows.Forms.TextBox()
        Me.buttonBrowse = New System.Windows.Forms.Button()
        Me.buttonCreateFolders = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblInfo
        '
        Me.lblInfo.Location = New System.Drawing.Point(12, 9)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(360, 76)
        Me.lblInfo.TabIndex = 0
        Me.lblInfo.Text = "This screen will populate an empty folder with the default picture sub-folders fo" & _
    "r each set." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Please provide the location of the empty folder you wish to use b" & _
    "elow, then press ""Create Folders""."
        '
        'lblStartingFolder
        '
        Me.lblStartingFolder.AutoSize = True
        Me.lblStartingFolder.Location = New System.Drawing.Point(12, 85)
        Me.lblStartingFolder.Name = "lblStartingFolder"
        Me.lblStartingFolder.Size = New System.Drawing.Size(76, 13)
        Me.lblStartingFolder.TabIndex = 1
        Me.lblStartingFolder.Text = "Images Folder:"
        '
        'txtImageFolder
        '
        Me.txtImageFolder.Location = New System.Drawing.Point(94, 82)
        Me.txtImageFolder.Name = "txtImageFolder"
        Me.txtImageFolder.Size = New System.Drawing.Size(197, 20)
        Me.txtImageFolder.TabIndex = 2
        '
        'buttonBrowse
        '
        Me.buttonBrowse.Location = New System.Drawing.Point(297, 80)
        Me.buttonBrowse.Name = "buttonBrowse"
        Me.buttonBrowse.Size = New System.Drawing.Size(75, 23)
        Me.buttonBrowse.TabIndex = 3
        Me.buttonBrowse.Text = "Browse..."
        Me.buttonBrowse.UseVisualStyleBackColor = True
        '
        'buttonCreateFolders
        '
        Me.buttonCreateFolders.Location = New System.Drawing.Point(136, 108)
        Me.buttonCreateFolders.Name = "buttonCreateFolders"
        Me.buttonCreateFolders.Size = New System.Drawing.Size(112, 23)
        Me.buttonCreateFolders.TabIndex = 4
        Me.buttonCreateFolders.Text = "Create Folders"
        Me.buttonCreateFolders.UseVisualStyleBackColor = True
        '
        'frmCreateImageFolders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 141)
        Me.Controls.Add(Me.buttonCreateFolders)
        Me.Controls.Add(Me.buttonBrowse)
        Me.Controls.Add(Me.txtImageFolder)
        Me.Controls.Add(Me.lblStartingFolder)
        Me.Controls.Add(Me.lblInfo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCreateImageFolders"
        Me.Text = "Create Image Folders"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents lblStartingFolder As System.Windows.Forms.Label
    Friend WithEvents txtImageFolder As System.Windows.Forms.TextBox
    Friend WithEvents buttonBrowse As System.Windows.Forms.Button
    Friend WithEvents buttonCreateFolders As System.Windows.Forms.Button
End Class
