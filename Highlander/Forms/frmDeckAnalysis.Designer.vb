﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeckAnalysis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeckAnalysis))
        Me.LblCardsType = New System.Windows.Forms.Label()
        Me.lblCardsImmortal = New System.Windows.Forms.Label()
        Me.lstCardsByType = New System.Windows.Forms.ListView()
        Me.ColumnType = New System.Windows.Forms.ColumnHeader()
        Me.ColumnNumber = New System.Windows.Forms.ColumnHeader()
        Me.ColumnPercent = New System.Windows.Forms.ColumnHeader()
        Me.lstCardsByImmortal = New System.Windows.Forms.ListView()
        Me.ColumnImmortal = New System.Windows.Forms.ColumnHeader()
        Me.ColumnTotalNum = New System.Windows.Forms.ColumnHeader()
        Me.ColumnTotalPercent = New System.Windows.Forms.ColumnHeader()
        Me.ColumnReserved = New System.Windows.Forms.ColumnHeader()
        Me.ColumnRestricted = New System.Windows.Forms.ColumnHeader()
        Me.PictureCardsTypeChart = New System.Windows.Forms.PictureBox()
        Me.PictureCardsImmortalChart = New System.Windows.Forms.PictureBox()
        Me.TabControlAnalysis = New System.Windows.Forms.TabControl()
        Me.TabPageSummary = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DeckToughnessGem3E = New System.Windows.Forms.PictureBox()
        Me.DeckReason = New System.Windows.Forms.Label()
        Me.DeckMasterGem = New System.Windows.Forms.PictureBox()
        Me.DeckToughness = New System.Windows.Forms.Label()
        Me.DeckAgility = New System.Windows.Forms.Label()
        Me.DeckReasonGem = New System.Windows.Forms.PictureBox()
        Me.DeckStrengthGem = New System.Windows.Forms.PictureBox()
        Me.DeckAgilityGem = New System.Windows.Forms.PictureBox()
        Me.DeckEmpathy = New System.Windows.Forms.Label()
        Me.DeckStrength = New System.Windows.Forms.Label()
        Me.DeckToughnessGem = New System.Windows.Forms.PictureBox()
        Me.DeckMaster = New System.Windows.Forms.Label()
        Me.DeckEmpathyGem = New System.Windows.Forms.PictureBox()
        Me.GroupPersonaAttributes = New System.Windows.Forms.GroupBox()
        Me.ImmortalReasonGem = New System.Windows.Forms.PictureBox()
        Me.ImmortalEmpathyGem = New System.Windows.Forms.PictureBox()
        Me.ImmortalToughnessGem3E = New System.Windows.Forms.PictureBox()
        Me.ImmortalToughnessGem = New System.Windows.Forms.PictureBox()
        Me.ImmortalAgilityGem = New System.Windows.Forms.PictureBox()
        Me.ImmortalReason = New System.Windows.Forms.Label()
        Me.ImmortalMasterGem = New System.Windows.Forms.PictureBox()
        Me.ImmortalToughness = New System.Windows.Forms.Label()
        Me.ImmortalAgility = New System.Windows.Forms.Label()
        Me.ImmortalStrengthGem = New System.Windows.Forms.PictureBox()
        Me.ImmortalMaster = New System.Windows.Forms.Label()
        Me.ImmortalEmpathy = New System.Windows.Forms.Label()
        Me.ImmortalStrength = New System.Windows.Forms.Label()
        Me.lblPreGameSize = New System.Windows.Forms.Label()
        Me.lblDeckSize = New System.Windows.Forms.Label()
        Me.lblDeckImmortal = New System.Windows.Forms.Label()
        Me.lblDeckTitle = New System.Windows.Forms.Label()
        Me.TabPageFormat = New System.Windows.Forms.TabPage()
        Me.lblType2Status = New System.Windows.Forms.Label()
        Me.lblType1Status = New System.Windows.Forms.Label()
        Me.lblMLEStatus = New System.Windows.Forms.Label()
        Me.lbl1stEditionStatus = New System.Windows.Forms.Label()
        Me.lblType2 = New System.Windows.Forms.Label()
        Me.lblType1 = New System.Windows.Forms.Label()
        Me.lblMLE = New System.Windows.Forms.Label()
        Me.lbl1stEdtion = New System.Windows.Forms.Label()
        Me.TabPageH1E = New System.Windows.Forms.TabPage()
        Me.lst1stEdition = New System.Windows.Forms.ListView()
        Me.ColumnCardTitle = New System.Windows.Forms.ColumnHeader()
        Me.ColumnDetails = New System.Windows.Forms.ColumnHeader()
        Me.TabPageMLE = New System.Windows.Forms.TabPage()
        Me.lstMLE = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader()
        Me.TabPageType1 = New System.Windows.Forms.TabPage()
        Me.lstType1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader()
        Me.TabPageType2 = New System.Windows.Forms.TabPage()
        Me.lstType2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader()
        Me.lblHighlanderProStatus = New System.Windows.Forms.Label()
        Me.lblHighlanderPro = New System.Windows.Forms.Label()
        Me.TabPageHighlanderPro = New System.Windows.Forms.TabPage()
        Me.lstHighlanderPro = New System.Windows.Forms.ListView()
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader()
        Me.ColumnHeader8 = New System.Windows.Forms.ColumnHeader()
        CType(Me.PictureCardsTypeChart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureCardsImmortalChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlAnalysis.SuspendLayout()
        Me.TabPageSummary.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DeckToughnessGem3E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeckMasterGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeckReasonGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeckStrengthGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeckAgilityGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeckToughnessGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeckEmpathyGem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPersonaAttributes.SuspendLayout()
        CType(Me.ImmortalReasonGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImmortalEmpathyGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImmortalToughnessGem3E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImmortalToughnessGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImmortalAgilityGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImmortalMasterGem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImmortalStrengthGem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageFormat.SuspendLayout()
        Me.TabPageH1E.SuspendLayout()
        Me.TabPageMLE.SuspendLayout()
        Me.TabPageType1.SuspendLayout()
        Me.TabPageType2.SuspendLayout()
        Me.TabPageHighlanderPro.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblCardsType
        '
        Me.LblCardsType.AutoSize = True
        Me.LblCardsType.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.LblCardsType.Location = New System.Drawing.Point(6, 147)
        Me.LblCardsType.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblCardsType.Name = "LblCardsType"
        Me.LblCardsType.Size = New System.Drawing.Size(107, 18)
        Me.LblCardsType.TabIndex = 0
        Me.LblCardsType.Text = "Cards by Type:"
        '
        'lblCardsImmortal
        '
        Me.lblCardsImmortal.AutoSize = True
        Me.lblCardsImmortal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblCardsImmortal.Location = New System.Drawing.Point(303, 147)
        Me.lblCardsImmortal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCardsImmortal.Name = "lblCardsImmortal"
        Me.lblCardsImmortal.Size = New System.Drawing.Size(133, 18)
        Me.lblCardsImmortal.TabIndex = 1
        Me.lblCardsImmortal.Text = "Cards by Sub-Title:"
        '
        'lstCardsByType
        '
        Me.lstCardsByType.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnType, Me.ColumnNumber, Me.ColumnPercent})
        Me.lstCardsByType.Location = New System.Drawing.Point(6, 171)
        Me.lstCardsByType.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstCardsByType.Name = "lstCardsByType"
        Me.lstCardsByType.Size = New System.Drawing.Size(293, 191)
        Me.lstCardsByType.TabIndex = 2
        Me.lstCardsByType.UseCompatibleStateImageBehavior = False
        Me.lstCardsByType.View = System.Windows.Forms.View.Details
        '
        'ColumnType
        '
        Me.ColumnType.Text = "Type"
        Me.ColumnType.Width = 134
        '
        'ColumnNumber
        '
        Me.ColumnNumber.Text = "#"
        Me.ColumnNumber.Width = 39
        '
        'ColumnPercent
        '
        Me.ColumnPercent.Text = "%"
        Me.ColumnPercent.Width = 39
        '
        'lstCardsByImmortal
        '
        Me.lstCardsByImmortal.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnImmortal, Me.ColumnTotalNum, Me.ColumnTotalPercent, Me.ColumnReserved, Me.ColumnRestricted})
        Me.lstCardsByImmortal.Location = New System.Drawing.Point(307, 171)
        Me.lstCardsByImmortal.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstCardsByImmortal.Name = "lstCardsByImmortal"
        Me.lstCardsByImmortal.Size = New System.Drawing.Size(352, 191)
        Me.lstCardsByImmortal.TabIndex = 3
        Me.lstCardsByImmortal.UseCompatibleStateImageBehavior = False
        Me.lstCardsByImmortal.View = System.Windows.Forms.View.Details
        '
        'ColumnImmortal
        '
        Me.ColumnImmortal.Text = "Immortal"
        Me.ColumnImmortal.Width = 123
        '
        'ColumnTotalNum
        '
        Me.ColumnTotalNum.Text = "#"
        Me.ColumnTotalNum.Width = 39
        '
        'ColumnTotalPercent
        '
        Me.ColumnTotalPercent.Text = "%"
        Me.ColumnTotalPercent.Width = 39
        '
        'ColumnReserved
        '
        Me.ColumnReserved.Text = "R"
        Me.ColumnReserved.Width = 39
        '
        'ColumnRestricted
        '
        Me.ColumnRestricted.Text = "S"
        Me.ColumnRestricted.Width = 39
        '
        'PictureCardsTypeChart
        '
        Me.PictureCardsTypeChart.Location = New System.Drawing.Point(6, 369)
        Me.PictureCardsTypeChart.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PictureCardsTypeChart.Name = "PictureCardsTypeChart"
        Me.PictureCardsTypeChart.Size = New System.Drawing.Size(294, 186)
        Me.PictureCardsTypeChart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureCardsTypeChart.TabIndex = 4
        Me.PictureCardsTypeChart.TabStop = False
        '
        'PictureCardsImmortalChart
        '
        Me.PictureCardsImmortalChart.Location = New System.Drawing.Point(307, 369)
        Me.PictureCardsImmortalChart.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PictureCardsImmortalChart.Name = "PictureCardsImmortalChart"
        Me.PictureCardsImmortalChart.Size = New System.Drawing.Size(352, 186)
        Me.PictureCardsImmortalChart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureCardsImmortalChart.TabIndex = 5
        Me.PictureCardsImmortalChart.TabStop = False
        '
        'TabControlAnalysis
        '
        Me.TabControlAnalysis.Controls.Add(Me.TabPageSummary)
        Me.TabControlAnalysis.Controls.Add(Me.TabPageFormat)
        Me.TabControlAnalysis.Controls.Add(Me.TabPageH1E)
        Me.TabControlAnalysis.Controls.Add(Me.TabPageMLE)
        Me.TabControlAnalysis.Controls.Add(Me.TabPageType1)
        Me.TabControlAnalysis.Controls.Add(Me.TabPageType2)
        Me.TabControlAnalysis.Controls.Add(Me.TabPageHighlanderPro)
        Me.TabControlAnalysis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlAnalysis.Location = New System.Drawing.Point(0, 0)
        Me.TabControlAnalysis.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabControlAnalysis.Name = "TabControlAnalysis"
        Me.TabControlAnalysis.SelectedIndex = 0
        Me.TabControlAnalysis.Size = New System.Drawing.Size(679, 591)
        Me.TabControlAnalysis.TabIndex = 6
        '
        'TabPageSummary
        '
        Me.TabPageSummary.Controls.Add(Me.GroupBox1)
        Me.TabPageSummary.Controls.Add(Me.GroupPersonaAttributes)
        Me.TabPageSummary.Controls.Add(Me.lblPreGameSize)
        Me.TabPageSummary.Controls.Add(Me.lblDeckSize)
        Me.TabPageSummary.Controls.Add(Me.lblDeckImmortal)
        Me.TabPageSummary.Controls.Add(Me.lblDeckTitle)
        Me.TabPageSummary.Controls.Add(Me.LblCardsType)
        Me.TabPageSummary.Controls.Add(Me.PictureCardsImmortalChart)
        Me.TabPageSummary.Controls.Add(Me.lblCardsImmortal)
        Me.TabPageSummary.Controls.Add(Me.PictureCardsTypeChart)
        Me.TabPageSummary.Controls.Add(Me.lstCardsByType)
        Me.TabPageSummary.Controls.Add(Me.lstCardsByImmortal)
        Me.TabPageSummary.Location = New System.Drawing.Point(4, 24)
        Me.TabPageSummary.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageSummary.Name = "TabPageSummary"
        Me.TabPageSummary.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageSummary.Size = New System.Drawing.Size(671, 563)
        Me.TabPageSummary.TabIndex = 0
        Me.TabPageSummary.Text = "Summary"
        Me.TabPageSummary.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DeckToughnessGem3E)
        Me.GroupBox1.Controls.Add(Me.DeckReason)
        Me.GroupBox1.Controls.Add(Me.DeckMasterGem)
        Me.GroupBox1.Controls.Add(Me.DeckToughness)
        Me.GroupBox1.Controls.Add(Me.DeckAgility)
        Me.GroupBox1.Controls.Add(Me.DeckReasonGem)
        Me.GroupBox1.Controls.Add(Me.DeckStrengthGem)
        Me.GroupBox1.Controls.Add(Me.DeckAgilityGem)
        Me.GroupBox1.Controls.Add(Me.DeckEmpathy)
        Me.GroupBox1.Controls.Add(Me.DeckStrength)
        Me.GroupBox1.Controls.Add(Me.DeckToughnessGem)
        Me.GroupBox1.Controls.Add(Me.DeckMaster)
        Me.GroupBox1.Controls.Add(Me.DeckEmpathyGem)
        Me.GroupBox1.Location = New System.Drawing.Point(310, 83)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(294, 60)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Deck Attributes"
        '
        'DeckToughnessGem3E
        '
        Me.DeckToughnessGem3E.Image = CType(resources.GetObject("DeckToughnessGem3E.Image"), System.Drawing.Image)
        Me.DeckToughnessGem3E.Location = New System.Drawing.Point(174, 23)
        Me.DeckToughnessGem3E.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckToughnessGem3E.Name = "DeckToughnessGem3E"
        Me.DeckToughnessGem3E.Size = New System.Drawing.Size(16, 19)
        Me.DeckToughnessGem3E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckToughnessGem3E.TabIndex = 28
        Me.DeckToughnessGem3E.TabStop = False
        '
        'DeckReason
        '
        Me.DeckReason.AutoSize = True
        Me.DeckReason.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.DeckReason.Location = New System.Drawing.Point(244, 22)
        Me.DeckReason.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DeckReason.Name = "DeckReason"
        Me.DeckReason.Size = New System.Drawing.Size(16, 17)
        Me.DeckReason.TabIndex = 19
        Me.DeckReason.Text = "0"
        Me.DeckReason.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DeckMasterGem
        '
        Me.DeckMasterGem.Image = CType(resources.GetObject("DeckMasterGem.Image"), System.Drawing.Image)
        Me.DeckMasterGem.Location = New System.Drawing.Point(31, 22)
        Me.DeckMasterGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckMasterGem.Name = "DeckMasterGem"
        Me.DeckMasterGem.Size = New System.Drawing.Size(17, 20)
        Me.DeckMasterGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckMasterGem.TabIndex = 16
        Me.DeckMasterGem.TabStop = False
        '
        'DeckToughness
        '
        Me.DeckToughness.AutoSize = True
        Me.DeckToughness.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.DeckToughness.Location = New System.Drawing.Point(150, 22)
        Me.DeckToughness.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DeckToughness.Name = "DeckToughness"
        Me.DeckToughness.Size = New System.Drawing.Size(16, 17)
        Me.DeckToughness.TabIndex = 23
        Me.DeckToughness.Text = "0"
        Me.DeckToughness.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DeckAgility
        '
        Me.DeckAgility.AutoSize = True
        Me.DeckAgility.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.DeckAgility.Location = New System.Drawing.Point(56, 22)
        Me.DeckAgility.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DeckAgility.Name = "DeckAgility"
        Me.DeckAgility.Size = New System.Drawing.Size(16, 17)
        Me.DeckAgility.TabIndex = 13
        Me.DeckAgility.Text = "0"
        Me.DeckAgility.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DeckReasonGem
        '
        Me.DeckReasonGem.Image = CType(resources.GetObject("DeckReasonGem.Image"), System.Drawing.Image)
        Me.DeckReasonGem.Location = New System.Drawing.Point(267, 22)
        Me.DeckReasonGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckReasonGem.Name = "DeckReasonGem"
        Me.DeckReasonGem.Size = New System.Drawing.Size(17, 20)
        Me.DeckReasonGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckReasonGem.TabIndex = 18
        Me.DeckReasonGem.TabStop = False
        '
        'DeckStrengthGem
        '
        Me.DeckStrengthGem.Image = CType(resources.GetObject("DeckStrengthGem.Image"), System.Drawing.Image)
        Me.DeckStrengthGem.Location = New System.Drawing.Point(126, 22)
        Me.DeckStrengthGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckStrengthGem.Name = "DeckStrengthGem"
        Me.DeckStrengthGem.Size = New System.Drawing.Size(17, 20)
        Me.DeckStrengthGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckStrengthGem.TabIndex = 20
        Me.DeckStrengthGem.TabStop = False
        '
        'DeckAgilityGem
        '
        Me.DeckAgilityGem.Image = CType(resources.GetObject("DeckAgilityGem.Image"), System.Drawing.Image)
        Me.DeckAgilityGem.Location = New System.Drawing.Point(79, 22)
        Me.DeckAgilityGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckAgilityGem.Name = "DeckAgilityGem"
        Me.DeckAgilityGem.Size = New System.Drawing.Size(16, 20)
        Me.DeckAgilityGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckAgilityGem.TabIndex = 12
        Me.DeckAgilityGem.TabStop = False
        '
        'DeckEmpathy
        '
        Me.DeckEmpathy.AutoSize = True
        Me.DeckEmpathy.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.DeckEmpathy.Location = New System.Drawing.Point(197, 22)
        Me.DeckEmpathy.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DeckEmpathy.Name = "DeckEmpathy"
        Me.DeckEmpathy.Size = New System.Drawing.Size(16, 17)
        Me.DeckEmpathy.TabIndex = 15
        Me.DeckEmpathy.Text = "0"
        Me.DeckEmpathy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DeckStrength
        '
        Me.DeckStrength.AutoSize = True
        Me.DeckStrength.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.DeckStrength.Location = New System.Drawing.Point(103, 22)
        Me.DeckStrength.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DeckStrength.Name = "DeckStrength"
        Me.DeckStrength.Size = New System.Drawing.Size(16, 17)
        Me.DeckStrength.TabIndex = 21
        Me.DeckStrength.Text = "0"
        Me.DeckStrength.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DeckToughnessGem
        '
        Me.DeckToughnessGem.Image = CType(resources.GetObject("DeckToughnessGem.Image"), System.Drawing.Image)
        Me.DeckToughnessGem.Location = New System.Drawing.Point(174, 22)
        Me.DeckToughnessGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckToughnessGem.Name = "DeckToughnessGem"
        Me.DeckToughnessGem.Size = New System.Drawing.Size(16, 20)
        Me.DeckToughnessGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckToughnessGem.TabIndex = 22
        Me.DeckToughnessGem.TabStop = False
        '
        'DeckMaster
        '
        Me.DeckMaster.AutoSize = True
        Me.DeckMaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.DeckMaster.Location = New System.Drawing.Point(8, 22)
        Me.DeckMaster.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DeckMaster.Name = "DeckMaster"
        Me.DeckMaster.Size = New System.Drawing.Size(16, 17)
        Me.DeckMaster.TabIndex = 17
        Me.DeckMaster.Text = "0"
        Me.DeckMaster.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DeckEmpathyGem
        '
        Me.DeckEmpathyGem.Image = CType(resources.GetObject("DeckEmpathyGem.Image"), System.Drawing.Image)
        Me.DeckEmpathyGem.Location = New System.Drawing.Point(220, 22)
        Me.DeckEmpathyGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DeckEmpathyGem.Name = "DeckEmpathyGem"
        Me.DeckEmpathyGem.Size = New System.Drawing.Size(16, 20)
        Me.DeckEmpathyGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.DeckEmpathyGem.TabIndex = 14
        Me.DeckEmpathyGem.TabStop = False
        '
        'GroupPersonaAttributes
        '
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalReasonGem)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalEmpathyGem)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalToughnessGem3E)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalToughnessGem)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalAgilityGem)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalReason)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalMasterGem)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalToughness)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalAgility)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalStrengthGem)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalMaster)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalEmpathy)
        Me.GroupPersonaAttributes.Controls.Add(Me.ImmortalStrength)
        Me.GroupPersonaAttributes.Location = New System.Drawing.Point(9, 83)
        Me.GroupPersonaAttributes.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupPersonaAttributes.Name = "GroupPersonaAttributes"
        Me.GroupPersonaAttributes.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupPersonaAttributes.Size = New System.Drawing.Size(294, 60)
        Me.GroupPersonaAttributes.TabIndex = 33
        Me.GroupPersonaAttributes.TabStop = False
        Me.GroupPersonaAttributes.Text = "Persona Attributes"
        '
        'ImmortalReasonGem
        '
        Me.ImmortalReasonGem.Image = CType(resources.GetObject("ImmortalReasonGem.Image"), System.Drawing.Image)
        Me.ImmortalReasonGem.Location = New System.Drawing.Point(264, 18)
        Me.ImmortalReasonGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalReasonGem.Name = "ImmortalReasonGem"
        Me.ImmortalReasonGem.Size = New System.Drawing.Size(17, 20)
        Me.ImmortalReasonGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalReasonGem.TabIndex = 29
        Me.ImmortalReasonGem.TabStop = False
        '
        'ImmortalEmpathyGem
        '
        Me.ImmortalEmpathyGem.Image = CType(resources.GetObject("ImmortalEmpathyGem.Image"), System.Drawing.Image)
        Me.ImmortalEmpathyGem.Location = New System.Drawing.Point(216, 18)
        Me.ImmortalEmpathyGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalEmpathyGem.Name = "ImmortalEmpathyGem"
        Me.ImmortalEmpathyGem.Size = New System.Drawing.Size(16, 20)
        Me.ImmortalEmpathyGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalEmpathyGem.TabIndex = 28
        Me.ImmortalEmpathyGem.TabStop = False
        '
        'ImmortalToughnessGem3E
        '
        Me.ImmortalToughnessGem3E.Image = CType(resources.GetObject("ImmortalToughnessGem3E.Image"), System.Drawing.Image)
        Me.ImmortalToughnessGem3E.Location = New System.Drawing.Point(170, 18)
        Me.ImmortalToughnessGem3E.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalToughnessGem3E.Name = "ImmortalToughnessGem3E"
        Me.ImmortalToughnessGem3E.Size = New System.Drawing.Size(16, 19)
        Me.ImmortalToughnessGem3E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalToughnessGem3E.TabIndex = 27
        Me.ImmortalToughnessGem3E.TabStop = False
        Me.ImmortalToughnessGem3E.Visible = False
        '
        'ImmortalToughnessGem
        '
        Me.ImmortalToughnessGem.Image = CType(resources.GetObject("ImmortalToughnessGem.Image"), System.Drawing.Image)
        Me.ImmortalToughnessGem.Location = New System.Drawing.Point(170, 18)
        Me.ImmortalToughnessGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalToughnessGem.Name = "ImmortalToughnessGem"
        Me.ImmortalToughnessGem.Size = New System.Drawing.Size(16, 20)
        Me.ImmortalToughnessGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalToughnessGem.TabIndex = 26
        Me.ImmortalToughnessGem.TabStop = False
        '
        'ImmortalAgilityGem
        '
        Me.ImmortalAgilityGem.Image = CType(resources.GetObject("ImmortalAgilityGem.Image"), System.Drawing.Image)
        Me.ImmortalAgilityGem.Location = New System.Drawing.Point(76, 18)
        Me.ImmortalAgilityGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalAgilityGem.Name = "ImmortalAgilityGem"
        Me.ImmortalAgilityGem.Size = New System.Drawing.Size(16, 20)
        Me.ImmortalAgilityGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalAgilityGem.TabIndex = 25
        Me.ImmortalAgilityGem.TabStop = False
        '
        'ImmortalReason
        '
        Me.ImmortalReason.AutoSize = True
        Me.ImmortalReason.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ImmortalReason.Location = New System.Drawing.Point(240, 18)
        Me.ImmortalReason.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImmortalReason.Name = "ImmortalReason"
        Me.ImmortalReason.Size = New System.Drawing.Size(16, 17)
        Me.ImmortalReason.TabIndex = 19
        Me.ImmortalReason.Text = "0"
        Me.ImmortalReason.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ImmortalMasterGem
        '
        Me.ImmortalMasterGem.Image = CType(resources.GetObject("ImmortalMasterGem.Image"), System.Drawing.Image)
        Me.ImmortalMasterGem.Location = New System.Drawing.Point(28, 18)
        Me.ImmortalMasterGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalMasterGem.Name = "ImmortalMasterGem"
        Me.ImmortalMasterGem.Size = New System.Drawing.Size(17, 20)
        Me.ImmortalMasterGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalMasterGem.TabIndex = 16
        Me.ImmortalMasterGem.TabStop = False
        '
        'ImmortalToughness
        '
        Me.ImmortalToughness.AutoSize = True
        Me.ImmortalToughness.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ImmortalToughness.Location = New System.Drawing.Point(147, 18)
        Me.ImmortalToughness.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImmortalToughness.Name = "ImmortalToughness"
        Me.ImmortalToughness.Size = New System.Drawing.Size(16, 17)
        Me.ImmortalToughness.TabIndex = 23
        Me.ImmortalToughness.Text = "0"
        Me.ImmortalToughness.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ImmortalAgility
        '
        Me.ImmortalAgility.AutoSize = True
        Me.ImmortalAgility.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ImmortalAgility.Location = New System.Drawing.Point(52, 18)
        Me.ImmortalAgility.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImmortalAgility.Name = "ImmortalAgility"
        Me.ImmortalAgility.Size = New System.Drawing.Size(16, 17)
        Me.ImmortalAgility.TabIndex = 13
        Me.ImmortalAgility.Text = "0"
        Me.ImmortalAgility.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ImmortalStrengthGem
        '
        Me.ImmortalStrengthGem.Image = CType(resources.GetObject("ImmortalStrengthGem.Image"), System.Drawing.Image)
        Me.ImmortalStrengthGem.Location = New System.Drawing.Point(124, 18)
        Me.ImmortalStrengthGem.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ImmortalStrengthGem.Name = "ImmortalStrengthGem"
        Me.ImmortalStrengthGem.Size = New System.Drawing.Size(17, 20)
        Me.ImmortalStrengthGem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.ImmortalStrengthGem.TabIndex = 20
        Me.ImmortalStrengthGem.TabStop = False
        '
        'ImmortalMaster
        '
        Me.ImmortalMaster.AutoSize = True
        Me.ImmortalMaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ImmortalMaster.Location = New System.Drawing.Point(6, 18)
        Me.ImmortalMaster.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImmortalMaster.Name = "ImmortalMaster"
        Me.ImmortalMaster.Size = New System.Drawing.Size(16, 17)
        Me.ImmortalMaster.TabIndex = 17
        Me.ImmortalMaster.Text = "0"
        Me.ImmortalMaster.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ImmortalEmpathy
        '
        Me.ImmortalEmpathy.AutoSize = True
        Me.ImmortalEmpathy.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ImmortalEmpathy.Location = New System.Drawing.Point(192, 18)
        Me.ImmortalEmpathy.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImmortalEmpathy.Name = "ImmortalEmpathy"
        Me.ImmortalEmpathy.Size = New System.Drawing.Size(16, 17)
        Me.ImmortalEmpathy.TabIndex = 15
        Me.ImmortalEmpathy.Text = "0"
        Me.ImmortalEmpathy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ImmortalStrength
        '
        Me.ImmortalStrength.AutoSize = True
        Me.ImmortalStrength.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.ImmortalStrength.Location = New System.Drawing.Point(100, 18)
        Me.ImmortalStrength.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ImmortalStrength.Name = "ImmortalStrength"
        Me.ImmortalStrength.Size = New System.Drawing.Size(16, 17)
        Me.ImmortalStrength.TabIndex = 21
        Me.ImmortalStrength.Text = "0"
        Me.ImmortalStrength.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPreGameSize
        '
        Me.lblPreGameSize.AutoSize = True
        Me.lblPreGameSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblPreGameSize.Location = New System.Drawing.Point(306, 57)
        Me.lblPreGameSize.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPreGameSize.Name = "lblPreGameSize"
        Me.lblPreGameSize.Size = New System.Drawing.Size(129, 20)
        Me.lblPreGameSize.TabIndex = 9
        Me.lblPreGameSize.Text = "Pre-Game Size:  "
        '
        'lblDeckSize
        '
        Me.lblDeckSize.AutoSize = True
        Me.lblDeckSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblDeckSize.Location = New System.Drawing.Point(9, 57)
        Me.lblDeckSize.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDeckSize.Name = "lblDeckSize"
        Me.lblDeckSize.Size = New System.Drawing.Size(93, 20)
        Me.lblDeckSize.TabIndex = 8
        Me.lblDeckSize.Text = "Deck Size:  "
        '
        'lblDeckImmortal
        '
        Me.lblDeckImmortal.AutoSize = True
        Me.lblDeckImmortal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblDeckImmortal.Location = New System.Drawing.Point(9, 33)
        Me.lblDeckImmortal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDeckImmortal.Name = "lblDeckImmortal"
        Me.lblDeckImmortal.Size = New System.Drawing.Size(83, 20)
        Me.lblDeckImmortal.TabIndex = 7
        Me.lblDeckImmortal.Text = "Immortal:  "
        '
        'lblDeckTitle
        '
        Me.lblDeckTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDeckTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblDeckTitle.Location = New System.Drawing.Point(9, 3)
        Me.lblDeckTitle.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDeckTitle.Name = "lblDeckTitle"
        Me.lblDeckTitle.Size = New System.Drawing.Size(653, 30)
        Me.lblDeckTitle.TabIndex = 6
        Me.lblDeckTitle.Text = "Untitled Deck"
        Me.lblDeckTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TabPageFormat
        '
        Me.TabPageFormat.Controls.Add(Me.lblHighlanderProStatus)
        Me.TabPageFormat.Controls.Add(Me.lblHighlanderPro)
        Me.TabPageFormat.Controls.Add(Me.lblType2Status)
        Me.TabPageFormat.Controls.Add(Me.lblType1Status)
        Me.TabPageFormat.Controls.Add(Me.lblMLEStatus)
        Me.TabPageFormat.Controls.Add(Me.lbl1stEditionStatus)
        Me.TabPageFormat.Controls.Add(Me.lblType2)
        Me.TabPageFormat.Controls.Add(Me.lblType1)
        Me.TabPageFormat.Controls.Add(Me.lblMLE)
        Me.TabPageFormat.Controls.Add(Me.lbl1stEdtion)
        Me.TabPageFormat.Location = New System.Drawing.Point(4, 24)
        Me.TabPageFormat.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageFormat.Name = "TabPageFormat"
        Me.TabPageFormat.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageFormat.Size = New System.Drawing.Size(671, 563)
        Me.TabPageFormat.TabIndex = 1
        Me.TabPageFormat.Text = "Formats"
        Me.TabPageFormat.UseVisualStyleBackColor = True
        '
        'lblType2Status
        '
        Me.lblType2Status.AutoSize = True
        Me.lblType2Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblType2Status.Location = New System.Drawing.Point(181, 104)
        Me.lblType2Status.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblType2Status.Name = "lblType2Status"
        Me.lblType2Status.Size = New System.Drawing.Size(56, 24)
        Me.lblType2Status.TabIndex = 9
        Me.lblType2Status.Text = "Legal"
        '
        'lblType1Status
        '
        Me.lblType1Status.AutoSize = True
        Me.lblType1Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblType1Status.Location = New System.Drawing.Point(181, 76)
        Me.lblType1Status.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblType1Status.Name = "lblType1Status"
        Me.lblType1Status.Size = New System.Drawing.Size(56, 24)
        Me.lblType1Status.TabIndex = 8
        Me.lblType1Status.Text = "Legal"
        '
        'lblMLEStatus
        '
        Me.lblMLEStatus.AutoSize = True
        Me.lblMLEStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblMLEStatus.Location = New System.Drawing.Point(181, 48)
        Me.lblMLEStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMLEStatus.Name = "lblMLEStatus"
        Me.lblMLEStatus.Size = New System.Drawing.Size(56, 24)
        Me.lblMLEStatus.TabIndex = 7
        Me.lblMLEStatus.Text = "Legal"
        '
        'lbl1stEditionStatus
        '
        Me.lbl1stEditionStatus.AutoSize = True
        Me.lbl1stEditionStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lbl1stEditionStatus.Location = New System.Drawing.Point(181, 21)
        Me.lbl1stEditionStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl1stEditionStatus.Name = "lbl1stEditionStatus"
        Me.lbl1stEditionStatus.Size = New System.Drawing.Size(56, 24)
        Me.lbl1stEditionStatus.TabIndex = 6
        Me.lbl1stEditionStatus.Text = "Legal"
        '
        'lblType2
        '
        Me.lblType2.AutoSize = True
        Me.lblType2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblType2.Location = New System.Drawing.Point(26, 104)
        Me.lblType2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblType2.Name = "lblType2"
        Me.lblType2.Size = New System.Drawing.Size(73, 24)
        Me.lblType2.TabIndex = 4
        Me.lblType2.Text = "Type 2:"
        '
        'lblType1
        '
        Me.lblType1.AutoSize = True
        Me.lblType1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblType1.Location = New System.Drawing.Point(26, 76)
        Me.lblType1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblType1.Name = "lblType1"
        Me.lblType1.Size = New System.Drawing.Size(73, 24)
        Me.lblType1.TabIndex = 3
        Me.lblType1.Text = "Type 1:"
        '
        'lblMLE
        '
        Me.lblMLE.AutoSize = True
        Me.lblMLE.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblMLE.Location = New System.Drawing.Point(26, 48)
        Me.lblMLE.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMLE.Name = "lblMLE"
        Me.lblMLE.Size = New System.Drawing.Size(54, 24)
        Me.lblMLE.TabIndex = 2
        Me.lblMLE.Text = "MLE:"
        '
        'lbl1stEdtion
        '
        Me.lbl1stEdtion.AutoSize = True
        Me.lbl1stEdtion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lbl1stEdtion.Location = New System.Drawing.Point(26, 21)
        Me.lbl1stEdtion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl1stEdtion.Name = "lbl1stEdtion"
        Me.lbl1stEdtion.Size = New System.Drawing.Size(101, 24)
        Me.lbl1stEdtion.TabIndex = 1
        Me.lbl1stEdtion.Text = "1st Edition:"
        '
        'TabPageH1E
        '
        Me.TabPageH1E.Controls.Add(Me.lst1stEdition)
        Me.TabPageH1E.Location = New System.Drawing.Point(4, 24)
        Me.TabPageH1E.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageH1E.Name = "TabPageH1E"
        Me.TabPageH1E.Size = New System.Drawing.Size(671, 563)
        Me.TabPageH1E.TabIndex = 2
        Me.TabPageH1E.Text = "1st Edition"
        Me.TabPageH1E.UseVisualStyleBackColor = True
        '
        'lst1stEdition
        '
        Me.lst1stEdition.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnCardTitle, Me.ColumnDetails})
        Me.lst1stEdition.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lst1stEdition.Location = New System.Drawing.Point(0, 0)
        Me.lst1stEdition.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lst1stEdition.Name = "lst1stEdition"
        Me.lst1stEdition.Size = New System.Drawing.Size(671, 563)
        Me.lst1stEdition.TabIndex = 0
        Me.lst1stEdition.UseCompatibleStateImageBehavior = False
        Me.lst1stEdition.View = System.Windows.Forms.View.Details
        '
        'ColumnCardTitle
        '
        Me.ColumnCardTitle.Text = "Card Name"
        Me.ColumnCardTitle.Width = 142
        '
        'ColumnDetails
        '
        Me.ColumnDetails.Text = "Details"
        Me.ColumnDetails.Width = 394
        '
        'TabPageMLE
        '
        Me.TabPageMLE.Controls.Add(Me.lstMLE)
        Me.TabPageMLE.Location = New System.Drawing.Point(4, 24)
        Me.TabPageMLE.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageMLE.Name = "TabPageMLE"
        Me.TabPageMLE.Size = New System.Drawing.Size(671, 563)
        Me.TabPageMLE.TabIndex = 3
        Me.TabPageMLE.Text = "MLE"
        Me.TabPageMLE.UseVisualStyleBackColor = True
        '
        'lstMLE
        '
        Me.lstMLE.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lstMLE.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstMLE.Location = New System.Drawing.Point(0, 0)
        Me.lstMLE.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstMLE.Name = "lstMLE"
        Me.lstMLE.Size = New System.Drawing.Size(671, 563)
        Me.lstMLE.TabIndex = 1
        Me.lstMLE.UseCompatibleStateImageBehavior = False
        Me.lstMLE.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Card Name"
        Me.ColumnHeader1.Width = 142
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Details"
        Me.ColumnHeader2.Width = 394
        '
        'TabPageType1
        '
        Me.TabPageType1.Controls.Add(Me.lstType1)
        Me.TabPageType1.Location = New System.Drawing.Point(4, 24)
        Me.TabPageType1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageType1.Name = "TabPageType1"
        Me.TabPageType1.Size = New System.Drawing.Size(671, 563)
        Me.TabPageType1.TabIndex = 4
        Me.TabPageType1.Text = "Type 1"
        Me.TabPageType1.UseVisualStyleBackColor = True
        '
        'lstType1
        '
        Me.lstType1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lstType1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstType1.Location = New System.Drawing.Point(0, 0)
        Me.lstType1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstType1.Name = "lstType1"
        Me.lstType1.Size = New System.Drawing.Size(671, 563)
        Me.lstType1.TabIndex = 1
        Me.lstType1.UseCompatibleStateImageBehavior = False
        Me.lstType1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Card Name"
        Me.ColumnHeader3.Width = 142
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Details"
        Me.ColumnHeader4.Width = 394
        '
        'TabPageType2
        '
        Me.TabPageType2.Controls.Add(Me.lstType2)
        Me.TabPageType2.Location = New System.Drawing.Point(4, 24)
        Me.TabPageType2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TabPageType2.Name = "TabPageType2"
        Me.TabPageType2.Size = New System.Drawing.Size(671, 563)
        Me.TabPageType2.TabIndex = 5
        Me.TabPageType2.Text = "Type 2"
        Me.TabPageType2.UseVisualStyleBackColor = True
        '
        'lstType2
        '
        Me.lstType2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lstType2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstType2.Location = New System.Drawing.Point(0, 0)
        Me.lstType2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstType2.Name = "lstType2"
        Me.lstType2.Size = New System.Drawing.Size(671, 563)
        Me.lstType2.TabIndex = 1
        Me.lstType2.UseCompatibleStateImageBehavior = False
        Me.lstType2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Card Name"
        Me.ColumnHeader5.Width = 142
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Details"
        Me.ColumnHeader6.Width = 394
        '
        'lblHighlanderProStatus
        '
        Me.lblHighlanderProStatus.AutoSize = True
        Me.lblHighlanderProStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblHighlanderProStatus.Location = New System.Drawing.Point(181, 128)
        Me.lblHighlanderProStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHighlanderProStatus.Name = "lblHighlanderProStatus"
        Me.lblHighlanderProStatus.Size = New System.Drawing.Size(56, 24)
        Me.lblHighlanderProStatus.TabIndex = 11
        Me.lblHighlanderProStatus.Text = "Legal"
        '
        'lblHighlanderPro
        '
        Me.lblHighlanderPro.AutoSize = True
        Me.lblHighlanderPro.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblHighlanderPro.Location = New System.Drawing.Point(26, 128)
        Me.lblHighlanderPro.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHighlanderPro.Name = "lblHighlanderPro"
        Me.lblHighlanderPro.Size = New System.Drawing.Size(142, 24)
        Me.lblHighlanderPro.TabIndex = 10
        Me.lblHighlanderPro.Text = "Highlander Pro:"
        '
        'TabPageHighlanderPro
        '
        Me.TabPageHighlanderPro.Controls.Add(Me.lstHighlanderPro)
        Me.TabPageHighlanderPro.Location = New System.Drawing.Point(4, 24)
        Me.TabPageHighlanderPro.Name = "TabPageHighlanderPro"
        Me.TabPageHighlanderPro.Size = New System.Drawing.Size(671, 563)
        Me.TabPageHighlanderPro.TabIndex = 6
        Me.TabPageHighlanderPro.Text = "Highlander Pro"
        Me.TabPageHighlanderPro.UseVisualStyleBackColor = True
        '
        'lstHighlanderPro
        '
        Me.lstHighlanderPro.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader8})
        Me.lstHighlanderPro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstHighlanderPro.Location = New System.Drawing.Point(0, 0)
        Me.lstHighlanderPro.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstHighlanderPro.Name = "lstHighlanderPro"
        Me.lstHighlanderPro.Size = New System.Drawing.Size(671, 563)
        Me.lstHighlanderPro.TabIndex = 2
        Me.lstHighlanderPro.UseCompatibleStateImageBehavior = False
        Me.lstHighlanderPro.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Card Name"
        Me.ColumnHeader7.Width = 142
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Details"
        Me.ColumnHeader8.Width = 394
        '
        'frmDeckAnalysis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 591)
        Me.Controls.Add(Me.TabControlAnalysis)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmDeckAnalysis"
        Me.Text = "Deck Analysis"
        CType(Me.PictureCardsTypeChart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureCardsImmortalChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlAnalysis.ResumeLayout(False)
        Me.TabPageSummary.ResumeLayout(False)
        Me.TabPageSummary.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DeckToughnessGem3E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeckMasterGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeckReasonGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeckStrengthGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeckAgilityGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeckToughnessGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeckEmpathyGem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPersonaAttributes.ResumeLayout(False)
        Me.GroupPersonaAttributes.PerformLayout()
        CType(Me.ImmortalReasonGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImmortalEmpathyGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImmortalToughnessGem3E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImmortalToughnessGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImmortalAgilityGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImmortalMasterGem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImmortalStrengthGem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageFormat.ResumeLayout(False)
        Me.TabPageFormat.PerformLayout()
        Me.TabPageH1E.ResumeLayout(False)
        Me.TabPageMLE.ResumeLayout(False)
        Me.TabPageType1.ResumeLayout(False)
        Me.TabPageType2.ResumeLayout(False)
        Me.TabPageHighlanderPro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblCardsType As System.Windows.Forms.Label
    Friend WithEvents lblCardsImmortal As System.Windows.Forms.Label
    Friend WithEvents lstCardsByType As System.Windows.Forms.ListView
    Friend WithEvents ColumnType As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnPercent As System.Windows.Forms.ColumnHeader
    Friend WithEvents lstCardsByImmortal As System.Windows.Forms.ListView
    Friend WithEvents ColumnImmortal As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnTotalNum As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnTotalPercent As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnReserved As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnRestricted As System.Windows.Forms.ColumnHeader
    Friend WithEvents PictureCardsTypeChart As System.Windows.Forms.PictureBox
    Friend WithEvents PictureCardsImmortalChart As System.Windows.Forms.PictureBox
    Friend WithEvents TabControlAnalysis As System.Windows.Forms.TabControl
    Friend WithEvents TabPageSummary As System.Windows.Forms.TabPage
    Friend WithEvents TabPageFormat As System.Windows.Forms.TabPage
    Friend WithEvents TabPageH1E As System.Windows.Forms.TabPage
    Friend WithEvents TabPageMLE As System.Windows.Forms.TabPage
    Friend WithEvents TabPageType1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPageType2 As System.Windows.Forms.TabPage
    Friend WithEvents lst1stEdition As System.Windows.Forms.ListView
    Friend WithEvents ColumnCardTitle As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnDetails As System.Windows.Forms.ColumnHeader
    Friend WithEvents lstMLE As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lstType1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lstType2 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblType2Status As System.Windows.Forms.Label
    Friend WithEvents lblType1Status As System.Windows.Forms.Label
    Friend WithEvents lblMLEStatus As System.Windows.Forms.Label
    Friend WithEvents lbl1stEditionStatus As System.Windows.Forms.Label
    Friend WithEvents lblType2 As System.Windows.Forms.Label
    Friend WithEvents lblType1 As System.Windows.Forms.Label
    Friend WithEvents lblMLE As System.Windows.Forms.Label
    Friend WithEvents lbl1stEdtion As System.Windows.Forms.Label
    Friend WithEvents lblDeckTitle As System.Windows.Forms.Label
    Friend WithEvents lblDeckImmortal As System.Windows.Forms.Label
    Friend WithEvents lblPreGameSize As System.Windows.Forms.Label
    Friend WithEvents lblDeckSize As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DeckToughness As System.Windows.Forms.Label
    Friend WithEvents DeckToughnessGem As System.Windows.Forms.PictureBox
    Friend WithEvents DeckStrength As System.Windows.Forms.Label
    Friend WithEvents DeckStrengthGem As System.Windows.Forms.PictureBox
    Friend WithEvents DeckReason As System.Windows.Forms.Label
    Friend WithEvents DeckReasonGem As System.Windows.Forms.PictureBox
    Friend WithEvents DeckMaster As System.Windows.Forms.Label
    Friend WithEvents DeckMasterGem As System.Windows.Forms.PictureBox
    Friend WithEvents DeckEmpathy As System.Windows.Forms.Label
    Friend WithEvents DeckEmpathyGem As System.Windows.Forms.PictureBox
    Friend WithEvents DeckAgility As System.Windows.Forms.Label
    Friend WithEvents DeckAgilityGem As System.Windows.Forms.PictureBox
    Friend WithEvents GroupPersonaAttributes As System.Windows.Forms.GroupBox
    Friend WithEvents ImmortalToughness As System.Windows.Forms.Label
    Friend WithEvents ImmortalStrength As System.Windows.Forms.Label
    Friend WithEvents ImmortalStrengthGem As System.Windows.Forms.PictureBox
    Friend WithEvents ImmortalReason As System.Windows.Forms.Label
    Friend WithEvents ImmortalMaster As System.Windows.Forms.Label
    Friend WithEvents ImmortalMasterGem As System.Windows.Forms.PictureBox
    Friend WithEvents ImmortalEmpathy As System.Windows.Forms.Label
    Friend WithEvents ImmortalAgility As System.Windows.Forms.Label
    Friend WithEvents ImmortalReasonGem As System.Windows.Forms.PictureBox
    Friend WithEvents ImmortalEmpathyGem As System.Windows.Forms.PictureBox
    Friend WithEvents ImmortalToughnessGem3E As System.Windows.Forms.PictureBox
    Friend WithEvents ImmortalToughnessGem As System.Windows.Forms.PictureBox
    Friend WithEvents ImmortalAgilityGem As System.Windows.Forms.PictureBox
    Friend WithEvents DeckToughnessGem3E As System.Windows.Forms.PictureBox
    Friend WithEvents lblHighlanderProStatus As Label
    Friend WithEvents lblHighlanderPro As Label
    Friend WithEvents TabPageHighlanderPro As TabPage
    Friend WithEvents lstHighlanderPro As ListView
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
End Class
