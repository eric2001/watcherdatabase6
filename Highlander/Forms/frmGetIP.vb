' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class frmGetIP
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblYourIP As System.Windows.Forms.Label
    Friend WithEvents buttonOk As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGetIP))
        Me.lblYourIP = New System.Windows.Forms.Label()
        Me.buttonOk = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblYourIP
        '
        Me.lblYourIP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblYourIP.Location = New System.Drawing.Point(8, 8)
        Me.lblYourIP.Name = "lblYourIP"
        Me.lblYourIP.Size = New System.Drawing.Size(256, 32)
        Me.lblYourIP.TabIndex = 0
        Me.lblYourIP.Text = "#IP_ADDRESS#"
        '
        'buttonOk
        '
        Me.buttonOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonOk.Location = New System.Drawing.Point(272, 8)
        Me.buttonOk.Name = "buttonOk"
        Me.buttonOk.Size = New System.Drawing.Size(75, 23)
        Me.buttonOk.TabIndex = 1
        Me.buttonOk.Text = "Ok"
        '
        'frmGetIP
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(354, 48)
        Me.Controls.Add(Me.buttonOk)
        Me.Controls.Add(Me.lblYourIP)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGetIP"
        Me.Text = "IP Address"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmGetIP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim ipAddress As String = classFileDownload.HTTPTextDownload("http://www.watcherdatabase.tk/checkip.txt")
        Dim localHostName As String
        Dim localIPAddress As String = ""
        localHostName = System.Net.Dns.GetHostName()

        Dim counter As Integer = 0
        While counter < System.Net.Dns.GetHostEntry(localHostName).AddressList.Length
            If System.Net.Dns.GetHostEntry(localHostName).AddressList(counter).ToString().Contains(".") Then
                localIPAddress = System.Net.Dns.GetHostEntry(localHostName).AddressList(counter).ToString()
            End If
            counter += 1
        End While
        If ipAddress.Length >= 0 Then
            lblYourIP.Text = "Your Internet IP Address is: " & ipAddress & vbCrLf
        Else
            lblYourIP.Text = "An Error Occured while attempting to determine your Internet IP Address." & vbCrLf
        End If
        lblYourIP.Text = lblYourIP.Text & "Your Local IP Address is: " & localIPAddress
    End Sub

    Private Sub buttonOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonOk.Click
        ' Close this window.
        Me.Close()
    End Sub
End Class
