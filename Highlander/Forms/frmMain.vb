' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Microsoft.Win32

Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
    Friend WithEvents menuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuWindow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuCompareHLTtoXML As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuNewDeck As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuOpenDeck As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuPrintMLECards As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuViewCollection As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuCascade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuTileHorizontal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuTileVertical As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuCollection As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuDecks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuNewTradeList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuEditTradeList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuGetIP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuOpenInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuSaveInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuSaveInventoryAs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuTools As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItemCollectionOverview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem1 As ToolStripSeparator
    Friend WithEvents MenuItem3 As ToolStripSeparator
    Friend WithEvents MenuItem2 As ToolStripSeparator
    Friend WithEvents menuDivCollection As ToolStripSeparator
    Friend WithEvents menuDivTrades As ToolStripSeparator
    Friend WithEvents menuDivAbout As ToolStripSeparator
    Friend WithEvents menuGenerateSetReport As System.Windows.Forms.ToolStripMenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
        Me.menuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuOpenInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuNewInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuSaveInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuSaveInventoryAs = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.menuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuCollection = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuViewCollection = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuDivCollection = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuItemCollectionOverview = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuGenerateSetReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuDivTrades = New System.Windows.Forms.ToolStripSeparator()
        Me.menuNewTradeList = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuEditTradeList = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuCompareHLTtoXML = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuDecks = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuNewDeck = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuOpenDeck = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTools = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuPrintMLECards = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuGetIP = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuWindow = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuCascade = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuTileHorizontal = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuTileVertical = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuDivAbout = New System.Windows.Forms.ToolStripSeparator()
        Me.menuAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainMenu1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuFile, Me.menuCollection, Me.menuDecks, Me.MenuTools, Me.menuWindow, Me.MenuItem6, Me.MenuItem7})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.MdiWindowListItem = Me.menuWindow
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(792, 24)
        Me.MainMenu1.TabIndex = 1
        '
        'menuFile
        '
        Me.menuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuOpenInventory, Me.MenuItem1, Me.MenuNewInventory, Me.MenuItem3, Me.MenuSaveInventory, Me.MenuSaveInventoryAs, Me.MenuItem2, Me.menuExit})
        Me.menuFile.Name = "menuFile"
        Me.menuFile.Size = New System.Drawing.Size(37, 20)
        Me.menuFile.Text = "File"
        '
        'MenuOpenInventory
        '
        Me.MenuOpenInventory.Name = "MenuOpenInventory"
        Me.MenuOpenInventory.Size = New System.Drawing.Size(176, 22)
        Me.MenuOpenInventory.Text = "Open Inventory"
        '
        'MenuItem1
        '
        Me.MenuItem1.Name = "MenuItem1"
        Me.MenuItem1.Size = New System.Drawing.Size(173, 6)
        '
        'MenuNewInventory
        '
        Me.MenuNewInventory.Name = "MenuNewInventory"
        Me.MenuNewInventory.Size = New System.Drawing.Size(176, 22)
        Me.MenuNewInventory.Text = "New Inventory"
        '
        'MenuItem3
        '
        Me.MenuItem3.Name = "MenuItem3"
        Me.MenuItem3.Size = New System.Drawing.Size(173, 6)
        '
        'MenuSaveInventory
        '
        Me.MenuSaveInventory.Name = "MenuSaveInventory"
        Me.MenuSaveInventory.Size = New System.Drawing.Size(176, 22)
        Me.MenuSaveInventory.Text = "Save Inventory"
        '
        'MenuSaveInventoryAs
        '
        Me.MenuSaveInventoryAs.Name = "MenuSaveInventoryAs"
        Me.MenuSaveInventoryAs.Size = New System.Drawing.Size(176, 22)
        Me.MenuSaveInventoryAs.Text = "Save Inventory As..."
        '
        'MenuItem2
        '
        Me.MenuItem2.Name = "MenuItem2"
        Me.MenuItem2.Size = New System.Drawing.Size(173, 6)
        '
        'menuExit
        '
        Me.menuExit.Name = "menuExit"
        Me.menuExit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.menuExit.Size = New System.Drawing.Size(176, 22)
        Me.menuExit.Text = "Exit"
        '
        'menuCollection
        '
        Me.menuCollection.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuViewCollection, Me.menuDivCollection, Me.MenuItemCollectionOverview, Me.menuGenerateSetReport, Me.menuDivTrades, Me.menuNewTradeList, Me.menuEditTradeList, Me.menuCompareHLTtoXML})
        Me.menuCollection.Name = "menuCollection"
        Me.menuCollection.Size = New System.Drawing.Size(73, 20)
        Me.menuCollection.Text = "Collection"
        '
        'menuViewCollection
        '
        Me.menuViewCollection.Name = "menuViewCollection"
        Me.menuViewCollection.Size = New System.Drawing.Size(274, 22)
        Me.menuViewCollection.Text = "View Collection"
        '
        'menuDivCollection
        '
        Me.menuDivCollection.Name = "menuDivCollection"
        Me.menuDivCollection.Size = New System.Drawing.Size(271, 6)
        '
        'MenuItemCollectionOverview
        '
        Me.MenuItemCollectionOverview.Name = "MenuItemCollectionOverview"
        Me.MenuItemCollectionOverview.Size = New System.Drawing.Size(274, 22)
        Me.MenuItemCollectionOverview.Text = "Generate Collection Overview Report"
        '
        'menuGenerateSetReport
        '
        Me.menuGenerateSetReport.Name = "menuGenerateSetReport"
        Me.menuGenerateSetReport.Size = New System.Drawing.Size(274, 22)
        Me.menuGenerateSetReport.Text = "Generate Collection Set Report"
        '
        'menuDivTrades
        '
        Me.menuDivTrades.Name = "menuDivTrades"
        Me.menuDivTrades.Size = New System.Drawing.Size(271, 6)
        '
        'menuNewTradeList
        '
        Me.menuNewTradeList.Name = "menuNewTradeList"
        Me.menuNewTradeList.Size = New System.Drawing.Size(274, 22)
        Me.menuNewTradeList.Text = "Create New Trade List"
        '
        'menuEditTradeList
        '
        Me.menuEditTradeList.Name = "menuEditTradeList"
        Me.menuEditTradeList.Size = New System.Drawing.Size(274, 22)
        Me.menuEditTradeList.Text = "Edit Existing Trade List"
        '
        'menuCompareHLTtoXML
        '
        Me.menuCompareHLTtoXML.Name = "menuCompareHLTtoXML"
        Me.menuCompareHLTtoXML.Size = New System.Drawing.Size(274, 22)
        Me.menuCompareHLTtoXML.Text = "Compare Trade List to Your Collection"
        '
        'menuDecks
        '
        Me.menuDecks.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuNewDeck, Me.menuOpenDeck})
        Me.menuDecks.Name = "menuDecks"
        Me.menuDecks.Size = New System.Drawing.Size(50, 20)
        Me.menuDecks.Text = "Decks"
        '
        'menuNewDeck
        '
        Me.menuNewDeck.Name = "menuNewDeck"
        Me.menuNewDeck.Size = New System.Drawing.Size(132, 22)
        Me.menuNewDeck.Text = "New Deck"
        '
        'menuOpenDeck
        '
        Me.menuOpenDeck.Name = "menuOpenDeck"
        Me.menuOpenDeck.Size = New System.Drawing.Size(132, 22)
        Me.menuOpenDeck.Text = "Open Deck"
        '
        'MenuTools
        '
        Me.MenuTools.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuPrintMLECards, Me.menuGetIP, Me.MenuItem4, Me.MenuItem5, Me.MenuItem9})
        Me.MenuTools.Name = "MenuTools"
        Me.MenuTools.Size = New System.Drawing.Size(47, 20)
        Me.MenuTools.Text = "Tools"
        '
        'menuPrintMLECards
        '
        Me.menuPrintMLECards.Name = "menuPrintMLECards"
        Me.menuPrintMLECards.Size = New System.Drawing.Size(222, 22)
        Me.menuPrintMLECards.Text = "Print Proxy Cards"
        '
        'menuGetIP
        '
        Me.menuGetIP.Name = "menuGetIP"
        Me.menuGetIP.Size = New System.Drawing.Size(222, 22)
        Me.menuGetIP.Text = "Determine IP Address"
        '
        'MenuItem4
        '
        Me.MenuItem4.Name = "MenuItem4"
        Me.MenuItem4.Size = New System.Drawing.Size(222, 22)
        Me.MenuItem4.Text = "Import Version 5 Records"
        '
        'MenuItem5
        '
        Me.MenuItem5.Name = "MenuItem5"
        Me.MenuItem5.Size = New System.Drawing.Size(222, 22)
        Me.MenuItem5.Text = "Create Empty Image Folders"
        '
        'MenuItem9
        '
        Me.MenuItem9.Name = "MenuItem9"
        Me.MenuItem9.Size = New System.Drawing.Size(222, 22)
        Me.MenuItem9.Text = "Settings"
        '
        'menuWindow
        '
        Me.menuWindow.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuCascade, Me.menuTileHorizontal, Me.menuTileVertical, Me.menuDivAbout, Me.menuAbout})
        Me.menuWindow.Name = "menuWindow"
        Me.menuWindow.Size = New System.Drawing.Size(63, 20)
        Me.menuWindow.Text = "Window"
        '
        'menuCascade
        '
        Me.menuCascade.Name = "menuCascade"
        Me.menuCascade.Size = New System.Drawing.Size(151, 22)
        Me.menuCascade.Text = "Cascade"
        '
        'menuTileHorizontal
        '
        Me.menuTileHorizontal.Name = "menuTileHorizontal"
        Me.menuTileHorizontal.Size = New System.Drawing.Size(151, 22)
        Me.menuTileHorizontal.Text = "Tile Horizontal"
        '
        'menuTileVertical
        '
        Me.menuTileVertical.Name = "menuTileVertical"
        Me.menuTileVertical.Size = New System.Drawing.Size(151, 22)
        Me.menuTileVertical.Text = "Tile Vertical"
        '
        'menuDivAbout
        '
        Me.menuDivAbout.Name = "menuDivAbout"
        Me.menuDivAbout.Size = New System.Drawing.Size(148, 6)
        '
        'menuAbout
        '
        Me.menuAbout.Name = "menuAbout"
        Me.menuAbout.Size = New System.Drawing.Size(151, 22)
        Me.menuAbout.Text = "About"
        '
        'MenuItem6
        '
        Me.MenuItem6.Name = "MenuItem6"
        Me.MenuItem6.Size = New System.Drawing.Size(40, 20)
        Me.MenuItem6.Text = "Test"
        Me.MenuItem6.Visible = False
        '
        'MenuItem7
        '
        Me.MenuItem7.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuItem8})
        Me.MenuItem7.Name = "MenuItem7"
        Me.MenuItem7.Size = New System.Drawing.Size(75, 20)
        Me.MenuItem7.Text = "Play Game"
        Me.MenuItem7.Visible = False
        '
        'MenuItem8
        '
        Me.MenuItem8.Name = "MenuItem8"
        Me.MenuItem8.Size = New System.Drawing.Size(141, 22)
        Me.MenuItem8.Text = "New Game..."
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(792, 553)
        Me.Controls.Add(Me.MainMenu1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MainMenu1
        Me.Name = "frmMain"
        Me.Text = "Watcher Database (NEW FILE)"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If (Me.Text.Substring(0, "Watcher Database (*".Length) = "Watcher Database (*") Or
            (Me.Text = "Watcher Database (NEW FILE)" And
             (Data.InventoryDB.GetInventoryRecordCount() > 0)) Then
            If MessageBox.Show("Save Inventory before Exiting?", "Save File", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                If Data.InventoryDB.FileName = "" Then
                    Dim SaveInventory As New SaveFileDialog
                    SaveInventory.DefaultExt = "hli"
                    SaveInventory.Filter = "Highlander Inventory (*.hli)|*.hli"
                    If SaveInventory.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Data.InventoryDB.SaveFile(SaveInventory.FileName)
                        MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    Data.InventoryDB.SaveFile()
                    MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Load saved configuration settings from the system registry

        ' Load default root images folder.
        If Not Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "ImageFolder", Nothing) = Nothing Then
            Data.CardsDB.HighlanderImagePath = Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "ImageFolder", Nothing)
        End If

        ' Load the sort order from the system registery.
        If Not Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultSetSort", Nothing) = Nothing Then
            Data.CardsDB.ExpansionSortOrder = Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultSetSort", Nothing)
        End If

        ' Load the display mode from the system registery.
        If Not Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaulDisplayMode", Nothing) = Nothing Then
            Data.CardsDB.CardDisplayMode = Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaulDisplayMode", Nothing)
        End If

        ' If there's a default inventory file set, load it.
        If Not My.Computer.Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultInventory", Nothing) = Nothing Then
            Data.InventoryDB.LoadFile(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultInventory", Nothing))
            Me.Text = "Watcher Database (" & Data.InventoryDB.FileName & ")"
        End If

    End Sub

    Private Sub menuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuAbout.Click
        ' Display the About window.
        Dim windowAbout As New frmAbout
        Me.AddOwnedForm(windowAbout)
        windowAbout.MdiParent = Me
        windowAbout.Show()
    End Sub

    Private Sub menuCompareHLTtoXML_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuCompareHLTtoXML.Click
        ' Open an existing Trade List (.hlt) file and compare it to your personal 
        '   collection, based on information in your cards.xml file.

        ' Display the File Open Dialog.  If the user presses Ok, then load the selected file.
        Dim fileDialog As New OpenFileDialog
        fileDialog.DefaultExt = "hlt"
        fileDialog.Filter = "Trade List (*.hlt)|*.hlt"
        If fileDialog.ShowDialog() = DialogResult.OK Then
            ' Create a new Trade List Viewer
            Dim windowTradeList As New frmViewTradeList

            ' Set it as a child window.
            Me.AddOwnedForm(windowTradeList)
            windowTradeList.MdiParent = Me

            ' Show the form, load the HLT file.
            windowTradeList.openHLTFile(fileDialog.FileName)
            windowTradeList.Show()
        End If
    End Sub

    Private Sub passwordScreen(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        ' Display the password screen when Shift+Control+P are pressed.
        If e.Shift And e.Control And e.KeyCode = 80 Then
            Dim windowLogin As New frmPassword
            Me.AddOwnedForm(windowLogin)
            windowLogin.MdiParent = Me
            windowLogin.Show()
        End If
    End Sub

    Private Sub menuNewDeck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuNewDeck.Click
        ' Open an empty Deck Editor Window.
        Dim windowDeckEditor As New frmDeckEditor

        ' Set the window as a child.
        Me.AddOwnedForm(windowDeckEditor)
        windowDeckEditor.MdiParent = Me

        ' Show the window.
        windowDeckEditor.Show()
    End Sub

    Private Sub menuOpenDeck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuOpenDeck.Click
        ' Open a previously saved deck in a new deck editor window.
        Dim fileDialog As New OpenFileDialog
        fileDialog.DefaultExt = "hld"
        fileDialog.Filter = "Highlander Decks (*.hld)|*.hld"

        ' Prompt the user for a file, if the user presses Ok, load it.
        If fileDialog.ShowDialog() = DialogResult.OK Then
            ' Create a new deck editor window.
            Dim windowDeckEditor As New frmDeckEditor

            ' Set the window as a child.
            Me.AddOwnedForm(windowDeckEditor)
            windowDeckEditor.MdiParent = Me

            ' Show the window.
            windowDeckEditor.Show()

            ' Load the HLD file.
            windowDeckEditor.openHLDFile(fileDialog.FileName)
        End If
    End Sub

    Private Sub menuPrintMLECards_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuPrintMLECards.Click
        ' Load a new Print Cards window, set it as a child to the main window.
        Dim windowPrintCards As New frmPrintCards
        Me.AddOwnedForm(windowPrintCards)
        windowPrintCards.MdiParent = Me
        windowPrintCards.Show()
    End Sub

    Private Sub menuViewCollection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuViewCollection.Click
        ' Create New View Collection Window, Set as Child
        Dim windowViewCollection As New frmViewCollection
        Me.AddOwnedForm(windowViewCollection)
        windowViewCollection.MdiParent = Me

        ' Load the form
        windowViewCollection.Show()
    End Sub

    Private Sub menuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuExit.Click
        ' Exit the program.
        Me.Close()
    End Sub

    Private Sub menuCascade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuCascade.Click
        ' Cascade all open windows.
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub menuTileHorizontal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuTileHorizontal.Click
        ' Tile all open windows.
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub menuTileVertical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuTileVertical.Click
        ' Vertically tile all open windows.
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub menuNewTradeList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuNewTradeList.Click
        ' Open an empty trade list window.
        Dim windowTradeList As New frmEditTradeList

        ' Set the new window up as a child.
        Me.AddOwnedForm(windowTradeList)
        windowTradeList.MdiParent = Me

        ' Load the window.
        windowTradeList.Show()
    End Sub

    Private Sub menuEditTradeList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuEditTradeList.Click
        ' Open an empty trade list window.
        Dim fileDialog As New OpenFileDialog
        fileDialog.DefaultExt = "hlt"
        fileDialog.Filter = "Trade List (*.hlt)|*.hlt"
        If fileDialog.ShowDialog() = DialogResult.OK Then
            Dim windowTradeList As New frmEditTradeList

            ' Set the new window up as a child.
            Me.AddOwnedForm(windowTradeList)
            windowTradeList.MdiParent = Me

            ' Load the specified file.
            windowTradeList.openHLTFile(fileDialog.FileName)

            ' Load the window.
            windowTradeList.Show()
        End If
    End Sub

    Private Sub menuGetIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuGetIP.Click
        Dim windowIPAddress As New frmGetIP
        Me.AddOwnedForm(windowIPAddress)
        windowIPAddress.MdiParent = Me
        windowIPAddress.Show()
    End Sub

    Private Sub menuGenerateSetReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuGenerateSetReport.Click
        Dim windowSelectSetForReport As New frmGenerateReport
        Me.AddOwnedForm(windowSelectSetForReport)
        windowSelectSetForReport.MdiParent = Me
        windowSelectSetForReport.Show()
    End Sub

    Private Sub MenuOpenInventory_Click(sender As System.Object, e As System.EventArgs) Handles MenuOpenInventory.Click
        For Each OneWindow As System.Windows.Forms.Form In Me.MdiChildren
            If OneWindow.Name = frmViewCollection.Name Then
                If MessageBox.Show("You must close the View Collection window before loading a new inventory." & vbCrLf & vbCrLf &
                                 "Would you like me to close it now?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    OneWindow.Close()
                Else
                    Exit Sub
                End If
            End If
        Next

        Dim SelectInventory As New OpenFileDialog
        SelectInventory.DefaultExt = "hli"
        SelectInventory.Filter = "Highlander Inventory (*.hli)|*.hli"
        If SelectInventory.ShowDialog() = DialogResult.OK Then
            Data.InventoryDB.LoadFile(SelectInventory.FileName)
            Me.Text = "Watcher Database (" & Data.InventoryDB.FileName & ")"
            MessageBox.Show("Inventory Loaded Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub MenuSaveInventory_Click(sender As System.Object, e As System.EventArgs) Handles MenuSaveInventory.Click
        If Data.InventoryDB.FileName = "" Then
            Dim SaveInventory As New SaveFileDialog
            SaveInventory.DefaultExt = "hli"
            SaveInventory.Filter = "Highlander Inventory (*.hli)|*.hli"
            If SaveInventory.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Data.InventoryDB.SaveFile(SaveInventory.FileName)
                Me.Text = "Watcher Database (" & Data.InventoryDB.FileName & ")"
                MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            Data.InventoryDB.SaveFile()
            Me.Text = "Watcher Database (" & Data.InventoryDB.FileName & ")"
            MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub MenuSaveInventoryAs_Click(sender As System.Object, e As System.EventArgs) Handles MenuSaveInventoryAs.Click
        Dim SaveInventory As New SaveFileDialog
        SaveInventory.DefaultExt = "hli"
        SaveInventory.Filter = "Highlander Inventory (*.hli)|*.hli"
        If SaveInventory.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Data.InventoryDB.SaveFile(SaveInventory.FileName)
            Me.Text = "Watcher Database (" & Data.InventoryDB.FileName & ")"
            MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub MenuNewInventory_Click(sender As System.Object, e As System.EventArgs) Handles MenuNewInventory.Click
        If (Me.Text.Substring(0, "Watcher Database (*".Length) = "Watcher Database (*") Or
            (Me.Text = "Watcher Database (NEW FILE)" And
            (Data.InventoryDB.GetInventoryRecordCount() > 0)) Then
            If MessageBox.Show("Save Changes to Existing Inventory?", "Save File", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                If Data.InventoryDB.FileName = "" Then
                    Dim SaveInventory As New SaveFileDialog
                    SaveInventory.DefaultExt = "hli"
                    SaveInventory.Filter = "Highlander Inventory (*.hli)|*.hli"
                    If SaveInventory.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Data.InventoryDB.SaveFile(SaveInventory.FileName)
                        MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    Data.InventoryDB.SaveFile()
                    MessageBox.Show("Inventory Saved Successfully.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
        Data.InventoryDB.ClearFile()
        Me.Text = "Watcher Database (NEW FILE)"
    End Sub

    Private Sub MenuItem4_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem4.Click
        Dim windowMDBImporter As New FormMDBImporter
        Me.AddOwnedForm(windowMDBImporter)
        windowMDBImporter.MdiParent = Me
        windowMDBImporter.Show()
    End Sub

    Private Sub MenuItem5_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem5.Click
        Dim windowCreateImageFolders As New frmCreateImageFolders
        Me.AddOwnedForm(windowCreateImageFolders)
        windowCreateImageFolders.MdiParent = Me
        windowCreateImageFolders.Show()
    End Sub

    Private Sub MenuItemCollectionOverview_Click(sender As System.Object, e As System.EventArgs) Handles MenuItemCollectionOverview.Click
        Dim windowProgress As New frmProgress
        Me.AddOwnedForm(windowProgress)
        windowProgress.MdiParent = Me
        windowProgress.Show()

        Dim windowReportViewer As New frmReportViewer
        Dim ReportData As New Reporting.CollectionOverviewData()
        Task.Factory.StartNew(Sub() ReportData.RefreshData())
        While ReportData.Maximum = 0
            Application.DoEvents()
        End While

        windowProgress.ProgressBarStatus.Value = 0
        windowProgress.ProgressBarStatus.Maximum = ReportData.Maximum
        While ReportData.Progress <> ReportData.Maximum
            windowProgress.ProgressBarStatus.Value = ReportData.Progress
            Application.DoEvents()
        End While

        windowReportViewer.ReportViewer1.LocalReport.DataSources.Clear()
        windowReportViewer.ReportViewer1.LocalReport.ReportPath = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports\HLSetSummaryReport.rdlc")
        windowReportViewer.ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("Cards", ReportData.Data.Tables("Card")))

        windowProgress.Hide()
        windowProgress.Dispose()
        windowProgress = Nothing

        '  Display the report.
        Me.AddOwnedForm(windowReportViewer)
        windowReportViewer.MdiParent = Me
        windowReportViewer.Text = "Collection Overview Report"
        windowReportViewer.Show()

    End Sub

    Private Sub MenuItem6_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem6.Click
        Dim asdf As New frmPlayGame2
        asdf.Show()
    End Sub

    Private Sub MenuItem8_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem8.Click
        Dim windowNewGame As New frmNewGame
        If windowNewGame.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim windowPlayGame As New frmPlayGame2
            windowPlayGame.txtPlayerName.Text = windowNewGame.txtPlayerName.Text
            windowPlayGame.txtOpponentName.Text = windowNewGame.txtOpponentName.Text
            If windowNewGame.RadioButtonYouPlayFirst.Checked Then
                windowPlayGame.txtPlayerTurnNum.Text = "1A"
                windowPlayGame.txtOpponentTurnNum.Text = "1B"
                windowPlayGame.PlayerTurn = True
            Else
                windowPlayGame.txtPlayerTurnNum.Text = "1B"
                windowPlayGame.txtOpponentTurnNum.Text = "1A"
                windowPlayGame.PlayerTurn = False
            End If

            windowPlayGame.openHLDFile(windowNewGame.txtDeckPath.Text)
            Me.AddOwnedForm(windowPlayGame)
            windowPlayGame.MdiParent = Me
            windowPlayGame.Show()
            windowPlayGame.txtLogBox.Text = " ** Begin Turn 1A **"
            windowPlayGame.ComboBoxPhase.SelectedIndex = 0
        End If
    End Sub

    Private Sub MenuItem9_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem9.Click
        Dim windowWDBSettings As New frmSettings
        Me.AddOwnedForm(windowWDBSettings)
        windowWDBSettings.MdiParent = Me
        windowWDBSettings.Show()
    End Sub

End Class
