﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportViewer))
        Me.DataHighlanderCards = New System.Data.DataSet()
        Me.DataTable1 = New System.Data.DataTable()
        Me.ID = New System.Data.DataColumn()
        Me.GroupBy = New System.Data.DataColumn()
        Me.Title = New System.Data.DataColumn()
        Me.Type = New System.Data.DataColumn()
        Me.Rarity = New System.Data.DataColumn()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        CType(Me.DataHighlanderCards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataHighlanderCards
        '
        Me.DataHighlanderCards.DataSetName = "NewDataSet"
        Me.DataHighlanderCards.Tables.AddRange(New System.Data.DataTable() {Me.DataTable1})
        '
        'DataTable1
        '
        Me.DataTable1.Columns.AddRange(New System.Data.DataColumn() {Me.ID, Me.GroupBy, Me.Title, Me.Type, Me.Rarity})
        Me.DataTable1.Constraints.AddRange(New System.Data.Constraint() {New System.Data.UniqueConstraint("Constraint1", New String() {"ID"}, False)})
        Me.DataTable1.TableName = "Cards"
        '
        'ID
        '
        Me.ID.AllowDBNull = False
        Me.ID.ColumnName = "ID"
        Me.ID.DataType = GetType(Long)
        '
        'GroupBy
        '
        Me.GroupBy.ColumnName = "GroupBy"
        '
        'Title
        '
        Me.Title.ColumnName = "Title"
        '
        'Type
        '
        Me.Type.ColumnName = "Type"
        '
        'Rarity
        '
        Me.Rarity.ColumnName = "Rarity"
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(784, 562)
        Me.ReportViewer1.TabIndex = 0
        '
        'frmReportViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmReportViewer"
        Me.Text = "Report Viewer"
        CType(Me.DataHighlanderCards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataHighlanderCards As System.Data.DataSet
    Friend WithEvents DataTable1 As System.Data.DataTable
    Friend WithEvents ID As System.Data.DataColumn
    Friend WithEvents GroupBy As System.Data.DataColumn
    Friend WithEvents Title As System.Data.DataColumn
    Friend WithEvents Type As System.Data.DataColumn
    Friend WithEvents Rarity As System.Data.DataColumn
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
End Class
