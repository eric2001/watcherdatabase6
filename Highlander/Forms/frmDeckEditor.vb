' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Linq
Imports System.Data
Imports Highlander.Data
Imports Highlander.DeckRules
Imports System.Security
Imports Microsoft.Reporting.WinForms
Imports System.Security.Permissions

Public Class frmDeckEditor
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CardsControl1 As cardsControl
    Friend WithEvents groupSearch As System.Windows.Forms.GroupBox
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyword As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents cardsDeckList As cardsListViewer
    Friend WithEvents cardsPreGameList As cardsListViewer
    Friend WithEvents buttonAddToDeck As System.Windows.Forms.Button
    Friend WithEvents buttonAddToPreGame As System.Windows.Forms.Button
    Friend WithEvents groupAddButtons As System.Windows.Forms.GroupBox
    Friend WithEvents menuDeckEditor As System.Windows.Forms.MenuStrip
    Friend WithEvents menuDeckEditorMain As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuSaveAs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dialogSaveHLD As System.Windows.Forms.SaveFileDialog
    Friend WithEvents cardDetails As cardDetailsNoEdit
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitRight As System.Windows.Forms.SplitContainer
    Friend WithEvents comboPublisher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPublisher As System.Windows.Forms.Label
    Friend WithEvents SplitMiddle As System.Windows.Forms.SplitContainer
    Friend WithEvents controlImageFrontBack As Highlander.cardImageFB
    Friend WithEvents menuAnalyze As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItem1 As ToolStripSeparator
    Friend WithEvents MenuItem3 As ToolStripSeparator
    Friend WithEvents buttonAddToSelectedPreGame As Button
    Friend WithEvents menuPrintDeck As System.Windows.Forms.ToolStripMenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeckEditor))
        Me.groupSearch = New System.Windows.Forms.GroupBox()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.lblKeyword = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.buttonAddToDeck = New System.Windows.Forms.Button()
        Me.buttonAddToPreGame = New System.Windows.Forms.Button()
        Me.groupAddButtons = New System.Windows.Forms.GroupBox()
        Me.buttonAddToSelectedPreGame = New System.Windows.Forms.Button()
        Me.menuDeckEditor = New System.Windows.Forms.MenuStrip()
        Me.menuDeckEditorMain = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuSaveAs = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.menuAnalyze = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.menuPrintDeck = New System.Windows.Forms.ToolStripMenuItem()
        Me.dialogSaveHLD = New System.Windows.Forms.SaveFileDialog()
        Me.SplitRight = New System.Windows.Forms.SplitContainer()
        Me.cardsDeckList = New Highlander.cardsListViewer()
        Me.cardsPreGameList = New Highlander.cardsListViewer()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.CardsControl1 = New Highlander.cardsControl()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.SplitMiddle = New System.Windows.Forms.SplitContainer()
        Me.cardDetails = New Highlander.cardDetailsNoEdit()
        Me.controlImageFrontBack = New Highlander.cardImageFB()
        Me.groupSearch.SuspendLayout()
        Me.groupAddButtons.SuspendLayout()
        Me.menuDeckEditor.SuspendLayout()
        CType(Me.SplitRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitRight.Panel1.SuspendLayout()
        Me.SplitRight.Panel2.SuspendLayout()
        Me.SplitRight.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.SplitMiddle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitMiddle.Panel1.SuspendLayout()
        Me.SplitMiddle.Panel2.SuspendLayout()
        Me.SplitMiddle.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupSearch
        '
        Me.groupSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupSearch.Controls.Add(Me.comboPublisher)
        Me.groupSearch.Controls.Add(Me.lblPublisher)
        Me.groupSearch.Controls.Add(Me.txtKeywordSearch)
        Me.groupSearch.Controls.Add(Me.lblKeyword)
        Me.groupSearch.Controls.Add(Me.lblSet)
        Me.groupSearch.Controls.Add(Me.comboSets)
        Me.groupSearch.Location = New System.Drawing.Point(1, 436)
        Me.groupSearch.Name = "groupSearch"
        Me.groupSearch.Size = New System.Drawing.Size(254, 120)
        Me.groupSearch.TabIndex = 57
        Me.groupSearch.TabStop = False
        Me.groupSearch.Text = "Filter"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(115, 21)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(129, 23)
        Me.comboPublisher.TabIndex = 30
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblPublisher.Location = New System.Drawing.Point(12, 20)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(73, 18)
        Me.lblPublisher.TabIndex = 29
        Me.lblPublisher.Text = "Publisher:"
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(115, 87)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(129, 23)
        Me.txtKeywordSearch.TabIndex = 26
        '
        'lblKeyword
        '
        Me.lblKeyword.AutoSize = True
        Me.lblKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblKeyword.Location = New System.Drawing.Point(10, 87)
        Me.lblKeyword.Name = "lblKeyword"
        Me.lblKeyword.Size = New System.Drawing.Size(70, 18)
        Me.lblKeyword.TabIndex = 25
        Me.lblKeyword.Text = "Keyword:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(10, 54)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(34, 18)
        Me.lblSet.TabIndex = 24
        Me.lblSet.Text = "Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(115, 54)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(129, 23)
        Me.comboSets.TabIndex = 23
        '
        'buttonAddToDeck
        '
        Me.buttonAddToDeck.Location = New System.Drawing.Point(16, 15)
        Me.buttonAddToDeck.Name = "buttonAddToDeck"
        Me.buttonAddToDeck.Size = New System.Drawing.Size(105, 28)
        Me.buttonAddToDeck.TabIndex = 58
        Me.buttonAddToDeck.Text = "Add To Deck"
        '
        'buttonAddToPreGame
        '
        Me.buttonAddToPreGame.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonAddToPreGame.Location = New System.Drawing.Point(127, 15)
        Me.buttonAddToPreGame.Name = "buttonAddToPreGame"
        Me.buttonAddToPreGame.Size = New System.Drawing.Size(109, 28)
        Me.buttonAddToPreGame.TabIndex = 59
        Me.buttonAddToPreGame.Text = "Add To Pre-Game"
        '
        'groupAddButtons
        '
        Me.groupAddButtons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupAddButtons.Controls.Add(Me.buttonAddToSelectedPreGame)
        Me.groupAddButtons.Controls.Add(Me.buttonAddToPreGame)
        Me.groupAddButtons.Controls.Add(Me.buttonAddToDeck)
        Me.groupAddButtons.Location = New System.Drawing.Point(1, 356)
        Me.groupAddButtons.Name = "groupAddButtons"
        Me.groupAddButtons.Size = New System.Drawing.Size(254, 80)
        Me.groupAddButtons.TabIndex = 60
        Me.groupAddButtons.TabStop = False
        '
        'buttonAddToSelectedPreGame
        '
        Me.buttonAddToSelectedPreGame.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonAddToSelectedPreGame.Location = New System.Drawing.Point(16, 46)
        Me.buttonAddToSelectedPreGame.Name = "buttonAddToSelectedPreGame"
        Me.buttonAddToSelectedPreGame.Size = New System.Drawing.Size(220, 23)
        Me.buttonAddToSelectedPreGame.TabIndex = 60
        Me.buttonAddToSelectedPreGame.Text = "Add Underneath Selected Pre-Game"
        Me.buttonAddToSelectedPreGame.UseVisualStyleBackColor = True
        '
        'menuDeckEditor
        '
        Me.menuDeckEditor.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuDeckEditorMain})
        Me.menuDeckEditor.Location = New System.Drawing.Point(0, 0)
        Me.menuDeckEditor.Name = "menuDeckEditor"
        Me.menuDeckEditor.Size = New System.Drawing.Size(784, 24)
        Me.menuDeckEditor.TabIndex = 68
        Me.menuDeckEditor.Visible = False
        '
        'menuDeckEditorMain
        '
        Me.menuDeckEditorMain.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuSave, Me.menuSaveAs, Me.MenuItem1, Me.menuAnalyze, Me.MenuItem3, Me.menuPrintDeck})
        Me.menuDeckEditorMain.Name = "menuDeckEditorMain"
        Me.menuDeckEditorMain.Size = New System.Drawing.Size(79, 20)
        Me.menuDeckEditorMain.Text = "Deck Editor"
        '
        'menuSave
        '
        Me.menuSave.Name = "menuSave"
        Me.menuSave.Size = New System.Drawing.Size(123, 22)
        Me.menuSave.Text = "Save"
        '
        'menuSaveAs
        '
        Me.menuSaveAs.Name = "menuSaveAs"
        Me.menuSaveAs.Size = New System.Drawing.Size(123, 22)
        Me.menuSaveAs.Text = "Save As..."
        '
        'MenuItem1
        '
        Me.MenuItem1.Name = "MenuItem1"
        Me.MenuItem1.Size = New System.Drawing.Size(120, 6)
        '
        'menuAnalyze
        '
        Me.menuAnalyze.Name = "menuAnalyze"
        Me.menuAnalyze.Size = New System.Drawing.Size(123, 22)
        Me.menuAnalyze.Text = "Analyze"
        '
        'MenuItem3
        '
        Me.MenuItem3.Name = "MenuItem3"
        Me.MenuItem3.Size = New System.Drawing.Size(120, 6)
        '
        'menuPrintDeck
        '
        Me.menuPrintDeck.Name = "menuPrintDeck"
        Me.menuPrintDeck.Size = New System.Drawing.Size(123, 22)
        Me.menuPrintDeck.Text = "Print"
        '
        'dialogSaveHLD
        '
        Me.dialogSaveHLD.DefaultExt = "hld"
        Me.dialogSaveHLD.Filter = "Highlander Deck (*.hld)|*.hld"
        '
        'SplitRight
        '
        Me.SplitRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitRight.Location = New System.Drawing.Point(0, 0)
        Me.SplitRight.Name = "SplitRight"
        Me.SplitRight.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitRight.Panel1
        '
        Me.SplitRight.Panel1.Controls.Add(Me.cardsDeckList)
        '
        'SplitRight.Panel2
        '
        Me.SplitRight.Panel2.Controls.Add(Me.cardsPreGameList)
        Me.SplitRight.Size = New System.Drawing.Size(253, 561)
        Me.SplitRight.SplitterDistance = 291
        Me.SplitRight.TabIndex = 66
        '
        'cardsDeckList
        '
        Me.cardsDeckList.DisplayOriginalCardTitles = False
        Me.cardsDeckList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsDeckList.Location = New System.Drawing.Point(0, 0)
        Me.cardsDeckList.Name = "cardsDeckList"
        Me.cardsDeckList.Size = New System.Drawing.Size(249, 287)
        Me.cardsDeckList.TabIndex = 2
        '
        'cardsPreGameList
        '
        Me.cardsPreGameList.DisplayOriginalCardTitles = False
        Me.cardsPreGameList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsPreGameList.Location = New System.Drawing.Point(0, 0)
        Me.cardsPreGameList.Name = "cardsPreGameList"
        Me.cardsPreGameList.Size = New System.Drawing.Size(249, 262)
        Me.cardsPreGameList.TabIndex = 3
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.groupSearch)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CardsControl1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.groupAddButtons)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Size = New System.Drawing.Size(784, 561)
        Me.SplitContainer1.SplitterDistance = 261
        Me.SplitContainer1.TabIndex = 67
        '
        'CardsControl1
        '
        Me.CardsControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsControl1.DisplayOriginalCardTitles = False
        Me.CardsControl1.Location = New System.Drawing.Point(1, 1)
        Me.CardsControl1.Name = "CardsControl1"
        Me.CardsControl1.Size = New System.Drawing.Size(254, 357)
        Me.CardsControl1.TabIndex = 0
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.SplitMiddle)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.SplitRight)
        Me.SplitContainer2.Size = New System.Drawing.Size(519, 561)
        Me.SplitContainer2.SplitterDistance = 262
        Me.SplitContainer2.TabIndex = 0
        '
        'SplitMiddle
        '
        Me.SplitMiddle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitMiddle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitMiddle.Location = New System.Drawing.Point(0, 0)
        Me.SplitMiddle.Name = "SplitMiddle"
        Me.SplitMiddle.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitMiddle.Panel1
        '
        Me.SplitMiddle.Panel1.Controls.Add(Me.cardDetails)
        '
        'SplitMiddle.Panel2
        '
        Me.SplitMiddle.Panel2.Controls.Add(Me.controlImageFrontBack)
        Me.SplitMiddle.Size = New System.Drawing.Size(262, 561)
        Me.SplitMiddle.SplitterDistance = 368
        Me.SplitMiddle.TabIndex = 64
        '
        'cardDetails
        '
        Me.cardDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardDetails.Location = New System.Drawing.Point(0, 0)
        Me.cardDetails.Name = "cardDetails"
        Me.cardDetails.Size = New System.Drawing.Size(258, 364)
        Me.cardDetails.TabIndex = 61
        '
        'controlImageFrontBack
        '
        Me.controlImageFrontBack.Dock = System.Windows.Forms.DockStyle.Fill
        Me.controlImageFrontBack.Location = New System.Drawing.Point(0, 0)
        Me.controlImageFrontBack.Name = "controlImageFrontBack"
        Me.controlImageFrontBack.Size = New System.Drawing.Size(258, 185)
        Me.controlImageFrontBack.TabIndex = 63
        '
        'frmDeckEditor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.menuDeckEditor)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.menuDeckEditor
        Me.Name = "frmDeckEditor"
        Me.Text = "Deck Editor - New Deck"
        Me.groupSearch.ResumeLayout(False)
        Me.groupSearch.PerformLayout()
        Me.groupAddButtons.ResumeLayout(False)
        Me.menuDeckEditor.ResumeLayout(False)
        Me.menuDeckEditor.PerformLayout()
        Me.SplitRight.Panel1.ResumeLayout(False)
        Me.SplitRight.Panel2.ResumeLayout(False)
        CType(Me.SplitRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitRight.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.SplitMiddle.Panel1.ResumeLayout(False)
        Me.SplitMiddle.Panel2.ResumeLayout(False)
        CType(Me.SplitMiddle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitMiddle.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Public Sub allCardsSelectChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When the selected card title is changed, display the card's image.

        ' If at least one card is selected, load the details and image for the 
        '   last card that's selected.
        If CardsControl1.getSelectedCards().Count > 0 Then
            controlImageFrontBack.LoadCard(CardsControl1.getSelectedCards()(CardsControl1.getSelectedCards().Count - 1).Tag)
            cardDetails.LoadCard(CardsControl1.getSelectedCards()(CardsControl1.getSelectedCards().Count - 1).Tag)
        End If
    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub handles double-clicking on the currently selected card 
        '   in the CardsControl object (the one that displays every card).
        '   It will attempt to place 1 copy of the selected card onto the 
        '   correct list, based on the card type.
        Dim listToUpdate As cardsListViewer

        Dim SelectedCard As Data.Card = Data.Card.LoadCard(CardsControl1.getSelectedCards()(0).Tag, 1)
        If SelectedCard Is Nothing Then
            Exit Sub ' This should never happen.
        End If

        ' Put pre-game cards on the pre-game list, everything else on the deck list.
        If SelectedCard.IsPreGame Then
            listToUpdate = cardsPreGameList
        Else
            listToUpdate = cardsDeckList
        End If

        ' Update the list.
        listToUpdate.addCard(SelectedCard)

        ' Load the image and details of the selected card.
        controlImageFrontBack.LoadCard(CardsControl1.getSelectedCards()(0).Tag)
        cardDetails.LoadCard(CardsControl1.getSelectedCards()(0).Tag)

    End Sub

    Public Sub userListSelectChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub is responsible for handling changes to the selected card on both
        '   the deck list and pre-game list.  It displays the front and details of the 
        '   newly selected card.

        ' Load the image for the front of the card.
        If (sender.SelectedItems.Count > 0) Then
            controlImageFrontBack.LoadCard(sender.SelectedItems(sender.SelectedItems.Count - 1))
            cardDetails.LoadCard(sender.SelectedItems(sender.SelectedItems.Count - 1))
        End If
    End Sub

    Private Sub buttonAddToDeck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAddToDeck.Click
        ' Add one copy of the selected card(s) to the deck list.
        Dim oneCard As ListViewItem
        Dim LastCardId As Integer = 0

        ' Loop through each selected entry in the cards control.
        For Each oneCard In CardsControl1.getSelectedCards()

            LastCardId = oneCard.Tag

            ' Load the image and detailed information for the current card.
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(oneCard.Tag, 1)
            If SelectedCard Is Nothing Then
                Exit Sub ' This should never happen.
            End If

            ' Add the current card to the deck list.
            cardsDeckList.addCard(SelectedCard)

            Application.DoEvents()
        Next

        If LastCardId > 0 Then
            ' Load the image and details of the last selected card.
            controlImageFrontBack.LoadCard(LastCardId)
            cardDetails.LoadCard(LastCardId)
        End If
    End Sub

    Private Sub buttonAddToPreGame_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAddToPreGame.Click
        ' Add one copy of the selected card(s) to the deck list.
        Dim oneCard As ListViewItem
        Dim LastCardId As Integer = 0

        ' Loop through each selected entry in the cards control.
        For Each oneCard In CardsControl1.getSelectedCards()

            LastCardId = oneCard.Tag

            ' Load the image and detailed information for the current card.
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(oneCard.Tag, 1)
            If SelectedCard Is Nothing Then
                Exit Sub ' This should never happen.
            End If

            ' Add the current card to the deck list.
            cardsPreGameList.addCard(SelectedCard)

            Application.DoEvents()
        Next

        If LastCardId > 0 Then
            ' Load the image and details of the last selected card.
            controlImageFrontBack.LoadCard(LastCardId)
            cardDetails.LoadCard(LastCardId)
        End If
    End Sub

    Public Sub openHLDFile(ByVal fileName As String)
        Dim deck As HighlanderDeck = HLD.Load(fileName)
        For Each card As Highlander.Data.Card In deck.Deck
            cardsDeckList.addCard(card, "")
        Next
        For Each card As Highlander.Data.Card In deck.PreGame
            cardsPreGameList.addCard(card, "")
        Next

        ' Set the window title and store the file name for later saving.
        dialogSaveHLD.FileName = fileName
        Me.Text = "Deck Editor (" & fileName & ")"
    End Sub

    Private Sub menuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuSave.Click
        ' This sub is responsible for saving the current deck to a .hld file.
        '   If the deck has already been saved once, the save as dialog is skipped.
        Dim fileOkToSave As Boolean = False

        ' If a file name as not previously been specified, show the save as dialog.
        If dialogSaveHLD.FileName = "" Then
            If dialogSaveHLD.ShowDialog = DialogResult.OK Then
                fileOkToSave = True
            End If
        Else
            fileOkToSave = True
        End If

        ' If there is a file name, then save the deck.
        If fileOkToSave = True Then
            Dim deck As New HighlanderDeck
            deck.PreGame = cardsPreGameList.CardList
            deck.Deck = cardsDeckList.CardList

            If HLD.Save(dialogSaveHLD.FileName, deck) Then
                Me.Text = "Deck Editor (" & dialogSaveHLD.FileName & ")"
                MessageBox.Show("Your file has been saved.", "Saved")
            End If

        End If
    End Sub

    Private Sub menuSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuSaveAs.Click
        ' This sub is responsible for saving the deck to a file in .hld format.
        '   The user is first prompted for a file name, then if Ok is pressed,
        '   the file is saved.

        ' Prompt the user for a file name, on OK save it.
        If dialogSaveHLD.ShowDialog = DialogResult.OK = True Then
            Dim deck As New HighlanderDeck
            deck.PreGame = cardsPreGameList.CardList
            deck.Deck = cardsDeckList.CardList

            If HLD.Save(dialogSaveHLD.FileName, deck) Then
                Me.Text = "Deck Editor (" & dialogSaveHLD.FileName & ")"
                MessageBox.Show("Your file has been saved.", "Saved")
            End If
        End If
    End Sub

    Private Sub generateSearchQuery()
        CardsControl1.filterCardsList(comboSets.SelectedItem, comboPublisher.SelectedItem, txtKeywordSearch.Text, 0, False)
    End Sub

    Private Sub txtKeywordSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' Re-query the database whenever the user enters or changes
        '   the keyword search text.
        generateSearchQuery()
    End Sub

    Private Sub comboSets_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Update the list of cards whenever the selected set is changed.
        generateSearchQuery()
    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()

        Dim SelectedPublisher As Data.Publisher = Data.Publisher.LoadPublisher(comboPublisher.SelectedItem.ToString())
        If SelectedPublisher IsNot Nothing Then
            If SelectedPublisher.Id = 0 Then ' All is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets(String.Empty, Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            Else ' One Publisher is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = " & SelectedPublisher.Id.ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            End If
        End If
        comboSets.SelectedIndex = 1
    End Sub

    Private Sub menuPrintDeck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuPrintDeck.Click
        Dim windowProgress As New frmProgress
        Me.ParentForm.AddOwnedForm(windowProgress)
        Me.AddOwnedForm(windowProgress)
        windowProgress.MdiParent = Me.ParentForm
        windowProgress.Show()

        Dim windowReportViewer As New frmReportViewer
        Dim ReportData As New Reporting.DeckListData()
        Dim DeckData As New Data.HighlanderDeck
        DeckData.Deck = cardsDeckList.CardList
        DeckData.PreGame = cardsPreGameList.CardList
        Task.Factory.StartNew(Sub() ReportData.RefreshData(DeckData))
        While ReportData.Maximum = 0
            Application.DoEvents()
        End While

        windowProgress.ProgressBarStatus.Value = 0
        windowProgress.ProgressBarStatus.Maximum = ReportData.Maximum
        While ReportData.Progress <> ReportData.Maximum
            windowProgress.ProgressBarStatus.Value = ReportData.Progress
            Application.DoEvents()
        End While

        windowReportViewer.ReportViewer1.LocalReport.DataSources.Clear()
        windowReportViewer.ReportViewer1.LocalReport.ReportPath = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports\HLDeckReport.rdlc")
        windowReportViewer.ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("card", ReportData.Data.Tables("Card")))

        ' Set the report Title to the deck's file name, or use Untitled Deck if the deck hasn't been saved yet
        Dim strDeckTitle As String = ""
        If dialogSaveHLD.FileName.Length > 0 Then
            strDeckTitle = dialogSaveHLD.FileName.Substring(dialogSaveHLD.FileName.LastIndexOf("\") + 1, dialogSaveHLD.FileName.LastIndexOf(".") - dialogSaveHLD.FileName.LastIndexOf("\") - 1)
        Else
            strDeckTitle = "Untitled Deck"
        End If

        Dim p(0) As Microsoft.Reporting.WinForms.ReportParameter
        p(0) = New Microsoft.Reporting.WinForms.ReportParameter("ReportTitle", strDeckTitle)
        windowReportViewer.ReportViewer1.LocalReport.SetParameters(p)

        windowProgress.Hide()
        windowProgress.Dispose()
        windowProgress = Nothing

        '  Display the report.
        Me.ParentForm.AddOwnedForm(windowReportViewer)
        Me.AddOwnedForm(windowReportViewer)
        windowReportViewer.MdiParent = Me.ParentForm
        windowReportViewer.Text = strDeckTitle
        windowReportViewer.Show()
    End Sub

    Private Sub frmDeckEditor_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Set up the control responsible for displaying all available cards.
        CardsControl1.controlSetup(New EventHandler(AddressOf allCardsSelectChanged), New EventHandler(AddressOf allCardsDoubleClick))

        ' Set up the control responsible for maintaining the "Deck" List.
        cardsDeckList.setTitle("Deck")
        AddHandler cardsDeckList.lstCards.SelectedIndexChanged, AddressOf userListSelectChanged

        ' Set up the control responsible for maintaining the "Pre-Game" List.
        cardsPreGameList.setTitle("Pre-Game")
        AddHandler cardsPreGameList.lstCards.SelectedIndexChanged, AddressOf userListSelectChanged
        AddHandler cardsPreGameList.lstSubCards.SelectedIndexChanged, AddressOf userListSelectChanged

        ' Load Publisher Names.
        For Each onePublisher As Data.Publisher In Data.Publisher.LoadPublishers()
            comboPublisher.Items.Add(onePublisher.Name)
        Next
        comboPublisher.SelectedIndex = 1
    End Sub

    Private Sub menuAnalyze_Click(sender As System.Object, e As System.EventArgs) Handles menuAnalyze.Click

        ' Build general statistics about the deck
        Dim statistics As New DeckRules.DeckStatistics()
        Dim summary As DeckRules.DeckSummary = statistics.Analyze(dialogSaveHLD.FileName, cardsDeckList.CardList, cardsPreGameList.CardList)

        ' Create a new Deck Analysis window and set it as a child form to the main window.
        Dim windowAnalysis As New frmDeckAnalysis
        Me.ParentForm.AddOwnedForm(windowAnalysis)
        Me.AddOwnedForm(windowAnalysis)
        windowAnalysis.MdiParent = Me.ParentForm

        ' Set window / deck title
        windowAnalysis.Text &= String.Format(" ({0})", summary.DeckTitle)
        windowAnalysis.lblDeckTitle.Text = summary.DeckTitle
        windowAnalysis.lblDeckImmortal.Text &= summary.Immortal

        ' Display Deck / Pre-Game size, master card count
        windowAnalysis.lblDeckSize.Text &= summary.DeckSize.ToString()
        windowAnalysis.lblPreGameSize.Text &= summary.PreGameSize.ToString()

        ' Display Gems
        windowAnalysis.ImmortalMaster.Text = summary.PersonaAttributes.Master
        windowAnalysis.ImmortalAgility.Text = summary.PersonaAttributes.Agility
        windowAnalysis.ImmortalEmpathy.Text = summary.PersonaAttributes.Empathy
        windowAnalysis.ImmortalReason.Text = summary.PersonaAttributes.Reason
        windowAnalysis.ImmortalStrength.Text = summary.PersonaAttributes.Strength
        windowAnalysis.ImmortalToughness.Text = summary.PersonaAttributes.Toughness

        windowAnalysis.DeckMaster.Text = summary.DeckAttributes.Master
        windowAnalysis.DeckAgility.Text = summary.DeckAttributes.Agility
        windowAnalysis.DeckEmpathy.Text = summary.DeckAttributes.Empathy
        windowAnalysis.DeckReason.Text = summary.DeckAttributes.Reason
        windowAnalysis.DeckStrength.Text = summary.DeckAttributes.Strength
        windowAnalysis.DeckToughness.Text = summary.DeckAttributes.Toughness

        If summary.PersonaAttributes.Display3EGems = True Then ' the 2er / 3e toughness gem is a different color
            windowAnalysis.ImmortalToughnessGem3E.Visible = True
            windowAnalysis.DeckToughnessGem3E.Visible = True

            windowAnalysis.ImmortalToughnessGem.Visible = False
            windowAnalysis.DeckToughnessGem.Visible = False
        Else
            windowAnalysis.ImmortalToughnessGem3E.Visible = False
            windowAnalysis.DeckToughnessGem3E.Visible = False

            windowAnalysis.ImmortalToughnessGem.Visible = True
            windowAnalysis.DeckToughnessGem.Visible = True
        End If

        ' Check if deck is valid for 1st edition rules
        Dim firstEditionResult As DeckRules.Result = (New DeckRules.RulesEngine()).Analyze(cardsDeckList.CardList, cardsPreGameList.CardList, DeckFormat.FirstEdition, summary)
        For Each DeckIssue As DeckRules.ValidationError In firstEditionResult.Errors.OrderBy(Function(i) i.CardTitle)
            Dim DeckWarning As New ListViewItem
            DeckWarning.Text = DeckIssue.CardTitle
            DeckWarning.SubItems.Add(DeckIssue.Message)
            windowAnalysis.lst1stEdition.Items.Add(DeckWarning)
        Next

        windowAnalysis.TabPageH1E.Text &= " (" & firstEditionResult.Errors.Count.ToString() & ")"
        If firstEditionResult.IsDeckValid Then
            windowAnalysis.lbl1stEditionStatus.Text = "Legal"
            windowAnalysis.lbl1stEditionStatus.ForeColor = Color.Green
        Else
            windowAnalysis.lbl1stEditionStatus.Text = "Illegal"
            windowAnalysis.lbl1stEditionStatus.ForeColor = Color.Red
        End If

        ' Check if deck is valid for Missing Link Expansion rules
        Dim mleResult As DeckRules.Result = (New DeckRules.RulesEngine()).Analyze(cardsDeckList.CardList, cardsPreGameList.CardList, DeckFormat.MissingLinkExpansion, summary)
        For Each DeckIssue As DeckRules.ValidationError In mleResult.Errors.OrderBy(Function(i) i.CardTitle)
            Dim DeckWarning As New ListViewItem
            DeckWarning.Text = DeckIssue.CardTitle
            DeckWarning.SubItems.Add(DeckIssue.Message)
            windowAnalysis.lstMLE.Items.Add(DeckWarning)
        Next

        windowAnalysis.TabPageMLE.Text &= " (" & mleResult.Errors.Count.ToString() & ")"
        If mleResult.IsDeckValid Then
            windowAnalysis.lblMLEStatus.Text = "Legal"
            windowAnalysis.lblMLEStatus.ForeColor = Color.Green
        Else
            windowAnalysis.lblMLEStatus.Text = "Illegal"
            windowAnalysis.lblMLEStatus.ForeColor = Color.Red
        End If

        ' Check if deck is valid for Type One rules
        Dim typeOneResult As DeckRules.Result = (New DeckRules.RulesEngine()).Analyze(cardsDeckList.CardList, cardsPreGameList.CardList, DeckFormat.Type1, summary)
        For Each DeckIssue As DeckRules.ValidationError In typeOneResult.Errors.OrderBy(Function(i) i.CardTitle)
            Dim DeckWarning As New ListViewItem
            DeckWarning.Text = DeckIssue.CardTitle
            DeckWarning.SubItems.Add(DeckIssue.Message)
            windowAnalysis.lstType1.Items.Add(DeckWarning)
        Next

        windowAnalysis.TabPageType1.Text &= " (" & typeOneResult.Errors.Count.ToString() & ")"
        If typeOneResult.IsDeckValid Then
            windowAnalysis.lblType1Status.Text = "Legal"
            windowAnalysis.lblType1Status.ForeColor = Color.Green
        Else
            windowAnalysis.lblType1Status.Text = "Illegal"
            windowAnalysis.lblType1Status.ForeColor = Color.Red
        End If

        ' Check if deck is valid for Type Two rules
        Dim typeTwoResult As DeckRules.Result = (New DeckRules.RulesEngine()).Analyze(cardsDeckList.CardList, cardsPreGameList.CardList, DeckFormat.Type2, summary)
        For Each DeckIssue As DeckRules.ValidationError In typeTwoResult.Errors.OrderBy(Function(i) i.CardTitle)
            Dim DeckWarning As New ListViewItem
            DeckWarning.Text = DeckIssue.CardTitle
            DeckWarning.SubItems.Add(DeckIssue.Message)
            windowAnalysis.lstType2.Items.Add(DeckWarning.Clone())
        Next

        windowAnalysis.TabPageType2.Text &= " (" & typeTwoResult.Errors.Count.ToString() & ")"
        If typeTwoResult.IsDeckValid Then
            windowAnalysis.lblType2Status.Text = "Legal"
            windowAnalysis.lblType2Status.ForeColor = Color.Green
        Else
            windowAnalysis.lblType2Status.Text = "Illegal"
            windowAnalysis.lblType2Status.ForeColor = Color.Red
        End If

        ' Check if deck is valid for Highlander Pro rules
        Dim highlanderProResult As DeckRules.Result = (New DeckRules.RulesEngine()).Analyze(cardsDeckList.CardList, cardsPreGameList.CardList, DeckFormat.HighlanderPro, summary)
        For Each DeckIssue As DeckRules.ValidationError In highlanderProResult.Errors.OrderBy(Function(i) i.CardTitle)
            Dim DeckWarning As New ListViewItem
            DeckWarning.Text = DeckIssue.CardTitle
            DeckWarning.SubItems.Add(DeckIssue.Message)
            windowAnalysis.lstHighlanderPro.Items.Add(DeckWarning.Clone())
        Next

        windowAnalysis.TabPageHighlanderPro.Text &= " (" & highlanderProResult.Errors.Count.ToString() & ")"
        If highlanderProResult.IsDeckValid Then
            windowAnalysis.lblHighlanderProStatus.Text = "Legal"
            windowAnalysis.lblHighlanderProStatus.ForeColor = Color.Green
        Else
            windowAnalysis.lblHighlanderProStatus.Text = "Illegal"
            windowAnalysis.lblHighlanderProStatus.ForeColor = Color.Red
        End If

        ' Load Deck Statistics for card types (Attack, Block, etc)
        Dim valuesType(summary.CardsByType.Count) As Double
        Dim counter As Integer = 0
        For Each CardType As DeckRules.TypeStat In summary.CardsByType
            Dim NewCatagory As New ListViewItem
            NewCatagory.ForeColor = classCharts.GetColor(windowAnalysis.lstCardsByType.Items.Count)
            NewCatagory.Text = CardType.CardType
            NewCatagory.SubItems.Add(CardType.Quantity)
            NewCatagory.SubItems.Add(String.Format("{0}%", Math.Round((CardType.Quantity / summary.DeckSize) * 100)))
            windowAnalysis.lstCardsByType.Items.Add(NewCatagory)
            valuesType(counter) = (CardType.Quantity / summary.DeckSize) * 100
            counter += 1
        Next
        windowAnalysis.PictureCardsTypeChart.Image = classCharts.MakePieChart(valuesType, Convert.ToInt32(windowAnalysis.PictureCardsTypeChart.ClientSize.Width * 0.01),
                                                                    Convert.ToInt32(windowAnalysis.PictureCardsTypeChart.ClientSize.Width * 0.49))

        ' Load Deck Statistics for sub titles (Immortal, WoC, etc)
        Dim valuesImmortal(summary.CardsBySubTitle.Count) As Double
        counter = 0
        For Each OneSubTitle As DeckRules.SubTitleStat In summary.CardsBySubTitle
            Dim NewCatagory As New ListViewItem
            NewCatagory.ForeColor = classCharts.GetColor(windowAnalysis.lstCardsByImmortal.Items.Count)
            NewCatagory.Text = OneSubTitle.Immortal
            NewCatagory.SubItems.Add(OneSubTitle.Quantity)
            NewCatagory.SubItems.Add(String.Format("{0}%", Math.Round((OneSubTitle.Quantity / summary.DeckSize) * 100)))
            NewCatagory.SubItems.Add(OneSubTitle.Reserved)
            NewCatagory.SubItems.Add(OneSubTitle.Signatured)
            windowAnalysis.lstCardsByImmortal.Items.Add(NewCatagory)
            valuesImmortal(counter) = (OneSubTitle.Quantity / summary.DeckSize) * 100
            counter += 1
        Next
        windowAnalysis.PictureCardsImmortalChart.Image = classCharts.MakePieChart(valuesImmortal, Convert.ToInt32(windowAnalysis.PictureCardsImmortalChart.ClientSize.Width * 0.01),
                                                            Convert.ToInt32(windowAnalysis.PictureCardsImmortalChart.ClientSize.Width * 0.49))

        ' Display the window
        windowAnalysis.Show()
    End Sub

    Private Sub buttonAddToSelectedPreGame_Click(sender As Object, e As EventArgs) Handles buttonAddToSelectedPreGame.Click
        ' Add one copy of the selected card(s) to the selected pre-game card.
        Dim oneCard As ListViewItem
        Dim LastCardId As Integer = 0

        ' Loop through each selected entry in the cards control.
        For Each oneCard In CardsControl1.getSelectedCards()

            LastCardId = oneCard.Tag

            ' Load the image and detailed information for the current card.
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(oneCard.Tag, 1)
            If SelectedCard Is Nothing Then
                Exit Sub ' This should never happen.
            End If

            If cardsPreGameList.lstCards.SelectedItems.Count > 0 Then
                ' Add the current card to the selected pre-game card
                cardsPreGameList.addSubCard(SelectedCard)
            Else
                MessageBox.Show("Please select at least one Pre-Game Card.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            Application.DoEvents()
        Next

        If LastCardId > 0 Then
            ' Load the image and details of the last selected card.
            controlImageFrontBack.LoadCard(LastCardId)
            cardDetails.LoadCard(LastCardId)
        End If
    End Sub
End Class
