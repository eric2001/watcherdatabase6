' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Highlander.Data

Public Class frmViewTradeList
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CardsControl1 As cardsControl
    Friend WithEvents cardDetailsList As cardDetailsNoEdit
    Friend WithEvents controlHLImage As Highlander.cardImageFB
    Friend WithEvents SplitRight As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitMiddle As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitLeft As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents cardsWantList As cardsListViewer
    Friend WithEvents cardsHaveList As cardsListViewer
    Friend WithEvents cardsTradeListFull As cardsListViewer
    Friend WithEvents cardDetails As cardDetailsNoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewTradeList))
        Me.CardsControl1 = New Highlander.cardsControl()
        Me.cardDetails = New Highlander.cardDetailsNoEdit()
        Me.controlHLImage = New Highlander.cardImageFB()
        Me.SplitRight = New System.Windows.Forms.SplitContainer()
        Me.SplitMiddle = New System.Windows.Forms.SplitContainer()
        Me.SplitLeft = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.cardsTradeListFull = New Highlander.cardsListViewer()
        Me.cardsWantList = New Highlander.cardsListViewer()
        Me.cardsHaveList = New Highlander.cardsListViewer()
        CType(Me.SplitRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitRight.Panel1.SuspendLayout()
        Me.SplitRight.Panel2.SuspendLayout()
        Me.SplitRight.SuspendLayout()
        CType(Me.SplitMiddle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitMiddle.Panel1.SuspendLayout()
        Me.SplitMiddle.Panel2.SuspendLayout()
        Me.SplitMiddle.SuspendLayout()
        CType(Me.SplitLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitLeft.Panel1.SuspendLayout()
        Me.SplitLeft.Panel2.SuspendLayout()
        Me.SplitLeft.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CardsControl1
        '
        Me.CardsControl1.DisplayOriginalCardTitles = False
        Me.CardsControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CardsControl1.Location = New System.Drawing.Point(0, 0)
        Me.CardsControl1.Name = "CardsControl1"
        Me.CardsControl1.Size = New System.Drawing.Size(271, 276)
        Me.CardsControl1.TabIndex = 3
        '
        'cardDetails
        '
        Me.cardDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardDetails.Location = New System.Drawing.Point(0, 0)
        Me.cardDetails.Name = "cardDetails"
        Me.cardDetails.Size = New System.Drawing.Size(252, 356)
        Me.cardDetails.TabIndex = 50
        '
        'controlHLImage
        '
        Me.controlHLImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.controlHLImage.Location = New System.Drawing.Point(0, 0)
        Me.controlHLImage.Name = "controlHLImage"
        Me.controlHLImage.Size = New System.Drawing.Size(252, 193)
        Me.controlHLImage.TabIndex = 54
        '
        'SplitRight
        '
        Me.SplitRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitRight.Location = New System.Drawing.Point(0, 0)
        Me.SplitRight.Name = "SplitRight"
        Me.SplitRight.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitRight.Panel1
        '
        Me.SplitRight.Panel1.Controls.Add(Me.cardDetails)
        '
        'SplitRight.Panel2
        '
        Me.SplitRight.Panel2.Controls.Add(Me.controlHLImage)
        Me.SplitRight.Size = New System.Drawing.Size(256, 561)
        Me.SplitRight.SplitterDistance = 360
        Me.SplitRight.TabIndex = 55
        '
        'SplitMiddle
        '
        Me.SplitMiddle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitMiddle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitMiddle.Location = New System.Drawing.Point(0, 0)
        Me.SplitMiddle.Name = "SplitMiddle"
        Me.SplitMiddle.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitMiddle.Panel1
        '
        Me.SplitMiddle.Panel1.Controls.Add(Me.cardsWantList)
        '
        'SplitMiddle.Panel2
        '
        Me.SplitMiddle.Panel2.Controls.Add(Me.cardsHaveList)
        Me.SplitMiddle.Size = New System.Drawing.Size(245, 561)
        Me.SplitMiddle.SplitterDistance = 280
        Me.SplitMiddle.TabIndex = 56
        '
        'SplitLeft
        '
        Me.SplitLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitLeft.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitLeft.Location = New System.Drawing.Point(0, 0)
        Me.SplitLeft.Name = "SplitLeft"
        Me.SplitLeft.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitLeft.Panel1
        '
        Me.SplitLeft.Panel1.Controls.Add(Me.CardsControl1)
        '
        'SplitLeft.Panel2
        '
        Me.SplitLeft.Panel2.Controls.Add(Me.cardsTradeListFull)
        Me.SplitLeft.Size = New System.Drawing.Size(275, 561)
        Me.SplitLeft.SplitterDistance = 280
        Me.SplitLeft.TabIndex = 57
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitLeft)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitMiddle)
        Me.SplitContainer1.Size = New System.Drawing.Size(524, 561)
        Me.SplitContainer1.SplitterDistance = 275
        Me.SplitContainer1.TabIndex = 58
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.SplitContainer1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.SplitRight)
        Me.SplitContainer2.Size = New System.Drawing.Size(784, 561)
        Me.SplitContainer2.SplitterDistance = 524
        Me.SplitContainer2.TabIndex = 59
        '
        'cardsTradeListFull
        '
        Me.cardsTradeListFull.DisplayOriginalCardTitles = False
        Me.cardsTradeListFull.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsTradeListFull.Location = New System.Drawing.Point(0, 0)
        Me.cardsTradeListFull.Name = "cardsTradeListFull"
        Me.cardsTradeListFull.Size = New System.Drawing.Size(271, 273)
        Me.cardsTradeListFull.TabIndex = 0
        '
        'cardsWantList
        '
        Me.cardsWantList.DisplayOriginalCardTitles = False
        Me.cardsWantList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsWantList.Location = New System.Drawing.Point(0, 0)
        Me.cardsWantList.Name = "cardsWantList"
        Me.cardsWantList.Size = New System.Drawing.Size(241, 276)
        Me.cardsWantList.TabIndex = 0
        '
        'cardsHaveList
        '
        Me.cardsHaveList.DisplayOriginalCardTitles = False
        Me.cardsHaveList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsHaveList.Location = New System.Drawing.Point(0, 0)
        Me.cardsHaveList.Name = "cardsHaveList"
        Me.cardsHaveList.Size = New System.Drawing.Size(241, 273)
        Me.cardsHaveList.TabIndex = 0
        '
        'frmViewTradeList
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.SplitContainer2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmViewTradeList"
        Me.Text = "Trade List"
        Me.SplitRight.Panel1.ResumeLayout(False)
        Me.SplitRight.Panel2.ResumeLayout(False)
        CType(Me.SplitRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitRight.ResumeLayout(False)
        Me.SplitMiddle.Panel1.ResumeLayout(False)
        Me.SplitMiddle.Panel2.ResumeLayout(False)
        CType(Me.SplitMiddle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitMiddle.ResumeLayout(False)
        Me.SplitLeft.Panel1.ResumeLayout(False)
        Me.SplitLeft.Panel2.ResumeLayout(False)
        CType(Me.SplitLeft, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitLeft.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Sub userListCardClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub is responsible for handling changes to the selected card on both
        '   the haves list and wants list.  It displays the front and details of the 
        '   newly selected card.

        ' Load the image for the front of the card.
        If (sender.SelectedItems.Count > 0) Then
            controlHLImage.LoadCard(sender.SelectedItems(sender.SelectedItems.Count - 1))
            cardDetails.LoadCard(sender.SelectedItems(sender.SelectedItems.Count - 1))
        End If
    End Sub

    Public Sub allCardsSelectChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When the selected card title is changed, display the card's image.

        ' If at least one card is selected, load the details and image for the 
        '   last card that's selected.
        If CardsControl1.getSelectedCards().Count > 0 Then
            controlHLImage.loadCard(CardsControl1.getSelectedCards()(CardsControl1.getSelectedCards().Count - 1).Tag)
            cardDetails.LoadCard(CardsControl1.getSelectedCards()(CardsControl1.getSelectedCards().Count - 1).Tag)
        End If
    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub handles double-clicking on the currently selected card 
        '   in the CardsControl object (the one that displays every card).
        ' When a cards title is double clicked on, open its details in a new window.
        Dim oneCard As ListViewItem
        For Each oneCard In CardsControl1.getSelectedCards()
            Dim detailsWindow As New frmCardDetails
            detailsWindow.CardDetails1.LoadCard(oneCard.Tag)
            detailsWindow.Text = oneCard.Text
            Me.ParentForm.AddOwnedForm(detailsWindow)
            detailsWindow.MdiParent = Me.ParentForm
            detailsWindow.Show()
        Next
    End Sub

    Public Sub openHLTFile(ByVal fileName As String)

        Dim Quantity As Integer = 0
        Dim deck As TradeList = HLT.Load(fileName)

        For Each card As Highlander.Data.Card In deck.Haves
            ' Add each card to the full list
            cardsTradeListFull.addCard(card, "(H)")

            ' If user does not have at least one copy of this card, add it to the Want list as well
            Quantity = Inventory.GetCardQuantity(card.Id)
            If Quantity = 0 Then
                cardsWantList.addCard(card.Id, card.Quantity, String.Empty)
            End If
        Next

        For Each card As Highlander.Data.Card In deck.Wants
            ' Add each card to the full list
            cardsTradeListFull.addCard(card, "(W)")

            ' If user has at least 1 copy of this card, add it to the Have list.
            '  If the user has more copies then are wanted, add the full quantity from the want list,
            '  otherwise use the max copies the user has for the quantity
            Quantity = Inventory.GetCardQuantity(card.Id)
            If Quantity > 0 Then
                Dim QuantityToAdd As Integer = card.Quantity
                If QuantityToAdd > Quantity Then
                    QuantityToAdd = Quantity
                End If
                cardsHaveList.addCard(card.Id, QuantityToAdd, String.Empty)
            End If
        Next

        ' Update the form with the trade list file name.
        Me.Text = "Trade List Editor (" & fileName & ")"
    End Sub

    Private Sub frmViewTradeList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Set up the control responsible for displaying all available cards.
        CardsControl1.controlSetup(New EventHandler(AddressOf allCardsSelectChanged), New EventHandler(AddressOf allCardsDoubleClick))
        CardsControl1.DisplayOriginalCardTitles = True
        CardsControl1.setTitle("Your Collection")
        CardsControl1.filterCardsList("(All)", "(All)", "", 2, False)
        ' Set up Haves List
        cardsHaveList.setTitle("Cards You Have")
        cardsHaveList.DisplayOriginalCardTitles = True
        cardsHaveList.DisableEdit()
        AddHandler cardsHaveList.lstCards.SelectedIndexChanged, AddressOf userListCardClick

        ' Set Up Wants List
        cardsWantList.setTitle("Cards You Don't Have")
        cardsWantList.DisplayOriginalCardTitles = True
        cardsWantList.DisableEdit()
        AddHandler cardsWantList.lstCards.SelectedIndexChanged, AddressOf userListCardClick

        ' Set Up "Trade List" List
        cardsTradeListFull.setTitle("Entire Trade List")
        cardsTradeListFull.DisplayOriginalCardTitles = True
        cardsTradeListFull.DisableEdit()
        AddHandler cardsTradeListFull.lstCards.SelectedIndexChanged, AddressOf userListCardClick
    End Sub
End Class
