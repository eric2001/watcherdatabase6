' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmViewCollection
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents openJpegDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents groupSearch As System.Windows.Forms.GroupBox
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyword As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents CardsControl As cardsControl
    Friend WithEvents GroupInventory As System.Windows.Forms.GroupBox
    Friend WithEvents buttonMinus As System.Windows.Forms.Button
    Friend WithEvents buttonPlus As System.Windows.Forms.Button
    Friend WithEvents txtInventoryQuantity As System.Windows.Forms.TextBox
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblInventoryID As System.Windows.Forms.Label
    Friend WithEvents buttonBrowseFileBack As System.Windows.Forms.Button
    Friend WithEvents buttonBrowseFileFront As System.Windows.Forms.Button
    Friend WithEvents txtInventoryFileFront As System.Windows.Forms.TextBox
    Friend WithEvents txtInventoryFileBack As System.Windows.Forms.TextBox
    Friend WithEvents txtInventoryMLEURL As System.Windows.Forms.TextBox
    Friend WithEvents lblInventoryMLEURL As System.Windows.Forms.Label
    Friend WithEvents lblInventoryFileFront As System.Windows.Forms.Label
    Friend WithEvents lblInventoryFileBack As System.Windows.Forms.Label
    Friend WithEvents txtInventoryNotes As System.Windows.Forms.TextBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents controlHLImage As Highlander.cardImage
    Friend WithEvents SplitContainerBottom As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainerTopLeft As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainerTopRight As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents comboPublisher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPublisher As System.Windows.Forms.Label
    Friend WithEvents GroupNavigation As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonMoveForward As System.Windows.Forms.Button
    Friend WithEvents ButtonMoveBack As System.Windows.Forms.Button
    Friend WithEvents CardDetails As Highlander.cardDetailsNoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewCollection))
        Me.openJpegDialog = New System.Windows.Forms.OpenFileDialog()
        Me.GroupInventory = New System.Windows.Forms.GroupBox()
        Me.txtInventoryNotes = New System.Windows.Forms.TextBox()
        Me.lblNotes = New System.Windows.Forms.Label()
        Me.buttonBrowseFileBack = New System.Windows.Forms.Button()
        Me.buttonBrowseFileFront = New System.Windows.Forms.Button()
        Me.txtInventoryFileFront = New System.Windows.Forms.TextBox()
        Me.txtInventoryFileBack = New System.Windows.Forms.TextBox()
        Me.txtInventoryMLEURL = New System.Windows.Forms.TextBox()
        Me.lblInventoryMLEURL = New System.Windows.Forms.Label()
        Me.lblInventoryFileFront = New System.Windows.Forms.Label()
        Me.lblInventoryFileBack = New System.Windows.Forms.Label()
        Me.buttonMinus = New System.Windows.Forms.Button()
        Me.buttonPlus = New System.Windows.Forms.Button()
        Me.txtInventoryQuantity = New System.Windows.Forms.TextBox()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblInventoryID = New System.Windows.Forms.Label()
        Me.groupSearch = New System.Windows.Forms.GroupBox()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.lblKeyword = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.SplitContainerBottom = New System.Windows.Forms.SplitContainer()
        Me.SplitContainerTopLeft = New System.Windows.Forms.SplitContainer()
        Me.GroupNavigation = New System.Windows.Forms.GroupBox()
        Me.ButtonMoveForward = New System.Windows.Forms.Button()
        Me.ButtonMoveBack = New System.Windows.Forms.Button()
        Me.CardsControl = New Highlander.cardsControl()
        Me.CardDetails = New Highlander.cardDetailsNoEdit()
        Me.SplitContainerTopRight = New System.Windows.Forms.SplitContainer()
        Me.controlHLImage = New Highlander.cardImage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.GroupInventory.SuspendLayout()
        Me.groupSearch.SuspendLayout()
        CType(Me.SplitContainerBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerBottom.Panel1.SuspendLayout()
        Me.SplitContainerBottom.Panel2.SuspendLayout()
        Me.SplitContainerBottom.SuspendLayout()
        CType(Me.SplitContainerTopLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerTopLeft.Panel1.SuspendLayout()
        Me.SplitContainerTopLeft.Panel2.SuspendLayout()
        Me.SplitContainerTopLeft.SuspendLayout()
        Me.GroupNavigation.SuspendLayout()
        CType(Me.SplitContainerTopRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerTopRight.Panel1.SuspendLayout()
        Me.SplitContainerTopRight.Panel2.SuspendLayout()
        Me.SplitContainerTopRight.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'openJpegDialog
        '
        Me.openJpegDialog.DefaultExt = "*.jpg"
        Me.openJpegDialog.Title = "Select Image"
        '
        'GroupInventory
        '
        Me.GroupInventory.Controls.Add(Me.txtInventoryNotes)
        Me.GroupInventory.Controls.Add(Me.lblNotes)
        Me.GroupInventory.Controls.Add(Me.buttonBrowseFileBack)
        Me.GroupInventory.Controls.Add(Me.buttonBrowseFileFront)
        Me.GroupInventory.Controls.Add(Me.txtInventoryFileFront)
        Me.GroupInventory.Controls.Add(Me.txtInventoryFileBack)
        Me.GroupInventory.Controls.Add(Me.txtInventoryMLEURL)
        Me.GroupInventory.Controls.Add(Me.lblInventoryMLEURL)
        Me.GroupInventory.Controls.Add(Me.lblInventoryFileFront)
        Me.GroupInventory.Controls.Add(Me.lblInventoryFileBack)
        Me.GroupInventory.Controls.Add(Me.buttonMinus)
        Me.GroupInventory.Controls.Add(Me.buttonPlus)
        Me.GroupInventory.Controls.Add(Me.txtInventoryQuantity)
        Me.GroupInventory.Controls.Add(Me.lblQuantity)
        Me.GroupInventory.Controls.Add(Me.lblInventoryID)
        Me.GroupInventory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupInventory.Location = New System.Drawing.Point(0, 0)
        Me.GroupInventory.Name = "GroupInventory"
        Me.GroupInventory.Size = New System.Drawing.Size(495, 129)
        Me.GroupInventory.TabIndex = 26
        Me.GroupInventory.TabStop = False
        Me.GroupInventory.Text = "Inventory"
        '
        'txtInventoryNotes
        '
        Me.txtInventoryNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInventoryNotes.Location = New System.Drawing.Point(404, 43)
        Me.txtInventoryNotes.Multiline = True
        Me.txtInventoryNotes.Name = "txtInventoryNotes"
        Me.txtInventoryNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInventoryNotes.Size = New System.Drawing.Size(87, 80)
        Me.txtInventoryNotes.TabIndex = 14
        '
        'lblNotes
        '
        Me.lblNotes.AutoSize = True
        Me.lblNotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblNotes.Location = New System.Drawing.Point(400, 20)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(52, 16)
        Me.lblNotes.TabIndex = 13
        Me.lblNotes.Text = "Notes:"
        '
        'buttonBrowseFileBack
        '
        Me.buttonBrowseFileBack.Location = New System.Drawing.Point(307, 97)
        Me.buttonBrowseFileBack.Name = "buttonBrowseFileBack"
        Me.buttonBrowseFileBack.Size = New System.Drawing.Size(90, 29)
        Me.buttonBrowseFileBack.TabIndex = 12
        Me.buttonBrowseFileBack.Text = "Browse..."
        Me.buttonBrowseFileBack.UseVisualStyleBackColor = True
        '
        'buttonBrowseFileFront
        '
        Me.buttonBrowseFileFront.Location = New System.Drawing.Point(307, 70)
        Me.buttonBrowseFileFront.Name = "buttonBrowseFileFront"
        Me.buttonBrowseFileFront.Size = New System.Drawing.Size(90, 28)
        Me.buttonBrowseFileFront.TabIndex = 11
        Me.buttonBrowseFileFront.Text = "Browse..."
        Me.buttonBrowseFileFront.UseVisualStyleBackColor = True
        '
        'txtInventoryFileFront
        '
        Me.txtInventoryFileFront.Location = New System.Drawing.Point(180, 73)
        Me.txtInventoryFileFront.Name = "txtInventoryFileFront"
        Me.txtInventoryFileFront.Size = New System.Drawing.Size(120, 23)
        Me.txtInventoryFileFront.TabIndex = 10
        '
        'txtInventoryFileBack
        '
        Me.txtInventoryFileBack.Location = New System.Drawing.Point(180, 101)
        Me.txtInventoryFileBack.Name = "txtInventoryFileBack"
        Me.txtInventoryFileBack.Size = New System.Drawing.Size(120, 23)
        Me.txtInventoryFileBack.TabIndex = 9
        '
        'txtInventoryMLEURL
        '
        Me.txtInventoryMLEURL.Location = New System.Drawing.Point(109, 46)
        Me.txtInventoryMLEURL.Name = "txtInventoryMLEURL"
        Me.txtInventoryMLEURL.Size = New System.Drawing.Size(288, 23)
        Me.txtInventoryMLEURL.TabIndex = 8
        '
        'lblInventoryMLEURL
        '
        Me.lblInventoryMLEURL.AutoSize = True
        Me.lblInventoryMLEURL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblInventoryMLEURL.Location = New System.Drawing.Point(14, 47)
        Me.lblInventoryMLEURL.Name = "lblInventoryMLEURL"
        Me.lblInventoryMLEURL.Size = New System.Drawing.Size(75, 16)
        Me.lblInventoryMLEURL.TabIndex = 7
        Me.lblInventoryMLEURL.Text = "MLE URL:"
        '
        'lblInventoryFileFront
        '
        Me.lblInventoryFileFront.AutoSize = True
        Me.lblInventoryFileFront.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblInventoryFileFront.Location = New System.Drawing.Point(14, 74)
        Me.lblInventoryFileFront.Name = "lblInventoryFileFront"
        Me.lblInventoryFileFront.Size = New System.Drawing.Size(131, 16)
        Me.lblInventoryFileFront.TabIndex = 6
        Me.lblInventoryFileFront.Text = "File Name (Front):"
        '
        'lblInventoryFileBack
        '
        Me.lblInventoryFileBack.AutoSize = True
        Me.lblInventoryFileBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblInventoryFileBack.Location = New System.Drawing.Point(14, 102)
        Me.lblInventoryFileBack.Name = "lblInventoryFileBack"
        Me.lblInventoryFileBack.Size = New System.Drawing.Size(131, 16)
        Me.lblInventoryFileBack.TabIndex = 5
        Me.lblInventoryFileBack.Text = "File Name (Back):"
        '
        'buttonMinus
        '
        Me.buttonMinus.Location = New System.Drawing.Point(236, 16)
        Me.buttonMinus.Name = "buttonMinus"
        Me.buttonMinus.Size = New System.Drawing.Size(28, 28)
        Me.buttonMinus.TabIndex = 4
        Me.buttonMinus.Text = "-"
        Me.buttonMinus.UseVisualStyleBackColor = True
        '
        'buttonPlus
        '
        Me.buttonPlus.Location = New System.Drawing.Point(265, 16)
        Me.buttonPlus.Name = "buttonPlus"
        Me.buttonPlus.Size = New System.Drawing.Size(28, 28)
        Me.buttonPlus.TabIndex = 3
        Me.buttonPlus.Text = "+"
        Me.buttonPlus.UseVisualStyleBackColor = True
        '
        'txtInventoryQuantity
        '
        Me.txtInventoryQuantity.Location = New System.Drawing.Point(109, 18)
        Me.txtInventoryQuantity.Name = "txtInventoryQuantity"
        Me.txtInventoryQuantity.Size = New System.Drawing.Size(120, 23)
        Me.txtInventoryQuantity.TabIndex = 2
        '
        'lblQuantity
        '
        Me.lblQuantity.AutoSize = True
        Me.lblQuantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblQuantity.Location = New System.Drawing.Point(14, 20)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(67, 16)
        Me.lblQuantity.TabIndex = 1
        Me.lblQuantity.Text = "Quantity:"
        '
        'lblInventoryID
        '
        Me.lblInventoryID.AutoSize = True
        Me.lblInventoryID.Location = New System.Drawing.Point(108, 23)
        Me.lblInventoryID.Name = "lblInventoryID"
        Me.lblInventoryID.Size = New System.Drawing.Size(0, 15)
        Me.lblInventoryID.TabIndex = 0
        '
        'groupSearch
        '
        Me.groupSearch.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupSearch.Controls.Add(Me.comboPublisher)
        Me.groupSearch.Controls.Add(Me.lblPublisher)
        Me.groupSearch.Controls.Add(Me.txtKeywordSearch)
        Me.groupSearch.Controls.Add(Me.lblKeyword)
        Me.groupSearch.Controls.Add(Me.lblSet)
        Me.groupSearch.Controls.Add(Me.comboSets)
        Me.groupSearch.Location = New System.Drawing.Point(0, 0)
        Me.groupSearch.Name = "groupSearch"
        Me.groupSearch.Size = New System.Drawing.Size(276, 131)
        Me.groupSearch.TabIndex = 27
        Me.groupSearch.TabStop = False
        Me.groupSearch.Text = "Filter"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(102, 18)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(167, 23)
        Me.comboPublisher.TabIndex = 28
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblPublisher.Location = New System.Drawing.Point(7, 17)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(73, 18)
        Me.lblPublisher.TabIndex = 27
        Me.lblPublisher.Text = "Publisher:"
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(102, 94)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(167, 23)
        Me.txtKeywordSearch.TabIndex = 26
        '
        'lblKeyword
        '
        Me.lblKeyword.AutoSize = True
        Me.lblKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblKeyword.Location = New System.Drawing.Point(7, 92)
        Me.lblKeyword.Name = "lblKeyword"
        Me.lblKeyword.Size = New System.Drawing.Size(70, 18)
        Me.lblKeyword.TabIndex = 25
        Me.lblKeyword.Text = "Keyword:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(7, 54)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(34, 18)
        Me.lblSet.TabIndex = 24
        Me.lblSet.Text = "Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(102, 55)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(167, 23)
        Me.comboSets.TabIndex = 23
        '
        'SplitContainerBottom
        '
        Me.SplitContainerBottom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerBottom.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerBottom.Name = "SplitContainerBottom"
        '
        'SplitContainerBottom.Panel1
        '
        Me.SplitContainerBottom.Panel1.Controls.Add(Me.groupSearch)
        '
        'SplitContainerBottom.Panel2
        '
        Me.SplitContainerBottom.Panel2.Controls.Add(Me.GroupInventory)
        Me.SplitContainerBottom.Size = New System.Drawing.Size(784, 133)
        Me.SplitContainerBottom.SplitterDistance = 281
        Me.SplitContainerBottom.TabIndex = 29
        '
        'SplitContainerTopLeft
        '
        Me.SplitContainerTopLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerTopLeft.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerTopLeft.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerTopLeft.Name = "SplitContainerTopLeft"
        '
        'SplitContainerTopLeft.Panel1
        '
        Me.SplitContainerTopLeft.Panel1.Controls.Add(Me.GroupNavigation)
        Me.SplitContainerTopLeft.Panel1.Controls.Add(Me.CardsControl)
        '
        'SplitContainerTopLeft.Panel2
        '
        Me.SplitContainerTopLeft.Panel2.Controls.Add(Me.CardDetails)
        Me.SplitContainerTopLeft.Size = New System.Drawing.Size(565, 424)
        Me.SplitContainerTopLeft.SplitterDistance = 281
        Me.SplitContainerTopLeft.TabIndex = 30
        '
        'GroupNavigation
        '
        Me.GroupNavigation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupNavigation.Controls.Add(Me.ButtonMoveForward)
        Me.GroupNavigation.Controls.Add(Me.ButtonMoveBack)
        Me.GroupNavigation.Location = New System.Drawing.Point(5, 368)
        Me.GroupNavigation.Name = "GroupNavigation"
        Me.GroupNavigation.Size = New System.Drawing.Size(273, 52)
        Me.GroupNavigation.TabIndex = 1
        Me.GroupNavigation.TabStop = False
        Me.GroupNavigation.Text = "Navigation"
        '
        'ButtonMoveForward
        '
        Me.ButtonMoveForward.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ButtonMoveForward.Location = New System.Drawing.Point(140, 15)
        Me.ButtonMoveForward.Name = "ButtonMoveForward"
        Me.ButtonMoveForward.Size = New System.Drawing.Size(90, 28)
        Me.ButtonMoveForward.TabIndex = 1
        Me.ButtonMoveForward.Text = "Next >>"
        Me.ButtonMoveForward.UseVisualStyleBackColor = True
        '
        'ButtonMoveBack
        '
        Me.ButtonMoveBack.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ButtonMoveBack.Location = New System.Drawing.Point(40, 15)
        Me.ButtonMoveBack.Name = "ButtonMoveBack"
        Me.ButtonMoveBack.Size = New System.Drawing.Size(90, 28)
        Me.ButtonMoveBack.TabIndex = 0
        Me.ButtonMoveBack.Text = "<< Previous"
        Me.ButtonMoveBack.UseVisualStyleBackColor = True
        '
        'CardsControl
        '
        Me.CardsControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsControl.Location = New System.Drawing.Point(5, 0)
        Me.CardsControl.Name = "CardsControl"
        Me.CardsControl.Size = New System.Drawing.Size(273, 362)
        Me.CardsControl.TabIndex = 0
        '
        'CardDetails
        '
        Me.CardDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CardDetails.Location = New System.Drawing.Point(0, 0)
        Me.CardDetails.Name = "CardDetails"
        Me.CardDetails.Size = New System.Drawing.Size(276, 420)
        Me.CardDetails.TabIndex = 2
        '
        'SplitContainerTopRight
        '
        Me.SplitContainerTopRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerTopRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerTopRight.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerTopRight.Name = "SplitContainerTopRight"
        '
        'SplitContainerTopRight.Panel1
        '
        Me.SplitContainerTopRight.Panel1.Controls.Add(Me.SplitContainerTopLeft)
        '
        'SplitContainerTopRight.Panel2
        '
        Me.SplitContainerTopRight.Panel2.Controls.Add(Me.controlHLImage)
        Me.SplitContainerTopRight.Size = New System.Drawing.Size(784, 424)
        Me.SplitContainerTopRight.SplitterDistance = 565
        Me.SplitContainerTopRight.TabIndex = 31
        '
        'controlHLImage
        '
        Me.controlHLImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.controlHLImage.Location = New System.Drawing.Point(0, 0)
        Me.controlHLImage.Name = "controlHLImage"
        Me.controlHLImage.Size = New System.Drawing.Size(211, 420)
        Me.controlHLImage.TabIndex = 28
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainerTopRight)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainerBottom)
        Me.SplitContainer1.Size = New System.Drawing.Size(784, 561)
        Me.SplitContainer1.SplitterDistance = 424
        Me.SplitContainer1.TabIndex = 32
        '
        'frmViewCollection
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmViewCollection"
        Me.Text = "View Collection"
        Me.GroupInventory.ResumeLayout(False)
        Me.GroupInventory.PerformLayout()
        Me.groupSearch.ResumeLayout(False)
        Me.groupSearch.PerformLayout()
        Me.SplitContainerBottom.Panel1.ResumeLayout(False)
        Me.SplitContainerBottom.Panel2.ResumeLayout(False)
        CType(Me.SplitContainerBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerBottom.ResumeLayout(False)
        Me.SplitContainerTopLeft.Panel1.ResumeLayout(False)
        Me.SplitContainerTopLeft.Panel2.ResumeLayout(False)
        CType(Me.SplitContainerTopLeft, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerTopLeft.ResumeLayout(False)
        Me.GroupNavigation.ResumeLayout(False)
        Me.SplitContainerTopRight.Panel1.ResumeLayout(False)
        Me.SplitContainerTopRight.Panel2.ResumeLayout(False)
        CType(Me.SplitContainerTopRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerTopRight.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub generateSearchQuery()
        CardsControl.filterCardsList(comboSets.SelectedItem, comboPublisher.SelectedItem, txtKeywordSearch.Text, 0, False)
    End Sub

    Private Sub txtKeywordSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' Re-query the database whenever the user enters or changes
        '   the keyword search text.
        generateSearchQuery()
    End Sub

    Public Sub allCardsSingleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When a card title is selected, display the card's image and load
        '   detailed information about it into the card details control.

        ' If a card was already loaded into the details form, save any user editable fields to the inventory.
        If lblInventoryID.Text <> "" Then
            If Data.Inventory.SaveInventoryForCard(lblInventoryID.Text,
                                                       Convert.ToInt32(txtInventoryQuantity.Text),
                                                       txtInventoryFileFront.Text,
                                                       txtInventoryFileBack.Text,
                                                       txtInventoryMLEURL.Text,
                                                       txtInventoryNotes.Text) Then
                If Data.InventoryDB.FileName <> "" Then
                    Me.ParentForm.Text = "Watcher Database (*" & Data.InventoryDB.FileName & ")"
                End If
            End If
        End If

        ' Display the selected card(s)
        For Each oneCard As ListViewItem In CardsControl.getSelectedCards()
            controlHLImage.loadCard(oneCard.Tag)
            CardDetails.LoadCard(oneCard.Tag)

            Dim SelectedInventory As Data.Inventory = Data.Inventory.GetInventoryForCard(oneCard.Tag)
            lblInventoryID.Text = oneCard.Tag
            txtInventoryQuantity.Text = SelectedInventory.Quantity.ToString()
            txtInventoryFileBack.Text = SelectedInventory.FileNameBack
            txtInventoryFileFront.Text = SelectedInventory.FileNameFront
            txtInventoryMLEURL.Text = SelectedInventory.MleUrl
            txtInventoryNotes.Text = SelectedInventory.Notes
        Next

    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When a cards title is double clicked on, open its details in a new window.
        Dim oneCard As ListViewItem
        For Each oneCard In CardsControl.getSelectedCards()
            Dim detailsWindow As New frmCardDetails
            detailsWindow.CardDetails1.LoadCard(oneCard.Tag)
            detailsWindow.Text = oneCard.Text
            Me.ParentForm.AddOwnedForm(detailsWindow)
            detailsWindow.MdiParent = Me.ParentForm
            detailsWindow.Show()
        Next
    End Sub

    Private Sub buttonMinus_Click(sender As System.Object, e As System.EventArgs) Handles buttonMinus.Click
        If txtInventoryQuantity.Text = "" Then
            txtInventoryQuantity.Text = 0
        End If
        If txtInventoryQuantity.Text > 0 Then
            txtInventoryQuantity.Text = (Convert.ToInt32(txtInventoryQuantity.Text) - 1).ToString()
        End If

        CardDetails.cardQuantity.Text = txtInventoryQuantity.Text
    End Sub

    Private Sub buttonPlus_Click(sender As System.Object, e As System.EventArgs) Handles buttonPlus.Click
        If txtInventoryQuantity.Text = "" Then
            txtInventoryQuantity.Text = 0
        End If
        txtInventoryQuantity.Text = (Convert.ToInt32(txtInventoryQuantity.Text) + 1).ToString()

        CardDetails.cardQuantity.Text = txtInventoryQuantity.Text
    End Sub

    Private Sub buttonBrowseFileFront_Click(sender As System.Object, e As System.EventArgs) Handles buttonBrowseFileFront.Click
        If openJpegDialog.ShowDialog() = DialogResult.OK Then
            txtInventoryFileFront.Text = openJpegDialog.FileName
        End If
    End Sub

    Private Sub buttonBrowseFileBack_Click(sender As System.Object, e As System.EventArgs) Handles buttonBrowseFileBack.Click
        If openJpegDialog.ShowDialog() = DialogResult.OK Then
            txtInventoryFileBack.Text = openJpegDialog.FileName
        End If
    End Sub

    Private Sub txtInventoryQuantity_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtInventoryQuantity.TextChanged
        If txtInventoryQuantity.Text = "" Then
            txtInventoryQuantity.Text = 0
        End If
    End Sub

    Private Sub frmViewCollection_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' If a card was already loaded into the details form, save any user editable fields to the inventory before closing the window.
        If lblInventoryID.Text <> "" Then
            If Data.Inventory.SaveInventoryForCard(lblInventoryID.Text,
                                                       Convert.ToInt32(txtInventoryQuantity.Text),
                                                       txtInventoryFileFront.Text,
                                                       txtInventoryFileBack.Text,
                                                       txtInventoryMLEURL.Text,
                                                       txtInventoryNotes.Text) Then
                If Data.InventoryDB.FileName <> "" Then
                    Me.ParentForm.Text = "Watcher Database (*" & Data.InventoryDB.FileName & ")"
                End If
            End If
        End If
    End Sub

    Private Sub frmViewCollection_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        ' Auto increase / decrease quantity when + and - are pressed.

        ' If the use is in a text box, don't do anything.
        If txtKeywordSearch.Focused Or txtInventoryMLEURL.Focused Or
            txtInventoryFileBack.Focused Or txtInventoryFileFront.Focused Or
            txtInventoryNotes.Focused Then
            Exit Sub
        End If

        If e.KeyCode = Keys.OemMinus Or e.KeyCode = Keys.Subtract Then
            buttonMinus_Click(sender, e)
        ElseIf e.KeyCode = Keys.Oemplus Or e.KeyCode = Keys.Add Then
            buttonPlus_Click(sender, e)
        End If
    End Sub

    Private Sub frmViewCollection_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CardsControl.controlSetup(New EventHandler(AddressOf allCardsSingleClick), New EventHandler(AddressOf allCardsDoubleClick))
        CardsControl.DisplayOriginalCardTitles = True

        ' Load Publisher Names.
        For Each onePublisher As Data.Publisher In Data.Publisher.LoadPublishers()
            comboPublisher.Items.Add(onePublisher.Name)
        Next
        comboPublisher.SelectedIndex = 1
    End Sub

    Private Sub comboSets_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Update the list of cards whenever the selected set is changed.
        generateSearchQuery()
    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()

        Dim SelectedPublisher As Data.Publisher = Data.Publisher.LoadPublisher(comboPublisher.SelectedItem.ToString())
        If SelectedPublisher IsNot Nothing Then
            If SelectedPublisher.Id = 0 Then ' All is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets(String.Empty, Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            Else ' One Publisher is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = " & SelectedPublisher.Id.ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            End If
        End If
        comboSets.SelectedIndex = 1
    End Sub

    Private Sub ButtonMoveBack_Click(sender As System.Object, e As System.EventArgs) Handles ButtonMoveBack.Click
        CardsControl.movePrevious()
    End Sub

    Private Sub ButtonMoveForward_Click(sender As System.Object, e As System.EventArgs) Handles ButtonMoveForward.Click
        CardsControl.moveNext()
    End Sub

End Class
