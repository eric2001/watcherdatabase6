' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class frmImageViewer
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents hlCard As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImageViewer))
        Me.hlCard = New System.Windows.Forms.PictureBox()
        CType(Me.hlCard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hlCard
        '
        Me.hlCard.Location = New System.Drawing.Point(0, 0)
        Me.hlCard.Name = "hlCard"
        Me.hlCard.Size = New System.Drawing.Size(116, 125)
        Me.hlCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.hlCard.TabIndex = 0
        Me.hlCard.TabStop = False
        '
        'frmImageViewer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(116, 106)
        Me.Controls.Add(Me.hlCard)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmImageViewer"
        Me.Text = "frmImageViewer"
        CType(Me.hlCard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Public Sub loadImage(ByVal imagePath As String)
        ' Load the image specified in imagePath into the form.
        '   In the event of an error, load empty.jpg instead.
        Try
            hlCard.Image = System.Drawing.Bitmap.FromFile(imagePath)
        Catch ex As Exception
            hlCard.Image = System.Drawing.Bitmap.FromFile(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "1eback.jpg"))
        End Try

        ' Resize the window to match the image's height and width.
        Me.Width = hlCard.Width + 4
        Me.Height = hlCard.Height + 26

        ' Set the image to resize with the window.
        hlCard.SizeMode = PictureBoxSizeMode.Zoom
        hlCard.Dock = DockStyle.Fill
    End Sub
End Class
