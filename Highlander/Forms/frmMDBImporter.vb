﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports System.Data.OleDb

Public Class FormMDBImporter
    Dim BrowseDialog As New OpenFileDialog

    Private Sub frmMDBImporter_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Data.InventoryDB.GetInventoryRecordCount() > 0 Then
            MessageBox.Show("You have existing inventory and / or file name data already loaded.  " &
                            "If you continue with the import, any conflicting data will be replaced " &
                            "with the imported data.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        Dim strProgramDirectory As String = ""
        If System.IO.Directory.Exists("C:\Program Files (x86)") Then
            strProgramDirectory = "C:\Program Files (x86)"
        Else
            strProgramDirectory = "C:\Program Files"
        End If

        If System.IO.Directory.Exists(strProgramDirectory & "\Watcher Database") Then
            strProgramDirectory &= "\Watcher Database"
        ElseIf System.IO.Directory.Exists(strProgramDirectory & "\Highlander") Then
            strProgramDirectory &= "\Highlander"
        End If

        If System.IO.File.Exists(strProgramDirectory & "\cards.mdb") Then
            strProgramDirectory &= "\cards.mdb"
            lblInfo.Text &= vbCrLf & strProgramDirectory
            BrowseDialog.FileName = strProgramDirectory
        End If

        Dim strUserDirectory As String = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)
        strUserDirectory &= "\VirtualStore"
        strUserDirectory &= strProgramDirectory.Substring(2)

        If System.IO.File.Exists(strUserDirectory) Then
            lblInfo.Text &= vbCrLf & strUserDirectory
            BrowseDialog.FileName = strUserDirectory
        End If
    End Sub

    Private Sub ButtonBrowse_Click(sender As System.Object, e As System.EventArgs) Handles ButtonBrowse.Click
        BrowseDialog.DefaultExt = "mdb"
        BrowseDialog.Filter = "Microsoft Access Database (*.mdb)|*.mdb"
        If BrowseDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtCardsMDB.Text = BrowseDialog.FileName
        End If
    End Sub

    Private Sub ButtonImport_Click(sender As System.Object, e As System.EventArgs) Handles ButtonImport.Click
        Dim con As New OleDb.OleDbConnection
        Dim dbProvider As String
        Dim dbSource As String
        Dim ds As New DataSet
        Dim da As OleDb.OleDbDataAdapter
        Dim sql As String

        dbProvider = "PROVIDER=Microsoft.Jet.OLEDB.4.0;"
        dbSource = "Data Source = " & txtCardsMDB.Text & ";"
        con.ConnectionString = dbProvider & dbSource
        con.Open()

        sql = "Select * FROM cards"
        da = New OleDb.OleDbDataAdapter(sql, con)
        Dim builder As OleDbCommandBuilder = New OleDbCommandBuilder(da)
        da.Fill(ds, "cards")

        Dim counter As Integer = 0
        ProgressImport.Maximum = ds.Tables(0).Rows.Count
        ProgressImport.Value = 0

        For counter = 0 To ds.Tables(0).Rows.Count - 1
            Dim CardResults() As DataRow
            CardResults = Data.CardsDB.Data.Tables("Card").Select("ID = " & ds.Tables(0).Rows(counter).Item("ID").ToString())
            If CardResults.Length > 0 Then
                Dim CurrentQuantity As String = ds.Tables(0).Rows(counter).Item("Number").ToString()
                Dim CurrentFileNameFront As String = ds.Tables(0).Rows(counter).Item("File Name").ToString()
                Dim CurrentFileNameBack As String = ""

                If CurrentQuantity = "" Then
                    CurrentQuantity = "0"
                End If
                If CurrentFileNameFront.Contains("|") Then
                    CurrentFileNameBack = CurrentFileNameFront.Substring(CurrentFileNameFront.IndexOf("|") + 1)
                    CurrentFileNameFront = CurrentFileNameFront.Substring(1, CurrentFileNameFront.IndexOf("|") - 1)
                End If

                If (CurrentQuantity <> Data.Inventory.GetCardQuantity(ds.Tables(0).Rows(counter).Item("ID"))) Or
                    (CurrentFileNameFront <> "" And CurrentFileNameFront <> CardResults(0).Item("File Name Front").ToString()) Or
                    (CurrentFileNameBack <> "" And CurrentFileNameBack <> CardResults(0).Item("File Name Back").ToString()) Then

                    Dim SelectedCard As Data.Inventory = Data.Inventory.GetInventoryForCard(ds.Tables(0).Rows(counter).Item("ID"))
                    SelectedCard.Quantity = CurrentQuantity
                    SelectedCard.FileNameBack = CurrentFileNameBack
                    SelectedCard.FileNameFront = CurrentFileNameFront
                    Data.Inventory.SaveInventoryForCard(SelectedCard)
                End If
            Else
                ListMissingCards.Items.Add(ds.Tables(0).Rows(counter).Item("ID").ToString())
                ListMissingCards.Items(ListMissingCards.Items.Count - 1).SubItems.Add(ds.Tables(0).Rows(counter).Item("Name").ToString())
                ListMissingCards.Items(ListMissingCards.Items.Count - 1).SubItems.Add(ds.Tables(0).Rows(counter).Item("Set").ToString())
            End If
            ProgressImport.Value += 1
        Next
        con.Close()

        If ListMissingCards.Items.Count > 0 Then
            If ListMissingCards.Items.Count = 1 Then
                MessageBox.Show("Import Complete.  " & ListMissingCards.Items.Count.ToString() & " card was not found in the version 6 database and has been skipped.  Select Save Inventory from the File menu to save the imported data.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Import Complete.  " & ListMissingCards.Items.Count.ToString() & " cards were not found in the version 6 database and have been skipped.  Select Save Inventory from the File menu to save the imported data.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Else
            MessageBox.Show("Import Complete.  Select Save Inventory from the File menu to save the imported data.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub SaveListToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SaveListToolStripMenuItem.Click
        If ListMissingCards.Items.Count = 0 Then
            MessageBox.Show("There's nothing to save.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim MissingCardsLogFile As New SaveFileDialog
            MissingCardsLogFile.DefaultExt = "txt"
            MissingCardsLogFile.Filter = "Text File (*.txt)|*.txt"
            If MissingCardsLogFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Dim LogFile As New System.IO.StreamWriter(MissingCardsLogFile.FileName)
                For Each OneCard As ListViewItem In ListMissingCards.Items
                    LogFile.WriteLine(OneCard.Text & vbTab & OneCard.SubItems(1).Text & vbTab & OneCard.SubItems(2).Text)
                Next
                LogFile.Close()
                LogFile.Dispose()
                LogFile = Nothing

                MessageBox.Show("Your File was Saved Successfully.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
End Class