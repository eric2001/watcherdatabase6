﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeckSelectPersona
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeckSelectPersona))
        Me.lblSelectPersona = New System.Windows.Forms.Label()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ButtonOk = New System.Windows.Forms.Button()
        Me.lstPersonas = New System.Windows.Forms.ListView()
        Me.ColumnPersona = New System.Windows.Forms.ColumnHeader()
        Me.SuspendLayout()
        '
        'lblSelectPersona
        '
        Me.lblSelectPersona.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblSelectPersona.Location = New System.Drawing.Point(14, 10)
        Me.lblSelectPersona.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSelectPersona.Name = "lblSelectPersona"
        Me.lblSelectPersona.Size = New System.Drawing.Size(420, 48)
        Me.lblSelectPersona.TabIndex = 0
        Me.lblSelectPersona.Text = "Your deck has multiple persona cards in it.  Please select your Primary persona c" &
    "ard to continue."
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(133, 269)
        Me.ButtonCancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(88, 27)
        Me.ButtonCancel.TabIndex = 2
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'ButtonOk
        '
        Me.ButtonOk.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ButtonOk.Location = New System.Drawing.Point(227, 269)
        Me.ButtonOk.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButtonOk.Name = "ButtonOk"
        Me.ButtonOk.Size = New System.Drawing.Size(88, 27)
        Me.ButtonOk.TabIndex = 3
        Me.ButtonOk.Text = "Ok"
        Me.ButtonOk.UseVisualStyleBackColor = True
        '
        'lstPersonas
        '
        Me.lstPersonas.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnPersona})
        Me.lstPersonas.Location = New System.Drawing.Point(18, 63)
        Me.lstPersonas.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.lstPersonas.Name = "lstPersonas"
        Me.lstPersonas.Size = New System.Drawing.Size(416, 198)
        Me.lstPersonas.TabIndex = 4
        Me.lstPersonas.UseCompatibleStateImageBehavior = False
        Me.lstPersonas.View = System.Windows.Forms.View.Details
        '
        'ColumnPersona
        '
        Me.ColumnPersona.Text = "Persona"
        Me.ColumnPersona.Width = 330
        '
        'frmDeckSelectPersona
        '
        Me.AcceptButton = Me.ButtonOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(444, 301)
        Me.Controls.Add(Me.lstPersonas)
        Me.Controls.Add(Me.ButtonOk)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.lblSelectPersona)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmDeckSelectPersona"
        Me.Text = "Select Persona"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblSelectPersona As System.Windows.Forms.Label
    Friend WithEvents ButtonCancel As System.Windows.Forms.Button
    Friend WithEvents ButtonOk As System.Windows.Forms.Button
    Friend WithEvents lstPersonas As System.Windows.Forms.ListView
    Friend WithEvents ColumnPersona As System.Windows.Forms.ColumnHeader
End Class
