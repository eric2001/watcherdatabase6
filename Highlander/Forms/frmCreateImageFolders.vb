﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmCreateImageFolders

    Private Sub frmCreateImageFolders_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtImageFolder.Text = Data.CardsDB.HighlanderImagePath
    End Sub

    Private Sub buttonBrowse_Click(sender As System.Object, e As System.EventArgs) Handles buttonBrowse.Click
        Dim FolderPath As New FolderBrowserDialog
        If FolderPath.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtImageFolder.Text = FolderPath.SelectedPath
        End If
    End Sub

    Private Sub buttonCreateFolders_Click(sender As System.Object, e As System.EventArgs) Handles buttonCreateFolders.Click
        If Not System.IO.Directory.Exists(txtImageFolder.Text) Then
            System.IO.Directory.CreateDirectory(txtImageFolder.Text)
        End If
        If Not System.IO.Directory.Exists(txtImageFolder.Text & "\Misprints and Oddities") Then
            System.IO.Directory.CreateDirectory(txtImageFolder.Text & "\Misprints and Oddities")
        End If
        For Each OneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher > 0")
            If Not System.IO.Directory.Exists(IO.Path.Combine(txtImageFolder.Text, OneSet.Publisher.Name)) Then
                System.IO.Directory.CreateDirectory(IO.Path.Combine(txtImageFolder.Text, OneSet.Publisher.Name))
            End If
            If Not System.IO.Directory.Exists(IO.Path.Combine(txtImageFolder.Text, OneSet.Publisher.Name, OneSet.Name.Replace("/", "and"))) Then
                System.IO.Directory.CreateDirectory(IO.Path.Combine(txtImageFolder.Text, OneSet.Publisher.Name, OneSet.Name.Replace("/", "and")))
            End If
        Next

        MessageBox.Show("Folders Created Successfully!", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub
End Class