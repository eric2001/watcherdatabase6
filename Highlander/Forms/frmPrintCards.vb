' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmPrintCards
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents groupBoxControls2 As System.Windows.Forms.GroupBox
    Friend WithEvents buttonPrint As System.Windows.Forms.Button
    Friend WithEvents buttonClear As System.Windows.Forms.Button
    Friend WithEvents groupBoxControls1 As System.Windows.Forms.GroupBox
    Friend WithEvents buttonAddFront As System.Windows.Forms.Button
    Friend WithEvents groupSearch As System.Windows.Forms.GroupBox
    Friend WithEvents checkBoxCardsWithImages As System.Windows.Forms.CheckBox
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyword As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents controlHLImage As cardImage
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents CardsListPrint As Highlander.cardsListViewer
    Friend WithEvents txtPrintQuantity As System.Windows.Forms.TextBox
    Friend WithEvents ButtonMinus As System.Windows.Forms.Button
    Friend WithEvents ButtonPlus As System.Windows.Forms.Button
    Friend WithEvents comboPublisher As ComboBox
    Friend WithEvents lblPublisher As Label
    Friend WithEvents buttonAddBack As Button
    Friend WithEvents CardsControl1 As cardsControl
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrintCards))
        Me.groupBoxControls2 = New System.Windows.Forms.GroupBox()
        Me.buttonPrint = New System.Windows.Forms.Button()
        Me.buttonClear = New System.Windows.Forms.Button()
        Me.groupBoxControls1 = New System.Windows.Forms.GroupBox()
        Me.buttonAddBack = New System.Windows.Forms.Button()
        Me.ButtonMinus = New System.Windows.Forms.Button()
        Me.ButtonPlus = New System.Windows.Forms.Button()
        Me.txtPrintQuantity = New System.Windows.Forms.TextBox()
        Me.buttonAddFront = New System.Windows.Forms.Button()
        Me.groupSearch = New System.Windows.Forms.GroupBox()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.checkBoxCardsWithImages = New System.Windows.Forms.CheckBox()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.lblKeyword = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.CardsControl1 = New Highlander.cardsControl()
        Me.controlHLImage = New Highlander.cardImage()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.CardsListPrint = New Highlander.cardsListViewer()
        Me.groupBoxControls2.SuspendLayout()
        Me.groupBoxControls1.SuspendLayout()
        Me.groupSearch.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupBoxControls2
        '
        Me.groupBoxControls2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBoxControls2.Controls.Add(Me.buttonPrint)
        Me.groupBoxControls2.Controls.Add(Me.buttonClear)
        Me.groupBoxControls2.Location = New System.Drawing.Point(4, 497)
        Me.groupBoxControls2.Name = "groupBoxControls2"
        Me.groupBoxControls2.Size = New System.Drawing.Size(236, 58)
        Me.groupBoxControls2.TabIndex = 46
        Me.groupBoxControls2.TabStop = False
        '
        'buttonPrint
        '
        Me.buttonPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.buttonPrint.Location = New System.Drawing.Point(10, 18)
        Me.buttonPrint.Name = "buttonPrint"
        Me.buttonPrint.Size = New System.Drawing.Size(96, 29)
        Me.buttonPrint.TabIndex = 32
        Me.buttonPrint.Text = "Print"
        '
        'buttonClear
        '
        Me.buttonClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonClear.Location = New System.Drawing.Point(131, 18)
        Me.buttonClear.Name = "buttonClear"
        Me.buttonClear.Size = New System.Drawing.Size(96, 29)
        Me.buttonClear.TabIndex = 33
        Me.buttonClear.Text = "Clear List"
        '
        'groupBoxControls1
        '
        Me.groupBoxControls1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBoxControls1.Controls.Add(Me.buttonAddBack)
        Me.groupBoxControls1.Controls.Add(Me.ButtonMinus)
        Me.groupBoxControls1.Controls.Add(Me.ButtonPlus)
        Me.groupBoxControls1.Controls.Add(Me.txtPrintQuantity)
        Me.groupBoxControls1.Controls.Add(Me.buttonAddFront)
        Me.groupBoxControls1.Location = New System.Drawing.Point(1, 290)
        Me.groupBoxControls1.Name = "groupBoxControls1"
        Me.groupBoxControls1.Size = New System.Drawing.Size(257, 118)
        Me.groupBoxControls1.TabIndex = 45
        Me.groupBoxControls1.TabStop = False
        '
        'buttonAddBack
        '
        Me.buttonAddBack.AutoSize = True
        Me.buttonAddBack.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.buttonAddBack.Location = New System.Drawing.Point(9, 84)
        Me.buttonAddBack.Name = "buttonAddBack"
        Me.buttonAddBack.Size = New System.Drawing.Size(127, 25)
        Me.buttonAddBack.TabIndex = 33
        Me.buttonAddBack.Text = "Add Back To The List"
        Me.buttonAddBack.UseVisualStyleBackColor = True
        '
        'ButtonMinus
        '
        Me.ButtonMinus.Location = New System.Drawing.Point(50, 19)
        Me.ButtonMinus.Name = "ButtonMinus"
        Me.ButtonMinus.Size = New System.Drawing.Size(28, 29)
        Me.ButtonMinus.TabIndex = 32
        Me.ButtonMinus.Text = "-"
        Me.ButtonMinus.UseVisualStyleBackColor = True
        '
        'ButtonPlus
        '
        Me.ButtonPlus.Location = New System.Drawing.Point(84, 19)
        Me.ButtonPlus.Name = "ButtonPlus"
        Me.ButtonPlus.Size = New System.Drawing.Size(28, 29)
        Me.ButtonPlus.TabIndex = 31
        Me.ButtonPlus.Text = "+"
        Me.ButtonPlus.UseVisualStyleBackColor = True
        '
        'txtPrintQuantity
        '
        Me.txtPrintQuantity.Location = New System.Drawing.Point(9, 22)
        Me.txtPrintQuantity.Name = "txtPrintQuantity"
        Me.txtPrintQuantity.Size = New System.Drawing.Size(34, 23)
        Me.txtPrintQuantity.TabIndex = 30
        Me.txtPrintQuantity.Text = "1"
        '
        'buttonAddFront
        '
        Me.buttonAddFront.AutoSize = True
        Me.buttonAddFront.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.buttonAddFront.Location = New System.Drawing.Point(9, 53)
        Me.buttonAddFront.Name = "buttonAddFront"
        Me.buttonAddFront.Size = New System.Drawing.Size(130, 25)
        Me.buttonAddFront.TabIndex = 29
        Me.buttonAddFront.Text = "Add Front To The List"
        '
        'groupSearch
        '
        Me.groupSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupSearch.Controls.Add(Me.comboPublisher)
        Me.groupSearch.Controls.Add(Me.lblPublisher)
        Me.groupSearch.Controls.Add(Me.checkBoxCardsWithImages)
        Me.groupSearch.Controls.Add(Me.txtKeywordSearch)
        Me.groupSearch.Controls.Add(Me.lblKeyword)
        Me.groupSearch.Controls.Add(Me.lblSet)
        Me.groupSearch.Controls.Add(Me.comboSets)
        Me.groupSearch.Location = New System.Drawing.Point(1, 414)
        Me.groupSearch.Name = "groupSearch"
        Me.groupSearch.Size = New System.Drawing.Size(257, 141)
        Me.groupSearch.TabIndex = 43
        Me.groupSearch.TabStop = False
        Me.groupSearch.Text = "Filter"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(96, 18)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(145, 23)
        Me.comboPublisher.TabIndex = 30
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblPublisher.Location = New System.Drawing.Point(11, 18)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(73, 18)
        Me.lblPublisher.TabIndex = 29
        Me.lblPublisher.Text = "Publisher:"
        '
        'checkBoxCardsWithImages
        '
        Me.checkBoxCardsWithImages.AutoSize = True
        Me.checkBoxCardsWithImages.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.checkBoxCardsWithImages.Location = New System.Drawing.Point(11, 105)
        Me.checkBoxCardsWithImages.Name = "checkBoxCardsWithImages"
        Me.checkBoxCardsWithImages.Size = New System.Drawing.Size(220, 22)
        Me.checkBoxCardsWithImages.TabIndex = 27
        Me.checkBoxCardsWithImages.Text = "Only show cards with images"
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(96, 76)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(145, 23)
        Me.txtKeywordSearch.TabIndex = 26
        '
        'lblKeyword
        '
        Me.lblKeyword.AutoSize = True
        Me.lblKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblKeyword.Location = New System.Drawing.Point(11, 76)
        Me.lblKeyword.Name = "lblKeyword"
        Me.lblKeyword.Size = New System.Drawing.Size(70, 18)
        Me.lblKeyword.TabIndex = 25
        Me.lblKeyword.Text = "Keyword:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(11, 47)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(34, 18)
        Me.lblSet.TabIndex = 24
        Me.lblSet.Text = "Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(96, 47)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(145, 23)
        Me.comboSets.TabIndex = 23
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.groupBoxControls1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.groupSearch)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CardsControl1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.controlHLImage)
        Me.SplitContainer1.Size = New System.Drawing.Size(531, 561)
        Me.SplitContainer1.SplitterDistance = 266
        Me.SplitContainer1.TabIndex = 51
        '
        'CardsControl1
        '
        Me.CardsControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsControl1.DisplayOriginalCardTitles = False
        Me.CardsControl1.Location = New System.Drawing.Point(1, 1)
        Me.CardsControl1.Name = "CardsControl1"
        Me.CardsControl1.Size = New System.Drawing.Size(257, 289)
        Me.CardsControl1.TabIndex = 41
        '
        'controlHLImage
        '
        Me.controlHLImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.controlHLImage.Location = New System.Drawing.Point(0, 0)
        Me.controlHLImage.Name = "controlHLImage"
        Me.controlHLImage.Size = New System.Drawing.Size(257, 557)
        Me.controlHLImage.TabIndex = 47
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.SplitContainer1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.CardsListPrint)
        Me.SplitContainer2.Panel2.Controls.Add(Me.groupBoxControls2)
        Me.SplitContainer2.Size = New System.Drawing.Size(784, 561)
        Me.SplitContainer2.SplitterDistance = 531
        Me.SplitContainer2.TabIndex = 52
        '
        'CardsListPrint
        '
        Me.CardsListPrint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsListPrint.DisplayOriginalCardTitles = False
        Me.CardsListPrint.Location = New System.Drawing.Point(4, 1)
        Me.CardsListPrint.Name = "CardsListPrint"
        Me.CardsListPrint.Size = New System.Drawing.Size(236, 496)
        Me.CardsListPrint.TabIndex = 47
        '
        'frmPrintCards
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.SplitContainer2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPrintCards"
        Me.Text = "Print Proxy Cards"
        Me.groupBoxControls2.ResumeLayout(False)
        Me.groupBoxControls1.ResumeLayout(False)
        Me.groupBoxControls1.PerformLayout()
        Me.groupSearch.ResumeLayout(False)
        Me.groupSearch.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    ' Declare datasets
    Dim printCounter As Integer
    Dim leftOverQuantity As Integer = 0

    Public Sub printCardsSingleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub is responsible for displaying the selected card in the 
        '   list of cards to print, when that selection is changed.

        ' If a card is selected, then load its ID number into the image control.
        If CardsListPrint.lstCards.SelectedItems.Count > 0 Then
            controlHLImage.loadCard(CardsListPrint.lstCards.SelectedItem.Id)
            If CardsListPrint.lstCards.SelectedItem.DisplayNamePrefix = "(B)" Then
                controlHLImage.loadImage("B")
            End If
        End If
    End Sub

    Public Sub allCardsSingleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When the selected card title is changed, display the card's image.
        Dim oneCard As ListViewItem
        For Each oneCard In CardsControl1.getSelectedCards()
            controlHLImage.loadCard(oneCard.Tag)
        Next
    End Sub

    Private Sub txtKeywordSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' Limit the list of visible cards based on the value of txtKeywordSearch
        generateSearchQuery()
    End Sub

    Private Sub comboSets_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Limit the list of visible cards based on the selected expansion set.
        generateSearchQuery()
    End Sub

    Private Sub generateSearchQuery()
        CardsControl1.filterCardsList(comboSets.SelectedItem, comboPublisher.SelectedItem, txtKeywordSearch.Text, 0, checkBoxCardsWithImages.Checked)
    End Sub

    Private Sub checkBoxCardsWithImages_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkBoxCardsWithImages.CheckedChanged
        ' if the check box checkBoxCardsWithImages is checked or unchecked,
        '   update the list of visible cards accordingly.
        generateSearchQuery()
    End Sub

    Private Sub buttonAddFront_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAddFront.Click
        ' Add the front of the currently selected card to the list of cards to be printed.
        Dim oneCard As ListViewItem

        ' Loop through each selected card, load each card into the image control,
        '   then add the card to the list of cards to print, using the image control
        '   to generate a file name.
        For Each oneCard In CardsControl1.getSelectedCards()
            CardsListPrint.addCard(oneCard.Tag, Convert.ToInt32(txtPrintQuantity.Text), "(F)")
        Next
    End Sub

    Private Sub buttonClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonClear.Click
        ' Delete all cards from the list of cards to be printed.
        CardsListPrint.ClearList()
    End Sub

    Private Sub buttonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrint.Click
        Dim PrintPreviewDialog1 As PrintPreviewDialog = New PrintPreviewDialog
        PrintPreviewDialog1.ShowIcon = False

        Dim PrintDocument1 As Printing.PrintDocument = New Printing.PrintDocument
        PrintPreviewDialog1.Document = PrintDocument1

        AddHandler PrintDocument1.PrintPage, AddressOf Me.PrintDocument1_PrintPage

        ' Allow the user to select a printer and specify any other printer preferences.
        Dim windowPrintPreferences As New PrintDialog
        windowPrintPreferences.PrinterSettings = PrintDocument1.PrinterSettings
        windowPrintPreferences.ShowDialog()

        ' Show the print preview dialog, allow the user to print from within it.
        printCounter = 0
        leftOverQuantity = 0
        PrintPreviewDialog1.ShowDialog()
    End Sub
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Dim width As Integer = 250
        Dim height As Integer = 350

        ' This will loop threw every selected image and spit out the file name and quantity
        Dim startingPosX As Integer = 30
        Dim startingPosY As Integer = 15
        Dim currX As Integer = startingPosX
        Dim currY As Integer = startingPosY
        Dim rowCounter As Integer = 0
        Dim colCounter As Integer = 0

        While printCounter < CardsListPrint.lstCards.Items.Count
            Dim currentCard As Data.Card = CardsListPrint.lstCards.Items(printCounter)
            Dim quantityCounter As Integer = 0
            If leftOverQuantity > 0 Then
                quantityCounter = leftOverQuantity
                leftOverQuantity = 0
            End If
            Dim newImage As Image
            If currentCard.DisplayNamePrefix = "(B)" Then
                newImage = Image.FromFile(currentCard.FilePathBack)
            Else
                newImage = Image.FromFile(currentCard.FilePathFront)
            End If
            While quantityCounter < currentCard.Quantity
                e.Graphics.DrawImage(newImage, currX, currY, width, height)
                colCounter = colCounter + 1
                If colCounter = 1 Then
                    currX = startingPosX + width + 4
                ElseIf colCounter = 2 Then
                    currX = startingPosX + (width * 2) + 7
                Else
                    colCounter = 0
                    rowCounter = rowCounter + 1
                    currX = startingPosX
                    If rowCounter = 1 Then
                        currY = startingPosY + height + 3
                    ElseIf rowCounter = 2 Then
                        currY = startingPosY + (height * 2) + 6
                    Else
                        ' New Page code goes here. 
                        If (printCounter + 1 < CardsListPrint.lstCards.Items.Count) Or (quantityCounter + 1 < currentCard.Quantity) Then
                            currY = startingPosY
                            e.HasMorePages = True
                            leftOverQuantity = quantityCounter + 1
                            Exit Sub
                        End If
                    End If
                End If
                quantityCounter = quantityCounter + 1
            End While
            printCounter = printCounter + 1
        End While

        e.HasMorePages = False
        printCounter = 0
    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub handles double-clicking on the currently selected card 
        '   in the CardsControl object (the one that displays every card).

        ' If a Title is double-clicked on, add its associated image front
        '   to the list of cards to print (as fronts are more common then backs).
        CardsListPrint.addCard(CardsControl1.getSelectedCards()(0).Tag, 1, "(F)")

    End Sub

    Private Sub frmPrintCards_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Set up the control that displays a list of all available cards.
        CardsControl1.controlSetup(New EventHandler(AddressOf allCardsSingleClick), New EventHandler(AddressOf allCardsDoubleClick))

        ' Load Publishers
        For Each onePublisher As Data.Publisher In Data.Publisher.LoadPublishers()
            comboPublisher.Items.Add(onePublisher.Name)
        Next
        comboPublisher.SelectedIndex = 1

        ' Set up the control that maintains the list of cards to print.
        AddHandler CardsListPrint.lstCards.SelectedIndexChanged, AddressOf printCardsSingleClick
        CardsListPrint.setTitle("Cards To Print")
        CardsListPrint.EnablePageCounter()
    End Sub

    Private Sub txtPrintQuantity_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtPrintQuantity.TextChanged
        If txtPrintQuantity.Text = "" Or txtPrintQuantity.Text = "0" Then
            txtPrintQuantity.Text = "1"
        End If

        If txtPrintQuantity.Text = "1" Then
            buttonAddFront.Text = "Add Front To The List"
            buttonAddBack.Text = "Add Back To The List"
        Else
            buttonAddFront.Text = "Add " & txtPrintQuantity.Text & " Fronts To The List"
            buttonAddBack.Text = "Add " & txtPrintQuantity.Text & " Backs To The List"
        End If
    End Sub

    Private Sub ButtonMinus_Click(sender As System.Object, e As System.EventArgs) Handles ButtonMinus.Click
        If txtPrintQuantity.Text <> "1" Then
            txtPrintQuantity.Text = (Convert.ToInt32(txtPrintQuantity.Text) - 1).ToString()
        End If
    End Sub

    Private Sub ButtonPlus_Click(sender As System.Object, e As System.EventArgs) Handles ButtonPlus.Click
        txtPrintQuantity.Text = (Convert.ToInt32(txtPrintQuantity.Text) + 1).ToString()
    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As Object, e As EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()

        Dim SelectedPublisher As Data.Publisher = Data.Publisher.LoadPublisher(comboPublisher.SelectedItem.ToString())
        If SelectedPublisher IsNot Nothing Then
            If SelectedPublisher.Id = 0 Then ' All is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets(String.Empty, Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            Else ' One Publisher is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = " & SelectedPublisher.Id.ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            End If
        End If
        comboSets.SelectedIndex = 1
    End Sub

    Private Sub buttonAddBack_Click(sender As Object, e As EventArgs) Handles buttonAddBack.Click
        ' Add the back of the currently selected card to the list of cards to be printed.
        Dim oneCard As ListViewItem

        ' Loop through each selected card, load each card into the image control,
        '   then add the card to the list of cards to print, using the image control
        '   to generate a file name.
        For Each oneCard In CardsControl1.getSelectedCards()
            CardsListPrint.addCard(oneCard.Tag, Convert.ToInt32(txtPrintQuantity.Text), "(B)")
        Next

    End Sub
End Class
