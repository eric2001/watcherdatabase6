' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmGenerateReport
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonCancel As System.Windows.Forms.Button
    Friend WithEvents buttonGenerateReport As System.Windows.Forms.Button
    Friend WithEvents lblSelectSet As System.Windows.Forms.Label
    Friend WithEvents lblPublisher As System.Windows.Forms.Label
    Friend WithEvents comboPublisher As System.Windows.Forms.ComboBox
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGenerateReport))
        Me.buttonCancel = New System.Windows.Forms.Button()
        Me.buttonGenerateReport = New System.Windows.Forms.Button()
        Me.lblSelectSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'buttonCancel
        '
        Me.buttonCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonCancel.Location = New System.Drawing.Point(138, 75)
        Me.buttonCancel.Name = "buttonCancel"
        Me.buttonCancel.Size = New System.Drawing.Size(90, 28)
        Me.buttonCancel.TabIndex = 0
        Me.buttonCancel.Text = "Cancel"
        '
        'buttonGenerateReport
        '
        Me.buttonGenerateReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonGenerateReport.Location = New System.Drawing.Point(244, 75)
        Me.buttonGenerateReport.Name = "buttonGenerateReport"
        Me.buttonGenerateReport.Size = New System.Drawing.Size(90, 28)
        Me.buttonGenerateReport.TabIndex = 1
        Me.buttonGenerateReport.Text = "Generate"
        '
        'lblSelectSet
        '
        Me.lblSelectSet.AutoSize = True
        Me.lblSelectSet.Location = New System.Drawing.Point(14, 46)
        Me.lblSelectSet.Name = "lblSelectSet"
        Me.lblSelectSet.Size = New System.Drawing.Size(82, 15)
        Me.lblSelectSet.TabIndex = 2
        Me.lblSelectSet.Text = "Expansion Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(109, 42)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(244, 23)
        Me.comboSets.TabIndex = 24
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Location = New System.Drawing.Point(14, 11)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(59, 15)
        Me.lblPublisher.TabIndex = 25
        Me.lblPublisher.Text = "Publisher:"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(109, 7)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(244, 23)
        Me.comboPublisher.TabIndex = 26
        '
        'frmGenerateReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(368, 110)
        Me.Controls.Add(Me.comboPublisher)
        Me.Controls.Add(Me.lblPublisher)
        Me.Controls.Add(Me.comboSets)
        Me.Controls.Add(Me.lblSelectSet)
        Me.Controls.Add(Me.buttonGenerateReport)
        Me.Controls.Add(Me.buttonCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGenerateReport"
        Me.Text = "Generate Report..."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Sub buttonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCancel.Click
        ' Close This Window
        Me.Close()
    End Sub

    Private Sub buttonGenerateReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonGenerateReport.Click

        If comboSets.SelectedItem Is Nothing Then
            MessageBox.Show("Please choose an Expansion Set from the list to generate a report for.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else
            Dim windowProgress As New frmProgress
            Me.ParentForm.AddOwnedForm(windowProgress)
            Me.AddOwnedForm(windowProgress)
            windowProgress.MdiParent = Me.ParentForm
            windowProgress.Show()

            Dim windowReportViewer As New frmReportViewer
            Dim ReportData As New Reporting.CollectionSetData()
            Dim SelectedExpansionSet As String = comboSets.SelectedItem.ToString()
            Task.Factory.StartNew(Sub() ReportData.RefreshData(SelectedExpansionSet))
            While ReportData.Maximum = 0
                Application.DoEvents()
            End While

            windowProgress.ProgressBarStatus.Value = 0
            windowProgress.ProgressBarStatus.Maximum = ReportData.Maximum
            While ReportData.Progress <> ReportData.Maximum
                windowProgress.ProgressBarStatus.Value = ReportData.Progress
                Application.DoEvents()
            End While

            windowReportViewer.ReportViewer1.LocalReport.DataSources.Clear()
            windowReportViewer.ReportViewer1.LocalReport.ReportPath = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports\HLSetReport.rdlc")
            windowReportViewer.ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("card", ReportData.Data.Tables("Card")))

            ' Set the title to the name of the selected expansion set
            Dim p(0) As Microsoft.Reporting.WinForms.ReportParameter
            p(0) = New Microsoft.Reporting.WinForms.ReportParameter("ReportTitle", comboSets.SelectedItem.ToString())
            windowReportViewer.ReportViewer1.LocalReport.SetParameters(p)

            ' Close the progress window
            windowProgress.Hide()
            windowProgress.Dispose()
            windowProgress = Nothing

            '  Display the report.
            Me.ParentForm.AddOwnedForm(windowReportViewer)
            Me.AddOwnedForm(windowReportViewer)
            windowReportViewer.MdiParent = Me.ParentForm
            windowReportViewer.Text = comboSets.SelectedItem.ToString() & " Report"
            windowReportViewer.Show()
        End If

    End Sub

    Private Sub frmGenerateReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Load Publisher Names.
        For Each onePublisher As DataRow In Data.SetsDB.Data.Tables("Publisher").Rows
            comboPublisher.Items.Add(onePublisher.Item("Long_Name"))
        Next
        comboPublisher.SelectedIndex = 0
    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()
        Dim SelectedPublisher As List(Of Data.Publisher) = Data.Publisher.LoadPublishers("Long_Name = '" & comboPublisher.SelectedItem.ToString() & "'")
        If SelectedPublisher.Count > 0 Then
            ' If the "(All)" publisher is selected, display all of the sets except for the "(All)" set, otherwise
            '   only shows sets associated to the selected publisher or to no publishers (MAO)
            If SelectedPublisher(0).Id = 0 Then
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("", Data.CardsDB.ExpansionSortOrder & " ASC")
                    If oneSet.Name <> "(All)" Then
                        comboSets.Items.Add(oneSet.Name)
                    End If
                Next
            Else
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = " & SelectedPublisher(0).Id.ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    If oneSet.Name <> "(All)" Then
                        comboSets.Items.Add(oneSet.Name)
                    End If
                Next
            End If
        End If
    End Sub
End Class
