﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Microsoft.Win32

Public Class frmSettings

    Private Sub frmSettings_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtImageFolderPath.Text = Data.CardsDB.HighlanderImagePath
        txtDefaultInventory.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultInventory", Nothing)

        Dim SortOrder As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultSetSort", "set_long")
        If (SortOrder = "set_date") Then
            RadioButtonDate.Checked = True
        Else
            RadioButtonSet.Checked = True
        End If

        Dim DisplayMode As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaulDisplayMode", "first_edition")
        If (DisplayMode = "type_one") Then
            RadioButtonType1.Checked = True
        Else
            RadioButton1stEdition.Checked = True
        End If
    End Sub

    Private Sub buttonSave_Click(sender As System.Object, e As System.EventArgs) Handles buttonSave.Click
        If Not System.IO.Directory.Exists(txtImageFolderPath.Text) Then
            If MessageBox.Show("Path " & txtImageFolderPath.Text & " does not exist.  Create it now?", _
                               "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = _
                           Windows.Forms.DialogResult.Yes Then
                System.IO.Directory.CreateDirectory(txtImageFolderPath.Text)
            End If
        End If

        ' Save selected settings to registry
        My.Computer.Registry.CurrentUser.CreateSubKey("WatcherDatabase")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\WatcherDatabase", "ImageFolder", txtImageFolderPath.Text)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultInventory", txtDefaultInventory.Text)

        Dim SortOrder As String = "set_long"
        If (RadioButtonDate.Checked = True) Then
            SortOrder = "set_date"
        End If
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaultSetSort", SortOrder)

        Dim DisplayMode As String = "first_edition"
        If (RadioButtonType1.Checked = True) Then
            DisplayMode = "type_one"
        End If
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\WatcherDatabase", "DefaulDisplayMode", DisplayMode)

        ' Cache current settings in memory
        Data.CardsDB.HighlanderImagePath = txtImageFolderPath.Text
        Data.CardsDB.ExpansionSortOrder = SortOrder
        Data.CardsDB.CardDisplayMode = DisplayMode

        Me.Close()
        MessageBox.Show("Settings Saved Successfully!", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub ButtonBrowseImages_Click(sender As System.Object, e As System.EventArgs) Handles ButtonBrowseImages.Click
        Dim FolderPath As New FolderBrowserDialog
        If FolderPath.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtImageFolderPath.Text = FolderPath.SelectedPath
        End If
    End Sub

    Private Sub buttonBrowseInventory_Click(sender As System.Object, e As System.EventArgs) Handles buttonBrowseInventory.Click
        Dim FilePath As New OpenFileDialog
        FilePath.DefaultExt = "hli"
        FilePath.Filter = "Highlander Inventory (*.hli)|*.hli"
        If FilePath.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtDefaultInventory.Text = FilePath.FileName
        End If
    End Sub

End Class