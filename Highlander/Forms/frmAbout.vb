' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class frmAbout
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtAgreement As System.Windows.Forms.RichTextBox
    Friend WithEvents txtGPL As System.Windows.Forms.RichTextBox
    Friend WithEvents tabAbout As System.Windows.Forms.TabControl
    Friend WithEvents TabPageAbout As System.Windows.Forms.TabPage
    Friend WithEvents TabPageLicense As System.Windows.Forms.TabPage
    Friend WithEvents pictureAboutImage As System.Windows.Forms.PictureBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents buttonOk As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAbout))
        Me.txtAgreement = New System.Windows.Forms.RichTextBox
        Me.txtGPL = New System.Windows.Forms.RichTextBox
        Me.tabAbout = New System.Windows.Forms.TabControl
        Me.TabPageAbout = New System.Windows.Forms.TabPage
        Me.lblVersion = New System.Windows.Forms.Label
        Me.pictureAboutImage = New System.Windows.Forms.PictureBox
        Me.TabPageLicense = New System.Windows.Forms.TabPage
        Me.buttonOk = New System.Windows.Forms.Button
        Me.tabAbout.SuspendLayout()
        Me.TabPageAbout.SuspendLayout()
        Me.TabPageLicense.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtAgreement
        '
        Me.txtAgreement.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtAgreement.Location = New System.Drawing.Point(4, 296)
        Me.txtAgreement.Name = "txtAgreement"
        Me.txtAgreement.ReadOnly = True
        Me.txtAgreement.Size = New System.Drawing.Size(392, 128)
        Me.txtAgreement.TabIndex = 1
        Me.txtAgreement.Text = "#LICENSE_FILE#"
        '
        'txtGPL
        '
        Me.txtGPL.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtGPL.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtGPL.Location = New System.Drawing.Point(0, 0)
        Me.txtGPL.Name = "txtGPL"
        Me.txtGPL.ReadOnly = True
        Me.txtGPL.Size = New System.Drawing.Size(400, 430)
        Me.txtGPL.TabIndex = 3
        Me.txtGPL.Text = "#GPL_TEXT#"
        '
        'tabAbout
        '
        Me.tabAbout.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabAbout.Controls.Add(Me.TabPageAbout)
        Me.tabAbout.Controls.Add(Me.TabPageLicense)
        Me.tabAbout.Location = New System.Drawing.Point(0, 0)
        Me.tabAbout.Name = "tabAbout"
        Me.tabAbout.SelectedIndex = 0
        Me.tabAbout.Size = New System.Drawing.Size(408, 456)
        Me.tabAbout.TabIndex = 4
        '
        'TabPageAbout
        '
        Me.TabPageAbout.Controls.Add(Me.lblVersion)
        Me.TabPageAbout.Controls.Add(Me.pictureAboutImage)
        Me.TabPageAbout.Controls.Add(Me.txtAgreement)
        Me.TabPageAbout.Location = New System.Drawing.Point(4, 22)
        Me.TabPageAbout.Name = "TabPageAbout"
        Me.TabPageAbout.Size = New System.Drawing.Size(400, 430)
        Me.TabPageAbout.TabIndex = 0
        Me.TabPageAbout.Text = "About"
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(153, 280)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(95, 16)
        Me.lblVersion.TabIndex = 3
        Me.lblVersion.Text = "Version:  X.X.X.X"
        '
        'pictureAboutImage
        '
        Me.pictureAboutImage.Image = CType(resources.GetObject("pictureAboutImage.Image"), System.Drawing.Image)
        Me.pictureAboutImage.Location = New System.Drawing.Point(0, 8)
        Me.pictureAboutImage.Name = "pictureAboutImage"
        Me.pictureAboutImage.Size = New System.Drawing.Size(400, 280)
        Me.pictureAboutImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureAboutImage.TabIndex = 1
        Me.pictureAboutImage.TabStop = False
        '
        'TabPageLicense
        '
        Me.TabPageLicense.Controls.Add(Me.txtGPL)
        Me.TabPageLicense.Location = New System.Drawing.Point(4, 22)
        Me.TabPageLicense.Name = "TabPageLicense"
        Me.TabPageLicense.Size = New System.Drawing.Size(400, 430)
        Me.TabPageLicense.TabIndex = 1
        Me.TabPageLicense.Text = "License"
        '
        'buttonOk
        '
        Me.buttonOk.Location = New System.Drawing.Point(168, 464)
        Me.buttonOk.Name = "buttonOk"
        Me.buttonOk.TabIndex = 5
        Me.buttonOk.Text = "Ok"
        '
        'frmAbout
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(410, 495)
        Me.Controls.Add(Me.buttonOk)
        Me.Controls.Add(Me.tabAbout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAbout"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "About..."
        Me.tabAbout.ResumeLayout(False)
        Me.TabPageAbout.ResumeLayout(False)
        Me.TabPageLicense.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAbout_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set the version number
        lblVersion.Text = "Version:  " & Application.ProductVersion & " Beta 4"
        txtAgreement.LoadFile(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "agreement.rtf"))
        txtGPL.LoadFile(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "license.rtf"))
    End Sub

    Private Sub buttonOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonOk.Click
        ' Close the About window.
        Me.Close()
    End Sub
End Class
