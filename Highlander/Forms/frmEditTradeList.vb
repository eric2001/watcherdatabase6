Imports System.Data
Imports Highlander.Data

Public Class frmEditTradeList
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CardsControl1 As cardsControl
    Friend WithEvents buttonAddHaves As System.Windows.Forms.Button
    Friend WithEvents buttonAddToWants As System.Windows.Forms.Button
    Friend WithEvents groupSearch As System.Windows.Forms.GroupBox
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyword As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents groupAddButtons As System.Windows.Forms.GroupBox
    Friend WithEvents lblDisplayHavesWants As System.Windows.Forms.Label
    Friend WithEvents comboHavesWants As System.Windows.Forms.ComboBox
    Friend WithEvents comboSpecialAdd As System.Windows.Forms.ComboBox
    Friend WithEvents buttonSpecialAdd As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
    Friend WithEvents dialogSaveHLT As System.Windows.Forms.SaveFileDialog
    Friend WithEvents menuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cardDetails As cardDetailsNoEdit
    Friend WithEvents menuTradeListEditor As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuSaveAs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitRight As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents MenuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuPrintHaves As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuPrintWants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuPrintBoth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents comboPublisher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPublisher As System.Windows.Forms.Label
    Friend WithEvents SplitContainerMiddle As System.Windows.Forms.SplitContainer
    Friend WithEvents MenuItem1 As ToolStripSeparator
    Friend WithEvents cardsHaveList As cardsListViewer
    Friend WithEvents cardsWantList As cardsListViewer
    Friend WithEvents MenuItem2 As ToolStripSeparator
    Friend WithEvents controlHLImage As cardImageFB
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditTradeList))
        Me.buttonAddHaves = New System.Windows.Forms.Button()
        Me.buttonAddToWants = New System.Windows.Forms.Button()
        Me.groupSearch = New System.Windows.Forms.GroupBox()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.comboHavesWants = New System.Windows.Forms.ComboBox()
        Me.lblDisplayHavesWants = New System.Windows.Forms.Label()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.lblKeyword = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.groupAddButtons = New System.Windows.Forms.GroupBox()
        Me.buttonSpecialAdd = New System.Windows.Forms.Button()
        Me.comboSpecialAdd = New System.Windows.Forms.ComboBox()
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
        Me.menuTradeListEditor = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuSaveAs = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuPrint = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuPrintBoth = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuPrintHaves = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuPrintWants = New System.Windows.Forms.ToolStripMenuItem()
        Me.dialogSaveHLT = New System.Windows.Forms.SaveFileDialog()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.CardsControl1 = New Highlander.cardsControl()
        Me.SplitContainerMiddle = New System.Windows.Forms.SplitContainer()
        Me.cardDetails = New Highlander.cardDetailsNoEdit()
        Me.controlHLImage = New Highlander.cardImageFB()
        Me.SplitRight = New System.Windows.Forms.SplitContainer()
        Me.cardsHaveList = New Highlander.cardsListViewer()
        Me.cardsWantList = New Highlander.cardsListViewer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.groupSearch.SuspendLayout()
        Me.groupAddButtons.SuspendLayout()
        Me.MainMenu1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainerMiddle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerMiddle.Panel1.SuspendLayout()
        Me.SplitContainerMiddle.Panel2.SuspendLayout()
        Me.SplitContainerMiddle.SuspendLayout()
        CType(Me.SplitRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitRight.Panel1.SuspendLayout()
        Me.SplitRight.Panel2.SuspendLayout()
        Me.SplitRight.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'buttonAddHaves
        '
        Me.buttonAddHaves.Location = New System.Drawing.Point(35, 20)
        Me.buttonAddHaves.Name = "buttonAddHaves"
        Me.buttonAddHaves.Size = New System.Drawing.Size(100, 28)
        Me.buttonAddHaves.TabIndex = 5
        Me.buttonAddHaves.Text = "Add 1 To Haves"
        '
        'buttonAddToWants
        '
        Me.buttonAddToWants.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonAddToWants.Location = New System.Drawing.Point(141, 20)
        Me.buttonAddToWants.Name = "buttonAddToWants"
        Me.buttonAddToWants.Size = New System.Drawing.Size(100, 28)
        Me.buttonAddToWants.TabIndex = 6
        Me.buttonAddToWants.Text = "Add 1 To Wants"
        '
        'groupSearch
        '
        Me.groupSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupSearch.Controls.Add(Me.comboPublisher)
        Me.groupSearch.Controls.Add(Me.lblPublisher)
        Me.groupSearch.Controls.Add(Me.comboHavesWants)
        Me.groupSearch.Controls.Add(Me.lblDisplayHavesWants)
        Me.groupSearch.Controls.Add(Me.txtKeywordSearch)
        Me.groupSearch.Controls.Add(Me.lblKeyword)
        Me.groupSearch.Controls.Add(Me.lblSet)
        Me.groupSearch.Controls.Add(Me.comboSets)
        Me.groupSearch.Location = New System.Drawing.Point(1, 359)
        Me.groupSearch.Name = "groupSearch"
        Me.groupSearch.Size = New System.Drawing.Size(272, 175)
        Me.groupSearch.TabIndex = 36
        Me.groupSearch.TabStop = False
        Me.groupSearch.Text = "Filter"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(122, 21)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(145, 23)
        Me.comboPublisher.TabIndex = 30
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblPublisher.Location = New System.Drawing.Point(14, 20)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(73, 18)
        Me.lblPublisher.TabIndex = 29
        Me.lblPublisher.Text = "Publisher:"
        '
        'comboHavesWants
        '
        Me.comboHavesWants.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboHavesWants.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboHavesWants.Items.AddRange(New Object() {"(All)", "0 Quantity Cards", "1 or More Quantity Cards", "2 or More Quantity Cards"})
        Me.comboHavesWants.Location = New System.Drawing.Point(122, 138)
        Me.comboHavesWants.Name = "comboHavesWants"
        Me.comboHavesWants.Size = New System.Drawing.Size(145, 23)
        Me.comboHavesWants.TabIndex = 28
        '
        'lblDisplayHavesWants
        '
        Me.lblDisplayHavesWants.AutoSize = True
        Me.lblDisplayHavesWants.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblDisplayHavesWants.Location = New System.Drawing.Point(14, 138)
        Me.lblDisplayHavesWants.Name = "lblDisplayHavesWants"
        Me.lblDisplayHavesWants.Size = New System.Drawing.Size(84, 18)
        Me.lblDisplayHavesWants.TabIndex = 27
        Me.lblDisplayHavesWants.Text = "Show Only:"
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(122, 98)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(145, 23)
        Me.txtKeywordSearch.TabIndex = 26
        '
        'lblKeyword
        '
        Me.lblKeyword.AutoSize = True
        Me.lblKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblKeyword.Location = New System.Drawing.Point(14, 98)
        Me.lblKeyword.Name = "lblKeyword"
        Me.lblKeyword.Size = New System.Drawing.Size(70, 18)
        Me.lblKeyword.TabIndex = 25
        Me.lblKeyword.Text = "Keyword:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(14, 59)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(34, 18)
        Me.lblSet.TabIndex = 24
        Me.lblSet.Text = "Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(122, 59)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(145, 23)
        Me.comboSets.TabIndex = 23
        '
        'groupAddButtons
        '
        Me.groupAddButtons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupAddButtons.Controls.Add(Me.buttonSpecialAdd)
        Me.groupAddButtons.Controls.Add(Me.comboSpecialAdd)
        Me.groupAddButtons.Controls.Add(Me.buttonAddHaves)
        Me.groupAddButtons.Controls.Add(Me.buttonAddToWants)
        Me.groupAddButtons.Location = New System.Drawing.Point(1, 262)
        Me.groupAddButtons.Name = "groupAddButtons"
        Me.groupAddButtons.Size = New System.Drawing.Size(272, 99)
        Me.groupAddButtons.TabIndex = 37
        Me.groupAddButtons.TabStop = False
        '
        'buttonSpecialAdd
        '
        Me.buttonSpecialAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonSpecialAdd.Location = New System.Drawing.Point(196, 55)
        Me.buttonSpecialAdd.Name = "buttonSpecialAdd"
        Me.buttonSpecialAdd.Size = New System.Drawing.Size(58, 29)
        Me.buttonSpecialAdd.TabIndex = 25
        Me.buttonSpecialAdd.Text = "Add"
        '
        'comboSpecialAdd
        '
        Me.comboSpecialAdd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSpecialAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSpecialAdd.Items.AddRange(New Object() {"Add 1 of Each Card To Haves List", "Add Available Quantity To Haves List", "Add Available Quantity -1 to Haves List", "Add 1 of Each Card To Wants List"})
        Me.comboSpecialAdd.Location = New System.Drawing.Point(14, 59)
        Me.comboSpecialAdd.Name = "comboSpecialAdd"
        Me.comboSpecialAdd.Size = New System.Drawing.Size(176, 23)
        Me.comboSpecialAdd.TabIndex = 24
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuTradeListEditor})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(784, 24)
        Me.MainMenu1.TabIndex = 44
        Me.MainMenu1.Visible = False
        '
        'menuTradeListEditor
        '
        Me.menuTradeListEditor.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuSave, Me.menuSaveAs, Me.MenuItem1, Me.MenuPrint})
        Me.menuTradeListEditor.Name = "menuTradeListEditor"
        Me.menuTradeListEditor.Size = New System.Drawing.Size(69, 20)
        Me.menuTradeListEditor.Text = "Trade List"
        '
        'menuSave
        '
        Me.menuSave.Name = "menuSave"
        Me.menuSave.Size = New System.Drawing.Size(123, 22)
        Me.menuSave.Text = "Save"
        '
        'menuSaveAs
        '
        Me.menuSaveAs.Name = "menuSaveAs"
        Me.menuSaveAs.Size = New System.Drawing.Size(123, 22)
        Me.menuSaveAs.Text = "Save As..."
        '
        'MenuItem1
        '
        Me.MenuItem1.Name = "MenuItem1"
        Me.MenuItem1.Size = New System.Drawing.Size(120, 6)
        '
        'MenuPrint
        '
        Me.MenuPrint.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuPrintBoth, Me.MenuItem2, Me.MenuPrintHaves, Me.MenuPrintWants})
        Me.MenuPrint.Name = "MenuPrint"
        Me.MenuPrint.Size = New System.Drawing.Size(123, 22)
        Me.MenuPrint.Text = "Print"
        '
        'MenuPrintBoth
        '
        Me.MenuPrintBoth.Name = "MenuPrintBoth"
        Me.MenuPrintBoth.Size = New System.Drawing.Size(156, 22)
        Me.MenuPrintBoth.Text = "Print Entire List"
        '
        'MenuItem2
        '
        Me.MenuItem2.Name = "MenuItem2"
        Me.MenuItem2.Size = New System.Drawing.Size(153, 6)
        '
        'MenuPrintHaves
        '
        Me.MenuPrintHaves.Name = "MenuPrintHaves"
        Me.MenuPrintHaves.Size = New System.Drawing.Size(156, 22)
        Me.MenuPrintHaves.Text = "Print Haves List"
        '
        'MenuPrintWants
        '
        Me.MenuPrintWants.Name = "MenuPrintWants"
        Me.MenuPrintWants.Size = New System.Drawing.Size(156, 22)
        Me.MenuPrintWants.Text = "Print Wants List"
        '
        'dialogSaveHLT
        '
        Me.dialogSaveHLT.DefaultExt = "hlt"
        Me.dialogSaveHLT.Filter = "Trade List (*.hlt)|*.hlt"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.groupAddButtons)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CardsControl1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.groupSearch)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainerMiddle)
        Me.SplitContainer1.Size = New System.Drawing.Size(546, 537)
        Me.SplitContainer1.SplitterDistance = 281
        Me.SplitContainer1.TabIndex = 41
        '
        'CardsControl1
        '
        Me.CardsControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsControl1.DisplayOriginalCardTitles = False
        Me.CardsControl1.Location = New System.Drawing.Point(1, 1)
        Me.CardsControl1.Name = "CardsControl1"
        Me.CardsControl1.Size = New System.Drawing.Size(272, 261)
        Me.CardsControl1.TabIndex = 0
        '
        'SplitContainerMiddle
        '
        Me.SplitContainerMiddle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerMiddle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerMiddle.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerMiddle.Name = "SplitContainerMiddle"
        Me.SplitContainerMiddle.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainerMiddle.Panel1
        '
        Me.SplitContainerMiddle.Panel1.Controls.Add(Me.cardDetails)
        '
        'SplitContainerMiddle.Panel2
        '
        Me.SplitContainerMiddle.Panel2.Controls.Add(Me.controlHLImage)
        Me.SplitContainerMiddle.Size = New System.Drawing.Size(261, 537)
        Me.SplitContainerMiddle.SplitterDistance = 348
        Me.SplitContainerMiddle.TabIndex = 39
        '
        'cardDetails
        '
        Me.cardDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardDetails.Location = New System.Drawing.Point(0, 0)
        Me.cardDetails.Name = "cardDetails"
        Me.cardDetails.Size = New System.Drawing.Size(257, 344)
        Me.cardDetails.TabIndex = 2
        '
        'controlHLImage
        '
        Me.controlHLImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.controlHLImage.Location = New System.Drawing.Point(0, 0)
        Me.controlHLImage.Name = "controlHLImage"
        Me.controlHLImage.Size = New System.Drawing.Size(257, 181)
        Me.controlHLImage.TabIndex = 38
        '
        'SplitRight
        '
        Me.SplitRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitRight.Location = New System.Drawing.Point(0, 0)
        Me.SplitRight.Name = "SplitRight"
        Me.SplitRight.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitRight.Panel1
        '
        Me.SplitRight.Panel1.Controls.Add(Me.cardsHaveList)
        '
        'SplitRight.Panel2
        '
        Me.SplitRight.Panel2.Controls.Add(Me.cardsWantList)
        Me.SplitRight.Size = New System.Drawing.Size(234, 537)
        Me.SplitRight.SplitterDistance = 266
        Me.SplitRight.TabIndex = 42
        '
        'cardsHaveList
        '
        Me.cardsHaveList.DisplayOriginalCardTitles = False
        Me.cardsHaveList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsHaveList.Location = New System.Drawing.Point(0, 0)
        Me.cardsHaveList.Name = "cardsHaveList"
        Me.cardsHaveList.Size = New System.Drawing.Size(230, 262)
        Me.cardsHaveList.TabIndex = 0
        '
        'cardsWantList
        '
        Me.cardsWantList.DisplayOriginalCardTitles = False
        Me.cardsWantList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cardsWantList.Location = New System.Drawing.Point(0, 0)
        Me.cardsWantList.Name = "cardsWantList"
        Me.cardsWantList.Size = New System.Drawing.Size(230, 263)
        Me.cardsWantList.TabIndex = 0
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 24)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.SplitContainer1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.SplitRight)
        Me.SplitContainer2.Size = New System.Drawing.Size(784, 537)
        Me.SplitContainer2.SplitterDistance = 546
        Me.SplitContainer2.TabIndex = 43
        '
        'frmEditTradeList
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.SplitContainer2)
        Me.Controls.Add(Me.MainMenu1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MainMenu1
        Me.Name = "frmEditTradeList"
        Me.Text = "Trade List Editor"
        Me.groupSearch.ResumeLayout(False)
        Me.groupSearch.PerformLayout()
        Me.groupAddButtons.ResumeLayout(False)
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainerMiddle.Panel1.ResumeLayout(False)
        Me.SplitContainerMiddle.Panel2.ResumeLayout(False)
        CType(Me.SplitContainerMiddle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerMiddle.ResumeLayout(False)
        Me.SplitRight.Panel1.ResumeLayout(False)
        Me.SplitRight.Panel2.ResumeLayout(False)
        CType(Me.SplitRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitRight.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Public Sub allCardsSelectChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When the selected card title is changed, display the card's image.

        ' If at least one card is selected, load the details and image for the 
        '   last card that's selected.
        If CardsControl1.getSelectedCards().Count > 0 Then
            controlHLImage.loadCard(CardsControl1.getSelectedCards()(CardsControl1.getSelectedCards().Count - 1).Tag)
            cardDetails.LoadCard(CardsControl1.getSelectedCards()(CardsControl1.getSelectedCards().Count - 1).Tag)
        End If
    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub handles double-clicking on the currently selected card 
        '   in the CardsControl object (the one that displays every card).
        '   If the quantity of the card is 0, it is added to the Wants list,
        '   if the quantity is greater then 0 it is added to the haves list.
        Dim listToUpdate As cardsListViewer

        Dim SelectedCard As Data.Card = Data.Card.LoadCard(CardsControl1.getSelectedCards()(0).Tag, 1)
        If SelectedCard Is Nothing Then
            Exit Sub ' This should never happen.
        End If

        ' If the quantity of the card is 0 (you don't have any copies of it) then 
        '   add the card to the wants list.  Else add the card to the haves list.
        Dim Quantity As Integer = Inventory.GetCardQuantity(SelectedCard.Id)
        If Quantity = 0 Then
            listToUpdate = cardsWantList
        Else
            listToUpdate = cardsHaveList
        End If

        ' Update the list.
        listToUpdate.addCard(SelectedCard)

        ' Load the image and details of the selected card.
        controlHLImage.LoadCard(SelectedCard)
        cardDetails.LoadCard(SelectedCard)
    End Sub

    Public Sub allCardsHavesWantsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub is responsible for handling changes to the selected card on both
        '   the haves list and wants list.  It displays the front and details of the 
        '   newly selected card.

        ' Load the image for the front of the card and the card details.
        If sender.SelectedItems.Count > 0 Then
            controlHLImage.LoadCard(sender.SelectedItems(sender.SelectedItems.Count - 1))
            cardDetails.LoadCard(sender.SelectedItems(sender.SelectedItems.Count - 1))
        End If
    End Sub

    Private Sub buttonAddHaves_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAddHaves.Click
        ' Add one copy of the selected card(s) to the "Haves" list.
        Dim oneCard As ListViewItem
        Dim LastSelectedCardId As Integer = 0

        ' Loop through each selected entry in the cards control.
        For Each oneCard In CardsControl1.getSelectedCards()

            LastSelectedCardId = oneCard.Tag

            ' Load the image and detailed information for the current card.
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(oneCard.Tag)
            If SelectedCard Is Nothing Then
                Exit Sub ' This should never happen.
            End If

            ' Add one copy to list
            SelectedCard.Quantity = 1

            ' Add the current card to the haves list.
            cardsHaveList.addCard(SelectedCard)

            Application.DoEvents()
        Next

        If LastSelectedCardId > 0 Then
            controlHLImage.LoadCard(LastSelectedCardId)
            cardDetails.LoadCard(LastSelectedCardId)
        End If
    End Sub

    Private Sub buttonAddToWants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAddToWants.Click
        ' This sub is responsible for adding one copy of the selected card(s) 
        '   to the Wants list.
        Dim oneCard As ListViewItem
        Dim LastSelectedCardId As Integer = 0

        ' Loop through each selected card in the cards control.
        For Each oneCard In CardsControl1.getSelectedCards()

            LastSelectedCardId = oneCard.Tag

            ' Load the image and details for the current card.
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(oneCard.Tag)
            If SelectedCard Is Nothing Then
                Exit Sub ' This should never happen.
            End If

            ' Add one copy to list
            SelectedCard.Quantity = 1

            ' Update the wants list with the current card.
            cardsWantList.addCard(SelectedCard)

            Application.DoEvents()
        Next

        If LastSelectedCardId > 0 Then
            controlHLImage.LoadCard(LastSelectedCardId)
            cardDetails.LoadCard(LastSelectedCardId)
        End If
    End Sub

    Private Sub comboSets_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Limit the cards displayed by the all cards control to a specific expansion set.
        generateSearchQuery()
    End Sub

    Private Sub txtKeywordSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' Limit the cards displayed by the all cards control to ones containing a specific keyword.
        generateSearchQuery()
    End Sub

    Private Sub comboHavesWants_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboHavesWants.SelectedIndexChanged
        ' Limit the cards being displayed based on the quantity of each card.
        generateSearchQuery()
    End Sub

    Private Sub generateSearchQuery()
        CardsControl1.filterCardsList(comboSets.SelectedItem, comboPublisher.SelectedItem, txtKeywordSearch.Text, comboHavesWants.SelectedIndex, False)
    End Sub

    Private Sub buttonSpecialAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSpecialAdd.Click
        '  This sub adds one or more copies of every visible card to either the Haves
        '   Or wants list, based on what the user inputs.
        Dim oneCard As ListViewItem

        ' Loop through each visible card in the cards control.
        For Each oneCard In CardsControl1.getVisibleCards()

            ' Load the image and details of the current card.
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(oneCard.Tag)
            If SelectedCard Is Nothing Then
                Exit Sub ' This should never happen.
            End If

            ' Get the current quantity
            SelectedCard.Quantity = Inventory.GetCardQuantity(SelectedCard.Id)

            ' Set the list to be updated (Haves or Wants) and the quantity to add.
            Dim listToUpdate As cardsListViewer

            If comboSpecialAdd.SelectedIndex = 0 Then
                'Add 1 of Each Visible Card To Haves List
                SelectedCard.Quantity = 1
                listToUpdate = cardsHaveList

            ElseIf comboSpecialAdd.SelectedIndex = 1 Then
                'Add Available Quantity of Each Visible Card To Haves List
                listToUpdate = cardsHaveList

            ElseIf comboSpecialAdd.SelectedIndex = 2 Then
                'Add 1 Less then Available Quantity of Visible Cards to Haves List
                If SelectedCard.Quantity > 0 Then
                    SelectedCard.Quantity -= 1
                End If
                listToUpdate = cardsHaveList

            Else 'If comboSpecialAdd.SelectedIndex = 3 Then
                'Add 1of Each Visible Card To Wants List
                SelectedCard.Quantity = 1
                listToUpdate = cardsWantList
            End If

            ' If the quantity of the current card is greater then 0, add it to the list.
            If SelectedCard.Quantity > 0 Then
                listToUpdate.addCard(SelectedCard)
            End If
            Application.DoEvents()
        Next
    End Sub

    Public Sub openHLTFile(ByVal fileName As String)
        Dim deck As TradeList = HLT.Load(fileName)

        For Each card As Highlander.Data.Card In deck.Haves
            cardsHaveList.addCard(card, "")
        Next

        For Each card As Highlander.Data.Card In deck.Wants
            cardsWantList.addCard(card, "")
        Next

        ' Update the form with the name of the saved .hlt file.
        Me.Text = "Trade List Editor (" & fileName & ")"
        dialogSaveHLT.FileName = fileName
    End Sub

    Private Sub menuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuSave.Click
        ' Save the current Haves and Wants list to a .hlt file.
        '   If the list has already been saved, update the existing file.

        ' If the file hasn't been saved before, open a Save As dialog
        Dim fileOkToSave As Boolean = False
        If dialogSaveHLT.FileName = "" Then
            If dialogSaveHLT.ShowDialog = DialogResult.OK Then
                fileOkToSave = True
            End If
        Else
            fileOkToSave = True
        End If

        ' If the file was already saved, or the user pressed Ok on the 
        '   save as dialog, then save the lists.
        If fileOkToSave = True Then

            Dim deck As New TradeList
            deck.Haves = cardsHaveList.CardList
            deck.Wants = cardsWantList.CardList

            If HLT.Save(dialogSaveHLT.FileName, deck) Then
                Me.Text = "Trade List Editor (" & dialogSaveHLT.FileName & ")"
                MessageBox.Show("Your file has been saved.", "Saved")
            End If
        End If
    End Sub

    Private Sub menuSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuSaveAs.Click
        ' This sub will prompt the user for a file name to save the trade list to.

        ' If the user presses Ok, save the file.
        If dialogSaveHLT.ShowDialog = DialogResult.OK Then

            Dim deck As New TradeList
            deck.Haves = cardsHaveList.CardList
            deck.Wants = cardsWantList.CardList

            If HLT.Save(dialogSaveHLT.FileName, deck) Then
                Me.Text = "Trade List Editor (" & dialogSaveHLT.FileName & ")"
                MessageBox.Show("Your file has been saved.", "Saved")
            End If
        End If
    End Sub

    Private Sub frmEditTradeList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Set up the control responsible for displaying all available cards.
        CardsControl1.controlSetup(New EventHandler(AddressOf allCardsSelectChanged), New EventHandler(AddressOf allCardsDoubleClick))
        CardsControl1.DisplayOriginalCardTitles = True

        ' Set up the control responsible for maintaining the "Haves" list.
        AddHandler cardsHaveList.lstCards.SelectedIndexChanged, AddressOf allCardsHavesWantsClick
        cardsHaveList.setTitle("Have List")
        cardsHaveList.DisplayOriginalCardTitles = True

        ' Set up the control responsible for maintaining the "Wants" list.
        AddHandler cardsWantList.lstCards.SelectedIndexChanged, AddressOf allCardsHavesWantsClick
        cardsWantList.setTitle("Want List")
        cardsHaveList.DisplayOriginalCardTitles = True

        ' Load Publisher Names.
        For Each onePublisher As Data.Publisher In Data.Publisher.LoadPublishers(String.Empty)
            comboPublisher.Items.Add(onePublisher.Name)
        Next
        comboPublisher.SelectedIndex = 1
    End Sub

    Private Sub MenuPrintHaves_Click(sender As System.Object, e As System.EventArgs) Handles MenuPrintHaves.Click
        PrintList("H")
    End Sub

    Private Sub MenuPrintWants_Click(sender As System.Object, e As System.EventArgs) Handles MenuPrintWants.Click
        PrintList("W")
    End Sub

    Private Sub MenuPrintBoth_Click(sender As System.Object, e As System.EventArgs) Handles MenuPrintBoth.Click
        PrintList("B")
    End Sub

    ''' <summary>
    ''' Creates a new Report window and populates it with the specified cards.
    ''' </summary>
    ''' <param name="ListToPrint">Identifies which list to print.
    ''' "H" -- Haves List.
    ''' "W" -- Wants List.
    ''' "B" -- Both Lists.</param>
    ''' <remarks></remarks>
    Private Sub PrintList(ByVal ListToPrint As String)

        Dim windowProgress As New frmProgress
        Me.ParentForm.AddOwnedForm(windowProgress)
        Me.AddOwnedForm(windowProgress)
        windowProgress.MdiParent = Me.ParentForm
        windowProgress.Show()

        Dim windowReportViewer As New frmReportViewer
        Dim ReportData As New Reporting.TradeListData()
        Dim TradeListData As New Data.TradeList
        TradeListData.Haves = cardsHaveList.CardList
        TradeListData.Wants = cardsWantList.CardList
        Task.Factory.StartNew(Sub() ReportData.RefreshData(TradeListData, ListToPrint))
        While ReportData.Maximum = 0
            Application.DoEvents()
        End While

        windowProgress.ProgressBarStatus.Value = 0
        windowProgress.ProgressBarStatus.Maximum = ReportData.Maximum
        While ReportData.Progress <> ReportData.Maximum
            windowProgress.ProgressBarStatus.Value = ReportData.Progress
            Application.DoEvents()
        End While

        windowReportViewer.ReportViewer1.LocalReport.DataSources.Clear()
        windowReportViewer.ReportViewer1.LocalReport.ReportPath = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports\HLDeckReport.rdlc")
        windowReportViewer.ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("card", ReportData.Data.Tables("Card")))

        ' Use the file name as the report title, if available
        Dim strListTitle As String = ""
        If dialogSaveHLT.FileName.Length > 0 Then
            strListTitle = dialogSaveHLT.FileName.Substring(dialogSaveHLT.FileName.LastIndexOf("\") + 1,
                                                            dialogSaveHLT.FileName.LastIndexOf(".") - dialogSaveHLT.FileName.LastIndexOf("\") - 1)
        Else
            strListTitle = "Untitled Trade List"
        End If

        Dim p(0) As Microsoft.Reporting.WinForms.ReportParameter
        p(0) = New Microsoft.Reporting.WinForms.ReportParameter("ReportTitle", strListTitle)
        windowReportViewer.ReportViewer1.LocalReport.SetParameters(p)

        windowProgress.Hide()
        windowProgress.Dispose()
        windowProgress = Nothing

        '  Display the report.
        Me.ParentForm.AddOwnedForm(windowReportViewer)
        Me.AddOwnedForm(windowReportViewer)
        windowReportViewer.MdiParent = Me.ParentForm
        windowReportViewer.Text = strListTitle
        windowReportViewer.Show()

    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()

        Dim SelectedPublisher As Data.Publisher = Data.Publisher.LoadPublisher(comboPublisher.SelectedItem.ToString())
        If SelectedPublisher IsNot Nothing Then
            If SelectedPublisher.Id = 0 Then ' All is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets(String.Empty, Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            Else ' One Publisher is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = " & SelectedPublisher.Id.ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            End If
        End If
        comboSets.SelectedIndex = 1
    End Sub

End Class
