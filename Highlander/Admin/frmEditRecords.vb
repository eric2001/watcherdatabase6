' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmEditRecords
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CardsControl1 As cardsControl
    Friend WithEvents CardDetailsEditAll1 As cardDetailsEditAll
    Friend WithEvents groupSearch As System.Windows.Forms.GroupBox
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyword As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents buttonUpdate As System.Windows.Forms.Button
    Friend WithEvents buttonMoveNext As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupNavigation As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainerLeft As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainerRight As System.Windows.Forms.SplitContainer
    Friend WithEvents comboPublisher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPublisher As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents CardImage1 As cardImage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents LabelHandsHelp As Label
    Friend WithEvents buttonMovePrevious As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditRecords))
        Me.groupSearch = New System.Windows.Forms.GroupBox()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.lblKeyword = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.buttonUpdate = New System.Windows.Forms.Button()
        Me.buttonMoveNext = New System.Windows.Forms.Button()
        Me.buttonMovePrevious = New System.Windows.Forms.Button()
        Me.CardDetailsEditAll1 = New Highlander.cardDetailsEditAll()
        Me.CardsControl1 = New Highlander.cardsControl()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupNavigation = New System.Windows.Forms.GroupBox()
        Me.SplitContainerLeft = New System.Windows.Forms.SplitContainer()
        Me.SplitContainerRight = New System.Windows.Forms.SplitContainer()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.CardImage1 = New Highlander.cardImage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.LabelHandsHelp = New System.Windows.Forms.Label()
        Me.groupSearch.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupNavigation.SuspendLayout()
        CType(Me.SplitContainerLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerLeft.Panel1.SuspendLayout()
        Me.SplitContainerLeft.Panel2.SuspendLayout()
        Me.SplitContainerLeft.SuspendLayout()
        CType(Me.SplitContainerRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerRight.Panel1.SuspendLayout()
        Me.SplitContainerRight.Panel2.SuspendLayout()
        Me.SplitContainerRight.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupSearch
        '
        Me.groupSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupSearch.Controls.Add(Me.comboPublisher)
        Me.groupSearch.Controls.Add(Me.lblPublisher)
        Me.groupSearch.Controls.Add(Me.txtKeywordSearch)
        Me.groupSearch.Controls.Add(Me.lblKeyword)
        Me.groupSearch.Controls.Add(Me.lblSet)
        Me.groupSearch.Controls.Add(Me.comboSets)
        Me.groupSearch.Location = New System.Drawing.Point(1, 434)
        Me.groupSearch.Name = "groupSearch"
        Me.groupSearch.Size = New System.Drawing.Size(330, 122)
        Me.groupSearch.TabIndex = 28
        Me.groupSearch.TabStop = False
        Me.groupSearch.Text = "Search / Sort"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(109, 21)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(214, 23)
        Me.comboPublisher.TabIndex = 30
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblPublisher.Location = New System.Drawing.Point(14, 20)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(73, 18)
        Me.lblPublisher.TabIndex = 29
        Me.lblPublisher.Text = "Publisher:"
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(109, 87)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(214, 23)
        Me.txtKeywordSearch.TabIndex = 26
        '
        'lblKeyword
        '
        Me.lblKeyword.AutoSize = True
        Me.lblKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblKeyword.Location = New System.Drawing.Point(14, 86)
        Me.lblKeyword.Name = "lblKeyword"
        Me.lblKeyword.Size = New System.Drawing.Size(70, 18)
        Me.lblKeyword.TabIndex = 25
        Me.lblKeyword.Text = "Keyword:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(14, 53)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(34, 18)
        Me.lblSet.TabIndex = 24
        Me.lblSet.Text = "Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.Location = New System.Drawing.Point(109, 54)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(214, 23)
        Me.comboSets.Sorted = True
        Me.comboSets.TabIndex = 23
        '
        'buttonUpdate
        '
        Me.buttonUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonUpdate.Location = New System.Drawing.Point(201, 18)
        Me.buttonUpdate.Name = "buttonUpdate"
        Me.buttonUpdate.Size = New System.Drawing.Size(115, 29)
        Me.buttonUpdate.TabIndex = 29
        Me.buttonUpdate.Text = "Update Record"
        '
        'buttonMoveNext
        '
        Me.buttonMoveNext.Location = New System.Drawing.Point(104, 18)
        Me.buttonMoveNext.Name = "buttonMoveNext"
        Me.buttonMoveNext.Size = New System.Drawing.Size(90, 29)
        Me.buttonMoveNext.TabIndex = 30
        Me.buttonMoveNext.Text = "Next >>"
        '
        'buttonMovePrevious
        '
        Me.buttonMovePrevious.Location = New System.Drawing.Point(7, 18)
        Me.buttonMovePrevious.Name = "buttonMovePrevious"
        Me.buttonMovePrevious.Size = New System.Drawing.Size(90, 29)
        Me.buttonMovePrevious.TabIndex = 31
        Me.buttonMovePrevious.Text = "<< Previous"
        '
        'CardDetailsEditAll1
        '
        Me.CardDetailsEditAll1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CardDetailsEditAll1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CardDetailsEditAll1.Location = New System.Drawing.Point(3, 19)
        Me.CardDetailsEditAll1.Name = "CardDetailsEditAll1"
        Me.CardDetailsEditAll1.Size = New System.Drawing.Size(315, 470)
        Me.CardDetailsEditAll1.TabIndex = 1
        '
        'CardsControl1
        '
        Me.CardsControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsControl1.DisplayOriginalCardTitles = False
        Me.CardsControl1.Location = New System.Drawing.Point(1, 1)
        Me.CardsControl1.Name = "CardsControl1"
        Me.CardsControl1.Size = New System.Drawing.Size(330, 426)
        Me.CardsControl1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.CardDetailsEditAll1)
        Me.GroupBox1.Location = New System.Drawing.Point(1, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(321, 492)
        Me.GroupBox1.TabIndex = 32
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Card Details"
        '
        'GroupNavigation
        '
        Me.GroupNavigation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupNavigation.Controls.Add(Me.buttonMovePrevious)
        Me.GroupNavigation.Controls.Add(Me.buttonMoveNext)
        Me.GroupNavigation.Controls.Add(Me.buttonUpdate)
        Me.GroupNavigation.Location = New System.Drawing.Point(1, 497)
        Me.GroupNavigation.Name = "GroupNavigation"
        Me.GroupNavigation.Size = New System.Drawing.Size(321, 61)
        Me.GroupNavigation.TabIndex = 33
        Me.GroupNavigation.TabStop = False
        Me.GroupNavigation.Text = "Controls"
        '
        'SplitContainerLeft
        '
        Me.SplitContainerLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerLeft.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerLeft.Name = "SplitContainerLeft"
        '
        'SplitContainerLeft.Panel1
        '
        Me.SplitContainerLeft.Panel1.Controls.Add(Me.CardsControl1)
        Me.SplitContainerLeft.Panel1.Controls.Add(Me.groupSearch)
        '
        'SplitContainerLeft.Panel2
        '
        Me.SplitContainerLeft.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitContainerLeft.Panel2.Controls.Add(Me.GroupNavigation)
        Me.SplitContainerLeft.Size = New System.Drawing.Size(673, 561)
        Me.SplitContainerLeft.SplitterDistance = 340
        Me.SplitContainerLeft.TabIndex = 34
        '
        'SplitContainerRight
        '
        Me.SplitContainerRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerRight.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerRight.Name = "SplitContainerRight"
        '
        'SplitContainerRight.Panel1
        '
        Me.SplitContainerRight.Panel1.Controls.Add(Me.SplitContainerLeft)
        '
        'SplitContainerRight.Panel2
        '
        Me.SplitContainerRight.Panel2.Controls.Add(Me.TabControl1)
        Me.SplitContainerRight.Size = New System.Drawing.Size(909, 561)
        Me.SplitContainerRight.SplitterDistance = 673
        Me.SplitContainerRight.TabIndex = 35
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(228, 557)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.CardImage1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(220, 529)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Image"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'CardImage1
        '
        Me.CardImage1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CardImage1.Location = New System.Drawing.Point(3, 3)
        Me.CardImage1.Name = "CardImage1"
        Me.CardImage1.Size = New System.Drawing.Size(214, 523)
        Me.CardImage1.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.LabelHandsHelp)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(220, 529)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Help"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'LabelHandsHelp
        '
        Me.LabelHandsHelp.Location = New System.Drawing.Point(6, 3)
        Me.LabelHandsHelp.Name = "LabelHandsHelp"
        Me.LabelHandsHelp.Size = New System.Drawing.Size(211, 73)
        Me.LabelHandsHelp.TabIndex = 0
        Me.LabelHandsHelp.Text = "Hands:  Expected values are: 1 -- One Hand; 2 -- Two Hands; o1 -- offhand; # / # " &
    "-- one or the other, ex: 1 / 2 (1 or 2 hands) or 1 / o1 (one or offhand)"
        '
        'frmEditRecords
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(909, 561)
        Me.Controls.Add(Me.SplitContainerRight)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEditRecords"
        Me.Text = "Edit Record(s)"
        Me.groupSearch.ResumeLayout(False)
        Me.groupSearch.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupNavigation.ResumeLayout(False)
        Me.SplitContainerLeft.Panel1.ResumeLayout(False)
        Me.SplitContainerLeft.Panel2.ResumeLayout(False)
        CType(Me.SplitContainerLeft, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerLeft.ResumeLayout(False)
        Me.SplitContainerRight.Panel1.ResumeLayout(False)
        Me.SplitContainerRight.Panel2.ResumeLayout(False)
        CType(Me.SplitContainerRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerRight.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Sub allCardsSingleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When a card title is selected, display the card's image and load
        '   detailed information about it into the card details control.
        Dim oneCard As ListViewItem
        For Each oneCard In CardsControl1.getSelectedCards()
            Data.CardsDB.Data.Tables("Card").DefaultView.RowFilter = "ID = '" & oneCard.Tag & "'"
            Data.CardsDB.Data.Tables("Type1Errata").DefaultView.RowFilter = "ID = '" & oneCard.Tag & "'"
            CardImage1.loadCard(oneCard.Tag)
        Next
    End Sub

    Public Sub allCardsDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When a cards title is double clicked on, open its details in a new window.
        Dim oneCard As ListViewItem
        For Each oneCard In CardsControl1.getSelectedCards()
            Dim detailsWindow As New frmCardDetails
            detailsWindow.CardDetails1.LoadCard(oneCard.Tag)
            detailsWindow.Text = oneCard.Text
            Me.ParentForm.AddOwnedForm(detailsWindow)
            detailsWindow.MdiParent = Me.ParentForm
            detailsWindow.Show()
        Next
    End Sub

    Public Sub alternateVersionSingleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When a card title is selected, display the card's image and load
        '   detailed information about it into the card details control.
        Dim oneCard As ListViewItem
        For Each oneCard In CardDetailsEditAll1.CardsControlVersions.getSelectedCards()
            CardImage1.loadCard(oneCard.Tag)
        Next
    End Sub

    Public Sub alternateVersionDoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' When a cards title is double clicked on, open its details in a new window.
        Dim oneCard As ListViewItem
        For Each oneCard In CardDetailsEditAll1.CardsControlVersions.getSelectedCards()
            Dim detailsWindow As New frmCardDetails
            detailsWindow.CardDetails1.LoadCard(oneCard.Tag)
            detailsWindow.Text = oneCard.Text
            Me.ParentForm.AddOwnedForm(detailsWindow)
            detailsWindow.MdiParent = Me.ParentForm
            detailsWindow.Show()
        Next
    End Sub

    Private Sub txtKeywordSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' Re-query the database whenever the user enters or changes
        '   the keyword search text.
        generateSearchQuery()
    End Sub

    Private Sub comboSets_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Update the list of cards whenever the selected set is changed.
        generateSearchQuery()
    End Sub

    Private Sub generateSearchQuery()
        CardsControl1.filterCardsList(comboSets.SelectedItem, comboPublisher.SelectedItem, txtKeywordSearch.Text, 0, False)
    End Sub

    Private Sub buttonUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonUpdate.Click
        ' Commit changes to the database.
        Data.CardsDB.Data.AcceptChanges()
    End Sub

    Private Sub buttonMoveNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonMoveNext.Click
        ' Move to the next record on the list.
        CardsControl1.moveNext()
    End Sub

    Private Sub buttonMovePrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonMovePrevious.Click
        ' Move to the previous record on the list.
        CardsControl1.movePrevious()
    End Sub

    Private Sub frmEditRecords_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Load Publisher Names.
        For Each onePublisher As Data.Publisher In Data.Publisher.LoadPublishers()
            comboPublisher.Items.Add(onePublisher.Name)
        Next
        comboPublisher.SelectedIndex = 1 ' Default selection to Thunder Castle Games

        ' Set up the databinds for the various objects on this form.
        CardsControl1.controlSetup(New EventHandler(AddressOf allCardsSingleClick), New EventHandler(AddressOf allCardsDoubleClick))
        CardDetailsEditAll1.dataBind()
        CardDetailsEditAll1.CardsControlVersions.controlSetup(New EventHandler(AddressOf alternateVersionSingleClick), New EventHandler(AddressOf alternateVersionDoubleClick))
    End Sub

    Private Sub comboPublisher_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles comboPublisher.SelectedIndexChanged
        comboSets.Items.Clear()

        ' Load sets for the selected publisher
        Dim SelectedPublisher As Data.Publisher = Data.Publisher.LoadPublisher(comboPublisher.SelectedItem.ToString())
        If SelectedPublisher IsNot Nothing Then
            If SelectedPublisher.Id = 0 Then ' All is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets(String.Empty, Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            Else ' One Publisher is selected
                For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = " & SelectedPublisher.Id.ToString() & " OR set_Publisher = 0", Data.CardsDB.ExpansionSortOrder & " ASC")
                    comboSets.Items.Add(oneSet.Name)
                Next
            End If
        End If

        ' If the All publisher is selected, default section to first real expansion set, otherwise default selection to All cards for the current publisher.
        If comboSets.Items.Count > 0 Then
            If comboPublisher.SelectedItem.ToString() = "(All)" Then
                comboSets.SelectedIndex = 1
            Else
                comboSets.SelectedIndex = 0
            End If
        End If
    End Sub

End Class
