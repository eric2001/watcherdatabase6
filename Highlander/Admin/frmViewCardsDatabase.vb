' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmViewCardsDatabase
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGridView
    Friend WithEvents statusAllCards As System.Windows.Forms.StatusStrip
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents StatusLabelAllCards As ToolStripStatusLabel
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewCardsDatabase))
        Me.statusAllCards = New System.Windows.Forms.StatusStrip()
        Me.DataGrid1 = New System.Windows.Forms.DataGridView()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.StatusLabelAllCards = New System.Windows.Forms.ToolStripStatusLabel()
        Me.statusAllCards.SuspendLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'statusAllCards
        '
        Me.statusAllCards.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabelAllCards})
        Me.statusAllCards.Location = New System.Drawing.Point(0, 207)
        Me.statusAllCards.Name = "statusAllCards"
        Me.statusAllCards.Size = New System.Drawing.Size(528, 22)
        Me.statusAllCards.TabIndex = 0
        Me.statusAllCards.Text = "StatusBar1"
        '
        'DataGrid1
        '
        Me.DataGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGrid1.Location = New System.Drawing.Point(0, 0)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(528, 169)
        Me.DataGrid1.TabIndex = 1
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.comboSets.Location = New System.Drawing.Point(0, 178)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(187, 23)
        Me.comboSets.Sorted = True
        Me.comboSets.TabIndex = 24
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(202, 178)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(153, 23)
        Me.txtKeywordSearch.TabIndex = 25
        '
        'StatusLabelAllCards
        '
        Me.StatusLabelAllCards.Name = "StatusLabelAllCards"
        Me.StatusLabelAllCards.Size = New System.Drawing.Size(0, 17)
        '
        'frmViewCardsDatabase
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(528, 229)
        Me.Controls.Add(Me.txtKeywordSearch)
        Me.Controls.Add(Me.comboSets)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.statusAllCards)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmViewCardsDatabase"
        Me.Opacity = 0.8R
        Me.Text = "View / Edit Database:  cards.xml"
        Me.statusAllCards.ResumeLayout(False)
        Me.statusAllCards.PerformLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Sub comboSets_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboSets.SelectedIndexChanged
        ' Run a new query whenever the selected expansion set changes.
        generateSearchQuery()
    End Sub

    Private Sub txtKeywordSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeywordSearch.TextChanged
        ' run a new query whenever the value of txtKeywordSearch.Text changes.
        generateSearchQuery()
    End Sub

    Private Sub generateSearchQuery()
        ' This sub is responsible for filtering the dataset for specific matches.
        Dim queryString As String = ""
        Dim keywordString As String = ""

        ' If the expansion set that is currently selected is NOT "(All)"
        '   then limit search results to cards from the selected set.
        If comboSets.SelectedItem <> "(All)" Then
            Dim selectedSet As Data.ExpansionSet = Data.ExpansionSet.LoadSets("set_long = '" & comboSets.SelectedItem.Replace("'", "''") & "'").FirstOrDefault()
            If selectedSet IsNot Nothing Then
                queryString = "Set = '" & selectedSet.Code & "'"
            End If
        End If

        ' If something was entered into the Keyword Search field, limit matches
        '   to entries that have the specified keyword in on of their fields.
        If txtKeywordSearch.Text <> "" Then
            keywordString = "(Immortal Like '%" & txtKeywordSearch.Text & "%') OR " &
                "(Title Like '%" & txtKeywordSearch.Text & "%') OR " &
                "(Text Like '%" & txtKeywordSearch.Text & "%') OR " &
                "(Rarity Like '%" & txtKeywordSearch.Text & "%') OR " &
                "(Type Like '%" & txtKeywordSearch.Text & "%')"

            ' If the queryString variable already has a value (from the expansion set) then
            '   add the keyword search to the end of it.
            '   Or else just set queryString to the keyword search string.
            If queryString <> "" Then
                queryString = "(" & queryString & ") AND (" & keywordString & ")"
            Else
                queryString = keywordString
            End If
        End If

        ' Filter the datagrid and update the card count.
        DataGrid1.DataSource.DefaultView.RowFilter = queryString
        StatusLabelAllCards.Text = BindingContext(DataGrid1.DataSource).Count.ToString & " Cards(s) Displayed, " & DataGrid1.DataSource.Rows.Count().ToString & " Card(s) Total"
    End Sub

    Private Sub frmViewCardsDatabase_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DataGrid1.DataSource = Data.CardsDB.Data.Tables("Card")
        ' Load Set Names.
        For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("")
            comboSets.Items.Add(oneSet.Name)
            If oneSet.Name = "(All)" Then
                comboSets.SelectedItem = oneSet.Name
            End If
        Next

        ' Update the card count when the form is first loaded.
        StatusLabelAllCards.Text = BindingContext(DataGrid1.DataSource).Count.ToString & " Cards(s) Displayed, " & DataGrid1.DataSource.Rows.Count().ToString & " Card(s) Total"
    End Sub
End Class
