﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDownloadMLESet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDownloadMLESet))
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.lblSelectSet = New System.Windows.Forms.Label()
        Me.lblFolder = New System.Windows.Forms.Label()
        Me.txtDownloadPath = New System.Windows.Forms.TextBox()
        Me.ButtonDownload = New System.Windows.Forms.Button()
        Me.ProgressBarDownload = New System.Windows.Forms.ProgressBar()
        Me.SuspendLayout()
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(106, 6)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(237, 21)
        Me.comboSets.Sorted = True
        Me.comboSets.TabIndex = 26
        '
        'lblSelectSet
        '
        Me.lblSelectSet.AutoSize = True
        Me.lblSelectSet.Location = New System.Drawing.Point(12, 9)
        Me.lblSelectSet.Name = "lblSelectSet"
        Me.lblSelectSet.Size = New System.Drawing.Size(78, 13)
        Me.lblSelectSet.TabIndex = 25
        Me.lblSelectSet.Text = "Expansion Set:"
        '
        'lblFolder
        '
        Me.lblFolder.AutoSize = True
        Me.lblFolder.Location = New System.Drawing.Point(12, 37)
        Me.lblFolder.Name = "lblFolder"
        Me.lblFolder.Size = New System.Drawing.Size(90, 13)
        Me.lblFolder.TabIndex = 27
        Me.lblFolder.Text = "Download Folder:"
        '
        'txtDownloadPath
        '
        Me.txtDownloadPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDownloadPath.Location = New System.Drawing.Point(106, 34)
        Me.txtDownloadPath.Name = "txtDownloadPath"
        Me.txtDownloadPath.Size = New System.Drawing.Size(237, 20)
        Me.txtDownloadPath.TabIndex = 28
        '
        'ButtonDownload
        '
        Me.ButtonDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonDownload.Location = New System.Drawing.Point(254, 89)
        Me.ButtonDownload.Name = "ButtonDownload"
        Me.ButtonDownload.Size = New System.Drawing.Size(75, 23)
        Me.ButtonDownload.TabIndex = 29
        Me.ButtonDownload.Text = "Download"
        Me.ButtonDownload.UseVisualStyleBackColor = True
        '
        'ProgressBarDownload
        '
        Me.ProgressBarDownload.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBarDownload.Location = New System.Drawing.Point(15, 60)
        Me.ProgressBarDownload.Name = "ProgressBarDownload"
        Me.ProgressBarDownload.Size = New System.Drawing.Size(328, 23)
        Me.ProgressBarDownload.TabIndex = 30
        '
        'frmDownloadMLESet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 117)
        Me.Controls.Add(Me.ProgressBarDownload)
        Me.Controls.Add(Me.ButtonDownload)
        Me.Controls.Add(Me.txtDownloadPath)
        Me.Controls.Add(Me.lblFolder)
        Me.Controls.Add(Me.comboSets)
        Me.Controls.Add(Me.lblSelectSet)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmDownloadMLESet"
        Me.Text = "Download MLE Cards"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents lblSelectSet As System.Windows.Forms.Label
    Friend WithEvents lblFolder As System.Windows.Forms.Label
    Friend WithEvents txtDownloadPath As System.Windows.Forms.TextBox
    Friend WithEvents ButtonDownload As System.Windows.Forms.Button
    Friend WithEvents ProgressBarDownload As System.Windows.Forms.ProgressBar
End Class
