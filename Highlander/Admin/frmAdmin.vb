' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class frmAdmin
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonCardsDB As System.Windows.Forms.Button
    Friend WithEvents buttonSetsDB As System.Windows.Forms.Button
    Friend WithEvents ButtonSave As System.Windows.Forms.Button
    Friend WithEvents buttonInventoryDB As System.Windows.Forms.Button
    Friend WithEvents ButtonDownload As System.Windows.Forms.Button
    Friend WithEvents ButtonDisplayRulesDB As System.Windows.Forms.Button
    Friend WithEvents buttonPublishersDB As Button
    Friend WithEvents buttonSettingsDB As Button
    Friend WithEvents txtEditRecords As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdmin))
        Me.buttonCardsDB = New System.Windows.Forms.Button()
        Me.buttonSetsDB = New System.Windows.Forms.Button()
        Me.txtEditRecords = New System.Windows.Forms.Button()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.buttonInventoryDB = New System.Windows.Forms.Button()
        Me.ButtonDownload = New System.Windows.Forms.Button()
        Me.ButtonDisplayRulesDB = New System.Windows.Forms.Button()
        Me.buttonPublishersDB = New System.Windows.Forms.Button()
        Me.buttonSettingsDB = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'buttonCardsDB
        '
        Me.buttonCardsDB.Location = New System.Drawing.Point(14, 15)
        Me.buttonCardsDB.Name = "buttonCardsDB"
        Me.buttonCardsDB.Size = New System.Drawing.Size(146, 28)
        Me.buttonCardsDB.TabIndex = 0
        Me.buttonCardsDB.Text = "View / Edit Cards"
        '
        'buttonSetsDB
        '
        Me.buttonSetsDB.Location = New System.Drawing.Point(14, 50)
        Me.buttonSetsDB.Name = "buttonSetsDB"
        Me.buttonSetsDB.Size = New System.Drawing.Size(146, 29)
        Me.buttonSetsDB.TabIndex = 1
        Me.buttonSetsDB.Text = "View / Edit Sets"
        '
        'txtEditRecords
        '
        Me.txtEditRecords.Location = New System.Drawing.Point(167, 15)
        Me.txtEditRecords.Name = "txtEditRecords"
        Me.txtEditRecords.Size = New System.Drawing.Size(145, 28)
        Me.txtEditRecords.TabIndex = 2
        Me.txtEditRecords.Text = "Edit Records"
        '
        'ButtonSave
        '
        Me.ButtonSave.Location = New System.Drawing.Point(86, 156)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(145, 28)
        Me.ButtonSave.TabIndex = 3
        Me.ButtonSave.Text = "Save Databases"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'buttonInventoryDB
        '
        Me.buttonInventoryDB.Location = New System.Drawing.Point(14, 122)
        Me.buttonInventoryDB.Name = "buttonInventoryDB"
        Me.buttonInventoryDB.Size = New System.Drawing.Size(146, 28)
        Me.buttonInventoryDB.TabIndex = 4
        Me.buttonInventoryDB.Text = "View / Edit Inventory"
        Me.buttonInventoryDB.UseVisualStyleBackColor = True
        '
        'ButtonDownload
        '
        Me.ButtonDownload.Location = New System.Drawing.Point(167, 86)
        Me.ButtonDownload.Name = "ButtonDownload"
        Me.ButtonDownload.Size = New System.Drawing.Size(145, 29)
        Me.ButtonDownload.TabIndex = 5
        Me.ButtonDownload.Text = "Download Cards"
        Me.ButtonDownload.UseVisualStyleBackColor = True
        '
        'ButtonDisplayRulesDB
        '
        Me.ButtonDisplayRulesDB.Location = New System.Drawing.Point(14, 86)
        Me.ButtonDisplayRulesDB.Name = "ButtonDisplayRulesDB"
        Me.ButtonDisplayRulesDB.Size = New System.Drawing.Size(146, 28)
        Me.ButtonDisplayRulesDB.TabIndex = 6
        Me.ButtonDisplayRulesDB.Text = "View / Edit Rules"
        '
        'buttonPublishersDB
        '
        Me.buttonPublishersDB.Location = New System.Drawing.Point(167, 50)
        Me.buttonPublishersDB.Name = "buttonPublishersDB"
        Me.buttonPublishersDB.Size = New System.Drawing.Size(146, 29)
        Me.buttonPublishersDB.TabIndex = 7
        Me.buttonPublishersDB.Text = "View / Edit Publishers"
        '
        'buttonSettingsDB
        '
        Me.buttonSettingsDB.Location = New System.Drawing.Point(167, 122)
        Me.buttonSettingsDB.Name = "buttonSettingsDB"
        Me.buttonSettingsDB.Size = New System.Drawing.Size(146, 28)
        Me.buttonSettingsDB.TabIndex = 8
        Me.buttonSettingsDB.Text = "View / Edit Settings"
        Me.buttonSettingsDB.UseVisualStyleBackColor = True
        '
        'frmAdmin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 16)
        Me.ClientSize = New System.Drawing.Size(323, 193)
        Me.Controls.Add(Me.buttonSettingsDB)
        Me.Controls.Add(Me.buttonPublishersDB)
        Me.Controls.Add(Me.ButtonDisplayRulesDB)
        Me.Controls.Add(Me.ButtonDownload)
        Me.Controls.Add(Me.buttonInventoryDB)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.txtEditRecords)
        Me.Controls.Add(Me.buttonSetsDB)
        Me.Controls.Add(Me.buttonCardsDB)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAdmin"
        Me.Text = "Administrative Controls"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub buttonCardsDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCardsDB.Click
        ' Load the cards database in edit mode.
        Dim windowCardsDatabase As New frmViewCardsDatabase
        Me.ParentForm.AddOwnedForm(windowCardsDatabase)
        windowCardsDatabase.MdiParent = Me.ParentForm
        windowCardsDatabase.Show()
    End Sub

    Private Sub buttonSetsDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSetsDB.Click
        ' Load the sets database in edit mode.
        Dim windowSetsDatabase As New frmViewSetDB
        Me.ParentForm.AddOwnedForm(windowSetsDatabase)
        windowSetsDatabase.MdiParent = Me.ParentForm
        windowSetsDatabase.Show()
    End Sub

    Private Sub txtEditRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEditRecords.Click
        Dim dbPatchGenerator As New frmEditRecords
        Me.ParentForm.AddOwnedForm(dbPatchGenerator)
        dbPatchGenerator.MdiParent = Me.ParentForm
        dbPatchGenerator.Show()
    End Sub

    Private Sub ButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles ButtonSave.Click
        SaveData()
    End Sub

    Private Sub frmAdmin_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ' Auto save when exiting the admin screen
        SaveData()
    End Sub

    Private Sub SaveData()
        Data.CardsDB.Save()
        Data.SetsDB.Save()

        MessageBox.Show("Cards and Sets data Saved.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub buttonInventoryDB_Click(sender As System.Object, e As System.EventArgs) Handles buttonInventoryDB.Click
        ' Load the current inventory file in an editor.
        Dim windowInventoryDatabase As New frmViewInventoryDB
        Me.ParentForm.AddOwnedForm(windowInventoryDatabase)
        windowInventoryDatabase.MdiParent = Me.ParentForm
        windowInventoryDatabase.Show()
    End Sub

    Private Sub ButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles ButtonDownload.Click
        ' Load the current inventory file in an editor.
        Dim windowDownloadMLECards As New frmDownloadMLESet
        Me.ParentForm.AddOwnedForm(windowDownloadMLECards)
        windowDownloadMLECards.MdiParent = Me.ParentForm
        windowDownloadMLECards.Show()

    End Sub

    Private Sub ButtonDisplayRulesDB_Click(sender As System.Object, e As System.EventArgs) Handles ButtonDisplayRulesDB.Click
        ' Load the Rules.xml database file in an editor.
        Dim windowRulesDatabase As New frmViewRulesDB
        Me.ParentForm.AddOwnedForm(windowRulesDatabase)
        windowRulesDatabase.MdiParent = Me.ParentForm
        windowRulesDatabase.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles buttonPublishersDB.Click
        ' Load the sets database in edit mode.
        Dim windowPublisherDatabase As New frmViewPublisherDB
        Me.ParentForm.AddOwnedForm(windowPublisherDatabase)
        windowPublisherDatabase.MdiParent = Me.ParentForm
        windowPublisherDatabase.Show()
    End Sub

    Private Sub buttonSettingsDB_Click(sender As Object, e As EventArgs) Handles buttonSettingsDB.Click
        ' Load the current inventory file in an editor.
        Dim windowSettingsDatabase As New frmViewSettingsDB
        Me.ParentForm.AddOwnedForm(windowSettingsDatabase)
        windowSettingsDatabase.MdiParent = Me.ParentForm
        windowSettingsDatabase.Show()
    End Sub
End Class
