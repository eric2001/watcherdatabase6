﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmViewRulesDB
    Dim RulesDB As New DataSet("Rules")

    Private Sub frmViewRulesDB_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Ask the user if the Rules db should be saved before closing, then save if the user says Yes.
        If (MessageBox.Show("Would you like to save the Rules.xml file?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes) Then
            RulesDB.WriteXml(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\Rules.xml"))
            MessageBox.Show("Rules.xml file Saved Successfully.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmViewRulesDB_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Load the cards.xml file
        RulesDB.ReadXmlSchema(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\Rules.xsd"))
        RulesDB.ReadXml(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\Rules.xml"))
        DataGridRules.DataSource = RulesDB
    End Sub
End Class