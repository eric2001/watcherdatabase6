﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewSettingsDB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewSettingsDB))
        Me.dataInventory = New System.Windows.Forms.DataGridView()
        CType(Me.dataInventory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dataInventory
        '
        Me.dataInventory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dataInventory.Location = New System.Drawing.Point(0, 0)
        Me.dataInventory.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.dataInventory.Name = "dataInventory"
        Me.dataInventory.Size = New System.Drawing.Size(448, 302)
        Me.dataInventory.TabIndex = 1
        '
        'frmViewSettingsDB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 302)
        Me.Controls.Add(Me.dataInventory)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmViewSettingsDB"
        Me.Text = "View / Edit Database:  inventory.xml: Settings"
        CType(Me.dataInventory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dataInventory As System.Windows.Forms.DataGridView
End Class
