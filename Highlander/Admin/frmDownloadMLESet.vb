﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class frmDownloadMLESet

    Private Sub frmDownloadMLESet_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' Load only MLE sets.
        For Each oneSet As Data.ExpansionSet In Data.ExpansionSet.LoadSets("set_Publisher = 2")
            comboSets.Items.Add(oneSet.Name)
        Next
        comboSets.Sorted = True
        comboSets.SelectedIndex = 0
    End Sub

    Private Sub ButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles ButtonDownload.Click
        If txtDownloadPath.Text = "" Then
            MessageBox.Show("Specify a download folder.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If Not txtDownloadPath.Text.EndsWith("\") Then
            txtDownloadPath.Text &= "\"
        End If

        Dim SetAbvr As String = ""
        If comboSets.SelectedItem <> "" Then
            Dim selectedSet As Data.ExpansionSet = Data.ExpansionSet.LoadSets("set_long = '" & comboSets.SelectedItem.ToString().Replace("'", "''") & "'").FirstOrDefault()
            If selectedSet IsNot Nothing Then
                SetAbvr = "Set = '" & selectedSet.Code & "'"
            End If
        End If

        Dim MLECards As List(Of Data.Card) = Data.Card.LoadCards(SetAbvr)
        If MLECards.Count = 0 Then
            MessageBox.Show("No cards to download.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        ProgressBarDownload.Maximum = MLECards.Count
        ProgressBarDownload.Value = 0
        For Each oneCard As Data.Card In MLECards
            Dim InventoryRecord As Data.Inventory = Data.Inventory.GetInventoryForCard(oneCard.Id)
            If InventoryRecord.MleUrl <> "" Then
                Dim strFileName As String = InventoryRecord.MleUrl.Substring(InventoryRecord.MleUrl.LastIndexOf("/") + 1)
                classFileDownload.HTTPBinaryDownload(InventoryRecord.MleUrl, txtDownloadPath.Text & strFileName)
            End If
            ProgressBarDownload.Value += 1
            Application.DoEvents()
        Next

        MessageBox.Show("Download Complete!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class