' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

Public Class cardDetailsEditAll
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    Friend WithEvents TabControlEditCard As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtSubType As System.Windows.Forms.TextBox
    Friend WithEvents lblSubType As System.Windows.Forms.Label
    Friend WithEvents check2EOnly As System.Windows.Forms.CheckBox
    Friend WithEvents checkboxUniqueness As System.Windows.Forms.CheckBox
    Friend WithEvents txtCardNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblCardNumber As System.Windows.Forms.Label
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents txtHands As System.Windows.Forms.TextBox
    Friend WithEvents txtFileNameBack As System.Windows.Forms.TextBox
    Friend WithEvents txtFileNameFront As System.Windows.Forms.TextBox
    Friend WithEvents txtGems As System.Windows.Forms.TextBox
    Friend WithEvents txtSet As System.Windows.Forms.TextBox
    Friend WithEvents txtRarity As System.Windows.Forms.TextBox
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents txtRestricted As System.Windows.Forms.TextBox
    Friend WithEvents txtGrid As System.Windows.Forms.TextBox
    Friend WithEvents txtImmortal As System.Windows.Forms.TextBox
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents lblGrid As System.Windows.Forms.Label
    Friend WithEvents lblGems As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblImmortal As System.Windows.Forms.Label
    Friend WithEvents lblHands As System.Windows.Forms.Label
    Friend WithEvents lblRestricted As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents lblRarity As System.Windows.Forms.Label
    Friend WithEvents lblFileNameBack As System.Windows.Forms.Label
    Friend WithEvents lblFileNameFront As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents txtIDNumber As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtType1SubType As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents checkType12EOnly As System.Windows.Forms.CheckBox
    Friend WithEvents checkboxType1Uniqueness As System.Windows.Forms.CheckBox
    Friend WithEvents txtType1CardNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtType1Text As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Hands As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Gems As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Set As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Rarity As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Type As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Restricted As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Grid As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Immortal As System.Windows.Forms.TextBox
    Friend WithEvents txtType1Title As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtType1IDNumber As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ButtonAdd As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As TabPage
    Public WithEvents CardsControlVersions As cardsControl
    Friend WithEvents checkboxDoubleDiamond As CheckBox
    Friend WithEvents checkboxType1DoubleDiamond As CheckBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.TabControlEditCard = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtSubType = New System.Windows.Forms.TextBox()
        Me.lblSubType = New System.Windows.Forms.Label()
        Me.check2EOnly = New System.Windows.Forms.CheckBox()
        Me.checkboxUniqueness = New System.Windows.Forms.CheckBox()
        Me.txtCardNumber = New System.Windows.Forms.TextBox()
        Me.lblCardNumber = New System.Windows.Forms.Label()
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.txtHands = New System.Windows.Forms.TextBox()
        Me.txtFileNameBack = New System.Windows.Forms.TextBox()
        Me.txtFileNameFront = New System.Windows.Forms.TextBox()
        Me.txtGems = New System.Windows.Forms.TextBox()
        Me.txtSet = New System.Windows.Forms.TextBox()
        Me.txtRarity = New System.Windows.Forms.TextBox()
        Me.txtType = New System.Windows.Forms.TextBox()
        Me.txtRestricted = New System.Windows.Forms.TextBox()
        Me.txtGrid = New System.Windows.Forms.TextBox()
        Me.txtImmortal = New System.Windows.Forms.TextBox()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.lblText = New System.Windows.Forms.Label()
        Me.lblGrid = New System.Windows.Forms.Label()
        Me.lblGems = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblImmortal = New System.Windows.Forms.Label()
        Me.lblHands = New System.Windows.Forms.Label()
        Me.lblRestricted = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.lblRarity = New System.Windows.Forms.Label()
        Me.lblFileNameBack = New System.Windows.Forms.Label()
        Me.lblFileNameFront = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.txtIDNumber = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.ButtonAdd = New System.Windows.Forms.Button()
        Me.txtType1SubType = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.checkType12EOnly = New System.Windows.Forms.CheckBox()
        Me.checkboxType1Uniqueness = New System.Windows.Forms.CheckBox()
        Me.txtType1CardNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtType1Text = New System.Windows.Forms.TextBox()
        Me.txtType1Hands = New System.Windows.Forms.TextBox()
        Me.txtType1Gems = New System.Windows.Forms.TextBox()
        Me.txtType1Set = New System.Windows.Forms.TextBox()
        Me.txtType1Rarity = New System.Windows.Forms.TextBox()
        Me.txtType1Type = New System.Windows.Forms.TextBox()
        Me.txtType1Restricted = New System.Windows.Forms.TextBox()
        Me.txtType1Grid = New System.Windows.Forms.TextBox()
        Me.txtType1Immortal = New System.Windows.Forms.TextBox()
        Me.txtType1Title = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtType1IDNumber = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.CardsControlVersions = New Highlander.cardsControl()
        Me.checkboxDoubleDiamond = New System.Windows.Forms.CheckBox()
        Me.checkboxType1DoubleDiamond = New System.Windows.Forms.CheckBox()
        Me.TabControlEditCard.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControlEditCard
        '
        Me.TabControlEditCard.Controls.Add(Me.TabPage1)
        Me.TabControlEditCard.Controls.Add(Me.TabPage2)
        Me.TabControlEditCard.Controls.Add(Me.TabPage3)
        Me.TabControlEditCard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlEditCard.Location = New System.Drawing.Point(0, 0)
        Me.TabControlEditCard.Name = "TabControlEditCard"
        Me.TabControlEditCard.SelectedIndex = 0
        Me.TabControlEditCard.Size = New System.Drawing.Size(290, 440)
        Me.TabControlEditCard.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.checkboxDoubleDiamond)
        Me.TabPage1.Controls.Add(Me.txtSubType)
        Me.TabPage1.Controls.Add(Me.lblSubType)
        Me.TabPage1.Controls.Add(Me.check2EOnly)
        Me.TabPage1.Controls.Add(Me.checkboxUniqueness)
        Me.TabPage1.Controls.Add(Me.txtCardNumber)
        Me.TabPage1.Controls.Add(Me.lblCardNumber)
        Me.TabPage1.Controls.Add(Me.txtText)
        Me.TabPage1.Controls.Add(Me.txtHands)
        Me.TabPage1.Controls.Add(Me.txtFileNameBack)
        Me.TabPage1.Controls.Add(Me.txtFileNameFront)
        Me.TabPage1.Controls.Add(Me.txtGems)
        Me.TabPage1.Controls.Add(Me.txtSet)
        Me.TabPage1.Controls.Add(Me.txtRarity)
        Me.TabPage1.Controls.Add(Me.txtType)
        Me.TabPage1.Controls.Add(Me.txtRestricted)
        Me.TabPage1.Controls.Add(Me.txtGrid)
        Me.TabPage1.Controls.Add(Me.txtImmortal)
        Me.TabPage1.Controls.Add(Me.txtTitle)
        Me.TabPage1.Controls.Add(Me.lblText)
        Me.TabPage1.Controls.Add(Me.lblGrid)
        Me.TabPage1.Controls.Add(Me.lblGems)
        Me.TabPage1.Controls.Add(Me.lblTitle)
        Me.TabPage1.Controls.Add(Me.lblImmortal)
        Me.TabPage1.Controls.Add(Me.lblHands)
        Me.TabPage1.Controls.Add(Me.lblRestricted)
        Me.TabPage1.Controls.Add(Me.lblType)
        Me.TabPage1.Controls.Add(Me.lblRarity)
        Me.TabPage1.Controls.Add(Me.lblFileNameBack)
        Me.TabPage1.Controls.Add(Me.lblFileNameFront)
        Me.TabPage1.Controls.Add(Me.lblSet)
        Me.TabPage1.Controls.Add(Me.txtIDNumber)
        Me.TabPage1.Controls.Add(Me.lblID)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(282, 412)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Card"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtSubType
        '
        Me.txtSubType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSubType.Location = New System.Drawing.Point(137, 147)
        Me.txtSubType.Name = "txtSubType"
        Me.txtSubType.Size = New System.Drawing.Size(136, 23)
        Me.txtSubType.TabIndex = 94
        Me.txtSubType.Text = "#SUB_TYPE#"
        '
        'lblSubType
        '
        Me.lblSubType.AutoSize = True
        Me.lblSubType.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblSubType.Location = New System.Drawing.Point(9, 147)
        Me.lblSubType.Name = "lblSubType"
        Me.lblSubType.Size = New System.Drawing.Size(83, 18)
        Me.lblSubType.TabIndex = 95
        Me.lblSubType.Text = "Sub Type:"
        '
        'check2EOnly
        '
        Me.check2EOnly.AutoSize = True
        Me.check2EOnly.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.check2EOnly.Location = New System.Drawing.Point(137, 217)
        Me.check2EOnly.Name = "check2EOnly"
        Me.check2EOnly.Size = New System.Drawing.Size(79, 22)
        Me.check2EOnly.TabIndex = 93
        Me.check2EOnly.Text = "2E Only"
        Me.check2EOnly.UseVisualStyleBackColor = True
        '
        'checkboxUniqueness
        '
        Me.checkboxUniqueness.AutoSize = True
        Me.checkboxUniqueness.Location = New System.Drawing.Point(13, 313)
        Me.checkboxUniqueness.Name = "checkboxUniqueness"
        Me.checkboxUniqueness.Size = New System.Drawing.Size(127, 19)
        Me.checkboxUniqueness.TabIndex = 92
        Me.checkboxUniqueness.Text = "Uniqueness Marker"
        Me.checkboxUniqueness.UseVisualStyleBackColor = True
        '
        'txtCardNumber
        '
        Me.txtCardNumber.Location = New System.Drawing.Point(73, 243)
        Me.txtCardNumber.Name = "txtCardNumber"
        Me.txtCardNumber.Size = New System.Drawing.Size(56, 23)
        Me.txtCardNumber.TabIndex = 91
        Me.txtCardNumber.Text = "#CARD_NUM#"
        '
        'lblCardNumber
        '
        Me.lblCardNumber.AutoSize = True
        Me.lblCardNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblCardNumber.Location = New System.Drawing.Point(9, 242)
        Me.lblCardNumber.Name = "lblCardNumber"
        Me.lblCardNumber.Size = New System.Drawing.Size(63, 18)
        Me.lblCardNumber.TabIndex = 90
        Me.lblCardNumber.Text = "Card #:"
        '
        'txtText
        '
        Me.txtText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtText.Location = New System.Drawing.Point(57, 332)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtText.Size = New System.Drawing.Size(216, 78)
        Me.txtText.TabIndex = 77
        Me.txtText.Text = "#TEXT#"
        '
        'txtHands
        '
        Me.txtHands.Location = New System.Drawing.Point(73, 219)
        Me.txtHands.Name = "txtHands"
        Me.txtHands.Size = New System.Drawing.Size(56, 23)
        Me.txtHands.TabIndex = 72
        Me.txtHands.Text = "#HANDS#"
        '
        'txtFileNameBack
        '
        Me.txtFileNameBack.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFileNameBack.Location = New System.Drawing.Point(153, 291)
        Me.txtFileNameBack.Name = "txtFileNameBack"
        Me.txtFileNameBack.Size = New System.Drawing.Size(120, 23)
        Me.txtFileNameBack.TabIndex = 81
        Me.txtFileNameBack.Text = "#FILE_NAME_BACK#"
        '
        'txtFileNameFront
        '
        Me.txtFileNameFront.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFileNameFront.Location = New System.Drawing.Point(153, 267)
        Me.txtFileNameFront.Name = "txtFileNameFront"
        Me.txtFileNameFront.Size = New System.Drawing.Size(120, 23)
        Me.txtFileNameFront.TabIndex = 79
        Me.txtFileNameFront.Text = "#FILE_NAME_FRONT#"
        '
        'txtGems
        '
        Me.txtGems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGems.Location = New System.Drawing.Point(137, 195)
        Me.txtGems.Name = "txtGems"
        Me.txtGems.Size = New System.Drawing.Size(136, 23)
        Me.txtGems.TabIndex = 70
        Me.txtGems.Text = "#GEMS#"
        '
        'txtSet
        '
        Me.txtSet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSet.Location = New System.Drawing.Point(137, 75)
        Me.txtSet.Name = "txtSet"
        Me.txtSet.Size = New System.Drawing.Size(136, 23)
        Me.txtSet.TabIndex = 66
        Me.txtSet.Text = "#SET#"
        '
        'txtRarity
        '
        Me.txtRarity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRarity.Location = New System.Drawing.Point(137, 99)
        Me.txtRarity.Name = "txtRarity"
        Me.txtRarity.Size = New System.Drawing.Size(136, 23)
        Me.txtRarity.TabIndex = 67
        Me.txtRarity.Text = "#RARITY#"
        '
        'txtType
        '
        Me.txtType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType.Location = New System.Drawing.Point(137, 123)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(136, 23)
        Me.txtType.TabIndex = 68
        Me.txtType.Text = "#TYPE#"
        '
        'txtRestricted
        '
        Me.txtRestricted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRestricted.Location = New System.Drawing.Point(217, 243)
        Me.txtRestricted.Name = "txtRestricted"
        Me.txtRestricted.Size = New System.Drawing.Size(56, 23)
        Me.txtRestricted.TabIndex = 75
        Me.txtRestricted.Text = "#RESTRICTED#"
        '
        'txtGrid
        '
        Me.txtGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGrid.Location = New System.Drawing.Point(137, 171)
        Me.txtGrid.Name = "txtGrid"
        Me.txtGrid.Size = New System.Drawing.Size(136, 23)
        Me.txtGrid.TabIndex = 69
        Me.txtGrid.Text = "#GRID#"
        '
        'txtImmortal
        '
        Me.txtImmortal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImmortal.Location = New System.Drawing.Point(137, 51)
        Me.txtImmortal.Name = "txtImmortal"
        Me.txtImmortal.Size = New System.Drawing.Size(136, 23)
        Me.txtImmortal.TabIndex = 65
        Me.txtImmortal.Text = "#IMMORTAL#"
        '
        'txtTitle
        '
        Me.txtTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTitle.Location = New System.Drawing.Point(137, 27)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(136, 23)
        Me.txtTitle.TabIndex = 64
        Me.txtTitle.Text = "#TITLE#"
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblText.Location = New System.Drawing.Point(9, 331)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(45, 18)
        Me.lblText.TabIndex = 89
        Me.lblText.Text = "Text:"
        '
        'lblGrid
        '
        Me.lblGrid.AutoSize = True
        Me.lblGrid.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblGrid.Location = New System.Drawing.Point(9, 171)
        Me.lblGrid.Name = "lblGrid"
        Me.lblGrid.Size = New System.Drawing.Size(45, 18)
        Me.lblGrid.TabIndex = 88
        Me.lblGrid.Text = "Grid:"
        '
        'lblGems
        '
        Me.lblGems.AutoSize = True
        Me.lblGems.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblGems.Location = New System.Drawing.Point(9, 195)
        Me.lblGems.Name = "lblGems"
        Me.lblGems.Size = New System.Drawing.Size(58, 18)
        Me.lblGems.TabIndex = 87
        Me.lblGems.Text = "Gems:"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblTitle.Location = New System.Drawing.Point(9, 27)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(45, 18)
        Me.lblTitle.TabIndex = 86
        Me.lblTitle.Text = "Title:"
        '
        'lblImmortal
        '
        Me.lblImmortal.AutoSize = True
        Me.lblImmortal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblImmortal.Location = New System.Drawing.Point(9, 51)
        Me.lblImmortal.Name = "lblImmortal"
        Me.lblImmortal.Size = New System.Drawing.Size(79, 18)
        Me.lblImmortal.TabIndex = 85
        Me.lblImmortal.Text = "Immortal:"
        '
        'lblHands
        '
        Me.lblHands.AutoSize = True
        Me.lblHands.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblHands.Location = New System.Drawing.Point(9, 219)
        Me.lblHands.Name = "lblHands"
        Me.lblHands.Size = New System.Drawing.Size(61, 18)
        Me.lblHands.TabIndex = 84
        Me.lblHands.Text = "Hands:"
        '
        'lblRestricted
        '
        Me.lblRestricted.AutoSize = True
        Me.lblRestricted.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblRestricted.Location = New System.Drawing.Point(137, 243)
        Me.lblRestricted.Name = "lblRestricted"
        Me.lblRestricted.Size = New System.Drawing.Size(90, 18)
        Me.lblRestricted.TabIndex = 83
        Me.lblRestricted.Text = "Restricted:"
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblType.Location = New System.Drawing.Point(9, 123)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(49, 18)
        Me.lblType.TabIndex = 82
        Me.lblType.Text = "Type:"
        '
        'lblRarity
        '
        Me.lblRarity.AutoSize = True
        Me.lblRarity.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblRarity.Location = New System.Drawing.Point(9, 99)
        Me.lblRarity.Name = "lblRarity"
        Me.lblRarity.Size = New System.Drawing.Size(57, 18)
        Me.lblRarity.TabIndex = 80
        Me.lblRarity.Text = "Rarity:"
        '
        'lblFileNameBack
        '
        Me.lblFileNameBack.AutoSize = True
        Me.lblFileNameBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblFileNameBack.Location = New System.Drawing.Point(9, 291)
        Me.lblFileNameBack.Name = "lblFileNameBack"
        Me.lblFileNameBack.Size = New System.Drawing.Size(144, 18)
        Me.lblFileNameBack.TabIndex = 78
        Me.lblFileNameBack.Text = "File Name (Back):"
        '
        'lblFileNameFront
        '
        Me.lblFileNameFront.AutoSize = True
        Me.lblFileNameFront.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblFileNameFront.Location = New System.Drawing.Point(9, 267)
        Me.lblFileNameFront.Name = "lblFileNameFront"
        Me.lblFileNameFront.Size = New System.Drawing.Size(146, 18)
        Me.lblFileNameFront.TabIndex = 76
        Me.lblFileNameFront.Text = "File Name (Front):"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(9, 75)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(38, 18)
        Me.lblSet.TabIndex = 74
        Me.lblSet.Text = "Set:"
        '
        'txtIDNumber
        '
        Me.txtIDNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtIDNumber.AutoSize = True
        Me.txtIDNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.txtIDNumber.Location = New System.Drawing.Point(161, 3)
        Me.txtIDNumber.Name = "txtIDNumber"
        Me.txtIDNumber.Size = New System.Drawing.Size(112, 18)
        Me.txtIDNumber.TabIndex = 73
        Me.txtIDNumber.Text = "#ID_NUMBER#"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblID.Location = New System.Drawing.Point(9, 3)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(93, 18)
        Me.lblID.TabIndex = 71
        Me.lblID.Text = "ID Number:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.checkboxType1DoubleDiamond)
        Me.TabPage2.Controls.Add(Me.ButtonAdd)
        Me.TabPage2.Controls.Add(Me.txtType1SubType)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.checkType12EOnly)
        Me.TabPage2.Controls.Add(Me.checkboxType1Uniqueness)
        Me.TabPage2.Controls.Add(Me.txtType1CardNumber)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.txtType1Text)
        Me.TabPage2.Controls.Add(Me.txtType1Hands)
        Me.TabPage2.Controls.Add(Me.txtType1Gems)
        Me.TabPage2.Controls.Add(Me.txtType1Set)
        Me.TabPage2.Controls.Add(Me.txtType1Rarity)
        Me.TabPage2.Controls.Add(Me.txtType1Type)
        Me.TabPage2.Controls.Add(Me.txtType1Restricted)
        Me.TabPage2.Controls.Add(Me.txtType1Grid)
        Me.TabPage2.Controls.Add(Me.txtType1Immortal)
        Me.TabPage2.Controls.Add(Me.txtType1Title)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.txtType1IDNumber)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(282, 412)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Type 1 Errata"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'ButtonAdd
        '
        Me.ButtonAdd.Location = New System.Drawing.Point(13, 385)
        Me.ButtonAdd.Name = "ButtonAdd"
        Me.ButtonAdd.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAdd.TabIndex = 128
        Me.ButtonAdd.Text = "Add"
        Me.ButtonAdd.UseVisualStyleBackColor = True
        '
        'txtType1SubType
        '
        Me.txtType1SubType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1SubType.Location = New System.Drawing.Point(137, 147)
        Me.txtType1SubType.Name = "txtType1SubType"
        Me.txtType1SubType.Size = New System.Drawing.Size(136, 23)
        Me.txtType1SubType.TabIndex = 126
        Me.txtType1SubType.Text = "#SUB_TYPE#"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label1.Location = New System.Drawing.Point(9, 147)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 18)
        Me.Label1.TabIndex = 127
        Me.Label1.Text = "Sub Type:"
        '
        'checkType12EOnly
        '
        Me.checkType12EOnly.AutoSize = True
        Me.checkType12EOnly.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.checkType12EOnly.Location = New System.Drawing.Point(137, 217)
        Me.checkType12EOnly.Name = "checkType12EOnly"
        Me.checkType12EOnly.Size = New System.Drawing.Size(79, 22)
        Me.checkType12EOnly.TabIndex = 125
        Me.checkType12EOnly.Text = "2E Only"
        Me.checkType12EOnly.UseVisualStyleBackColor = True
        '
        'checkboxType1Uniqueness
        '
        Me.checkboxType1Uniqueness.AutoSize = True
        Me.checkboxType1Uniqueness.Location = New System.Drawing.Point(13, 268)
        Me.checkboxType1Uniqueness.Name = "checkboxType1Uniqueness"
        Me.checkboxType1Uniqueness.Size = New System.Drawing.Size(127, 19)
        Me.checkboxType1Uniqueness.TabIndex = 124
        Me.checkboxType1Uniqueness.Text = "Uniqueness Marker"
        Me.checkboxType1Uniqueness.UseVisualStyleBackColor = True
        '
        'txtType1CardNumber
        '
        Me.txtType1CardNumber.Location = New System.Drawing.Point(73, 243)
        Me.txtType1CardNumber.Name = "txtType1CardNumber"
        Me.txtType1CardNumber.Size = New System.Drawing.Size(56, 23)
        Me.txtType1CardNumber.TabIndex = 123
        Me.txtType1CardNumber.Text = "#CARD_NUM#"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label2.Location = New System.Drawing.Point(9, 242)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 18)
        Me.Label2.TabIndex = 122
        Me.Label2.Text = "Card #:"
        '
        'txtType1Text
        '
        Me.txtType1Text.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Text.Location = New System.Drawing.Point(57, 287)
        Me.txtType1Text.Multiline = True
        Me.txtType1Text.Name = "txtType1Text"
        Me.txtType1Text.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtType1Text.Size = New System.Drawing.Size(216, 90)
        Me.txtType1Text.TabIndex = 109
        Me.txtType1Text.Text = "#TEXT#"
        '
        'txtType1Hands
        '
        Me.txtType1Hands.Location = New System.Drawing.Point(73, 219)
        Me.txtType1Hands.Name = "txtType1Hands"
        Me.txtType1Hands.Size = New System.Drawing.Size(56, 23)
        Me.txtType1Hands.TabIndex = 104
        Me.txtType1Hands.Text = "#HANDS#"
        '
        'txtType1Gems
        '
        Me.txtType1Gems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Gems.Location = New System.Drawing.Point(137, 195)
        Me.txtType1Gems.Name = "txtType1Gems"
        Me.txtType1Gems.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Gems.TabIndex = 102
        Me.txtType1Gems.Text = "#GEMS#"
        '
        'txtType1Set
        '
        Me.txtType1Set.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Set.Location = New System.Drawing.Point(137, 75)
        Me.txtType1Set.Name = "txtType1Set"
        Me.txtType1Set.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Set.TabIndex = 98
        Me.txtType1Set.Text = "#SET#"
        '
        'txtType1Rarity
        '
        Me.txtType1Rarity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Rarity.Location = New System.Drawing.Point(137, 99)
        Me.txtType1Rarity.Name = "txtType1Rarity"
        Me.txtType1Rarity.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Rarity.TabIndex = 99
        Me.txtType1Rarity.Text = "#RARITY#"
        '
        'txtType1Type
        '
        Me.txtType1Type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Type.Location = New System.Drawing.Point(137, 123)
        Me.txtType1Type.Name = "txtType1Type"
        Me.txtType1Type.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Type.TabIndex = 100
        Me.txtType1Type.Text = "#TYPE#"
        '
        'txtType1Restricted
        '
        Me.txtType1Restricted.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Restricted.Location = New System.Drawing.Point(217, 243)
        Me.txtType1Restricted.Name = "txtType1Restricted"
        Me.txtType1Restricted.Size = New System.Drawing.Size(56, 23)
        Me.txtType1Restricted.TabIndex = 107
        Me.txtType1Restricted.Text = "#RESTRICTED#"
        '
        'txtType1Grid
        '
        Me.txtType1Grid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Grid.Location = New System.Drawing.Point(137, 171)
        Me.txtType1Grid.Name = "txtType1Grid"
        Me.txtType1Grid.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Grid.TabIndex = 101
        Me.txtType1Grid.Text = "#GRID#"
        '
        'txtType1Immortal
        '
        Me.txtType1Immortal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Immortal.Location = New System.Drawing.Point(137, 51)
        Me.txtType1Immortal.Name = "txtType1Immortal"
        Me.txtType1Immortal.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Immortal.TabIndex = 97
        Me.txtType1Immortal.Text = "#IMMORTAL#"
        '
        'txtType1Title
        '
        Me.txtType1Title.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1Title.Location = New System.Drawing.Point(137, 27)
        Me.txtType1Title.Name = "txtType1Title"
        Me.txtType1Title.Size = New System.Drawing.Size(136, 23)
        Me.txtType1Title.TabIndex = 96
        Me.txtType1Title.Text = "#TITLE#"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label3.Location = New System.Drawing.Point(9, 286)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 18)
        Me.Label3.TabIndex = 121
        Me.Label3.Text = "Text:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label4.Location = New System.Drawing.Point(9, 171)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 18)
        Me.Label4.TabIndex = 120
        Me.Label4.Text = "Grid:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label5.Location = New System.Drawing.Point(9, 195)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 18)
        Me.Label5.TabIndex = 119
        Me.Label5.Text = "Gems:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label6.Location = New System.Drawing.Point(9, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 18)
        Me.Label6.TabIndex = 118
        Me.Label6.Text = "Title:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label7.Location = New System.Drawing.Point(9, 51)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(79, 18)
        Me.Label7.TabIndex = 117
        Me.Label7.Text = "Immortal:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label8.Location = New System.Drawing.Point(9, 219)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 18)
        Me.Label8.TabIndex = 116
        Me.Label8.Text = "Hands:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label9.Location = New System.Drawing.Point(137, 243)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 18)
        Me.Label9.TabIndex = 115
        Me.Label9.Text = "Restricted:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label10.Location = New System.Drawing.Point(9, 123)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 18)
        Me.Label10.TabIndex = 114
        Me.Label10.Text = "Type:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label11.Location = New System.Drawing.Point(9, 99)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(57, 18)
        Me.Label11.TabIndex = 112
        Me.Label11.Text = "Rarity:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label14.Location = New System.Drawing.Point(9, 75)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(38, 18)
        Me.Label14.TabIndex = 106
        Me.Label14.Text = "Set:"
        '
        'txtType1IDNumber
        '
        Me.txtType1IDNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtType1IDNumber.AutoSize = True
        Me.txtType1IDNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.txtType1IDNumber.Location = New System.Drawing.Point(161, 3)
        Me.txtType1IDNumber.Name = "txtType1IDNumber"
        Me.txtType1IDNumber.Size = New System.Drawing.Size(112, 18)
        Me.txtType1IDNumber.TabIndex = 105
        Me.txtType1IDNumber.Text = "#ID_NUMBER#"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.Label16.Location = New System.Drawing.Point(9, 3)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(93, 18)
        Me.Label16.TabIndex = 103
        Me.Label16.Text = "ID Number:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.CardsControlVersions)
        Me.TabPage3.Location = New System.Drawing.Point(4, 24)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(282, 412)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Other Versions"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'CardsControlVersions
        '
        Me.CardsControlVersions.DisplayOriginalCardTitles = False
        Me.CardsControlVersions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CardsControlVersions.Location = New System.Drawing.Point(0, 0)
        Me.CardsControlVersions.Name = "CardsControlVersions"
        Me.CardsControlVersions.Size = New System.Drawing.Size(282, 412)
        Me.CardsControlVersions.TabIndex = 0
        '
        'checkboxDoubleDiamond
        '
        Me.checkboxDoubleDiamond.AutoSize = True
        Me.checkboxDoubleDiamond.Location = New System.Drawing.Point(146, 313)
        Me.checkboxDoubleDiamond.Name = "checkboxDoubleDiamond"
        Me.checkboxDoubleDiamond.Size = New System.Drawing.Size(116, 19)
        Me.checkboxDoubleDiamond.TabIndex = 96
        Me.checkboxDoubleDiamond.Text = "Double Diamond"
        Me.checkboxDoubleDiamond.UseVisualStyleBackColor = True
        '
        'checkboxType1DoubleDiamond
        '
        Me.checkboxType1DoubleDiamond.AutoSize = True
        Me.checkboxType1DoubleDiamond.Location = New System.Drawing.Point(157, 268)
        Me.checkboxType1DoubleDiamond.Name = "checkboxType1DoubleDiamond"
        Me.checkboxType1DoubleDiamond.Size = New System.Drawing.Size(116, 19)
        Me.checkboxType1DoubleDiamond.TabIndex = 129
        Me.checkboxType1DoubleDiamond.Text = "Double Diamond"
        Me.checkboxType1DoubleDiamond.UseVisualStyleBackColor = True
        '
        'cardDetailsEditAll
        '
        Me.Controls.Add(Me.TabControlEditCard)
        Me.Name = "cardDetailsEditAll"
        Me.Size = New System.Drawing.Size(290, 440)
        Me.TabControlEditCard.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    ' Declare datasets

    Public Sub dataBind()
        ' Set up databinds for this control.  

        ' Data bind the various text controls.
        txtIDNumber.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "ID")
        txtTitle.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Title")
        txtImmortal.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Immortal")
        txtSet.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Set")
        txtRarity.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Rarity")
        txtType.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Type")
        txtSubType.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Sub Type")
        txtGrid.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Grid")
        txtGems.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Gems")
        txtHands.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Hands")
        txtCardNumber.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "CardNumber")
        txtRestricted.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Restricted")
        txtText.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "Text")
        txtFileNameFront.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "File Name Front")
        txtFileNameBack.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Card"), "File Name Back")
        checkboxUniqueness.DataBindings.Add("Checked", Data.CardsDB.Data.Tables("Card"), "UniquenessMarker")
        checkboxDoubleDiamond.DataBindings.Add("Checked", Data.CardsDB.Data.Tables("Card"), "DoubleDiamond")
        check2EOnly.DataBindings.Add("Checked", Data.CardsDB.Data.Tables("Card"), "2E Only")

        ' Data Bind the fields on the errata tab
        txtType1IDNumber.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "ID")
        txtType1Title.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Title")
        txtType1Immortal.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Immortal")
        txtType1Set.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Set")
        txtType1Rarity.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Rarity")
        txtType1Type.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Type")
        txtType1SubType.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Sub Type")
        txtType1Grid.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Grid")
        txtType1Gems.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Gems")
        txtType1Hands.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Hands")
        txtType1CardNumber.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "CardNumber")
        txtType1Restricted.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Restricted")
        txtType1Text.DataBindings.Add("Text", Data.CardsDB.Data.Tables("Type1Errata"), "Text")
        checkboxType1Uniqueness.DataBindings.Add("Checked", Data.CardsDB.Data.Tables("Type1Errata"), "UniquenessMarker")
        checkboxType1DoubleDiamond.DataBindings.Add("Checked", Data.CardsDB.Data.Tables("Type1Errata"), "DoubleDiamond")
        checkType12EOnly.DataBindings.Add("Checked", Data.CardsDB.Data.Tables("Type1Errata"), "2E Only")
    End Sub

    Private Sub ButtonAdd_Click(sender As System.Object, e As System.EventArgs) Handles ButtonAdd.Click
        ' Add new record to errata table
        Dim newRow As DataRow = Data.CardsDB.Data.Tables("Type1Errata").NewRow
        With newRow
            .Item("ID") = txtIDNumber.Text
            .Item("Title") = txtTitle.Text
            .Item("Immortal") = txtImmortal.Text
            .Item("Set") = txtSet.Text
            .Item("Rarity") = txtRarity.Text
            .Item("Type") = txtType.Text
            .Item("Sub Type") = txtSubType.Text
            .Item("Grid") = txtGrid.Text
            .Item("Gems") = txtGems.Text
            .Item("Hands") = txtHands.Text
            .Item("CardNumber") = txtCardNumber.Text
            .Item("Restricted") = txtRestricted.Text
            .Item("Text") = txtText.Text
            .Item("UniquenessMarker") = checkboxUniqueness.Checked
            .Item("DoubleDiamond") = checkboxDoubleDiamond.Checked
            .Item("2E Only") = check2EOnly.Checked
        End With
        Data.CardsDB.Data.Tables("Type1Errata").Rows.Add(newRow)

    End Sub

    Private Sub txtIDNumber_TextChanged(sender As Object, e As EventArgs) Handles txtIDNumber.TextChanged
        ' When the current card selection changes, filter the Alternate Versions tab based on the current selection
        If Not String.IsNullOrEmpty(txtIDNumber.Text) AndAlso txtIDNumber.Text <> "#ID_NUMBER#" Then
            Dim SelectedCard As Data.Card = Data.Card.LoadCard(Convert.ToInt32(txtIDNumber.Text))
            If SelectedCard IsNot Nothing Then
                Dim Title As String = SelectedCard.DisplayTitle
                If Title.Contains("(") Then
                    Title = Title.Substring(0, Title.IndexOf("(")).Trim()
                End If
                Dim searchString As String = String.Format("Immortal|Equals|{0};Title|StartsWith|{1}", SelectedCard.Immortal, Title)

                CardsControlVersions.filterCardsList("(All)", "(All)", String.Empty, 0, False, searchString)

                TabControlEditCard.TabPages(2).Text = String.Format("Other Versions ({0})", CardsControlVersions.getVisibleCards().Count)
            End If
        End If
    End Sub
End Class
