﻿Public Class classCharts
    Private Shared colors() As Color = {Color.Blue, Color.Green, Color.Red, Color.Magenta, _
                                        Color.Gray, Color.Maroon, Color.Purple, Color.Crimson, _
                                        Color.SpringGreen, Color.DarkTurquoise, Color.Black}
    ' Draw a pie chart with the indicated values.
    ' Credit: http://www.vb-helper.com/howto_net_pie_chart_from_values.html
    Public Shared Function GetColor(ByVal ID As Integer) As Color
        While ID >= colors.Length
            ID -= colors.Length
        End While
        Return colors(ID)
    End Function
    Public Shared Function MakePieChart(ByVal values() As Double, ByVal margin As Integer, ByVal _
        radius As Integer) As Bitmap
        ' Convert the values into angles in radians.
        Dim angles(values.Length) As Single
        Dim total As Double = 0
        For i As Integer = 0 To values.Length - 1
            total = total + values(i)
        Next i
        For i As Integer = 0 To values.Length - 1
            angles(i + 1) = CSng(360 * values(i) / total)
        Next i

        ' Add a first value that is a tiny positive value.
        angles(0) = 0.0000001

        ' Make each angle be the sum of those before it.
        For i As Integer = 1 To values.Length
            angles(i) = angles(i) + angles(i - 1)
            If angles(i) > 360 Then angles(i) = 360
            Debug.WriteLine(angles(i)) '@
        Next i

        ' Prepare the Bitmap.
        Dim bm As New Bitmap( _
            CInt(2 * (margin + radius)), _
            CInt(2 * (margin + radius)))
        Dim gr As Graphics = Graphics.FromImage(bm)

        ' Draw the pie chart.
        Dim clr As Integer = 0
        For i As Integer = 0 To angles.Length - 2
            gr.FillPie( _
                New SolidBrush(colors(clr)), _
                margin, margin, _
                2 * radius, 2 * radius, _
                angles(i), angles(i + 1) - angles(i))
            gr.DrawPie(Pens.Black, margin, margin, _
                2 * radius, 2 * radius, _
                angles(i), angles(i + 1) - angles(i))
            clr = (clr + 1) Mod colors.Length
        Next i

        gr.Dispose()
        Return bm
    End Function

End Class
