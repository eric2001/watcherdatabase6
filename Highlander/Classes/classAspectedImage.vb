﻿Public Class classAspectedImage
    ' This function loads the thumbnails with their correct aspect ratio.
    '   Credit: http://www.windowsdevelop.com/windows-forms-general/vb-imagelist-control-maintaining-aspect-ratio-7247.shtml
    Public Shared Function CreateImage(ByVal ImagePath As String, ByVal HWanted As Integer, ByVal WWanted As Integer) As Image
        Dim myBitmap, WhiteSpace As System.Drawing.Bitmap
        Dim myGraphics As Graphics
        Dim myDestination As Rectangle
        Dim MaxDimension As Integer

        'create an instance of bitmap based on a file
        myBitmap = New System.Drawing.Bitmap(fileName:=ImagePath)
        'create a new square blank bitmap the right size
        If myBitmap.Height >= myBitmap.Width Then MaxDimension = myBitmap.Height Else MaxDimension = myBitmap.Width
        WhiteSpace = New System.Drawing.Bitmap(MaxDimension, MaxDimension)

        'get the drawing surface of the new blank bitmap
        myGraphics = Graphics.FromImage(WhiteSpace)

        'find out if the photo is landscape or portrait
        Dim WhiteGap As Double

        If myBitmap.Height > myBitmap.Width Then 'portrait
            WhiteGap = ((myBitmap.Width - myBitmap.Height) / 2) * -1
            myDestination = New Rectangle(x:=CInt(WhiteGap), y:=0, Width:=myBitmap.Width, Height:=myBitmap.Height)
        Else 'landscape
            WhiteGap = ((myBitmap.Width - myBitmap.Height) / 2)
            'create a destination rectangle
            myDestination = New Rectangle(x:=0, y:=CInt(WhiteGap), Width:=myBitmap.Width, Height:=myBitmap.Height)
        End If

        'draw the image on the white square
        myGraphics.DrawImage(image:=myBitmap, rect:=myDestination)

        CreateImage = WhiteSpace
    End Function ' END CreateImage

End Class
