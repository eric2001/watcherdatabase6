' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class classFileDownload
    Public Shared Function HTTPTextDownload(ByVal url As String) As String
        ' Download a web page and return the result.  On error return "".
        Dim request As System.Net.WebRequest
        Dim response As System.Net.HttpWebResponse

        ' Make sure url is not null
        If url.Length = 0 Then
            Return ""
        End If

        ' Connect to remote web server, on error exit.
        request = System.Net.WebRequest.Create(url)
        Try
            response = CType(request.GetResponse(), System.Net.HttpWebResponse)
            request.Timeout = 90 * 1000 ' seconds * 1000
            request.Credentials = System.Net.CredentialCache.DefaultCredentials
        Catch ex As Exception
            Return ""
        End Try

        ' Download the file, on error exit.
        If response.StatusDescription.ToString = "OK" Then
            Dim responseFromServer As String
            Dim dataStream As System.IO.Stream
            Dim reader As System.IO.StreamReader
            Try
                dataStream = response.GetResponseStream()
                reader = New System.IO.StreamReader(dataStream)
                responseFromServer = reader.ReadToEnd()
                reader.Close()
                dataStream.Close()
            Catch ex As Exception
                Return ""
            End Try

            Return responseFromServer
        Else
            Return ""
        End If
    End Function

    Public Shared Function HTTPBinaryDownload(ByVal strURL As String, ByVal strSavedFileName As String) As Boolean
        ' Connect to remote server and request the file.
        Dim response As System.Net.HttpWebResponse
        Dim request As System.Net.HttpWebRequest = CType(System.Net.WebRequest.Create(strURL), System.Net.HttpWebRequest)
        request.UserAgent = "Watcher Database 6"
        request.Credentials = System.Net.CredentialCache.DefaultCredentials
        request.Method = "GET"

        Try
            response = CType(request.GetResponse(), System.Net.HttpWebResponse)
            request.Timeout = 90 * 1000 ' seconds * 1000
            request.Credentials = System.Net.CredentialCache.DefaultCredentials
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString(), strURL)
            Return False
        End Try

        ' If the server responded with "OK", download the file.
        Dim DownloadedLength As Integer = 0
        If response.StatusDescription.ToString = "OK" Then
            Application.DoEvents()

            ' Start downloading the file.
            Dim dataStream As System.IO.Stream
            Dim reader As System.IO.BinaryReader
            Try
                Dim downloadedFile As New System.IO.FileStream(strSavedFileName, IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write)
                Dim writeFile As New System.IO.BinaryWriter(downloadedFile)
                dataStream = response.GetResponseStream()
                reader = New System.IO.BinaryReader(dataStream)
                Dim length As Integer
                Dim buffer(4096) As Byte
                length = reader.Read(buffer, 0, 4096)
                DownloadedLength = DownloadedLength + length

                ' Download the rest of the file in chunks so
                '   we can keep the app responsive and update
                '   any visual progress indicators.
                While (length > 0) And (Not (buffer Is Nothing))
                    Application.DoEvents()
                    writeFile.Write(buffer, 0, length)
                    Application.DoEvents()
                    length = reader.Read(buffer, 0, 4096)
                    DownloadedLength = DownloadedLength + length

                End While

                ' File download complete, close out everything before exiting.
                writeFile.Flush()
                writeFile.Close()
                reader.Close()
                dataStream.Close()

            Catch ex As Exception
                ' In the event of an error, display the message and return False.
                MessageBox.Show(ex.Message.ToString(), strURL)
                Return False
            End Try
        Else
            ' If the server didn't respond with "OK" return false.
            Return False
        End If
        ' If nothing went wrong by now, return true for successful.
        Return True
    End Function ' END
End Class
