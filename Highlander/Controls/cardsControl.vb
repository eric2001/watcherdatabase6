' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

' This control is used to display a clickable list of card titles.
Public Class cardsControl
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents groupCards As System.Windows.Forms.GroupBox
    Friend WithEvents statusBarCards As System.Windows.Forms.StatusStrip
    Friend WithEvents lstCards As System.Windows.Forms.ListView
    Friend WithEvents ContextMenuSort As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SortByImmortalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortByTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortByRarityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortBySetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortByCardNumberToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusLabelCards As ToolStripStatusLabel
    Friend WithEvents colCardTitle As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.groupCards = New System.Windows.Forms.GroupBox()
        Me.lstCards = New System.Windows.Forms.ListView()
        Me.colCardTitle = New System.Windows.Forms.ColumnHeader()
        Me.ContextMenuSort = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SortByImmortalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortByTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortByRarityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortBySetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortByCardNumberToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.statusBarCards = New System.Windows.Forms.StatusStrip()
        Me.StatusLabelCards = New System.Windows.Forms.ToolStripStatusLabel()
        Me.groupCards.SuspendLayout()
        Me.ContextMenuSort.SuspendLayout()
        Me.statusBarCards.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupCards
        '
        Me.groupCards.Controls.Add(Me.lstCards)
        Me.groupCards.Controls.Add(Me.statusBarCards)
        Me.groupCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupCards.Location = New System.Drawing.Point(0, 0)
        Me.groupCards.Name = "groupCards"
        Me.groupCards.Size = New System.Drawing.Size(272, 384)
        Me.groupCards.TabIndex = 3
        Me.groupCards.TabStop = False
        Me.groupCards.Text = "Cards"
        '
        'lstCards
        '
        Me.lstCards.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colCardTitle})
        Me.lstCards.ContextMenuStrip = Me.ContextMenuSort
        Me.lstCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstCards.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lstCards.Location = New System.Drawing.Point(3, 19)
        Me.lstCards.Name = "lstCards"
        Me.lstCards.Size = New System.Drawing.Size(266, 340)
        Me.lstCards.TabIndex = 7
        Me.lstCards.UseCompatibleStateImageBehavior = False
        Me.lstCards.View = System.Windows.Forms.View.Details
        '
        'colCardTitle
        '
        Me.colCardTitle.Text = "Title"
        Me.colCardTitle.Width = 261
        '
        'ContextMenuSort
        '
        Me.ContextMenuSort.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SortByImmortalToolStripMenuItem, Me.SortByTypeToolStripMenuItem, Me.SortByRarityToolStripMenuItem, Me.SortBySetToolStripMenuItem, Me.SortByCardNumberToolStripMenuItem})
        Me.ContextMenuSort.Name = "ContextMenuSort"
        Me.ContextMenuSort.Size = New System.Drawing.Size(187, 114)
        '
        'SortByImmortalToolStripMenuItem
        '
        Me.SortByImmortalToolStripMenuItem.Name = "SortByImmortalToolStripMenuItem"
        Me.SortByImmortalToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SortByImmortalToolStripMenuItem.Text = "Sort By Immortal"
        '
        'SortByTypeToolStripMenuItem
        '
        Me.SortByTypeToolStripMenuItem.Name = "SortByTypeToolStripMenuItem"
        Me.SortByTypeToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SortByTypeToolStripMenuItem.Text = "Sort By Type"
        '
        'SortByRarityToolStripMenuItem
        '
        Me.SortByRarityToolStripMenuItem.Name = "SortByRarityToolStripMenuItem"
        Me.SortByRarityToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SortByRarityToolStripMenuItem.Text = "Sort By Rarity"
        '
        'SortBySetToolStripMenuItem
        '
        Me.SortBySetToolStripMenuItem.Name = "SortBySetToolStripMenuItem"
        Me.SortBySetToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SortBySetToolStripMenuItem.Text = "Sort By Set"
        '
        'SortByCardNumberToolStripMenuItem
        '
        Me.SortByCardNumberToolStripMenuItem.Name = "SortByCardNumberToolStripMenuItem"
        Me.SortByCardNumberToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SortByCardNumberToolStripMenuItem.Text = "Sort By Card Number"
        '
        'statusBarCards
        '
        Me.statusBarCards.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabelCards})
        Me.statusBarCards.Location = New System.Drawing.Point(3, 359)
        Me.statusBarCards.Name = "statusBarCards"
        Me.statusBarCards.Size = New System.Drawing.Size(266, 22)
        Me.statusBarCards.SizingGrip = False
        Me.statusBarCards.TabIndex = 6
        '
        'StatusLabelCards
        '
        Me.StatusLabelCards.Name = "StatusLabelCards"
        Me.StatusLabelCards.Size = New System.Drawing.Size(54, 17)
        Me.StatusLabelCards.Text = "0 Card(s)"
        Me.StatusLabelCards.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cardsControl
        '
        Me.Controls.Add(Me.groupCards)
        Me.Name = "cardsControl"
        Me.Size = New System.Drawing.Size(272, 384)
        Me.groupCards.ResumeLayout(False)
        Me.groupCards.PerformLayout()
        Me.ContextMenuSort.ResumeLayout(False)
        Me.statusBarCards.ResumeLayout(False)
        Me.statusBarCards.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Property DisplayOriginalCardTitles As Boolean = False

    Private SortString As String = "Immortal, Title"
    Private PublisherName As String = ""
    Private SearchText As String = ""
    Private SetString As String = ""
    Private MinQuantity As Integer = 0
    Private CustomFileName As Boolean = False

    Public Sub controlSetup(ByVal singleClickFunction As System.Delegate, ByVal doubleClickFunction As System.Delegate)
        ' Initial setup for this control.

        ' Set up a function / sub to handle single clicking on a card.
        AddHandler lstCards.Click, singleClickFunction

        ' Set up a function / sub to handle using arrow keys to change the selected
        '   record (instead of single clicking).
        AddHandler lstCards.SelectedIndexChanged, singleClickFunction

        ' Set up a function / sub to handle double clicking on a card.
        AddHandler lstCards.DoubleClick, doubleClickFunction

        ' Update the card count.
        StatusLabelCards.Text = "0 Card(s) Displayed, " & Data.CardsDB.Data.Tables("Card").Rows.Count & " Card(s) Total"
    End Sub

    Public Function getSelectedCards() As ListView.SelectedListViewItemCollection
        ' Return a collection of all selected entries in the list.
        Return lstCards.SelectedItems
    End Function

    Public Function getVisibleCards() As ListView.ListViewItemCollection
        ' Return a collection of all selected entries in the list.
        Return lstCards.Items
    End Function

    ''' <summary>
    ''' Populates the list of cards based on the supplied criteria.
    ''' </summary>
    ''' <param name="strSet">A full set name (ex: "Series Edition", or "(All)" for everything).</param>
    ''' <param name="strPublisherID">A full publisher name (ex: "Thunder Castle Games", or "(All)" for everything).</param>
    ''' <param name="strKeyword">A keyword to search for ("Ex: "direct damage" or "Richie Ryan").</param>
    ''' <param name="intQuantity">0 => Any Quantity, 1 => Quantity 0, 2 => Quantity 1 or Higher, 3 => Quantity 2 or Higher.</param>
    ''' <param name="boolFileName">True to only display files with custom file names, otherwise false.</param>
    ''' <remarks></remarks>
    Public Sub filterCardsList(ByVal strSet As String, ByVal strPublisherID As String, ByVal strKeyword As String, ByVal intQuantity As Integer, ByVal boolFileName As Boolean, ByVal Optional CustomQuery As String = "")
        ' This sub generates an SQL query string to filter the cards
        '   database based on user input.  It then applies the filter,
        '   along with the specified sort to the dataset.

        ' Declare variables.
        Dim queryString As String = ""
        Dim keywordString As String = ""
        Dim havesWantsQuery As String = ""

        ' Store search parameters in case it needs to be re-generated later.
        SearchText = strKeyword
        SetString = strSet
        MinQuantity = intQuantity
        CustomFileName = boolFileName
        PublisherName = strPublisherID

        ' If the selected set is not "(All)" then limit the search results
        '   to just one expansion set.
        If strSet <> "(All)" Then
            Dim resultsSet() As DataRow = Data.SetsDB.Data.Tables("set").Select("set_long = '" & strSet.Replace("'", "''") & "'")
            If resultsSet.Length > 0 Then
                queryString = "Set = '" & resultsSet(0).Item("set_short") & "'"
            End If
        End If

        ' Hide SYS cards
        If queryString <> "" Then
            queryString = "(" & queryString & ") AND Set <> 'SYS'"
        Else
            queryString = "Set <> 'SYS'"
        End If

        ' If the selected publisher is not "(All") then limit search results to that publisher.
        If strPublisherID <> "(All)" And strPublisherID <> "" Then
            Dim resultsPublisher() As DataRow = Data.SetsDB.Data.Tables("Publisher").Select("Long_Name = '" & strPublisherID & "'")
            If resultsPublisher.Length > 0 Then
                Dim PublisherString As String = "(PublisherID = " & resultsPublisher(0).Item("ID").ToString() & ")"
                If queryString <> "" Then
                    queryString = "(" & queryString & ") AND " & PublisherString
                Else
                    queryString = PublisherString
                End If
            End If
        End If

        ' Clear the list and query the dataset.
        lstCards.Items.Clear()

        ' Generate a collection of cards that match the specified criteria.
        Dim searchResults As List(Of Data.Card) = Data.Card.LoadCards(queryString)

        ' if a keyword or phrase was entered, limit search results to cards
        '   that contain the phrase in at least one of its fields.
        If Not String.IsNullOrEmpty(strKeyword) Then
            strKeyword = strKeyword.ToLower()
            If DisplayOriginalCardTitles Then
                searchResults = searchResults.Where(Function(c) c.Immortal.ToLower().Contains(strKeyword) Or
                                                        c.Title.ToLower().Contains(strKeyword) Or
                                                        c.Text.ToLower().Contains(strKeyword) Or
                                                        c.Rarity.ToLower().Contains(strKeyword) Or
                                                        c.Grid.ToLower().Contains(strKeyword) Or
                                                        c.CardNumber.ToLower().Contains(strKeyword) Or
                                                        c.Type.ToLower().Contains(strKeyword)).ToList()
            Else
                searchResults = searchResults.Where(Function(c) c.Immortal.ToLower().Contains(strKeyword) Or
                                                        c.DisplayTitle.ToLower().Contains(strKeyword) Or
                                                        c.Text.ToLower().Contains(strKeyword) Or
                                                        c.Rarity.ToLower().Contains(strKeyword) Or
                                                        c.Grid.ToLower().Contains(strKeyword) Or
                                                        c.CardNumber.ToLower().Contains(strKeyword) Or
                                                        c.Type.ToLower().Contains(strKeyword)).ToList()
            End If
        End If

        If Not String.IsNullOrEmpty(CustomQuery) Then
            CustomQuery = CustomQuery.ToLower()
            For Each searchCondition As String In CustomQuery.Split(";")
                Dim searchParts() As String = searchCondition.Split("|")
                If searchParts(0) = "immortal" Then
                    If searchParts(1) = "startswith" Then
                        searchResults = searchResults.Where(Function(c) c.Immortal.ToLower().StartsWith(searchParts(2))).ToList()
                    ElseIf searchParts(1) = "equals" Then
                        searchResults = searchResults.Where(Function(c) c.Immortal.ToLower() = searchParts(2)).ToList()
                    Else
                        searchResults = searchResults.Where(Function(c) c.Immortal.ToLower().Contains(searchParts(2))).ToList()
                    End If
                End If
                If searchParts(0) = "restricted" Then
                    If searchParts(1) = "startswith" Then
                        searchResults = searchResults.Where(Function(c) c.Restricted.ToLower().StartsWith(searchParts(2))).ToList()
                    ElseIf searchParts(1) = "equals" Then
                        searchResults = searchResults.Where(Function(c) c.Restricted.ToLower() = searchParts(2)).ToList()
                    Else
                        searchResults = searchResults.Where(Function(c) c.Restricted.ToLower().Contains(searchParts(2))).ToList()
                    End If
                End If
                If searchParts(0) = "title" Then
                    If DisplayOriginalCardTitles Then
                        If searchParts(1) = "startswith" Then
                            searchResults = searchResults.Where(Function(c) c.Title.ToLower().StartsWith(searchParts(2))).ToList()
                        ElseIf searchParts(1) = "equals" Then
                            searchResults = searchResults.Where(Function(c) c.Title.ToLower() = searchParts(2)).ToList()
                        Else
                            searchResults = searchResults.Where(Function(c) c.Title.ToLower().Contains(searchParts(2))).ToList()
                        End If
                    Else
                        If searchParts(1) = "startswith" Then
                            searchResults = searchResults.Where(Function(c) c.DisplayTitle.ToLower().StartsWith(searchParts(2))).ToList()
                        ElseIf searchParts(1) = "equals" Then
                            searchResults = searchResults.Where(Function(c) c.DisplayTitle.ToLower() = searchParts(2)).ToList()
                        Else
                            searchResults = searchResults.Where(Function(c) c.DisplayTitle.ToLower().Contains(searchParts(2))).ToList()
                        End If
                    End If
                End If
            Next
        End If

        lstCards.BeginUpdate()
        ' If there are any matches, update lstCards.
        If searchResults.Count > 0 Then

            ' Sort the results
            Dim sortedSearchResults As List(Of Data.Card)
            If DisplayOriginalCardTitles Then
                Select Case SortString
                    Case "CardNumber"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.CardNumber).ThenBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList()
                    Case "Set"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList()
                    Case "Rarity"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.Rarity).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList()
                    Case "Type"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.Type).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList()
                    Case Else
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList()
                End Select
            Else
                Select Case SortString
                    Case "CardNumber"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.CardNumber).ThenBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList()
                    Case "Set"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList()
                    Case "Rarity"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.Rarity).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList()
                    Case "Type"
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.Type).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList()
                    Case Else
                        sortedSearchResults = searchResults.OrderBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList()
                End Select

            End If

            For Each oneCard As Data.Card In sortedSearchResults
                Dim boolAddCard As Boolean = True
                If intQuantity > 0 Then
                    Dim intCurrentQuantity As Integer = Data.Inventory.GetCardQuantity(oneCard.Id)
                    If intQuantity = 1 Then
                        ' Show only 0 Quantity Cards
                        If intCurrentQuantity <> 0 Then
                            boolAddCard = False
                        End If
                    ElseIf intQuantity = 2 Then
                        ' show cards with 1 or more copies in the collection
                        If intCurrentQuantity = 0 Then
                            boolAddCard = False
                        End If
                    ElseIf intQuantity = 3 Then
                        ' show cards with more then 1 copy in the collection
                        If intCurrentQuantity <= 1 Then
                            boolAddCard = False
                        End If
                    End If
                End If

                If boolFileName = True Then
                    If oneCard.FilePathFront.EndsWith("\1eback.jpg") OrElse
                         oneCard.FilePathFront.EndsWith("\2eback.jpg") OrElse
                         oneCard.FilePathFront.EndsWith("\3eback.jpg") OrElse
                         oneCard.FilePathFront.EndsWith("\4eback.jpg") Then
                        If oneCard.FilePathBack.EndsWith("\1eback.jpg") OrElse
                         oneCard.FilePathBack.EndsWith("\2eback.jpg") OrElse
                         oneCard.FilePathBack.EndsWith("\3eback.jpg") OrElse
                         oneCard.FilePathBack.EndsWith("\4eback.jpg") Then
                            boolAddCard = False
                        End If
                    End If
                End If

                If boolAddCard Then

                    Dim newEntry As New ListViewItem
                    If DisplayOriginalCardTitles Then
                        newEntry.Text = oneCard.OriginalDisplayName
                    Else
                        newEntry.Text = oneCard.DisplayName
                    End If

                    ' Store the card ID# in .Tag.
                    newEntry.Tag = oneCard.Id.ToString()

                    ' Add this record to the list.
                    lstCards.Items.Add(newEntry)
                End If
            Next
        End If
        lstCards.EndUpdate()

        ' Select the first card
        If lstCards.Items.Count > 0 Then
            lstCards.Items(0).Selected = True
        End If

        ' Update the card count.
        StatusLabelCards.Text = lstCards.Items.Count.ToString & " Card(s) Displayed, " & Data.CardsDB.Data.Tables("Card").Rows.Count & " Card(s) Total"
    End Sub

    Public Sub setTitle(ByVal title As String)
        ' Set a custom title for this control.
        groupCards.Text = title
    End Sub

    Public Sub moveNext()
        ' Move the selected record to the next one on the list.

        ' Make sure something is selected.
        If lstCards.SelectedIndices.Count > 0 Then

            ' If the last record is selected, do nothing.
            If lstCards.SelectedIndices(0) < lstCards.Items.Count - 1 Then

                ' Select the next record, then deselect the current record.
                lstCards.Items(lstCards.SelectedIndices(0) + 1).Selected = True
                lstCards.Items(lstCards.SelectedIndices(0)).Selected = False
            End If
        End If
    End Sub

    Public Sub movePrevious()
        ' Move the selected record to the previous one on the list.

        ' First make sure something is selected.
        If lstCards.SelectedIndices.Count > 0 Then

            ' If the first record is selected, do nothing.
            If lstCards.SelectedIndices(0) <> 0 Then

                ' Select the previous record, then deselect the current record.
                lstCards.Items(lstCards.SelectedIndices(0) - 1).Selected = True
                lstCards.Items(lstCards.SelectedIndices(0) + 1).Selected = False
            End If
        End If
    End Sub

    Private Sub SortByImmortalToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByImmortalToolStripMenuItem.Click
        SortString = "Immortal"
        Me.filterCardsList(SetString, PublisherName, SearchText, MinQuantity, CustomFileName)
    End Sub

    Private Sub SortByTypeToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByTypeToolStripMenuItem.Click
        SortString = "Type"
        Me.filterCardsList(SetString, PublisherName, SearchText, MinQuantity, CustomFileName)
    End Sub

    Private Sub SortByRarityToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByRarityToolStripMenuItem.Click
        SortString = "Rarity"
        Me.filterCardsList(SetString, PublisherName, SearchText, MinQuantity, CustomFileName)
    End Sub

    Private Sub SortBySetToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortBySetToolStripMenuItem.Click
        SortString = "Set"
        Me.filterCardsList(SetString, PublisherName, SearchText, MinQuantity, CustomFileName)
    End Sub

    Private Sub SortByCardNumberToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByCardNumberToolStripMenuItem.Click
        SortString = "CardNumber"
        Me.filterCardsList(SetString, PublisherName, SearchText, MinQuantity, CustomFileName)
    End Sub

    Private Sub lstCards_Resize(sender As Object, e As EventArgs) Handles lstCards.Resize
        ' Keep the width of the title column synced with the overall width of the control
        lstCards.Columns(0).Width = lstCards.Width - 21
        lstCards.Refresh()
    End Sub
End Class
