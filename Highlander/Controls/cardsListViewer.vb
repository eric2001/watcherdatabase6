' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.Diagnostics.Metrics
Imports System.Reflection

' This object maintains a list of cards.
'   For use in places such as the deck editor and trade list editor.
Public Class cardsListViewer
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonAdd As System.Windows.Forms.Button
    Friend WithEvents buttonDelete As System.Windows.Forms.Button
    Friend WithEvents groupCards As System.Windows.Forms.GroupBox
    Friend WithEvents ContextMenuSort As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SortByImmortalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortBySetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortByRarityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lstCardsContainer As SplitContainer
    Friend WithEvents lstCards As ListBox
    Friend WithEvents lstSubCards As ListBox
    Friend WithEvents SortByTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.buttonAdd = New System.Windows.Forms.Button()
        Me.buttonDelete = New System.Windows.Forms.Button()
        Me.groupCards = New System.Windows.Forms.GroupBox()
        Me.ContextMenuSort = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SortByImmortalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortByTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortByRarityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortBySetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lstCardsContainer = New System.Windows.Forms.SplitContainer()
        Me.lstCards = New System.Windows.Forms.ListBox()
        Me.lstSubCards = New System.Windows.Forms.ListBox()
        Me.groupCards.SuspendLayout()
        Me.ContextMenuSort.SuspendLayout()
        CType(Me.lstCardsContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lstCardsContainer.Panel1.SuspendLayout()
        Me.lstCardsContainer.Panel2.SuspendLayout()
        Me.lstCardsContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        'buttonAdd
        '
        Me.buttonAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.buttonAdd.Location = New System.Drawing.Point(16, 160)
        Me.buttonAdd.Name = "buttonAdd"
        Me.buttonAdd.Size = New System.Drawing.Size(75, 23)
        Me.buttonAdd.TabIndex = 1
        Me.buttonAdd.Text = "Add 1"
        '
        'buttonDelete
        '
        Me.buttonDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonDelete.Location = New System.Drawing.Point(112, 160)
        Me.buttonDelete.Name = "buttonDelete"
        Me.buttonDelete.Size = New System.Drawing.Size(75, 23)
        Me.buttonDelete.TabIndex = 2
        Me.buttonDelete.Text = "Delete 1"
        '
        'groupCards
        '
        Me.groupCards.Controls.Add(Me.lstCardsContainer)
        Me.groupCards.Controls.Add(Me.buttonDelete)
        Me.groupCards.Controls.Add(Me.buttonAdd)
        Me.groupCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupCards.Location = New System.Drawing.Point(0, 0)
        Me.groupCards.Name = "groupCards"
        Me.groupCards.Size = New System.Drawing.Size(200, 192)
        Me.groupCards.TabIndex = 5
        Me.groupCards.TabStop = False
        Me.groupCards.Text = "GroupBox1"
        '
        'ContextMenuSort
        '
        Me.ContextMenuSort.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SortByImmortalToolStripMenuItem, Me.SortByTypeToolStripMenuItem, Me.SortByRarityToolStripMenuItem, Me.SortBySetToolStripMenuItem})
        Me.ContextMenuSort.Name = "ContextMenuSort"
        Me.ContextMenuSort.Size = New System.Drawing.Size(164, 92)
        '
        'SortByImmortalToolStripMenuItem
        '
        Me.SortByImmortalToolStripMenuItem.Name = "SortByImmortalToolStripMenuItem"
        Me.SortByImmortalToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.SortByImmortalToolStripMenuItem.Text = "Sort By Immortal"
        '
        'SortByTypeToolStripMenuItem
        '
        Me.SortByTypeToolStripMenuItem.Name = "SortByTypeToolStripMenuItem"
        Me.SortByTypeToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.SortByTypeToolStripMenuItem.Text = "Sort By Type"
        '
        'SortByRarityToolStripMenuItem
        '
        Me.SortByRarityToolStripMenuItem.Name = "SortByRarityToolStripMenuItem"
        Me.SortByRarityToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.SortByRarityToolStripMenuItem.Text = "Sort By Rarity"
        '
        'SortBySetToolStripMenuItem
        '
        Me.SortBySetToolStripMenuItem.Name = "SortBySetToolStripMenuItem"
        Me.SortBySetToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.SortBySetToolStripMenuItem.Text = "Sort By Set"
        '
        'lstCardsContainer
        '
        Me.lstCardsContainer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstCardsContainer.Location = New System.Drawing.Point(16, 22)
        Me.lstCardsContainer.Name = "lstCardsContainer"
        Me.lstCardsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'lstCardsContainer.Panel1
        '
        Me.lstCardsContainer.Panel1.Controls.Add(Me.lstCards)
        '
        'lstCardsContainer.Panel2
        '
        Me.lstCardsContainer.Panel2.Controls.Add(Me.lstSubCards)
        Me.lstCardsContainer.Size = New System.Drawing.Size(171, 132)
        Me.lstCardsContainer.SplitterDistance = 66
        Me.lstCardsContainer.TabIndex = 4
        '
        'lstCards
        '
        Me.lstCards.ContextMenuStrip = Me.ContextMenuSort
        Me.lstCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstCards.FormattingEnabled = True
        Me.lstCards.ItemHeight = 15
        Me.lstCards.Location = New System.Drawing.Point(0, 0)
        Me.lstCards.Name = "lstCards"
        Me.lstCards.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstCards.Size = New System.Drawing.Size(171, 66)
        Me.lstCards.TabIndex = 4
        '
        'lstSubCards
        '
        Me.lstSubCards.ContextMenuStrip = Me.ContextMenuSort
        Me.lstSubCards.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstSubCards.FormattingEnabled = True
        Me.lstSubCards.ItemHeight = 15
        Me.lstSubCards.Location = New System.Drawing.Point(0, 0)
        Me.lstSubCards.Name = "lstSubCards"
        Me.lstSubCards.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstSubCards.Size = New System.Drawing.Size(171, 62)
        Me.lstSubCards.TabIndex = 5
        '
        'cardsListViewer
        '
        Me.Controls.Add(Me.groupCards)
        Me.Name = "cardsListViewer"
        Me.Size = New System.Drawing.Size(200, 192)
        Me.groupCards.ResumeLayout(False)
        Me.ContextMenuSort.ResumeLayout(False)
        Me.lstCardsContainer.Panel1.ResumeLayout(False)
        Me.lstCardsContainer.Panel2.ResumeLayout(False)
        CType(Me.lstCardsContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lstCardsContainer.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Property CardList As New List(Of Data.Card)

    Public Property DisplayOriginalCardTitles As Boolean = False

    Private Property groupTitle As String = ""
    Private Property DisplayPageCount As Boolean = False
    Private Property SortOrder As String = "Immortal"

    Private Sub UpdateGroupTitle()
        Dim cardQuantityCount As Integer = CardList.Sum(Function(c) c.Quantity)

        If DisplayPageCount = False Then
            groupCards.Text = groupTitle & " (" & cardQuantityCount.ToString & ")"
        Else
            ' Used for the Print MLE Cards Screen.
            Dim strCardsToPrintStatus As String = "Cards To Print ("
            Dim intTotalPages As Integer
            Dim strTemp As String = Convert.ToString(cardQuantityCount / 9)
            If strTemp.IndexOf(".") > 0 Then
                intTotalPages = Convert.ToInt32(strTemp.Substring(0, strTemp.IndexOf("."))) + 1
            Else
                intTotalPages = Convert.ToInt32(strTemp)
            End If
            'Dim intTotalPages As Integer = Convert.ToString(intTotalCards / 9).Substring(0, Convert.ToString(intTotalCards / 9).IndexOf("."))
            If cardQuantityCount = 1 Then
                strCardsToPrintStatus = strCardsToPrintStatus & "1 Card / "
            Else
                strCardsToPrintStatus = strCardsToPrintStatus & cardQuantityCount & " Cards / "
            End If
            If intTotalPages = 1 Then
                strCardsToPrintStatus = strCardsToPrintStatus & "1 Page)"
            Else
                strCardsToPrintStatus = strCardsToPrintStatus & intTotalPages & " Pages)"
            End If
            groupCards.Text = strCardsToPrintStatus

        End If

        SortData()
    End Sub

    Private Sub SortData()
        ' Refresh the data
        '@TODO:  Is there a better way to get this to auto update without rebinding every time?

        Dim SelectedIDs As List(Of Tuple(Of Integer, String)) = New List(Of Tuple(Of Integer, String))()
        For Each OneCard As Data.Card In lstCards.SelectedItems
            SelectedIDs.Add(New Tuple(Of Integer, String)(OneCard.Id, OneCard.DisplayNamePrefix))
        Next

        Dim DisplayData As ObservableCollection(Of Data.Card)

        If DisplayOriginalCardTitles Then
            Select Case SortOrder
                Case "Set"
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                Case "Rarity"
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.Rarity).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                Case "Type"
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.Type).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                Case Else
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
            End Select
        Else
            Select Case SortOrder
                Case "Set"
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                Case "Rarity"
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.Rarity).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                Case "Type"
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.Type).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                Case Else
                    DisplayData = New ObservableCollection(Of Data.Card)(CardList.OrderBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
            End Select
        End If

        lstCards.DataSource = DisplayData
        lstCards.DisplayMember = "DisplayName"
        lstCards.ValueMember = "Id"

        lstCards.ClearSelected()
        If lstCards.Items.Count > 0 Then
            lstCards.SetSelected(0, False)
        End If

        For i As Integer = 0 To lstCards.Items.Count - 1
            If SelectedIDs.Where(Function(id) id.Item1 = lstCards.Items(i).id And id.Item2 = lstCards.Items(i).DisplayNamePrefix).Count > 0 Then
                lstCards.SetSelected(i, True)
            End If
        Next i
    End Sub

    Public Sub deleteCard(ByVal idNumber As Integer, ByVal quantity As Integer, ByVal Optional DisplayNamePrefix As String = "")
        Dim SelectedCard As Highlander.Data.Card = CardList.Where(Function(c) c.Id = idNumber And c.DisplayNamePrefix = DisplayNamePrefix).FirstOrDefault()
        If SelectedCard IsNot Nothing Then
            Dim NewQuantity As Integer = SelectedCard.Quantity - quantity
            If NewQuantity < 0 Then
                NewQuantity = 0
            End If
            SelectedCard.Quantity = NewQuantity

            Dim DisplayName As String = GetCardDisplayName(SelectedCard, DisplayNamePrefix)
            SelectedCard.DisplayName = "(" & SelectedCard.Quantity.ToString() & ") " & DisplayName

            If SelectedCard.Quantity = 0 Then
                CardList.Remove(SelectedCard)
            End If

            UpdateGroupTitle()
        End If
    End Sub

    Public Sub addSubCard(ByVal idNumber As Integer, ByVal quantity As Integer, ByVal Optional DisplayNamePrefix As String = "")
        Dim SelectedCardList As Highlander.Data.Card = CardList.Where(Function(c) c.Id = lstSubCards.Tag).FirstOrDefault()
        If SelectedCardList IsNot Nothing Then
            Dim DisplayName As String

            Dim SelectedCard As Highlander.Data.Card = SelectedCardList.SubItems.Where(Function(c) c.Id = idNumber And c.DisplayNamePrefix = DisplayNamePrefix).FirstOrDefault()
            If SelectedCard IsNot Nothing Then
                DisplayName = GetCardDisplayName(SelectedCard, DisplayNamePrefix)
                SelectedCard.Quantity += quantity
                SelectedCard.DisplayName = "(" & SelectedCard.Quantity.ToString() & ") " & DisplayName

                UpdateGroupTitle()
            End If
        End If
    End Sub

    Public Sub deleteSubCard(ByVal idNumber As Integer, ByVal quantity As Integer, ByVal Optional DisplayNamePrefix As String = "")
        Dim SelectedCardList As Highlander.Data.Card = CardList.Where(Function(c) c.Id = lstSubCards.Tag).FirstOrDefault()
        If SelectedCardList IsNot Nothing Then
            Dim SelectedCard As Highlander.Data.Card = SelectedCardList.SubItems.Where(Function(c) c.Id = idNumber And c.DisplayNamePrefix = DisplayNamePrefix).FirstOrDefault()

            If SelectedCard IsNot Nothing Then
                Dim NewQuantity As Integer = SelectedCard.Quantity - quantity
                If NewQuantity < 0 Then
                    NewQuantity = 0
                End If
                SelectedCard.Quantity = NewQuantity

                Dim DisplayName As String = GetCardDisplayName(SelectedCard, DisplayNamePrefix)
                SelectedCard.DisplayName = "(" & SelectedCard.Quantity.ToString() & ") " & DisplayName

                If SelectedCard.Quantity = 0 Then
                    SelectedCardList.SubItems.Remove(SelectedCard)

                    ' Add 0 to the quantity to force the display to refresh
                    Me.addCard(SelectedCardList.Id, 0, SelectedCardList.DisplayNamePrefix)
                End If

                UpdateGroupTitle()
            End If
        End If
    End Sub

    Public Sub addCard(ByVal NewCard As Highlander.Data.Card, Optional ByVal DisplayNamePrefix As String = "")
        Dim DisplayName As String

        Dim SelectedCard As Highlander.Data.Card = CardList.Where(Function(c) c.Id = NewCard.Id And c.DisplayNamePrefix = DisplayNamePrefix).FirstOrDefault()
        If SelectedCard IsNot Nothing Then
            DisplayName = GetCardDisplayName(SelectedCard, DisplayNamePrefix)
            SelectedCard.Quantity += NewCard.Quantity
            SelectedCard.DisplayName = "(" & SelectedCard.Quantity.ToString() & ") " & DisplayName
        Else
            DisplayName = GetCardDisplayName(NewCard, DisplayNamePrefix)
            NewCard.DisplayName = "(" & NewCard.Quantity.ToString() & ") " & DisplayName
            NewCard.DisplayNamePrefix = DisplayNamePrefix

            CardList.Add(NewCard)
        End If

        UpdateGroupTitle()
    End Sub

    Public Sub addSubCard(ByVal NewCard As Highlander.Data.Card, Optional ByVal DisplayNamePrefix As String = "")
        Dim SelectedIDs As List(Of Tuple(Of Integer, String)) = New List(Of Tuple(Of Integer, String))()

        ' Loop through each selected card on the list
        For Each OneCard As Data.Card In lstCards.SelectedItems
            Dim DisplayName As String
            SelectedIDs.Add(New Tuple(Of Integer, String)(OneCard.Id, OneCard.DisplayNamePrefix))


            ' Check if the current card is already underneath the selected card.
            '  If it is, increase the quantity, otherwise add it.
            Dim SelectedCard As Highlander.Data.Card = OneCard.SubItems.Where(Function(c) c.Id = NewCard.Id And c.DisplayNamePrefix = DisplayNamePrefix).FirstOrDefault()
            If SelectedCard IsNot Nothing Then
                DisplayName = GetCardDisplayName(SelectedCard, DisplayNamePrefix)
                SelectedCard.Quantity += NewCard.Quantity
                SelectedCard.DisplayName = "(" & SelectedCard.Quantity.ToString() & ") " & DisplayName
            Else
                DisplayName = GetCardDisplayName(NewCard, DisplayNamePrefix)
                NewCard.DisplayName = "(" & NewCard.Quantity.ToString() & ") " & DisplayName
                NewCard.DisplayNamePrefix = DisplayNamePrefix

                OneCard.SubItems.Add(NewCard)
            End If

            ' Add 0 to the quantity to force the display to refresh
            Me.addCard(OneCard.Id, 0, OneCard.DisplayNamePrefix)
        Next

        ' Restore selected list after updates
        lstCards.ClearSelected()
        If lstCards.Items.Count > 0 Then
            lstCards.SetSelected(0, False)
        End If

        For i As Integer = 0 To lstCards.Items.Count - 1
            If SelectedIDs.Where(Function(id) id.Item1 = lstCards.Items(i).id And id.Item2 = lstCards.Items(i).DisplayNamePrefix).Count > 0 Then
                lstCards.SetSelected(i, True)
            End If
        Next i

    End Sub

    Public Sub addCard(ByVal IDNumber As Integer, ByVal Quantity As Integer, ByVal DisplayNamePrefix As String)
        Dim currentCard As Data.Card = Highlander.Data.Card.LoadCard(IDNumber, Quantity)
        If currentCard IsNot Nothing Then ' if IDNumber is valid...
            addCard(currentCard, DisplayNamePrefix)
        End If
    End Sub

    Private Sub buttonDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonDelete.Click
        If lstSubCards.Tag.ToString() = "" Then
            ' Add to the main list
            Dim SelectedIDs As List(Of Tuple(Of Integer, String)) = New List(Of Tuple(Of Integer, String))()

            For Each OneCard As Data.Card In lstCards.SelectedItems
                SelectedIDs.Add(New Tuple(Of Integer, String)(OneCard.Id, OneCard.DisplayNamePrefix))
                Me.deleteCard(OneCard.Id, 1, OneCard.DisplayNamePrefix)
            Next

            lstCards.ClearSelected()
            If lstCards.Items.Count > 0 Then
                lstCards.SetSelected(0, False)
            End If

            For i As Integer = 0 To lstCards.Items.Count - 1
                If SelectedIDs.Where(Function(id) id.Item1 = lstCards.Items(i).id And id.Item2 = lstCards.Items(i).DisplayNamePrefix).Count > 0 Then
                    lstCards.SetSelected(i, True)
                End If
            Next i

        Else
            ' Add to the sub-cards list
            Dim SelectedIDs As List(Of Tuple(Of Integer, String)) = New List(Of Tuple(Of Integer, String))()

            For Each OneCard As Data.Card In lstSubCards.SelectedItems
                SelectedIDs.Add(New Tuple(Of Integer, String)(OneCard.Id, OneCard.DisplayNamePrefix))
                Me.deleteSubCard(OneCard.Id, 1, OneCard.DisplayNamePrefix)
            Next

            lstSubCards.ClearSelected()
            If lstSubCards.Items.Count > 0 Then
                lstSubCards.SetSelected(0, False)
            End If

            For i As Integer = 0 To lstSubCards.Items.Count - 1
                If SelectedIDs.Where(Function(id) id.Item1 = lstSubCards.Items(i).id And id.Item2 = lstSubCards.Items(i).DisplayNamePrefix).Count > 0 Then
                    lstSubCards.SetSelected(i, True)
                End If
            Next i
        End If
    End Sub

    Private Sub buttonAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAdd.Click
        If lstSubCards.Tag.ToString() = "" Then
            ' Add to the main list
            Dim SelectedIDs As List(Of Tuple(Of Integer, String)) = New List(Of Tuple(Of Integer, String))()

            For Each OneCard As Data.Card In lstCards.SelectedItems
                SelectedIDs.Add(New Tuple(Of Integer, String)(OneCard.Id, OneCard.DisplayNamePrefix))
                Me.addCard(OneCard.Id, 1, OneCard.DisplayNamePrefix)
            Next

            lstCards.ClearSelected()
            If lstCards.Items.Count > 0 Then
                lstCards.SetSelected(0, False)
            End If

            For i As Integer = 0 To lstCards.Items.Count - 1
                If SelectedIDs.Where(Function(id) id.Item1 = lstCards.Items(i).id And id.Item2 = lstCards.Items(i).DisplayNamePrefix).Count > 0 Then
                    lstCards.SetSelected(i, True)
                End If
            Next i
        Else
            ' Add to the sub-card list
            Dim SelectedIDs As List(Of Tuple(Of Integer, String)) = New List(Of Tuple(Of Integer, String))()

            For Each OneCard As Data.Card In lstSubCards.SelectedItems
                SelectedIDs.Add(New Tuple(Of Integer, String)(OneCard.Id, OneCard.DisplayNamePrefix))
                Me.addSubCard(OneCard.Id, 1, OneCard.DisplayNamePrefix)
            Next

            lstSubCards.ClearSelected()
            If lstSubCards.Items.Count > 0 Then
                lstSubCards.SetSelected(0, False)
            End If

            For i As Integer = 0 To lstSubCards.Items.Count - 1
                If SelectedIDs.Where(Function(id) id.Item1 = lstSubCards.Items(i).id And id.Item2 = lstSubCards.Items(i).DisplayNamePrefix).Count > 0 Then
                    lstSubCards.SetSelected(i, True)
                End If
            Next i
        End If
    End Sub

    Public Sub setTitle(ByVal title As String)

        ' Hide the sub-cards panel
        lstCardsContainer.Panel2Collapsed = True
        lstCardsContainer.Panel2.Hide()

        AddHandler lstCards.SelectedIndexChanged, AddressOf ListSelectChanged

        groupTitle = title
        UpdateGroupTitle()
    End Sub

    Public Sub ListSelectChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' This sub is responsible for handling changes to the selected card.
        '  Note that this is internal behavior to the control, there could also be one or more
        '  handlers configured at the form level as well.

        ' Check of the current card has a sub-list
        If (sender.SelectedItems.Count > 0) Then
            Dim CurrentCard As Data.Card = sender.SelectedItems(sender.SelectedItems.Count - 1)
            If CurrentCard.SubItems.Count > 0 Then

                ' Load the sub-list
                Dim DisplayData As ObservableCollection(Of Data.Card)

                If DisplayOriginalCardTitles Then
                    Select Case SortOrder
                        Case "Set"
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                        Case "Rarity"
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.Rarity).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                        Case "Type"
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.Type).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                        Case Else
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.Immortal).ThenBy(Function(c) c.Title).ToList())
                    End Select
                Else
                    Select Case SortOrder
                        Case "Set"
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.ExpansionSet.Name).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                        Case "Rarity"
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.Rarity).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                        Case "Type"
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.Type).ThenBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                        Case Else
                            DisplayData = New ObservableCollection(Of Data.Card)(CurrentCard.SubItems.OrderBy(Function(c) c.Immortal).ThenBy(Function(c) c.DisplayTitle).ToList())
                    End Select
                End If

                ' Data bind the sub-list
                lstSubCards.DataSource = DisplayData
                lstSubCards.DisplayMember = "DisplayName"
                lstSubCards.ValueMember = "Id"
                lstSubCards.Tag = CurrentCard.Id

                ' Show the sub-cards panel
                lstCardsContainer.Panel2Collapsed = False
                lstCardsContainer.Panel2.Show()

            Else
                ' Hide the sub-cards panel
                lstCardsContainer.Panel2Collapsed = True
                lstCardsContainer.Panel2.Hide()
                lstSubCards.Tag = ""
            End If
        End If
    End Sub

    Public Sub DisableEdit()
        lstCardsContainer.Height = buttonDelete.Top + buttonDelete.Height - 15
        buttonDelete.Visible = False
        buttonAdd.Visible = False
    End Sub

    Public Sub EnablePageCounter()
        DisplayPageCount = True
    End Sub

    Private Sub SortByImmortalToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByImmortalToolStripMenuItem.Click
        SortOrder = "Immortal"
        SortData()
    End Sub

    Private Sub SortBySetToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortBySetToolStripMenuItem.Click
        SortOrder = "Set"
        SortData()
    End Sub

    Private Sub SortByRarityToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByRarityToolStripMenuItem.Click
        SortOrder = "Rarity"
        SortData()
    End Sub

    Private Sub SortByTypeToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SortByTypeToolStripMenuItem.Click
        SortOrder = "Type"
        SortData()
    End Sub

    Public Sub ClearList()
        ' Clear the list of cards
        CardList.Clear()
        UpdateGroupTitle()
    End Sub

    Public Function GetTotalCards() As Integer
        Return CardList.Sum(Function(c) c.Quantity)
    End Function

    ''' <summary>
    ''' Generate a display name to use in the list control for the specified card.
    ''' </summary>
    ''' <param name="CurrentCard">The card to generate a display name for.</param>
    ''' <param name="DisplayNamePrefix">An optional prefix to display before the card name (used for trade lists)</param>
    ''' <returns></returns>
    Private Function GetCardDisplayName(ByVal CurrentCard As Highlander.Data.Card, Optional ByVal DisplayNamePrefix As String = "") As String
        Dim DisplayName As String
        ' Allow global configuration to be overridden for trade lists
        If DisplayOriginalCardTitles Then
            DisplayName = CurrentCard.OriginalDisplayName
        Else
            CurrentCard.DisplayName = String.Empty ' reset the display name to clear the previous quantity
            DisplayName = CurrentCard.DisplayName
        End If

        If CurrentCard.SubItems.Count > 0 Then
            DisplayName = String.Format("*{0}", DisplayName)
        End If

        If DisplayNamePrefix <> "" Then
            DisplayName = String.Format("{0} {1}", DisplayNamePrefix, DisplayName)
        End If

        Return DisplayName
    End Function

End Class
