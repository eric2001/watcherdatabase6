' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

' This control is used to display images for a card front and back.
Public Class cardImage
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents groupImage As System.Windows.Forms.GroupBox
    Friend WithEvents buttonShowImageBack As System.Windows.Forms.Button
    Friend WithEvents buttonShowImageFront As System.Windows.Forms.Button
    Friend WithEvents pictureHLCard As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.groupImage = New System.Windows.Forms.GroupBox()
        Me.buttonShowImageBack = New System.Windows.Forms.Button()
        Me.buttonShowImageFront = New System.Windows.Forms.Button()
        Me.pictureHLCard = New System.Windows.Forms.PictureBox()
        Me.groupImage.SuspendLayout()
        CType(Me.pictureHLCard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'groupImage
        '
        Me.groupImage.Controls.Add(Me.buttonShowImageBack)
        Me.groupImage.Controls.Add(Me.buttonShowImageFront)
        Me.groupImage.Controls.Add(Me.pictureHLCard)
        Me.groupImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupImage.Location = New System.Drawing.Point(0, 0)
        Me.groupImage.Name = "groupImage"
        Me.groupImage.Size = New System.Drawing.Size(240, 392)
        Me.groupImage.TabIndex = 27
        Me.groupImage.TabStop = False
        Me.groupImage.Text = "Image"
        '
        'buttonShowImageBack
        '
        Me.buttonShowImageBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonShowImageBack.Location = New System.Drawing.Point(144, 344)
        Me.buttonShowImageBack.Name = "buttonShowImageBack"
        Me.buttonShowImageBack.Size = New System.Drawing.Size(75, 23)
        Me.buttonShowImageBack.TabIndex = 18
        Me.buttonShowImageBack.Text = "Show Back"
        '
        'buttonShowImageFront
        '
        Me.buttonShowImageFront.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.buttonShowImageFront.Location = New System.Drawing.Point(16, 344)
        Me.buttonShowImageFront.Name = "buttonShowImageFront"
        Me.buttonShowImageFront.Size = New System.Drawing.Size(75, 23)
        Me.buttonShowImageFront.TabIndex = 17
        Me.buttonShowImageFront.Text = "Show Front"
        '
        'pictureHLCard
        '
        Me.pictureHLCard.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pictureHLCard.Image = Global.Highlander.My.Resources.Resources.empty
        Me.pictureHLCard.Location = New System.Drawing.Point(16, 24)
        Me.pictureHLCard.Name = "pictureHLCard"
        Me.pictureHLCard.Size = New System.Drawing.Size(213, 306)
        Me.pictureHLCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureHLCard.TabIndex = 14
        Me.pictureHLCard.TabStop = False
        '
        'cardImage
        '
        Me.Controls.Add(Me.groupImage)
        Me.Name = "cardImage"
        Me.Size = New System.Drawing.Size(240, 392)
        Me.groupImage.ResumeLayout(False)
        CType(Me.pictureHLCard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    ' Declare variables.
    Private emptyJpeg As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "1eback.jpg")
    Private currentCard As New Data.Card
    Private loadedImage As String = "F"


    Public Sub loadCard(ByVal newID As Integer)
        ' This sub is responsible for setting this control to a specific card.
        '   The value of newID will determine which card is displayed by the control.
        currentCard = Data.Card.LoadCard(newID)

        ' Load the image front for the newly selected card.
        loadedImage = "F"
        loadImage("F")
    End Sub

    Public Sub loadImage(ByVal choice As String)
        ' This sub will load either the front or back of the current card.
        '   If choice is set to "B" it will load the Back, for any other value
        '   it will load the front.

        ' Run in an error handler, in case the image is corrupt or missing.
        '   In the event of an error, load emptyJpeg.
        Try
            If choice = "B" Then
                pictureHLCard.Image = System.Drawing.Bitmap.FromFile(currentCard.FilePathBack)
            Else
                pictureHLCard.Image = System.Drawing.Bitmap.FromFile(currentCard.FilePathFront)
            End If
        Catch ex As Exception
            pictureHLCard.Image = System.Drawing.Bitmap.FromFile(emptyJpeg)
        End Try
    End Sub

    Private Sub buttonShowImageFront_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonShowImageFront.Click
        ' Load the front of the current card when the user presses "Show Front".
        loadedImage = "F"
        loadImage("F")
    End Sub

    Private Sub buttonShowImageBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonShowImageBack.Click
        ' Load the back of the current card when the user presses "Show Back".
        loadedImage = "B"
        loadImage("B")
    End Sub

    Public Function getCurrentID() As Integer
        ' Returns the ID number of the card currently being displayed.
        Return currentCard.Id
    End Function

    Private Function getCurrentImage() As String
        ' Returns the path / file name of the image currently being displayed by 
        '   this control.
        If loadedImage = "B" Then
            Return currentCard.FilePathBack
        Else
            Return currentCard.FilePathFront
        End If
    End Function

    Private Sub pictureHLCard_DoubleClick(sender As Object, e As System.EventArgs) Handles pictureHLCard.DoubleClick
        ' If the image is double clicked, load it in a new window, fullsized.
        Dim fullImage As New frmImageViewer
        Me.ParentForm.ParentForm.AddOwnedForm(fullImage)
        fullImage.MdiParent = Me.ParentForm.ParentForm
        fullImage.Show()
        fullImage.loadImage(getCurrentImage())

        If currentCard IsNot Nothing AndAlso currentCard.Id > 0 Then
            fullImage.Text = currentCard.DisplayName
            If loadedImage = "B" Then
                fullImage.Text &= " Back"
            Else
                fullImage.Text &= " Front"
            End If
        Else
            fullImage.Text = ""
        End If
    End Sub
End Class
