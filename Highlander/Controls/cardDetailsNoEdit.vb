' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

' This control displays the details of one card.
'   It is identical to cardDetails.vb, except for one
'   difference - it does not allow the card quantity to 
'   be altered.
Public Class cardDetailsNoEdit
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents groupCardDetails As System.Windows.Forms.GroupBox
    Friend WithEvents lblCardSetFull As System.Windows.Forms.Label
    Friend WithEvents cardText As System.Windows.Forms.TextBox
    Friend WithEvents lblRestNum As System.Windows.Forms.Label
    Friend WithEvents pictureSignature As System.Windows.Forms.PictureBox
    Friend WithEvents pictureReserved As System.Windows.Forms.PictureBox
    Friend WithEvents picture2EOnly As System.Windows.Forms.PictureBox
    Friend WithEvents panelGems2E As System.Windows.Forms.Panel
    Friend WithEvents cardT2E As System.Windows.Forms.Label
    Friend WithEvents cardS2E As System.Windows.Forms.Label
    Friend WithEvents cardR2E As System.Windows.Forms.Label
    Friend WithEvents cardM2E As System.Windows.Forms.Label
    Friend WithEvents cardE2E As System.Windows.Forms.Label
    Friend WithEvents cardA2E As System.Windows.Forms.Label
    Friend WithEvents Hand2 As System.Windows.Forms.PictureBox
    Friend WithEvents Hand1 As System.Windows.Forms.PictureBox
    Friend WithEvents card2EOnly As System.Windows.Forms.Label
    Friend WithEvents cardHands As System.Windows.Forms.Label
    Friend WithEvents cardRestricted As System.Windows.Forms.Label
    Friend WithEvents cardGrid As System.Windows.Forms.Label
    Friend WithEvents cardGems As System.Windows.Forms.Label
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents cardRarity As System.Windows.Forms.Label
    Friend WithEvents lblRarity As System.Windows.Forms.Label
    Friend WithEvents cardType As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents cardImmortal As System.Windows.Forms.Label
    Friend WithEvents lblImmortal As System.Windows.Forms.Label
    Friend WithEvents cardTitle As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents dataHLSets As System.Data.DataSet
    Friend WithEvents cardQuantity As System.Windows.Forms.Label
    Friend WithEvents CardNumber As System.Windows.Forms.Label
    Friend WithEvents GemR2E As System.Windows.Forms.PictureBox
    Friend WithEvents GemE2E As System.Windows.Forms.PictureBox
    Friend WithEvents GemT3E As System.Windows.Forms.PictureBox
    Friend WithEvents GemT2E As System.Windows.Forms.PictureBox
    Friend WithEvents GemS2E As System.Windows.Forms.PictureBox
    Friend WithEvents gemA2E As System.Windows.Forms.PictureBox
    Friend WithEvents GemM2E As System.Windows.Forms.PictureBox
    Friend WithEvents OffHand As System.Windows.Forms.PictureBox
    Friend WithEvents LabelHandsCount As System.Windows.Forms.Label
    Friend WithEvents lblTypeHidden As System.Windows.Forms.Label
    Friend WithEvents panelGems3EP1 As System.Windows.Forms.Panel
    Friend WithEvents PanelGrid As Panel
    Friend WithEvents pictureLR As Panel
    Friend WithEvents pictureMR As Panel
    Friend WithEvents pictureLC As Panel
    Friend WithEvents pictureMC As Panel
    Friend WithEvents pictureLL As Panel
    Friend WithEvents pictureUR As Panel
    Friend WithEvents pictureML As Panel
    Friend WithEvents pictureUC As Panel
    Friend WithEvents pictureUL As Panel
    Friend WithEvents HandOr As PictureBox
    Friend WithEvents label3EP1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cardDetailsNoEdit))
        Me.groupCardDetails = New System.Windows.Forms.GroupBox()
        Me.HandOr = New System.Windows.Forms.PictureBox()
        Me.PanelGrid = New System.Windows.Forms.Panel()
        Me.pictureLR = New System.Windows.Forms.Panel()
        Me.pictureMR = New System.Windows.Forms.Panel()
        Me.pictureLC = New System.Windows.Forms.Panel()
        Me.pictureMC = New System.Windows.Forms.Panel()
        Me.pictureLL = New System.Windows.Forms.Panel()
        Me.pictureUR = New System.Windows.Forms.Panel()
        Me.pictureML = New System.Windows.Forms.Panel()
        Me.pictureUC = New System.Windows.Forms.Panel()
        Me.pictureUL = New System.Windows.Forms.Panel()
        Me.panelGems3EP1 = New System.Windows.Forms.Panel()
        Me.label3EP1 = New System.Windows.Forms.Label()
        Me.lblTypeHidden = New System.Windows.Forms.Label()
        Me.LabelHandsCount = New System.Windows.Forms.Label()
        Me.OffHand = New System.Windows.Forms.PictureBox()
        Me.CardNumber = New System.Windows.Forms.Label()
        Me.cardQuantity = New System.Windows.Forms.Label()
        Me.lblCardSetFull = New System.Windows.Forms.Label()
        Me.cardText = New System.Windows.Forms.TextBox()
        Me.lblRestNum = New System.Windows.Forms.Label()
        Me.pictureSignature = New System.Windows.Forms.PictureBox()
        Me.pictureReserved = New System.Windows.Forms.PictureBox()
        Me.picture2EOnly = New System.Windows.Forms.PictureBox()
        Me.panelGems2E = New System.Windows.Forms.Panel()
        Me.GemR2E = New System.Windows.Forms.PictureBox()
        Me.GemE2E = New System.Windows.Forms.PictureBox()
        Me.GemT3E = New System.Windows.Forms.PictureBox()
        Me.GemT2E = New System.Windows.Forms.PictureBox()
        Me.GemS2E = New System.Windows.Forms.PictureBox()
        Me.gemA2E = New System.Windows.Forms.PictureBox()
        Me.GemM2E = New System.Windows.Forms.PictureBox()
        Me.cardT2E = New System.Windows.Forms.Label()
        Me.cardS2E = New System.Windows.Forms.Label()
        Me.cardR2E = New System.Windows.Forms.Label()
        Me.cardM2E = New System.Windows.Forms.Label()
        Me.cardE2E = New System.Windows.Forms.Label()
        Me.cardA2E = New System.Windows.Forms.Label()
        Me.Hand2 = New System.Windows.Forms.PictureBox()
        Me.Hand1 = New System.Windows.Forms.PictureBox()
        Me.card2EOnly = New System.Windows.Forms.Label()
        Me.cardHands = New System.Windows.Forms.Label()
        Me.cardRestricted = New System.Windows.Forms.Label()
        Me.cardGrid = New System.Windows.Forms.Label()
        Me.cardGems = New System.Windows.Forms.Label()
        Me.lblText = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.cardRarity = New System.Windows.Forms.Label()
        Me.lblRarity = New System.Windows.Forms.Label()
        Me.cardType = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.cardImmortal = New System.Windows.Forms.Label()
        Me.lblImmortal = New System.Windows.Forms.Label()
        Me.cardTitle = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.dataHLSets = New System.Data.DataSet()
        Me.groupCardDetails.SuspendLayout()
        CType(Me.HandOr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelGrid.SuspendLayout()
        Me.panelGems3EP1.SuspendLayout()
        CType(Me.OffHand, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureSignature, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureReserved, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picture2EOnly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelGems2E.SuspendLayout()
        CType(Me.GemR2E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GemE2E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GemT3E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GemT2E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GemS2E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gemA2E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GemM2E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hand2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hand1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataHLSets, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'groupCardDetails
        '
        Me.groupCardDetails.Controls.Add(Me.HandOr)
        Me.groupCardDetails.Controls.Add(Me.PanelGrid)
        Me.groupCardDetails.Controls.Add(Me.panelGems3EP1)
        Me.groupCardDetails.Controls.Add(Me.lblTypeHidden)
        Me.groupCardDetails.Controls.Add(Me.LabelHandsCount)
        Me.groupCardDetails.Controls.Add(Me.OffHand)
        Me.groupCardDetails.Controls.Add(Me.CardNumber)
        Me.groupCardDetails.Controls.Add(Me.cardQuantity)
        Me.groupCardDetails.Controls.Add(Me.lblCardSetFull)
        Me.groupCardDetails.Controls.Add(Me.cardText)
        Me.groupCardDetails.Controls.Add(Me.lblRestNum)
        Me.groupCardDetails.Controls.Add(Me.pictureSignature)
        Me.groupCardDetails.Controls.Add(Me.pictureReserved)
        Me.groupCardDetails.Controls.Add(Me.picture2EOnly)
        Me.groupCardDetails.Controls.Add(Me.panelGems2E)
        Me.groupCardDetails.Controls.Add(Me.Hand2)
        Me.groupCardDetails.Controls.Add(Me.Hand1)
        Me.groupCardDetails.Controls.Add(Me.card2EOnly)
        Me.groupCardDetails.Controls.Add(Me.cardHands)
        Me.groupCardDetails.Controls.Add(Me.cardRestricted)
        Me.groupCardDetails.Controls.Add(Me.cardGrid)
        Me.groupCardDetails.Controls.Add(Me.cardGems)
        Me.groupCardDetails.Controls.Add(Me.lblText)
        Me.groupCardDetails.Controls.Add(Me.lblQuantity)
        Me.groupCardDetails.Controls.Add(Me.lblSet)
        Me.groupCardDetails.Controls.Add(Me.cardRarity)
        Me.groupCardDetails.Controls.Add(Me.lblRarity)
        Me.groupCardDetails.Controls.Add(Me.cardType)
        Me.groupCardDetails.Controls.Add(Me.lblType)
        Me.groupCardDetails.Controls.Add(Me.cardImmortal)
        Me.groupCardDetails.Controls.Add(Me.lblImmortal)
        Me.groupCardDetails.Controls.Add(Me.cardTitle)
        Me.groupCardDetails.Controls.Add(Me.lblTitle)
        Me.groupCardDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupCardDetails.Location = New System.Drawing.Point(0, 0)
        Me.groupCardDetails.Name = "groupCardDetails"
        Me.groupCardDetails.Size = New System.Drawing.Size(272, 384)
        Me.groupCardDetails.TabIndex = 5
        Me.groupCardDetails.TabStop = False
        Me.groupCardDetails.Text = "Details"
        '
        'HandOr
        '
        Me.HandOr.ErrorImage = Nothing
        Me.HandOr.Image = CType(resources.GetObject("HandOr.Image"), System.Drawing.Image)
        Me.HandOr.InitialImage = Nothing
        Me.HandOr.Location = New System.Drawing.Point(144, 145)
        Me.HandOr.Name = "HandOr"
        Me.HandOr.Size = New System.Drawing.Size(30, 21)
        Me.HandOr.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.HandOr.TabIndex = 70
        Me.HandOr.TabStop = False
        Me.HandOr.Visible = False
        '
        'PanelGrid
        '
        Me.PanelGrid.BackColor = System.Drawing.Color.Black
        Me.PanelGrid.Controls.Add(Me.pictureLR)
        Me.PanelGrid.Controls.Add(Me.pictureMR)
        Me.PanelGrid.Controls.Add(Me.pictureLC)
        Me.PanelGrid.Controls.Add(Me.pictureMC)
        Me.PanelGrid.Controls.Add(Me.pictureLL)
        Me.PanelGrid.Controls.Add(Me.pictureUR)
        Me.PanelGrid.Controls.Add(Me.pictureML)
        Me.PanelGrid.Controls.Add(Me.pictureUC)
        Me.PanelGrid.Controls.Add(Me.pictureUL)
        Me.PanelGrid.Location = New System.Drawing.Point(8, 192)
        Me.PanelGrid.Name = "PanelGrid"
        Me.PanelGrid.Size = New System.Drawing.Size(44, 56)
        Me.PanelGrid.TabIndex = 69
        '
        'pictureLR
        '
        Me.pictureLR.BackColor = System.Drawing.Color.White
        Me.pictureLR.Location = New System.Drawing.Point(29, 37)
        Me.pictureLR.Name = "pictureLR"
        Me.pictureLR.Size = New System.Drawing.Size(10, 14)
        Me.pictureLR.TabIndex = 5
        '
        'pictureMR
        '
        Me.pictureMR.BackColor = System.Drawing.Color.White
        Me.pictureMR.Location = New System.Drawing.Point(29, 21)
        Me.pictureMR.Name = "pictureMR"
        Me.pictureMR.Size = New System.Drawing.Size(10, 14)
        Me.pictureMR.TabIndex = 5
        '
        'pictureLC
        '
        Me.pictureLC.BackColor = System.Drawing.Color.White
        Me.pictureLC.Location = New System.Drawing.Point(17, 37)
        Me.pictureLC.Name = "pictureLC"
        Me.pictureLC.Size = New System.Drawing.Size(10, 14)
        Me.pictureLC.TabIndex = 4
        '
        'pictureMC
        '
        Me.pictureMC.BackColor = System.Drawing.Color.White
        Me.pictureMC.Location = New System.Drawing.Point(17, 21)
        Me.pictureMC.Name = "pictureMC"
        Me.pictureMC.Size = New System.Drawing.Size(10, 14)
        Me.pictureMC.TabIndex = 4
        '
        'pictureLL
        '
        Me.pictureLL.BackColor = System.Drawing.Color.White
        Me.pictureLL.Location = New System.Drawing.Point(5, 37)
        Me.pictureLL.Name = "pictureLL"
        Me.pictureLL.Size = New System.Drawing.Size(10, 14)
        Me.pictureLL.TabIndex = 3
        '
        'pictureUR
        '
        Me.pictureUR.BackColor = System.Drawing.Color.White
        Me.pictureUR.Location = New System.Drawing.Point(29, 5)
        Me.pictureUR.Name = "pictureUR"
        Me.pictureUR.Size = New System.Drawing.Size(10, 14)
        Me.pictureUR.TabIndex = 2
        '
        'pictureML
        '
        Me.pictureML.BackColor = System.Drawing.Color.White
        Me.pictureML.Location = New System.Drawing.Point(5, 21)
        Me.pictureML.Name = "pictureML"
        Me.pictureML.Size = New System.Drawing.Size(10, 14)
        Me.pictureML.TabIndex = 3
        '
        'pictureUC
        '
        Me.pictureUC.BackColor = System.Drawing.Color.White
        Me.pictureUC.Location = New System.Drawing.Point(17, 5)
        Me.pictureUC.Name = "pictureUC"
        Me.pictureUC.Size = New System.Drawing.Size(10, 14)
        Me.pictureUC.TabIndex = 1
        '
        'pictureUL
        '
        Me.pictureUL.BackColor = System.Drawing.Color.White
        Me.pictureUL.Location = New System.Drawing.Point(5, 5)
        Me.pictureUL.Name = "pictureUL"
        Me.pictureUL.Size = New System.Drawing.Size(10, 14)
        Me.pictureUL.TabIndex = 0
        '
        'panelGems3EP1
        '
        Me.panelGems3EP1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelGems3EP1.Controls.Add(Me.label3EP1)
        Me.panelGems3EP1.Location = New System.Drawing.Point(3, 341)
        Me.panelGems3EP1.Name = "panelGems3EP1"
        Me.panelGems3EP1.Size = New System.Drawing.Size(266, 40)
        Me.panelGems3EP1.TabIndex = 64
        '
        'label3EP1
        '
        Me.label3EP1.AutoSize = True
        Me.label3EP1.Font = New System.Drawing.Font("Courier New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.label3EP1.Location = New System.Drawing.Point(5, 7)
        Me.label3EP1.Name = "label3EP1"
        Me.label3EP1.Size = New System.Drawing.Size(40, 27)
        Me.label3EP1.TabIndex = 1
        Me.label3EP1.Text = "+1"
        '
        'lblTypeHidden
        '
        Me.lblTypeHidden.AutoSize = True
        Me.lblTypeHidden.Location = New System.Drawing.Point(152, 79)
        Me.lblTypeHidden.Name = "lblTypeHidden"
        Me.lblTypeHidden.Size = New System.Drawing.Size(41, 15)
        Me.lblTypeHidden.TabIndex = 63
        Me.lblTypeHidden.Text = "Label1"
        Me.lblTypeHidden.Visible = False
        '
        'LabelHandsCount
        '
        Me.LabelHandsCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelHandsCount.AutoSize = True
        Me.LabelHandsCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.LabelHandsCount.Location = New System.Drawing.Point(176, 145)
        Me.LabelHandsCount.Name = "LabelHandsCount"
        Me.LabelHandsCount.Size = New System.Drawing.Size(16, 18)
        Me.LabelHandsCount.TabIndex = 62
        Me.LabelHandsCount.Text = "0"
        Me.LabelHandsCount.Visible = False
        '
        'OffHand
        '
        Me.OffHand.Image = CType(resources.GetObject("OffHand.Image"), System.Drawing.Image)
        Me.OffHand.Location = New System.Drawing.Point(155, 144)
        Me.OffHand.Name = "OffHand"
        Me.OffHand.Size = New System.Drawing.Size(40, 21)
        Me.OffHand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.OffHand.TabIndex = 61
        Me.OffHand.TabStop = False
        Me.OffHand.Visible = False
        '
        'CardNumber
        '
        Me.CardNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CardNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.CardNumber.Location = New System.Drawing.Point(6, 249)
        Me.CardNumber.Name = "CardNumber"
        Me.CardNumber.Size = New System.Drawing.Size(51, 87)
        Me.CardNumber.TabIndex = 60
        Me.CardNumber.Text = "CARD#"
        Me.CardNumber.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'cardQuantity
        '
        Me.cardQuantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardQuantity.Location = New System.Drawing.Point(89, 148)
        Me.cardQuantity.Name = "cardQuantity"
        Me.cardQuantity.Size = New System.Drawing.Size(51, 20)
        Me.cardQuantity.TabIndex = 58
        Me.cardQuantity.Text = "QUANTITY"
        '
        'lblCardSetFull
        '
        Me.lblCardSetFull.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCardSetFull.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblCardSetFull.Location = New System.Drawing.Point(89, 125)
        Me.lblCardSetFull.Name = "lblCardSetFull"
        Me.lblCardSetFull.Size = New System.Drawing.Size(175, 20)
        Me.lblCardSetFull.TabIndex = 57
        Me.lblCardSetFull.Text = "SET"
        '
        'cardText
        '
        Me.cardText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cardText.Location = New System.Drawing.Point(59, 171)
        Me.cardText.Multiline = True
        Me.cardText.Name = "cardText"
        Me.cardText.ReadOnly = True
        Me.cardText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.cardText.Size = New System.Drawing.Size(207, 165)
        Me.cardText.TabIndex = 47
        Me.cardText.Text = "CARD_TEXT"
        '
        'lblRestNum
        '
        Me.lblRestNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRestNum.AutoSize = True
        Me.lblRestNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.lblRestNum.Location = New System.Drawing.Point(238, 145)
        Me.lblRestNum.Name = "lblRestNum"
        Me.lblRestNum.Size = New System.Drawing.Size(24, 26)
        Me.lblRestNum.TabIndex = 35
        Me.lblRestNum.Text = "0"
        Me.lblRestNum.Visible = False
        '
        'pictureSignature
        '
        Me.pictureSignature.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pictureSignature.Image = CType(resources.GetObject("pictureSignature.Image"), System.Drawing.Image)
        Me.pictureSignature.Location = New System.Drawing.Point(223, 144)
        Me.pictureSignature.Name = "pictureSignature"
        Me.pictureSignature.Size = New System.Drawing.Size(14, 26)
        Me.pictureSignature.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pictureSignature.TabIndex = 34
        Me.pictureSignature.TabStop = False
        Me.pictureSignature.Visible = False
        '
        'pictureReserved
        '
        Me.pictureReserved.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pictureReserved.Image = CType(resources.GetObject("pictureReserved.Image"), System.Drawing.Image)
        Me.pictureReserved.Location = New System.Drawing.Point(222, 145)
        Me.pictureReserved.Name = "pictureReserved"
        Me.pictureReserved.Size = New System.Drawing.Size(12, 26)
        Me.pictureReserved.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pictureReserved.TabIndex = 33
        Me.pictureReserved.TabStop = False
        Me.pictureReserved.Visible = False
        '
        'picture2EOnly
        '
        Me.picture2EOnly.Image = CType(resources.GetObject("picture2EOnly.Image"), System.Drawing.Image)
        Me.picture2EOnly.Location = New System.Drawing.Point(11, 39)
        Me.picture2EOnly.Name = "picture2EOnly"
        Me.picture2EOnly.Size = New System.Drawing.Size(17, 17)
        Me.picture2EOnly.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picture2EOnly.TabIndex = 32
        Me.picture2EOnly.TabStop = False
        Me.picture2EOnly.Visible = False
        '
        'panelGems2E
        '
        Me.panelGems2E.Controls.Add(Me.GemR2E)
        Me.panelGems2E.Controls.Add(Me.GemE2E)
        Me.panelGems2E.Controls.Add(Me.GemT3E)
        Me.panelGems2E.Controls.Add(Me.GemT2E)
        Me.panelGems2E.Controls.Add(Me.GemS2E)
        Me.panelGems2E.Controls.Add(Me.gemA2E)
        Me.panelGems2E.Controls.Add(Me.GemM2E)
        Me.panelGems2E.Controls.Add(Me.cardT2E)
        Me.panelGems2E.Controls.Add(Me.cardS2E)
        Me.panelGems2E.Controls.Add(Me.cardR2E)
        Me.panelGems2E.Controls.Add(Me.cardM2E)
        Me.panelGems2E.Controls.Add(Me.cardE2E)
        Me.panelGems2E.Controls.Add(Me.cardA2E)
        Me.panelGems2E.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelGems2E.Location = New System.Drawing.Point(3, 341)
        Me.panelGems2E.Name = "panelGems2E"
        Me.panelGems2E.Size = New System.Drawing.Size(266, 40)
        Me.panelGems2E.TabIndex = 31
        '
        'GemR2E
        '
        Me.GemR2E.Image = CType(resources.GetObject("GemR2E.Image"), System.Drawing.Image)
        Me.GemR2E.Location = New System.Drawing.Point(220, 10)
        Me.GemR2E.Name = "GemR2E"
        Me.GemR2E.Size = New System.Drawing.Size(17, 20)
        Me.GemR2E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.GemR2E.TabIndex = 35
        Me.GemR2E.TabStop = False
        '
        'GemE2E
        '
        Me.GemE2E.Image = CType(resources.GetObject("GemE2E.Image"), System.Drawing.Image)
        Me.GemE2E.Location = New System.Drawing.Point(181, 10)
        Me.GemE2E.Name = "GemE2E"
        Me.GemE2E.Size = New System.Drawing.Size(16, 20)
        Me.GemE2E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.GemE2E.TabIndex = 34
        Me.GemE2E.TabStop = False
        '
        'GemT3E
        '
        Me.GemT3E.Image = CType(resources.GetObject("GemT3E.Image"), System.Drawing.Image)
        Me.GemT3E.Location = New System.Drawing.Point(142, 10)
        Me.GemT3E.Name = "GemT3E"
        Me.GemT3E.Size = New System.Drawing.Size(16, 19)
        Me.GemT3E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.GemT3E.TabIndex = 33
        Me.GemT3E.TabStop = False
        '
        'GemT2E
        '
        Me.GemT2E.Image = CType(resources.GetObject("GemT2E.Image"), System.Drawing.Image)
        Me.GemT2E.Location = New System.Drawing.Point(142, 10)
        Me.GemT2E.Name = "GemT2E"
        Me.GemT2E.Size = New System.Drawing.Size(16, 20)
        Me.GemT2E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.GemT2E.TabIndex = 32
        Me.GemT2E.TabStop = False
        '
        'GemS2E
        '
        Me.GemS2E.Image = CType(resources.GetObject("GemS2E.Image"), System.Drawing.Image)
        Me.GemS2E.Location = New System.Drawing.Point(102, 10)
        Me.GemS2E.Name = "GemS2E"
        Me.GemS2E.Size = New System.Drawing.Size(17, 20)
        Me.GemS2E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.GemS2E.TabIndex = 31
        Me.GemS2E.TabStop = False
        '
        'gemA2E
        '
        Me.gemA2E.Image = CType(resources.GetObject("gemA2E.Image"), System.Drawing.Image)
        Me.gemA2E.Location = New System.Drawing.Point(63, 10)
        Me.gemA2E.Name = "gemA2E"
        Me.gemA2E.Size = New System.Drawing.Size(16, 20)
        Me.gemA2E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.gemA2E.TabIndex = 30
        Me.gemA2E.TabStop = False
        '
        'GemM2E
        '
        Me.GemM2E.Image = CType(resources.GetObject("GemM2E.Image"), System.Drawing.Image)
        Me.GemM2E.Location = New System.Drawing.Point(23, 10)
        Me.GemM2E.Name = "GemM2E"
        Me.GemM2E.Size = New System.Drawing.Size(17, 20)
        Me.GemM2E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.GemM2E.TabIndex = 29
        Me.GemM2E.TabStop = False
        '
        'cardT2E
        '
        Me.cardT2E.AutoSize = True
        Me.cardT2E.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardT2E.Location = New System.Drawing.Point(123, 10)
        Me.cardT2E.Name = "cardT2E"
        Me.cardT2E.Size = New System.Drawing.Size(24, 17)
        Me.cardT2E.TabIndex = 11
        Me.cardT2E.Text = "00"
        Me.cardT2E.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cardS2E
        '
        Me.cardS2E.AutoSize = True
        Me.cardS2E.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardS2E.Location = New System.Drawing.Point(83, 10)
        Me.cardS2E.Name = "cardS2E"
        Me.cardS2E.Size = New System.Drawing.Size(24, 17)
        Me.cardS2E.TabIndex = 9
        Me.cardS2E.Text = "00"
        Me.cardS2E.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cardR2E
        '
        Me.cardR2E.AutoSize = True
        Me.cardR2E.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardR2E.Location = New System.Drawing.Point(201, 10)
        Me.cardR2E.Name = "cardR2E"
        Me.cardR2E.Size = New System.Drawing.Size(24, 17)
        Me.cardR2E.TabIndex = 7
        Me.cardR2E.Text = "00"
        Me.cardR2E.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cardM2E
        '
        Me.cardM2E.AutoSize = True
        Me.cardM2E.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardM2E.Location = New System.Drawing.Point(4, 10)
        Me.cardM2E.Name = "cardM2E"
        Me.cardM2E.Size = New System.Drawing.Size(24, 17)
        Me.cardM2E.TabIndex = 5
        Me.cardM2E.Text = "00"
        Me.cardM2E.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cardE2E
        '
        Me.cardE2E.AutoSize = True
        Me.cardE2E.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardE2E.Location = New System.Drawing.Point(162, 10)
        Me.cardE2E.Name = "cardE2E"
        Me.cardE2E.Size = New System.Drawing.Size(24, 17)
        Me.cardE2E.TabIndex = 3
        Me.cardE2E.Text = "00"
        Me.cardE2E.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cardA2E
        '
        Me.cardA2E.AutoSize = True
        Me.cardA2E.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardA2E.Location = New System.Drawing.Point(44, 10)
        Me.cardA2E.Name = "cardA2E"
        Me.cardA2E.Size = New System.Drawing.Size(24, 17)
        Me.cardA2E.TabIndex = 1
        Me.cardA2E.Text = "00"
        Me.cardA2E.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Hand2
        '
        Me.Hand2.Image = CType(resources.GetObject("Hand2.Image"), System.Drawing.Image)
        Me.Hand2.Location = New System.Drawing.Point(200, 145)
        Me.Hand2.Name = "Hand2"
        Me.Hand2.Size = New System.Drawing.Size(17, 21)
        Me.Hand2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Hand2.TabIndex = 30
        Me.Hand2.TabStop = False
        Me.Hand2.Visible = False
        '
        'Hand1
        '
        Me.Hand1.Image = CType(resources.GetObject("Hand1.Image"), System.Drawing.Image)
        Me.Hand1.Location = New System.Drawing.Point(177, 145)
        Me.Hand1.Name = "Hand1"
        Me.Hand1.Size = New System.Drawing.Size(17, 21)
        Me.Hand1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Hand1.TabIndex = 29
        Me.Hand1.TabStop = False
        Me.Hand1.Visible = False
        '
        'card2EOnly
        '
        Me.card2EOnly.Location = New System.Drawing.Point(112, 171)
        Me.card2EOnly.Name = "card2EOnly"
        Me.card2EOnly.Size = New System.Drawing.Size(50, 16)
        Me.card2EOnly.TabIndex = 27
        Me.card2EOnly.Text = "2E_ONLY"
        '
        'cardHands
        '
        Me.cardHands.Location = New System.Drawing.Point(112, 171)
        Me.cardHands.Name = "cardHands"
        Me.cardHands.Size = New System.Drawing.Size(50, 16)
        Me.cardHands.TabIndex = 25
        Me.cardHands.Text = "HANDS"
        '
        'cardRestricted
        '
        Me.cardRestricted.Location = New System.Drawing.Point(112, 171)
        Me.cardRestricted.Name = "cardRestricted"
        Me.cardRestricted.Size = New System.Drawing.Size(50, 16)
        Me.cardRestricted.TabIndex = 23
        Me.cardRestricted.Text = "RESTRICTED"
        '
        'cardGrid
        '
        Me.cardGrid.Location = New System.Drawing.Point(112, 171)
        Me.cardGrid.Name = "cardGrid"
        Me.cardGrid.Size = New System.Drawing.Size(50, 16)
        Me.cardGrid.TabIndex = 21
        Me.cardGrid.Text = "GRID"
        '
        'cardGems
        '
        Me.cardGems.Location = New System.Drawing.Point(112, 171)
        Me.cardGems.Name = "cardGems"
        Me.cardGems.Size = New System.Drawing.Size(50, 16)
        Me.cardGems.TabIndex = 19
        Me.cardGems.Text = "GEMS"
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblText.Location = New System.Drawing.Point(8, 171)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(45, 18)
        Me.lblText.TabIndex = 16
        Me.lblText.Text = "Text:"
        '
        'lblQuantity
        '
        Me.lblQuantity.AutoSize = True
        Me.lblQuantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblQuantity.Location = New System.Drawing.Point(8, 148)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(75, 18)
        Me.lblQuantity.TabIndex = 10
        Me.lblQuantity.Text = "Quantity:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblSet.Location = New System.Drawing.Point(8, 125)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(38, 18)
        Me.lblSet.TabIndex = 8
        Me.lblSet.Text = "Set:"
        '
        'cardRarity
        '
        Me.cardRarity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cardRarity.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardRarity.Location = New System.Drawing.Point(89, 102)
        Me.cardRarity.Name = "cardRarity"
        Me.cardRarity.Size = New System.Drawing.Size(175, 20)
        Me.cardRarity.TabIndex = 7
        Me.cardRarity.Text = "RARITY"
        '
        'lblRarity
        '
        Me.lblRarity.AutoSize = True
        Me.lblRarity.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblRarity.Location = New System.Drawing.Point(8, 102)
        Me.lblRarity.Name = "lblRarity"
        Me.lblRarity.Size = New System.Drawing.Size(57, 18)
        Me.lblRarity.TabIndex = 6
        Me.lblRarity.Text = "Rarity:"
        '
        'cardType
        '
        Me.cardType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cardType.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardType.Location = New System.Drawing.Point(89, 79)
        Me.cardType.Name = "cardType"
        Me.cardType.Size = New System.Drawing.Size(175, 20)
        Me.cardType.TabIndex = 5
        Me.cardType.Text = "TYPE"
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblType.Location = New System.Drawing.Point(8, 79)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(49, 18)
        Me.lblType.TabIndex = 4
        Me.lblType.Text = "Type:"
        '
        'cardImmortal
        '
        Me.cardImmortal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cardImmortal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardImmortal.Location = New System.Drawing.Point(89, 56)
        Me.cardImmortal.Name = "cardImmortal"
        Me.cardImmortal.Size = New System.Drawing.Size(175, 20)
        Me.cardImmortal.TabIndex = 3
        Me.cardImmortal.Text = "IMMORTAL"
        '
        'lblImmortal
        '
        Me.lblImmortal.AutoSize = True
        Me.lblImmortal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblImmortal.Location = New System.Drawing.Point(8, 56)
        Me.lblImmortal.Name = "lblImmortal"
        Me.lblImmortal.Size = New System.Drawing.Size(79, 18)
        Me.lblImmortal.TabIndex = 2
        Me.lblImmortal.Text = "Immortal:"
        '
        'cardTitle
        '
        Me.cardTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cardTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cardTitle.Location = New System.Drawing.Point(89, 16)
        Me.cardTitle.Name = "cardTitle"
        Me.cardTitle.Size = New System.Drawing.Size(175, 40)
        Me.cardTitle.TabIndex = 1
        Me.cardTitle.Text = "TITLE"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point)
        Me.lblTitle.Location = New System.Drawing.Point(8, 16)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(45, 18)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "Title:"
        '
        'dataHLSets
        '
        Me.dataHLSets.DataSetName = "NewDataSet"
        Me.dataHLSets.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'cardDetailsNoEdit
        '
        Me.Controls.Add(Me.groupCardDetails)
        Me.Name = "cardDetailsNoEdit"
        Me.Size = New System.Drawing.Size(272, 384)
        Me.groupCardDetails.ResumeLayout(False)
        Me.groupCardDetails.PerformLayout()
        CType(Me.HandOr, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelGrid.ResumeLayout(False)
        Me.panelGems3EP1.ResumeLayout(False)
        Me.panelGems3EP1.PerformLayout()
        CType(Me.OffHand, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureSignature, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureReserved, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picture2EOnly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelGems2E.ResumeLayout(False)
        Me.panelGems2E.PerformLayout()
        CType(Me.GemR2E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GemE2E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GemT3E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GemT2E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GemS2E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gemA2E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GemM2E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hand2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hand1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataHLSets, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cardHands_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cardHands.TextChanged
        ' Display Either One or Two Hand Icons, based on the value of cardHands.Text
        ' Marcus Korolus parrying blade as an OR for the hands (either one handed or off handed) so allow for multiple hand combinations with a / in the string.
        Hand1.Visible = False
        Hand2.Visible = False
        HandOr.Visible = False
        OffHand.Visible = False
        LabelHandsCount.Visible = False
        Dim Hands As String = cardHands.Text.ToLower()
        If Hands = "1" Then
            Hand2.Visible = True
        ElseIf Hands = "2" Then
            Hand2.Visible = True
            Hand1.Visible = True
        ElseIf Hands = "o1" Then
            OffHand.Visible = True
        ElseIf Hands = "1 / 2" Then
            HandOr.Visible = True
            Hand1.Visible = True
            Hand2.Visible = True
        ElseIf Hands = "1 / o1" Then
            Hand2.Visible = True
            OffHand.Visible = True
        ElseIf Hands <> String.Empty Then
            LabelHandsCount.Text = Hands
            Hand1.Visible = False
            Hand2.Visible = True
            OffHand.Visible = False
            LabelHandsCount.Visible = True
        End If
    End Sub

    Private Sub cardGems_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cardGems.TextChanged
        ' If the current card has a value in cardGems.Text, display the Gems 
        '   panel, else hide the gems panel.
        Dim gemString As String
        If cardGems.Text = "" Then
            If panelGems2E.Visible = True Or panelGems3EP1.Visible = True Then
                cardText.Height += 40
                CardNumber.Height += 40
            End If
            panelGems2E.Visible = False
            panelGems3EP1.Visible = False
        Else
            ' When loading the Gems panel, set the value for each gem to 0
            '   then edit as needed.
            If panelGems2E.Visible = False And panelGems3EP1.Visible = False Then
                cardText.Height -= 40
                CardNumber.Height -= 40
            End If
            cardA2E.Text = "0"
            cardE2E.Text = "0"
            cardM2E.Text = "0"
            cardR2E.Text = "0"
            cardS2E.Text = "0"
            cardT2E.Text = "0"

            ' Loop through the cardGems.Text string in two character increments.
            '   The first character specifies which gem, the second character is the 
            '   gem's value.  This way if a value is 0, the gem can be omitted from the
            '   database.
            gemString = cardGems.Text

            If gemString = "+1" Then
                panelGems2E.Visible = False
                panelGems3EP1.Visible = True
            Else
                panelGems2E.Visible = True
                panelGems3EP1.Visible = False
                While gemString.Length > 0

                    ' Because gem values can be either 1 or two digits, loop through the string and 
                    '   find the next gem letter so we know where to stop.
                    Dim gemValueLength As Integer = 1
                    While (Not (gemString.Chars(gemValueLength) = "M") And
                           Not (gemString.Chars(gemValueLength) = "A") And
                           Not (gemString.Chars(gemValueLength) = "S") And
                           Not (gemString.Chars(gemValueLength) = "T") And
                           Not (gemString.Chars(gemValueLength) = "E") And
                           Not (gemString.Chars(gemValueLength) = "R") And
                           Not (gemString.Length - 1 = gemValueLength))
                        gemValueLength += 1
                    End While
                    ' If we hit the end of the string before finding another gem letter, 
                    '    then use the string length as the ending value.
                    If gemValueLength + 1 = gemString.Length Then
                        gemValueLength += 1
                    End If

                    '  Store the value for the current gem in the window.
                    If gemString.Chars(0) = "M" Then
                        cardM2E.Text = gemString.Substring(1, gemValueLength - 1)
                    End If
                    If gemString.Chars(0) = "A" Then
                        cardA2E.Text = gemString.Substring(1, gemValueLength - 1)
                    End If
                    If gemString.Chars(0) = "S" Then
                        cardS2E.Text = gemString.Substring(1, gemValueLength - 1)
                    End If
                    If gemString.Chars(0) = "T" Then
                        cardT2E.Text = gemString.Substring(1, gemValueLength - 1)
                    End If
                    If gemString.Chars(0) = "E" Then
                        cardE2E.Text = gemString.Substring(1, gemValueLength - 1)
                    End If
                    If gemString.Chars(0) = "R" Then
                        cardR2E.Text = gemString.Substring(1, gemValueLength - 1)
                    End If
                    gemString = gemString.Substring(gemValueLength)
                End While
            End If

        End If
    End Sub

    Private Sub card2EOnly_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles card2EOnly.TextChanged
        ' Display or hide the 2E Only icon, based on the value of card2EOnly.Text
        If card2EOnly.Text = "True" Then
            picture2EOnly.Visible = True
        Else
            picture2EOnly.Visible = False
        End If
    End Sub

    Private Sub cardRestricted_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cardRestricted.TextChanged
        ' Load the Reserved and Signatured icons as needed.
        '   Additionally set the restriction number, if listed.
        pictureReserved.Visible = False
        pictureSignature.Visible = False
        lblRestNum.Visible = False
        If cardRestricted.Text.Trim.Length > 0 Then
            If cardRestricted.Text.Chars(0) = "R" Then
                ' Display Reserved information
                pictureReserved.Visible = True
                If cardRestricted.Text.Length > 1 Then
                    lblRestNum.Visible = True
                    lblRestNum.Text = cardRestricted.Text.Substring(1)
                End If
            ElseIf cardRestricted.Text.Chars(0) = "S" Then
                ' Display Signatured Information.
                pictureSignature.Visible = True
                If cardRestricted.Text.Length > 1 Then
                    lblRestNum.Visible = True
                    lblRestNum.Text = cardRestricted.Text.Substring(1)
                End If
            Else
                ' Display Restricted Information.
                If lblTypeHidden.Text <> "Persona" Then
                    lblRestNum.Visible = True
                    lblRestNum.Text = cardRestricted.Text
                End If
            End If

        End If
    End Sub

    Private Sub cardGrid_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cardGrid.TextChanged
        UpdateGrid()
    End Sub

    Public Sub LoadCard(OneCard As Data.Card)

        If OneCard IsNot Nothing Then

            If Data.CardsDB.CardDisplayMode = "type_one" Then
                cardTitle.Text = OneCard.SecondEditionTitle.Replace("&", "&&")

                cardType.Text = OneCard.Type.ToString()
                If Not String.IsNullOrEmpty(OneCard.SecondEditionSubType) Then
                    cardType.Text &= ": " & OneCard.SecondEditionSubType
                End If

            Else
                cardTitle.Text = OneCard.Title.Replace("&", "&&")
                cardType.Text = OneCard.Type.ToString()
                If Not String.IsNullOrEmpty(OneCard.SubType) Then
                    cardType.Text &= ": " & OneCard.SubType
                End If
            End If

            ' Insert a diamond into the title string if the uniqueness marker is set.
            If (OneCard.UniquenessMarker = True) Then
                cardTitle.Text = ChrW(9830) & cardTitle.Text
            End If
            If (OneCard.DoubleDiamond = True) Then
                cardTitle.Text = ChrW(9830) & ChrW(9830) & cardTitle.Text
            End If

            cardImmortal.Text = OneCard.Immortal.Replace("&", "&&")
            lblTypeHidden.Text = OneCard.Type
            cardRarity.Text = OneCard.Rarity
            cardQuantity.Text = OneCard.Quantity
            cardText.Text = OneCard.Text
            cardGems.Text = OneCard.Gems
            cardGrid.Text = OneCard.Grid
            cardRestricted.Text = OneCard.Restricted
            cardHands.Text = OneCard.Hands
            card2EOnly.Text = OneCard.SecondEditionOnly
            CardNumber.Text = OneCard.CardNumber

            lblCardSetFull.Text = OneCard.ExpansionSet.Name

            ' Adjust the color of the Toughness gem based on if the  card is 2E or other.
            If OneCard.ExpansionSet.Edition = Data.Edition.SecondEditionRevised Or
                    OneCard.ExpansionSet.Edition = Data.Edition.ThirdEdition Then
                GemT2E.Visible = False
                GemT3E.Visible = True
            Else
                GemT2E.Visible = True
                GemT3E.Visible = False
            End If
        End If

    End Sub

    Public Sub LoadCard(ByVal CardID As Integer)
        ' Load the specified card.
        Dim OneCard As Data.Card = Data.Card.LoadCard(CardID, Data.Inventory.GetCardQuantity(CardID))

        LoadCard(OneCard)
    End Sub

    Private Sub cardType_TextChanged(sender As Object, e As System.EventArgs) Handles cardType.TextChanged
        UpdateGrid()
    End Sub

    Private Sub UpdateGrid()
        ' For attacks, blocks, dodges, etc. Display a card grid.
        PanelGrid.Visible = False
        pictureUL.BackColor = Color.White
        pictureUC.BackColor = Color.White
        pictureUR.BackColor = Color.White
        pictureML.BackColor = Color.White
        pictureMC.BackColor = Color.White
        pictureMR.BackColor = Color.White
        pictureLL.BackColor = Color.White
        pictureLC.BackColor = Color.White
        pictureLR.BackColor = Color.White

        Dim HighlightedColor As Color = Color.White
        If lblTypeHidden.Text = "Block" Or lblTypeHidden.Text = "Martial Arts Block" Or lblTypeHidden.Text = "Basic Block" Then
            HighlightedColor = Color.Blue
        ElseIf lblTypeHidden.Text = "Attack" Or lblTypeHidden.Text = "Martial Arts Attack" Or lblTypeHidden.Text = "Basic Attack" Then
            HighlightedColor = Color.Red
        ElseIf lblTypeHidden.Text = "Dodge" Then
            HighlightedColor = Color.Green
        ElseIf lblTypeHidden.Text = "Special Attack" Or lblTypeHidden.Text = "Attack/Defense" Or lblTypeHidden.Text = "Ranged Attack" Then
            HighlightedColor = Color.Gold
        ElseIf lblTypeHidden.Text = "Combat Trick" Or lblTypeHidden.Text = "Unexpected Combat Trick" Then
            HighlightedColor = Color.Purple
        Else
            HighlightedColor = Color.Gray
        End If
        ' Make sure the grid string is the correct length.
        If cardGrid.Text.Length = 9 Then
            PanelGrid.Visible = True

            ' For each "X" in the string, set the corresponding grid to visible.
            If cardGrid.Text.Chars(0) = "X" Then
                pictureUL.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(1) = "X" Then
                pictureUC.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(2) = "X" Then
                pictureUR.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(3) = "X" Then
                pictureML.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(4) = "X" Then
                pictureMC.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(5) = "X" Then
                pictureMR.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(6) = "X" Then
                pictureLL.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(7) = "X" Then
                pictureLC.BackColor = HighlightedColor
            End If
            If cardGrid.Text.Chars(8) = "X" Then
                pictureLR.BackColor = HighlightedColor
            End If
        End If
    End Sub
End Class
