' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data

' This control is used to display images for a card front and back at the same time.
Public Class cardImageFB
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents groupImage As System.Windows.Forms.GroupBox
    Friend WithEvents pictureHLBack As System.Windows.Forms.PictureBox
    Friend WithEvents pictureHLFront As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.groupImage = New System.Windows.Forms.GroupBox()
        Me.pictureHLBack = New System.Windows.Forms.PictureBox()
        Me.pictureHLFront = New System.Windows.Forms.PictureBox()
        Me.groupImage.SuspendLayout()
        CType(Me.pictureHLBack, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureHLFront, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'groupImage
        '
        Me.groupImage.Controls.Add(Me.pictureHLBack)
        Me.groupImage.Controls.Add(Me.pictureHLFront)
        Me.groupImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupImage.Location = New System.Drawing.Point(0, 0)
        Me.groupImage.Name = "groupImage"
        Me.groupImage.Size = New System.Drawing.Size(272, 176)
        Me.groupImage.TabIndex = 27
        Me.groupImage.TabStop = False
        Me.groupImage.Text = "Image"
        '
        'pictureHLBack
        '
        Me.pictureHLBack.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pictureHLBack.Image = Global.Highlander.My.Resources.Resources.empty
        Me.pictureHLBack.Location = New System.Drawing.Point(135, 16)
        Me.pictureHLBack.Name = "pictureHLBack"
        Me.pictureHLBack.Size = New System.Drawing.Size(135, 153)
        Me.pictureHLBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureHLBack.TabIndex = 3
        Me.pictureHLBack.TabStop = False
        '
        'pictureHLFront
        '
        Me.pictureHLFront.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pictureHLFront.Image = Global.Highlander.My.Resources.Resources.empty
        Me.pictureHLFront.Location = New System.Drawing.Point(1, 16)
        Me.pictureHLFront.Name = "pictureHLFront"
        Me.pictureHLFront.Size = New System.Drawing.Size(135, 153)
        Me.pictureHLFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureHLFront.TabIndex = 2
        Me.pictureHLFront.TabStop = False
        '
        'cardImageFB
        '
        Me.Controls.Add(Me.groupImage)
        Me.Name = "cardImageFB"
        Me.Size = New System.Drawing.Size(272, 176)
        Me.groupImage.ResumeLayout(False)
        CType(Me.pictureHLBack, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureHLFront, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    ' Declare variables.
    Private emptyJpeg As String = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "1eback.jpg")
    Private currentCard As New Data.Card

    Public Sub LoadCard(ByVal NewCard As Data.Card)
        currentCard = NewCard

        ' Load the image front and back for the newly selected card.
        Try
            pictureHLFront.Image = System.Drawing.Bitmap.FromFile(currentCard.FilePathFront)
        Catch ex As Exception
            pictureHLFront.Image = System.Drawing.Bitmap.FromFile(emptyJpeg)
        End Try

        Try
            pictureHLBack.Image = System.Drawing.Bitmap.FromFile(currentCard.FilePathBack)
        Catch ex As Exception
            pictureHLBack.Image = System.Drawing.Bitmap.FromFile(emptyJpeg)
        End Try
    End Sub

    Public Sub LoadCard(ByVal newID As Integer)
        ' This sub is responsible for setting this control to a specific card.
        '   The value of newID will determine which card is displayed by the control.
        Dim NewCard As Data.Card = Data.Card.LoadCard(newID)

        LoadCard(NewCard)
    End Sub

    Public Function getCurrentID() As Integer
        ' Returns the ID number of the card currently being displayed.
        Return currentCard.Id
    End Function

    Private Sub pictureHLFront_DoubleClick(sender As Object, e As System.EventArgs) Handles pictureHLFront.DoubleClick
        loadPictureWindow("F")
    End Sub

    Private Sub pictureHLBack_DoubleClick(sender As Object, e As System.EventArgs) Handles pictureHLBack.DoubleClick
        loadPictureWindow("B")
    End Sub

    Private Sub loadPictureWindow(ByVal side As String)
        Dim ImagePath As String = emptyJpeg
        If side = "B" Then
            ImagePath = currentCard.FilePathBack
        Else
            ImagePath = currentCard.FilePathFront
        End If

        ' If the image is double clicked, load it in a new window, fullsized.
        Dim fullImage As New frmImageViewer
        Me.ParentForm.ParentForm.AddOwnedForm(fullImage)
        fullImage.MdiParent = Me.ParentForm.ParentForm
        fullImage.Show()
        fullImage.loadImage(ImagePath)

        If currentCard IsNot Nothing AndAlso currentCard.Id > 0 Then
            fullImage.Text = currentCard.DisplayName
            If side = "B" Then
                fullImage.Text &= " Back"
            Else
                fullImage.Text &= " Front"
            End If
        Else
            fullImage.Text = ""
        End If
    End Sub


    Private Sub groupImage_Resize(sender As Object, e As System.EventArgs) Handles groupImage.Resize
        Dim Middle As Integer = (groupImage.Width / 2) - 2
        pictureHLFront.Width = Middle

        pictureHLBack.Left = Middle
        pictureHLBack.Top = pictureHLFront.Top
        pictureHLBack.Width = pictureHLFront.Width
        pictureHLBack.Height = pictureHLFront.Height
    End Sub
End Class
