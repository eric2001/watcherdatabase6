﻿Imports System.Data

Public Class FormPlayGameControlTest

    Private Sub FormPlayGameControlTest_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ListView1.Items(0).ToolTipText = "Asdf" & vbCrLf & "Some more text"

        AddCardToList(1, ListView1)
        AddCardToList(2, ListView1)
        AddCardToList(3, ListView1)
        AddCardToList(4, ListView1)
        AddCardToList(5, ListView1)
        AddCardToList(6, ListView1)
        AddCardToList(7, ListView1)
        AddCardToList(8, ListView1)
        AddCardToList(9, ListView1)
        AddCardToList(10, ListView1)
        AddCardToList(4000, ListView1)
        AddCardToList(4001, ListView2)
        AddCardToList(4002, ListView2)
        AddCardToList(4003, ListView2)
        AddCardToList(4004, ListView2)
        AddCardToList(4005, ListView2)
        AddCardToList(4006, ListView2)
        AddCardToList(4007, ListView2)
    End Sub

    Public Sub AddCardToList(ByVal CardID As Integer, CardList As System.Windows.Forms.ListView)
        Dim newCard As New ListViewItem
        Dim results() As DataRow = Data.CardsDB.Data.Tables("Card").Select("ID = " & CardID.ToString())
        If results.Length > 0 Then
            Dim currentCard As Data.Card = Data.Card.LoadCard(CardID)
            newCard.Text = currentCard.DisplayName
            newCard.Tag = currentCard.Id.ToString()
            Dim strImagePath As String = currentCard.FileNameFront
            ImageListCards.Images.Add(CardID, classAspectedImage.CreateImage(strImagePath, ImageListCards.ImageSize.Height, ImageListCards.ImageSize.Width))
            newCard.ImageKey = CardID

            Dim strCardText As String = ""
            strCardText = "Type: " & currentCard.Type & vbCrLf & currentCard.Text
            If currentCard.Grid <> "" Then
                strCardText = "GRID: " & currentCard.Grid & vbCrLf & strCardText
            End If
            newCard.ToolTipText = strCardText
            CardList.Items.Add(newCard)
        End If
    End Sub
End Class