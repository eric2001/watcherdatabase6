﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlayGame2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPlayGame2))
        Me.GroupGameStatus = New System.Windows.Forms.GroupBox()
        Me.txtPlayerName = New System.Windows.Forms.TextBox()
        Me.lblPlayerName = New System.Windows.Forms.Label()
        Me.txtOpponentName = New System.Windows.Forms.TextBox()
        Me.lblOpponentName = New System.Windows.Forms.Label()
        Me.ComboBoxPhase = New System.Windows.Forms.ComboBox()
        Me.lblOpponentAbility = New System.Windows.Forms.Label()
        Me.lblOpponentTurnNum = New System.Windows.Forms.Label()
        Me.txtOpponentAbility = New System.Windows.Forms.TextBox()
        Me.txtPlayerAbility = New System.Windows.Forms.TextBox()
        Me.txtOpponentTurnNum = New System.Windows.Forms.TextBox()
        Me.txtPlayerTurnNum = New System.Windows.Forms.TextBox()
        Me.lblPlayerAbility = New System.Windows.Forms.Label()
        Me.lblPlayerTurnNum = New System.Windows.Forms.Label()
        Me.GroupOpponent = New System.Windows.Forms.GroupBox()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.ListOpponentInPlay = New System.Windows.Forms.ListView()
        Me.ContextMenuStripOpponentInPlay = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DiscardCardsToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsFromTheGameToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsPermanentlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StealCardsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageListCards = New System.Windows.Forms.ImageList(Me.components)
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.ListOpponentDiscard = New System.Windows.Forms.ListView()
        Me.ContextMenuStripOpponentDiscard = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ReturnCardsToPlayToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsFromTheGameToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsPermanentlyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveAllCardsPermanentlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.ListOpponentRemovedFromGame = New System.Windows.Forms.ListView()
        Me.ContextMenuStripOpponentXedFromGame = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ReturnCardsToPlayToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturnCardsToPreGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsPermanentlyToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupOpponentPreGame = New System.Windows.Forms.GroupBox()
        Me.ListOpponentPreGame = New System.Windows.Forms.ListView()
        Me.ContextMenuStripOpponentPreGame = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PlayCardsToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsFromTheGameToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsPermanentlyToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupPlayer = New System.Windows.Forms.GroupBox()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.ListPlayerInPlay = New System.Windows.Forms.ListView()
        Me.ContextMenuStripPlayerInPlay = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DiscardCardsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsFromGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturnCardsToHandToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StealCardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.ListPlayerDiscard = New System.Windows.Forms.ListView()
        Me.ContextMenuStripPlayerDiscard = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ReturnCardsToPlayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturnCardsToHandToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsFromGameToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlaceCardsOnTopOfDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlaceCardsOnBottomOfDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.ListPlayerRemovedFromGame = New System.Windows.Forms.ListView()
        Me.ContextMenuStripPlayerXedFromGame = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ReturnToPlayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturnToPreGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.ListPlayerPreGame = New System.Windows.Forms.ListView()
        Me.ContextMenuStripPlayerPreGame = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PlayCardsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCardsFromTheGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupPlayerDeck = New System.Windows.Forms.GroupBox()
        Me.PictureDeck = New System.Windows.Forms.PictureBox()
        Me.ContextMenuStripDeck = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DrawUpToAbilityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShuffleDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShuffleDiscardIntoDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MakeExertionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LookAtTopXCardsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LookAtDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupPlayerHand = New System.Windows.Forms.GroupBox()
        Me.ListPlayerHand = New System.Windows.Forms.ListView()
        Me.ContextMenuStripHand = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PlayCardsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiscardCardsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveFromGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiscardToTopOfDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiscardToBottomOfDeckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlayUnderneathToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.GroupBoxOpponentPlayCards = New System.Windows.Forms.GroupBox()
        Me.ButtonOpponentAddCards = New System.Windows.Forms.Button()
        Me.ComboBoxAddCardsAction = New System.Windows.Forms.ComboBox()
        Me.groupSearch = New System.Windows.Forms.GroupBox()
        Me.comboPublisher = New System.Windows.Forms.ComboBox()
        Me.lblPublisher = New System.Windows.Forms.Label()
        Me.txtKeywordSearch = New System.Windows.Forms.TextBox()
        Me.lblKeyword = New System.Windows.Forms.Label()
        Me.lblSet = New System.Windows.Forms.Label()
        Me.comboSets = New System.Windows.Forms.ComboBox()
        Me.ButtonCardDetails = New System.Windows.Forms.Button()
        Me.CardsAllCards = New Highlander.cardsControl()
        Me.CurrentCardImage = New Highlander.cardImageFB()
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer5 = New System.Windows.Forms.SplitContainer()
        Me.ListPlayerDeck = New System.Windows.Forms.ListView()
        Me.ColumnRandomVal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnDisplayName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtChatText = New System.Windows.Forms.TextBox()
        Me.ButtonSend = New System.Windows.Forms.Button()
        Me.txtLogBox = New System.Windows.Forms.TextBox()
        Me.GroupGameStatus.SuspendLayout()
        Me.GroupOpponent.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.ContextMenuStripOpponentInPlay.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.ContextMenuStripOpponentDiscard.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.ContextMenuStripOpponentXedFromGame.SuspendLayout()
        Me.GroupOpponentPreGame.SuspendLayout()
        Me.ContextMenuStripOpponentPreGame.SuspendLayout()
        Me.GroupPlayer.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.ContextMenuStripPlayerInPlay.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.ContextMenuStripPlayerDiscard.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.ContextMenuStripPlayerXedFromGame.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.ContextMenuStripPlayerPreGame.SuspendLayout()
        Me.GroupPlayerDeck.SuspendLayout()
        CType(Me.PictureDeck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStripDeck.SuspendLayout()
        Me.GroupPlayerHand.SuspendLayout()
        Me.ContextMenuStripHand.SuspendLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.GroupBoxOpponentPlayCards.SuspendLayout()
        Me.groupSearch.SuspendLayout()
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.Panel2.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        CType(Me.SplitContainer5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer5.Panel1.SuspendLayout()
        Me.SplitContainer5.Panel2.SuspendLayout()
        Me.SplitContainer5.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupGameStatus
        '
        Me.GroupGameStatus.Controls.Add(Me.txtPlayerName)
        Me.GroupGameStatus.Controls.Add(Me.lblPlayerName)
        Me.GroupGameStatus.Controls.Add(Me.txtOpponentName)
        Me.GroupGameStatus.Controls.Add(Me.lblOpponentName)
        Me.GroupGameStatus.Controls.Add(Me.ComboBoxPhase)
        Me.GroupGameStatus.Controls.Add(Me.lblOpponentAbility)
        Me.GroupGameStatus.Controls.Add(Me.lblOpponentTurnNum)
        Me.GroupGameStatus.Controls.Add(Me.txtOpponentAbility)
        Me.GroupGameStatus.Controls.Add(Me.txtPlayerAbility)
        Me.GroupGameStatus.Controls.Add(Me.txtOpponentTurnNum)
        Me.GroupGameStatus.Controls.Add(Me.txtPlayerTurnNum)
        Me.GroupGameStatus.Controls.Add(Me.lblPlayerAbility)
        Me.GroupGameStatus.Controls.Add(Me.lblPlayerTurnNum)
        Me.GroupGameStatus.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupGameStatus.Location = New System.Drawing.Point(0, 0)
        Me.GroupGameStatus.Name = "GroupGameStatus"
        Me.GroupGameStatus.Size = New System.Drawing.Size(784, 40)
        Me.GroupGameStatus.TabIndex = 0
        Me.GroupGameStatus.TabStop = False
        Me.GroupGameStatus.Text = "Game Details"
        '
        'txtPlayerName
        '
        Me.txtPlayerName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPlayerName.Location = New System.Drawing.Point(384, 12)
        Me.txtPlayerName.Name = "txtPlayerName"
        Me.txtPlayerName.ReadOnly = True
        Me.txtPlayerName.Size = New System.Drawing.Size(42, 20)
        Me.txtPlayerName.TabIndex = 13
        Me.txtPlayerName.Text = "22A"
        '
        'lblPlayerName
        '
        Me.lblPlayerName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPlayerName.AutoSize = True
        Me.lblPlayerName.Location = New System.Drawing.Point(340, 16)
        Me.lblPlayerName.Name = "lblPlayerName"
        Me.lblPlayerName.Size = New System.Drawing.Size(38, 13)
        Me.lblPlayerName.TabIndex = 12
        Me.lblPlayerName.Text = "Name:"
        '
        'txtOpponentName
        '
        Me.txtOpponentName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOpponentName.Location = New System.Drawing.Point(718, 12)
        Me.txtOpponentName.Name = "txtOpponentName"
        Me.txtOpponentName.ReadOnly = True
        Me.txtOpponentName.Size = New System.Drawing.Size(60, 20)
        Me.txtOpponentName.TabIndex = 11
        Me.txtOpponentName.Text = "22A"
        '
        'lblOpponentName
        '
        Me.lblOpponentName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOpponentName.AutoSize = True
        Me.lblOpponentName.Location = New System.Drawing.Point(674, 16)
        Me.lblOpponentName.Name = "lblOpponentName"
        Me.lblOpponentName.Size = New System.Drawing.Size(38, 13)
        Me.lblOpponentName.TabIndex = 10
        Me.lblOpponentName.Text = "Name:"
        '
        'ComboBoxPhase
        '
        Me.ComboBoxPhase.FormattingEnabled = True
        Me.ComboBoxPhase.Items.AddRange(New Object() {"Sweep Phase", "Defense Phase", "Attack Phase", "Ability Adjustment Phase", "Discard Phase", "End Turn"})
        Me.ComboBoxPhase.Location = New System.Drawing.Point(3, 13)
        Me.ComboBoxPhase.Name = "ComboBoxPhase"
        Me.ComboBoxPhase.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxPhase.TabIndex = 9
        '
        'lblOpponentAbility
        '
        Me.lblOpponentAbility.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOpponentAbility.AutoSize = True
        Me.lblOpponentAbility.Location = New System.Drawing.Point(584, 16)
        Me.lblOpponentAbility.Name = "lblOpponentAbility"
        Me.lblOpponentAbility.Size = New System.Drawing.Size(37, 13)
        Me.lblOpponentAbility.TabIndex = 8
        Me.lblOpponentAbility.Text = "Ability:"
        '
        'lblOpponentTurnNum
        '
        Me.lblOpponentTurnNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOpponentTurnNum.AutoSize = True
        Me.lblOpponentTurnNum.Location = New System.Drawing.Point(432, 16)
        Me.lblOpponentTurnNum.Name = "lblOpponentTurnNum"
        Me.lblOpponentTurnNum.Size = New System.Drawing.Size(99, 13)
        Me.lblOpponentTurnNum.TabIndex = 7
        Me.lblOpponentTurnNum.Text = "Opponent's Turn #:"
        '
        'txtOpponentAbility
        '
        Me.txtOpponentAbility.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOpponentAbility.Location = New System.Drawing.Point(627, 13)
        Me.txtOpponentAbility.Name = "txtOpponentAbility"
        Me.txtOpponentAbility.Size = New System.Drawing.Size(41, 20)
        Me.txtOpponentAbility.TabIndex = 6
        Me.txtOpponentAbility.Text = "15"
        '
        'txtPlayerAbility
        '
        Me.txtPlayerAbility.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPlayerAbility.Location = New System.Drawing.Point(293, 13)
        Me.txtPlayerAbility.Name = "txtPlayerAbility"
        Me.txtPlayerAbility.Size = New System.Drawing.Size(41, 20)
        Me.txtPlayerAbility.TabIndex = 5
        Me.txtPlayerAbility.Text = "15"
        '
        'txtOpponentTurnNum
        '
        Me.txtOpponentTurnNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOpponentTurnNum.Location = New System.Drawing.Point(537, 13)
        Me.txtOpponentTurnNum.Name = "txtOpponentTurnNum"
        Me.txtOpponentTurnNum.ReadOnly = True
        Me.txtOpponentTurnNum.Size = New System.Drawing.Size(41, 20)
        Me.txtOpponentTurnNum.TabIndex = 4
        Me.txtOpponentTurnNum.Text = "22B"
        '
        'txtPlayerTurnNum
        '
        Me.txtPlayerTurnNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPlayerTurnNum.Location = New System.Drawing.Point(203, 13)
        Me.txtPlayerTurnNum.Name = "txtPlayerTurnNum"
        Me.txtPlayerTurnNum.ReadOnly = True
        Me.txtPlayerTurnNum.Size = New System.Drawing.Size(41, 20)
        Me.txtPlayerTurnNum.TabIndex = 3
        Me.txtPlayerTurnNum.Text = "22A"
        '
        'lblPlayerAbility
        '
        Me.lblPlayerAbility.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPlayerAbility.AutoSize = True
        Me.lblPlayerAbility.Location = New System.Drawing.Point(250, 16)
        Me.lblPlayerAbility.Name = "lblPlayerAbility"
        Me.lblPlayerAbility.Size = New System.Drawing.Size(37, 13)
        Me.lblPlayerAbility.TabIndex = 2
        Me.lblPlayerAbility.Text = "Ability:"
        '
        'lblPlayerTurnNum
        '
        Me.lblPlayerTurnNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPlayerTurnNum.AutoSize = True
        Me.lblPlayerTurnNum.Location = New System.Drawing.Point(130, 16)
        Me.lblPlayerTurnNum.Name = "lblPlayerTurnNum"
        Me.lblPlayerTurnNum.Size = New System.Drawing.Size(67, 13)
        Me.lblPlayerTurnNum.TabIndex = 1
        Me.lblPlayerTurnNum.Text = "Your Turn #:"
        '
        'GroupOpponent
        '
        Me.GroupOpponent.Controls.Add(Me.SplitContainer1)
        Me.GroupOpponent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupOpponent.Location = New System.Drawing.Point(0, 0)
        Me.GroupOpponent.Name = "GroupOpponent"
        Me.GroupOpponent.Size = New System.Drawing.Size(444, 257)
        Me.GroupOpponent.TabIndex = 3
        Me.GroupOpponent.TabStop = False
        Me.GroupOpponent.Text = "Opponent"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 16)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TabControl1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupOpponentPreGame)
        Me.SplitContainer1.Size = New System.Drawing.Size(438, 238)
        Me.SplitContainer1.SplitterDistance = 281
        Me.SplitContainer1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.AllowDrop = True
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(277, 234)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.ListOpponentInPlay)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(269, 208)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "In Play"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'ListOpponentInPlay
        '
        Me.ListOpponentInPlay.AllowDrop = True
        Me.ListOpponentInPlay.ContextMenuStrip = Me.ContextMenuStripOpponentInPlay
        Me.ListOpponentInPlay.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListOpponentInPlay.FullRowSelect = True
        Me.ListOpponentInPlay.LargeImageList = Me.ImageListCards
        Me.ListOpponentInPlay.Location = New System.Drawing.Point(3, 3)
        Me.ListOpponentInPlay.Name = "ListOpponentInPlay"
        Me.ListOpponentInPlay.ShowItemToolTips = True
        Me.ListOpponentInPlay.Size = New System.Drawing.Size(263, 202)
        Me.ListOpponentInPlay.TabIndex = 2
        Me.ListOpponentInPlay.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripOpponentInPlay
        '
        Me.ContextMenuStripOpponentInPlay.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiscardCardsToolStripMenuItem2, Me.RemoveCardsFromTheGameToolStripMenuItem1, Me.RemoveCardsPermanentlyToolStripMenuItem, Me.StealCardsToolStripMenuItem})
        Me.ContextMenuStripOpponentInPlay.Name = "ContextMenuStripOpponentInPlay"
        Me.ContextMenuStripOpponentInPlay.Size = New System.Drawing.Size(239, 92)
        '
        'DiscardCardsToolStripMenuItem2
        '
        Me.DiscardCardsToolStripMenuItem2.Name = "DiscardCardsToolStripMenuItem2"
        Me.DiscardCardsToolStripMenuItem2.Size = New System.Drawing.Size(238, 22)
        Me.DiscardCardsToolStripMenuItem2.Text = "Discard Cards"
        '
        'RemoveCardsFromTheGameToolStripMenuItem1
        '
        Me.RemoveCardsFromTheGameToolStripMenuItem1.Name = "RemoveCardsFromTheGameToolStripMenuItem1"
        Me.RemoveCardsFromTheGameToolStripMenuItem1.Size = New System.Drawing.Size(238, 22)
        Me.RemoveCardsFromTheGameToolStripMenuItem1.Text = "Remove Cards From The Game"
        '
        'RemoveCardsPermanentlyToolStripMenuItem
        '
        Me.RemoveCardsPermanentlyToolStripMenuItem.Name = "RemoveCardsPermanentlyToolStripMenuItem"
        Me.RemoveCardsPermanentlyToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.RemoveCardsPermanentlyToolStripMenuItem.Text = "Remove Cards Permanently"
        '
        'StealCardsToolStripMenuItem
        '
        Me.StealCardsToolStripMenuItem.Name = "StealCardsToolStripMenuItem"
        Me.StealCardsToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.StealCardsToolStripMenuItem.Text = "Steal Cards"
        '
        'ImageListCards
        '
        Me.ImageListCards.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListCards.ImageSize = New System.Drawing.Size(64, 64)
        Me.ImageListCards.TransparentColor = System.Drawing.Color.Transparent
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.ListOpponentDiscard)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(269, 208)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Discard"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'ListOpponentDiscard
        '
        Me.ListOpponentDiscard.AllowDrop = True
        Me.ListOpponentDiscard.ContextMenuStrip = Me.ContextMenuStripOpponentDiscard
        Me.ListOpponentDiscard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListOpponentDiscard.FullRowSelect = True
        Me.ListOpponentDiscard.LargeImageList = Me.ImageListCards
        Me.ListOpponentDiscard.Location = New System.Drawing.Point(3, 3)
        Me.ListOpponentDiscard.Name = "ListOpponentDiscard"
        Me.ListOpponentDiscard.ShowItemToolTips = True
        Me.ListOpponentDiscard.Size = New System.Drawing.Size(263, 202)
        Me.ListOpponentDiscard.TabIndex = 2
        Me.ListOpponentDiscard.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripOpponentDiscard
        '
        Me.ContextMenuStripOpponentDiscard.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReturnCardsToPlayToolStripMenuItem1, Me.RemoveCardsFromTheGameToolStripMenuItem2, Me.RemoveCardsPermanentlyToolStripMenuItem1, Me.RemoveAllCardsPermanentlyToolStripMenuItem})
        Me.ContextMenuStripOpponentDiscard.Name = "ContextMenuStripOpponentDiscard"
        Me.ContextMenuStripOpponentDiscard.Size = New System.Drawing.Size(238, 92)
        '
        'ReturnCardsToPlayToolStripMenuItem1
        '
        Me.ReturnCardsToPlayToolStripMenuItem1.Name = "ReturnCardsToPlayToolStripMenuItem1"
        Me.ReturnCardsToPlayToolStripMenuItem1.Size = New System.Drawing.Size(237, 22)
        Me.ReturnCardsToPlayToolStripMenuItem1.Text = "Return Cards to Play"
        '
        'RemoveCardsFromTheGameToolStripMenuItem2
        '
        Me.RemoveCardsFromTheGameToolStripMenuItem2.Name = "RemoveCardsFromTheGameToolStripMenuItem2"
        Me.RemoveCardsFromTheGameToolStripMenuItem2.Size = New System.Drawing.Size(237, 22)
        Me.RemoveCardsFromTheGameToolStripMenuItem2.Text = "Remove Cards from the Game"
        '
        'RemoveCardsPermanentlyToolStripMenuItem1
        '
        Me.RemoveCardsPermanentlyToolStripMenuItem1.Name = "RemoveCardsPermanentlyToolStripMenuItem1"
        Me.RemoveCardsPermanentlyToolStripMenuItem1.Size = New System.Drawing.Size(237, 22)
        Me.RemoveCardsPermanentlyToolStripMenuItem1.Text = "Remove Cards Permanently"
        '
        'RemoveAllCardsPermanentlyToolStripMenuItem
        '
        Me.RemoveAllCardsPermanentlyToolStripMenuItem.Name = "RemoveAllCardsPermanentlyToolStripMenuItem"
        Me.RemoveAllCardsPermanentlyToolStripMenuItem.Size = New System.Drawing.Size(237, 22)
        Me.RemoveAllCardsPermanentlyToolStripMenuItem.Text = "Remove All Cards Permanently"
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.ListOpponentRemovedFromGame)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(269, 208)
        Me.TabPage7.TabIndex = 2
        Me.TabPage7.Text = "X-ed From Game"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'ListOpponentRemovedFromGame
        '
        Me.ListOpponentRemovedFromGame.AllowDrop = True
        Me.ListOpponentRemovedFromGame.ContextMenuStrip = Me.ContextMenuStripOpponentXedFromGame
        Me.ListOpponentRemovedFromGame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListOpponentRemovedFromGame.FullRowSelect = True
        Me.ListOpponentRemovedFromGame.LargeImageList = Me.ImageListCards
        Me.ListOpponentRemovedFromGame.Location = New System.Drawing.Point(0, 0)
        Me.ListOpponentRemovedFromGame.Name = "ListOpponentRemovedFromGame"
        Me.ListOpponentRemovedFromGame.ShowItemToolTips = True
        Me.ListOpponentRemovedFromGame.Size = New System.Drawing.Size(269, 208)
        Me.ListOpponentRemovedFromGame.TabIndex = 2
        Me.ListOpponentRemovedFromGame.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripOpponentXedFromGame
        '
        Me.ContextMenuStripOpponentXedFromGame.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReturnCardsToPlayToolStripMenuItem2, Me.ReturnCardsToPreGameToolStripMenuItem, Me.RemoveCardsPermanentlyToolStripMenuItem2})
        Me.ContextMenuStripOpponentXedFromGame.Name = "ContextMenuStripOpponentXedFromGame"
        Me.ContextMenuStripOpponentXedFromGame.Size = New System.Drawing.Size(221, 70)
        '
        'ReturnCardsToPlayToolStripMenuItem2
        '
        Me.ReturnCardsToPlayToolStripMenuItem2.Name = "ReturnCardsToPlayToolStripMenuItem2"
        Me.ReturnCardsToPlayToolStripMenuItem2.Size = New System.Drawing.Size(220, 22)
        Me.ReturnCardsToPlayToolStripMenuItem2.Text = "Return Cards to Play"
        '
        'ReturnCardsToPreGameToolStripMenuItem
        '
        Me.ReturnCardsToPreGameToolStripMenuItem.Name = "ReturnCardsToPreGameToolStripMenuItem"
        Me.ReturnCardsToPreGameToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.ReturnCardsToPreGameToolStripMenuItem.Text = "Return Cards to Pre-Game"
        '
        'RemoveCardsPermanentlyToolStripMenuItem2
        '
        Me.RemoveCardsPermanentlyToolStripMenuItem2.Name = "RemoveCardsPermanentlyToolStripMenuItem2"
        Me.RemoveCardsPermanentlyToolStripMenuItem2.Size = New System.Drawing.Size(220, 22)
        Me.RemoveCardsPermanentlyToolStripMenuItem2.Text = "Remove Cards Permanently"
        '
        'GroupOpponentPreGame
        '
        Me.GroupOpponentPreGame.Controls.Add(Me.ListOpponentPreGame)
        Me.GroupOpponentPreGame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupOpponentPreGame.Location = New System.Drawing.Point(0, 0)
        Me.GroupOpponentPreGame.Name = "GroupOpponentPreGame"
        Me.GroupOpponentPreGame.Size = New System.Drawing.Size(149, 234)
        Me.GroupOpponentPreGame.TabIndex = 0
        Me.GroupOpponentPreGame.TabStop = False
        Me.GroupOpponentPreGame.Text = "Pre-Game"
        '
        'ListOpponentPreGame
        '
        Me.ListOpponentPreGame.AllowDrop = True
        Me.ListOpponentPreGame.ContextMenuStrip = Me.ContextMenuStripOpponentPreGame
        Me.ListOpponentPreGame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListOpponentPreGame.FullRowSelect = True
        Me.ListOpponentPreGame.LargeImageList = Me.ImageListCards
        Me.ListOpponentPreGame.Location = New System.Drawing.Point(3, 16)
        Me.ListOpponentPreGame.Name = "ListOpponentPreGame"
        Me.ListOpponentPreGame.ShowItemToolTips = True
        Me.ListOpponentPreGame.Size = New System.Drawing.Size(143, 215)
        Me.ListOpponentPreGame.TabIndex = 2
        Me.ListOpponentPreGame.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripOpponentPreGame
        '
        Me.ContextMenuStripOpponentPreGame.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlayCardsToolStripMenuItem2, Me.RemoveCardsFromTheGameToolStripMenuItem3, Me.RemoveCardsPermanentlyToolStripMenuItem3})
        Me.ContextMenuStripOpponentPreGame.Name = "ContextMenuStripOpponentPreGame"
        Me.ContextMenuStripOpponentPreGame.Size = New System.Drawing.Size(236, 70)
        '
        'PlayCardsToolStripMenuItem2
        '
        Me.PlayCardsToolStripMenuItem2.Name = "PlayCardsToolStripMenuItem2"
        Me.PlayCardsToolStripMenuItem2.Size = New System.Drawing.Size(235, 22)
        Me.PlayCardsToolStripMenuItem2.Text = "Play Cards"
        '
        'RemoveCardsFromTheGameToolStripMenuItem3
        '
        Me.RemoveCardsFromTheGameToolStripMenuItem3.Name = "RemoveCardsFromTheGameToolStripMenuItem3"
        Me.RemoveCardsFromTheGameToolStripMenuItem3.Size = New System.Drawing.Size(235, 22)
        Me.RemoveCardsFromTheGameToolStripMenuItem3.Text = "Remove Cards From the Game"
        '
        'RemoveCardsPermanentlyToolStripMenuItem3
        '
        Me.RemoveCardsPermanentlyToolStripMenuItem3.Name = "RemoveCardsPermanentlyToolStripMenuItem3"
        Me.RemoveCardsPermanentlyToolStripMenuItem3.Size = New System.Drawing.Size(235, 22)
        Me.RemoveCardsPermanentlyToolStripMenuItem3.Text = "Remove Cards Permanently"
        '
        'GroupPlayer
        '
        Me.GroupPlayer.Controls.Add(Me.SplitContainer2)
        Me.GroupPlayer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupPlayer.Location = New System.Drawing.Point(0, 0)
        Me.GroupPlayer.Name = "GroupPlayer"
        Me.GroupPlayer.Size = New System.Drawing.Size(444, 253)
        Me.GroupPlayer.TabIndex = 4
        Me.GroupPlayer.TabStop = False
        Me.GroupPlayer.Text = "Your Cards"
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(3, 16)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.TabControl2)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupPlayerDeck)
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupPlayerHand)
        Me.SplitContainer2.Size = New System.Drawing.Size(438, 234)
        Me.SplitContainer2.SplitterDistance = 119
        Me.SplitContainer2.TabIndex = 0
        '
        'TabControl2
        '
        Me.TabControl2.AllowDrop = True
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Controls.Add(Me.TabPage6)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl2.Location = New System.Drawing.Point(0, 0)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(434, 115)
        Me.TabControl2.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.ListPlayerInPlay)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(426, 89)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "In Play"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'ListPlayerInPlay
        '
        Me.ListPlayerInPlay.AllowDrop = True
        Me.ListPlayerInPlay.ContextMenuStrip = Me.ContextMenuStripPlayerInPlay
        Me.ListPlayerInPlay.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListPlayerInPlay.FullRowSelect = True
        Me.ListPlayerInPlay.LargeImageList = Me.ImageListCards
        Me.ListPlayerInPlay.Location = New System.Drawing.Point(3, 3)
        Me.ListPlayerInPlay.Name = "ListPlayerInPlay"
        Me.ListPlayerInPlay.ShowItemToolTips = True
        Me.ListPlayerInPlay.Size = New System.Drawing.Size(420, 83)
        Me.ListPlayerInPlay.TabIndex = 2
        Me.ListPlayerInPlay.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripPlayerInPlay
        '
        Me.ContextMenuStripPlayerInPlay.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiscardCardsToolStripMenuItem1, Me.RemoveCardsFromGameToolStripMenuItem, Me.ReturnCardsToHandToolStripMenuItem, Me.StealCardToolStripMenuItem})
        Me.ContextMenuStripPlayerInPlay.Name = "ContextMenuStripPlayerInPlay"
        Me.ContextMenuStripPlayerInPlay.Size = New System.Drawing.Size(216, 92)
        '
        'DiscardCardsToolStripMenuItem1
        '
        Me.DiscardCardsToolStripMenuItem1.Name = "DiscardCardsToolStripMenuItem1"
        Me.DiscardCardsToolStripMenuItem1.Size = New System.Drawing.Size(215, 22)
        Me.DiscardCardsToolStripMenuItem1.Text = "Discard Cards"
        '
        'RemoveCardsFromGameToolStripMenuItem
        '
        Me.RemoveCardsFromGameToolStripMenuItem.Name = "RemoveCardsFromGameToolStripMenuItem"
        Me.RemoveCardsFromGameToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.RemoveCardsFromGameToolStripMenuItem.Text = "Remove Cards From Game"
        '
        'ReturnCardsToHandToolStripMenuItem
        '
        Me.ReturnCardsToHandToolStripMenuItem.Name = "ReturnCardsToHandToolStripMenuItem"
        Me.ReturnCardsToHandToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.ReturnCardsToHandToolStripMenuItem.Text = "Return Cards to Hand"
        '
        'StealCardToolStripMenuItem
        '
        Me.StealCardToolStripMenuItem.Name = "StealCardToolStripMenuItem"
        Me.StealCardToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.StealCardToolStripMenuItem.Text = "Steal Card"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.ListPlayerDiscard)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(426, 89)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Discard"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'ListPlayerDiscard
        '
        Me.ListPlayerDiscard.AllowDrop = True
        Me.ListPlayerDiscard.ContextMenuStrip = Me.ContextMenuStripPlayerDiscard
        Me.ListPlayerDiscard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListPlayerDiscard.FullRowSelect = True
        Me.ListPlayerDiscard.LargeImageList = Me.ImageListCards
        Me.ListPlayerDiscard.Location = New System.Drawing.Point(3, 3)
        Me.ListPlayerDiscard.Name = "ListPlayerDiscard"
        Me.ListPlayerDiscard.ShowItemToolTips = True
        Me.ListPlayerDiscard.Size = New System.Drawing.Size(420, 83)
        Me.ListPlayerDiscard.TabIndex = 2
        Me.ListPlayerDiscard.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripPlayerDiscard
        '
        Me.ContextMenuStripPlayerDiscard.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReturnCardsToPlayToolStripMenuItem, Me.ReturnCardsToHandToolStripMenuItem1, Me.RemoveCardsFromGameToolStripMenuItem1, Me.PlaceCardsOnTopOfDeckToolStripMenuItem, Me.PlaceCardsOnBottomOfDeckToolStripMenuItem})
        Me.ContextMenuStripPlayerDiscard.Name = "ContextMenuStripPlayerDiscard"
        Me.ContextMenuStripPlayerDiscard.Size = New System.Drawing.Size(239, 114)
        '
        'ReturnCardsToPlayToolStripMenuItem
        '
        Me.ReturnCardsToPlayToolStripMenuItem.Name = "ReturnCardsToPlayToolStripMenuItem"
        Me.ReturnCardsToPlayToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.ReturnCardsToPlayToolStripMenuItem.Text = "Return Cards to Play"
        '
        'ReturnCardsToHandToolStripMenuItem1
        '
        Me.ReturnCardsToHandToolStripMenuItem1.Name = "ReturnCardsToHandToolStripMenuItem1"
        Me.ReturnCardsToHandToolStripMenuItem1.Size = New System.Drawing.Size(238, 22)
        Me.ReturnCardsToHandToolStripMenuItem1.Text = "Return Cards to Hand"
        '
        'RemoveCardsFromGameToolStripMenuItem1
        '
        Me.RemoveCardsFromGameToolStripMenuItem1.Name = "RemoveCardsFromGameToolStripMenuItem1"
        Me.RemoveCardsFromGameToolStripMenuItem1.Size = New System.Drawing.Size(238, 22)
        Me.RemoveCardsFromGameToolStripMenuItem1.Text = "Remove Cards From Game"
        '
        'PlaceCardsOnTopOfDeckToolStripMenuItem
        '
        Me.PlaceCardsOnTopOfDeckToolStripMenuItem.Name = "PlaceCardsOnTopOfDeckToolStripMenuItem"
        Me.PlaceCardsOnTopOfDeckToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.PlaceCardsOnTopOfDeckToolStripMenuItem.Text = "Place Cards on top of Deck"
        '
        'PlaceCardsOnBottomOfDeckToolStripMenuItem
        '
        Me.PlaceCardsOnBottomOfDeckToolStripMenuItem.Name = "PlaceCardsOnBottomOfDeckToolStripMenuItem"
        Me.PlaceCardsOnBottomOfDeckToolStripMenuItem.Size = New System.Drawing.Size(238, 22)
        Me.PlaceCardsOnBottomOfDeckToolStripMenuItem.Text = "Place Cards on Bottom of Deck"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.ListPlayerRemovedFromGame)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(426, 89)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "X-ed From Game"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'ListPlayerRemovedFromGame
        '
        Me.ListPlayerRemovedFromGame.AllowDrop = True
        Me.ListPlayerRemovedFromGame.ContextMenuStrip = Me.ContextMenuStripPlayerXedFromGame
        Me.ListPlayerRemovedFromGame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListPlayerRemovedFromGame.FullRowSelect = True
        Me.ListPlayerRemovedFromGame.LargeImageList = Me.ImageListCards
        Me.ListPlayerRemovedFromGame.Location = New System.Drawing.Point(0, 0)
        Me.ListPlayerRemovedFromGame.Name = "ListPlayerRemovedFromGame"
        Me.ListPlayerRemovedFromGame.ShowItemToolTips = True
        Me.ListPlayerRemovedFromGame.Size = New System.Drawing.Size(426, 89)
        Me.ListPlayerRemovedFromGame.TabIndex = 2
        Me.ListPlayerRemovedFromGame.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripPlayerXedFromGame
        '
        Me.ContextMenuStripPlayerXedFromGame.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReturnToPlayToolStripMenuItem, Me.ReturnToPreGameToolStripMenuItem})
        Me.ContextMenuStripPlayerXedFromGame.Name = "ContextMenuStripPlayerXedFromGame"
        Me.ContextMenuStripPlayerXedFromGame.Size = New System.Drawing.Size(183, 48)
        '
        'ReturnToPlayToolStripMenuItem
        '
        Me.ReturnToPlayToolStripMenuItem.Name = "ReturnToPlayToolStripMenuItem"
        Me.ReturnToPlayToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ReturnToPlayToolStripMenuItem.Text = "Return To Play"
        '
        'ReturnToPreGameToolStripMenuItem
        '
        Me.ReturnToPreGameToolStripMenuItem.Name = "ReturnToPreGameToolStripMenuItem"
        Me.ReturnToPreGameToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ReturnToPreGameToolStripMenuItem.Text = "Return To Pre-Game"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.ListPlayerPreGame)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(426, 89)
        Me.TabPage6.TabIndex = 3
        Me.TabPage6.Text = "Pre-Game"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'ListPlayerPreGame
        '
        Me.ListPlayerPreGame.AllowDrop = True
        Me.ListPlayerPreGame.ContextMenuStrip = Me.ContextMenuStripPlayerPreGame
        Me.ListPlayerPreGame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListPlayerPreGame.FullRowSelect = True
        Me.ListPlayerPreGame.LargeImageList = Me.ImageListCards
        Me.ListPlayerPreGame.Location = New System.Drawing.Point(0, 0)
        Me.ListPlayerPreGame.Name = "ListPlayerPreGame"
        Me.ListPlayerPreGame.ShowItemToolTips = True
        Me.ListPlayerPreGame.Size = New System.Drawing.Size(426, 89)
        Me.ListPlayerPreGame.TabIndex = 2
        Me.ListPlayerPreGame.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripPlayerPreGame
        '
        Me.ContextMenuStripPlayerPreGame.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlayCardsToolStripMenuItem1, Me.RemoveCardsFromTheGameToolStripMenuItem})
        Me.ContextMenuStripPlayerPreGame.Name = "ContextMenuStripPlayerPreGame"
        Me.ContextMenuStripPlayerPreGame.Size = New System.Drawing.Size(236, 48)
        '
        'PlayCardsToolStripMenuItem1
        '
        Me.PlayCardsToolStripMenuItem1.Name = "PlayCardsToolStripMenuItem1"
        Me.PlayCardsToolStripMenuItem1.Size = New System.Drawing.Size(235, 22)
        Me.PlayCardsToolStripMenuItem1.Text = "Play Cards"
        '
        'RemoveCardsFromTheGameToolStripMenuItem
        '
        Me.RemoveCardsFromTheGameToolStripMenuItem.Name = "RemoveCardsFromTheGameToolStripMenuItem"
        Me.RemoveCardsFromTheGameToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.RemoveCardsFromTheGameToolStripMenuItem.Text = "Remove Cards From the Game"
        '
        'GroupPlayerDeck
        '
        Me.GroupPlayerDeck.Controls.Add(Me.PictureDeck)
        Me.GroupPlayerDeck.Dock = System.Windows.Forms.DockStyle.Right
        Me.GroupPlayerDeck.Location = New System.Drawing.Point(348, 0)
        Me.GroupPlayerDeck.Name = "GroupPlayerDeck"
        Me.GroupPlayerDeck.Size = New System.Drawing.Size(86, 107)
        Me.GroupPlayerDeck.TabIndex = 2
        Me.GroupPlayerDeck.TabStop = False
        Me.GroupPlayerDeck.Text = "Deck (0)"
        '
        'PictureDeck
        '
        Me.PictureDeck.ContextMenuStrip = Me.ContextMenuStripDeck
        Me.PictureDeck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureDeck.Image = Global.Highlander.My.Resources.Resources.empty
        Me.PictureDeck.Location = New System.Drawing.Point(3, 16)
        Me.PictureDeck.Name = "PictureDeck"
        Me.PictureDeck.Size = New System.Drawing.Size(80, 88)
        Me.PictureDeck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureDeck.TabIndex = 0
        Me.PictureDeck.TabStop = False
        '
        'ContextMenuStripDeck
        '
        Me.ContextMenuStripDeck.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DrawUpToAbilityToolStripMenuItem, Me.ShuffleDeckToolStripMenuItem, Me.ShuffleDiscardIntoDeckToolStripMenuItem, Me.MakeExertionToolStripMenuItem, Me.LookAtTopXCardsToolStripMenuItem, Me.LookAtDeckToolStripMenuItem})
        Me.ContextMenuStripDeck.Name = "ContextMenuStripDeck"
        Me.ContextMenuStripDeck.Size = New System.Drawing.Size(207, 136)
        '
        'DrawUpToAbilityToolStripMenuItem
        '
        Me.DrawUpToAbilityToolStripMenuItem.Name = "DrawUpToAbilityToolStripMenuItem"
        Me.DrawUpToAbilityToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.DrawUpToAbilityToolStripMenuItem.Text = "Draw Up to Ability"
        '
        'ShuffleDeckToolStripMenuItem
        '
        Me.ShuffleDeckToolStripMenuItem.Name = "ShuffleDeckToolStripMenuItem"
        Me.ShuffleDeckToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.ShuffleDeckToolStripMenuItem.Text = "Shuffle Deck"
        '
        'ShuffleDiscardIntoDeckToolStripMenuItem
        '
        Me.ShuffleDiscardIntoDeckToolStripMenuItem.Name = "ShuffleDiscardIntoDeckToolStripMenuItem"
        Me.ShuffleDiscardIntoDeckToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.ShuffleDiscardIntoDeckToolStripMenuItem.Text = "Shuffle Discard into Deck"
        '
        'MakeExertionToolStripMenuItem
        '
        Me.MakeExertionToolStripMenuItem.Name = "MakeExertionToolStripMenuItem"
        Me.MakeExertionToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.MakeExertionToolStripMenuItem.Text = "Make Exertion"
        '
        'LookAtTopXCardsToolStripMenuItem
        '
        Me.LookAtTopXCardsToolStripMenuItem.Name = "LookAtTopXCardsToolStripMenuItem"
        Me.LookAtTopXCardsToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.LookAtTopXCardsToolStripMenuItem.Text = "Look at Top X Cards"
        '
        'LookAtDeckToolStripMenuItem
        '
        Me.LookAtDeckToolStripMenuItem.Name = "LookAtDeckToolStripMenuItem"
        Me.LookAtDeckToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.LookAtDeckToolStripMenuItem.Text = "Look at Deck"
        '
        'GroupPlayerHand
        '
        Me.GroupPlayerHand.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPlayerHand.Controls.Add(Me.ListPlayerHand)
        Me.GroupPlayerHand.Location = New System.Drawing.Point(0, 0)
        Me.GroupPlayerHand.Name = "GroupPlayerHand"
        Me.GroupPlayerHand.Size = New System.Drawing.Size(345, 107)
        Me.GroupPlayerHand.TabIndex = 1
        Me.GroupPlayerHand.TabStop = False
        Me.GroupPlayerHand.Text = "Your Hand"
        '
        'ListPlayerHand
        '
        Me.ListPlayerHand.AllowDrop = True
        Me.ListPlayerHand.ContextMenuStrip = Me.ContextMenuStripHand
        Me.ListPlayerHand.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListPlayerHand.FullRowSelect = True
        Me.ListPlayerHand.LargeImageList = Me.ImageListCards
        Me.ListPlayerHand.Location = New System.Drawing.Point(3, 16)
        Me.ListPlayerHand.Name = "ListPlayerHand"
        Me.ListPlayerHand.ShowItemToolTips = True
        Me.ListPlayerHand.Size = New System.Drawing.Size(339, 88)
        Me.ListPlayerHand.TabIndex = 2
        Me.ListPlayerHand.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripHand
        '
        Me.ContextMenuStripHand.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlayCardsToolStripMenuItem, Me.DiscardCardsToolStripMenuItem, Me.RemoveFromGameToolStripMenuItem, Me.DiscardToTopOfDeckToolStripMenuItem, Me.DiscardToBottomOfDeckToolStripMenuItem, Me.PlayUnderneathToolStripMenuItem})
        Me.ContextMenuStripHand.Name = "ContextMenuStripHand"
        Me.ContextMenuStripHand.Size = New System.Drawing.Size(214, 136)
        '
        'PlayCardsToolStripMenuItem
        '
        Me.PlayCardsToolStripMenuItem.Name = "PlayCardsToolStripMenuItem"
        Me.PlayCardsToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.PlayCardsToolStripMenuItem.Text = "Play Cards"
        '
        'DiscardCardsToolStripMenuItem
        '
        Me.DiscardCardsToolStripMenuItem.Name = "DiscardCardsToolStripMenuItem"
        Me.DiscardCardsToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.DiscardCardsToolStripMenuItem.Text = "Discard Cards"
        '
        'RemoveFromGameToolStripMenuItem
        '
        Me.RemoveFromGameToolStripMenuItem.Name = "RemoveFromGameToolStripMenuItem"
        Me.RemoveFromGameToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.RemoveFromGameToolStripMenuItem.Text = "Remove From Game"
        '
        'DiscardToTopOfDeckToolStripMenuItem
        '
        Me.DiscardToTopOfDeckToolStripMenuItem.Name = "DiscardToTopOfDeckToolStripMenuItem"
        Me.DiscardToTopOfDeckToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.DiscardToTopOfDeckToolStripMenuItem.Text = "Discard to Top of Deck"
        '
        'DiscardToBottomOfDeckToolStripMenuItem
        '
        Me.DiscardToBottomOfDeckToolStripMenuItem.Name = "DiscardToBottomOfDeckToolStripMenuItem"
        Me.DiscardToBottomOfDeckToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.DiscardToBottomOfDeckToolStripMenuItem.Text = "Discard to Bottom of Deck"
        '
        'PlayUnderneathToolStripMenuItem
        '
        Me.PlayUnderneathToolStripMenuItem.Enabled = False
        Me.PlayUnderneathToolStripMenuItem.Name = "PlayUnderneathToolStripMenuItem"
        Me.PlayUnderneathToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.PlayUnderneathToolStripMenuItem.Text = "Play Underneath"
        '
        'SplitContainer3
        '
        Me.SplitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.GroupBoxOpponentPlayCards)
        Me.SplitContainer3.Panel1.Controls.Add(Me.groupSearch)
        Me.SplitContainer3.Panel1.Controls.Add(Me.ButtonCardDetails)
        Me.SplitContainer3.Panel1.Controls.Add(Me.CardsAllCards)
        Me.SplitContainer3.Panel1.Controls.Add(Me.CurrentCardImage)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.SplitContainer4)
        Me.SplitContainer3.Size = New System.Drawing.Size(677, 522)
        Me.SplitContainer3.SplitterDistance = 225
        Me.SplitContainer3.TabIndex = 5
        '
        'GroupBoxOpponentPlayCards
        '
        Me.GroupBoxOpponentPlayCards.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxOpponentPlayCards.Controls.Add(Me.ButtonOpponentAddCards)
        Me.GroupBoxOpponentPlayCards.Controls.Add(Me.ComboBoxAddCardsAction)
        Me.GroupBoxOpponentPlayCards.Location = New System.Drawing.Point(1, 185)
        Me.GroupBoxOpponentPlayCards.Name = "GroupBoxOpponentPlayCards"
        Me.GroupBoxOpponentPlayCards.Size = New System.Drawing.Size(220, 41)
        Me.GroupBoxOpponentPlayCards.TabIndex = 59
        Me.GroupBoxOpponentPlayCards.TabStop = False
        Me.GroupBoxOpponentPlayCards.Text = "Add Selected Cards To"
        '
        'ButtonOpponentAddCards
        '
        Me.ButtonOpponentAddCards.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonOpponentAddCards.Location = New System.Drawing.Point(174, 13)
        Me.ButtonOpponentAddCards.Name = "ButtonOpponentAddCards"
        Me.ButtonOpponentAddCards.Size = New System.Drawing.Size(43, 23)
        Me.ButtonOpponentAddCards.TabIndex = 1
        Me.ButtonOpponentAddCards.Text = "Add"
        Me.ButtonOpponentAddCards.UseVisualStyleBackColor = True
        '
        'ComboBoxAddCardsAction
        '
        Me.ComboBoxAddCardsAction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBoxAddCardsAction.FormattingEnabled = True
        Me.ComboBoxAddCardsAction.Items.AddRange(New Object() {"Opponent's In Play", "Opponent's Discard", "Opponent's X-ed From Game", "Opponent's Pre-Game", "Your In Play", "Your Discard", "Your X-ed From Game", "Your Pre-Game", "Your Hand"})
        Me.ComboBoxAddCardsAction.Location = New System.Drawing.Point(2, 15)
        Me.ComboBoxAddCardsAction.Name = "ComboBoxAddCardsAction"
        Me.ComboBoxAddCardsAction.Size = New System.Drawing.Size(166, 21)
        Me.ComboBoxAddCardsAction.TabIndex = 0
        '
        'groupSearch
        '
        Me.groupSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupSearch.Controls.Add(Me.comboPublisher)
        Me.groupSearch.Controls.Add(Me.lblPublisher)
        Me.groupSearch.Controls.Add(Me.txtKeywordSearch)
        Me.groupSearch.Controls.Add(Me.lblKeyword)
        Me.groupSearch.Controls.Add(Me.lblSet)
        Me.groupSearch.Controls.Add(Me.comboSets)
        Me.groupSearch.Location = New System.Drawing.Point(1, 232)
        Me.groupSearch.Name = "groupSearch"
        Me.groupSearch.Size = New System.Drawing.Size(220, 98)
        Me.groupSearch.TabIndex = 58
        Me.groupSearch.TabStop = False
        Me.groupSearch.Text = "Filter"
        '
        'comboPublisher
        '
        Me.comboPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboPublisher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPublisher.FormattingEnabled = True
        Me.comboPublisher.Location = New System.Drawing.Point(96, 17)
        Me.comboPublisher.Name = "comboPublisher"
        Me.comboPublisher.Size = New System.Drawing.Size(116, 21)
        Me.comboPublisher.TabIndex = 30
        '
        'lblPublisher
        '
        Me.lblPublisher.AutoSize = True
        Me.lblPublisher.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPublisher.Location = New System.Drawing.Point(10, 16)
        Me.lblPublisher.Name = "lblPublisher"
        Me.lblPublisher.Size = New System.Drawing.Size(73, 18)
        Me.lblPublisher.TabIndex = 29
        Me.lblPublisher.Text = "Publisher:"
        '
        'txtKeywordSearch
        '
        Me.txtKeywordSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeywordSearch.Location = New System.Drawing.Point(96, 71)
        Me.txtKeywordSearch.Name = "txtKeywordSearch"
        Me.txtKeywordSearch.Size = New System.Drawing.Size(116, 20)
        Me.txtKeywordSearch.TabIndex = 26
        '
        'lblKeyword
        '
        Me.lblKeyword.AutoSize = True
        Me.lblKeyword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKeyword.Location = New System.Drawing.Point(8, 71)
        Me.lblKeyword.Name = "lblKeyword"
        Me.lblKeyword.Size = New System.Drawing.Size(70, 18)
        Me.lblKeyword.TabIndex = 25
        Me.lblKeyword.Text = "Keyword:"
        '
        'lblSet
        '
        Me.lblSet.AutoSize = True
        Me.lblSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSet.Location = New System.Drawing.Point(8, 44)
        Me.lblSet.Name = "lblSet"
        Me.lblSet.Size = New System.Drawing.Size(34, 18)
        Me.lblSet.TabIndex = 24
        Me.lblSet.Text = "Set:"
        '
        'comboSets
        '
        Me.comboSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSets.Location = New System.Drawing.Point(96, 44)
        Me.comboSets.Name = "comboSets"
        Me.comboSets.Size = New System.Drawing.Size(116, 21)
        Me.comboSets.TabIndex = 23
        '
        'ButtonCardDetails
        '
        Me.ButtonCardDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonCardDetails.Location = New System.Drawing.Point(69, 492)
        Me.ButtonCardDetails.Name = "ButtonCardDetails"
        Me.ButtonCardDetails.Size = New System.Drawing.Size(75, 23)
        Me.ButtonCardDetails.TabIndex = 3
        Me.ButtonCardDetails.Text = "Details..."
        Me.ButtonCardDetails.UseVisualStyleBackColor = True
        '
        'CardsAllCards
        '
        Me.CardsAllCards.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CardsAllCards.Location = New System.Drawing.Point(0, 0)
        Me.CardsAllCards.Name = "CardsAllCards"
        Me.CardsAllCards.Size = New System.Drawing.Size(221, 179)
        Me.CardsAllCards.TabIndex = 1
        '
        'CurrentCardImage
        '
        Me.CurrentCardImage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CurrentCardImage.Location = New System.Drawing.Point(1, 336)
        Me.CurrentCardImage.Name = "CurrentCardImage"
        Me.CurrentCardImage.Size = New System.Drawing.Size(217, 152)
        Me.CurrentCardImage.TabIndex = 2
        '
        'SplitContainer4
        '
        Me.SplitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer4.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer4.Name = "SplitContainer4"
        Me.SplitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.Controls.Add(Me.GroupOpponent)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.Controls.Add(Me.GroupPlayer)
        Me.SplitContainer4.Size = New System.Drawing.Size(448, 522)
        Me.SplitContainer4.SplitterDistance = 261
        Me.SplitContainer4.TabIndex = 6
        '
        'SplitContainer5
        '
        Me.SplitContainer5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer5.Location = New System.Drawing.Point(0, 40)
        Me.SplitContainer5.Name = "SplitContainer5"
        '
        'SplitContainer5.Panel1
        '
        Me.SplitContainer5.Panel1.Controls.Add(Me.SplitContainer3)
        '
        'SplitContainer5.Panel2
        '
        Me.SplitContainer5.Panel2.Controls.Add(Me.ListPlayerDeck)
        Me.SplitContainer5.Panel2.Controls.Add(Me.txtChatText)
        Me.SplitContainer5.Panel2.Controls.Add(Me.ButtonSend)
        Me.SplitContainer5.Panel2.Controls.Add(Me.txtLogBox)
        Me.SplitContainer5.Size = New System.Drawing.Size(784, 522)
        Me.SplitContainer5.SplitterDistance = 677
        Me.SplitContainer5.TabIndex = 6
        '
        'ListPlayerDeck
        '
        Me.ListPlayerDeck.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListPlayerDeck.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnRandomVal, Me.ColumnID, Me.ColumnDisplayName})
        Me.ListPlayerDeck.Location = New System.Drawing.Point(3, 303)
        Me.ListPlayerDeck.Name = "ListPlayerDeck"
        Me.ListPlayerDeck.Size = New System.Drawing.Size(92, 97)
        Me.ListPlayerDeck.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.ListPlayerDeck.TabIndex = 3
        Me.ListPlayerDeck.UseCompatibleStateImageBehavior = False
        Me.ListPlayerDeck.View = System.Windows.Forms.View.Details
        '
        'ColumnRandomVal
        '
        Me.ColumnRandomVal.Text = "Rand"
        '
        'ColumnID
        '
        Me.ColumnID.Text = "ID"
        '
        'ColumnDisplayName
        '
        Me.ColumnDisplayName.Text = "Name"
        '
        'txtChatText
        '
        Me.txtChatText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtChatText.Location = New System.Drawing.Point(3, 494)
        Me.txtChatText.Name = "txtChatText"
        Me.txtChatText.Size = New System.Drawing.Size(49, 20)
        Me.txtChatText.TabIndex = 2
        '
        'ButtonSend
        '
        Me.ButtonSend.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonSend.Location = New System.Drawing.Point(58, 492)
        Me.ButtonSend.Name = "ButtonSend"
        Me.ButtonSend.Size = New System.Drawing.Size(42, 23)
        Me.ButtonSend.TabIndex = 1
        Me.ButtonSend.Text = "Send"
        Me.ButtonSend.UseVisualStyleBackColor = True
        '
        'txtLogBox
        '
        Me.txtLogBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLogBox.Location = New System.Drawing.Point(0, 0)
        Me.txtLogBox.Multiline = True
        Me.txtLogBox.Name = "txtLogBox"
        Me.txtLogBox.ReadOnly = True
        Me.txtLogBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLogBox.Size = New System.Drawing.Size(100, 486)
        Me.txtLogBox.TabIndex = 0
        '
        'frmPlayGame2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.SplitContainer5)
        Me.Controls.Add(Me.GroupGameStatus)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPlayGame2"
        Me.Text = "Play Game"
        Me.GroupGameStatus.ResumeLayout(False)
        Me.GroupGameStatus.PerformLayout()
        Me.GroupOpponent.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.ContextMenuStripOpponentInPlay.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.ContextMenuStripOpponentDiscard.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.ContextMenuStripOpponentXedFromGame.ResumeLayout(False)
        Me.GroupOpponentPreGame.ResumeLayout(False)
        Me.ContextMenuStripOpponentPreGame.ResumeLayout(False)
        Me.GroupPlayer.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.ContextMenuStripPlayerInPlay.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.ContextMenuStripPlayerDiscard.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.ContextMenuStripPlayerXedFromGame.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.ContextMenuStripPlayerPreGame.ResumeLayout(False)
        Me.GroupPlayerDeck.ResumeLayout(False)
        CType(Me.PictureDeck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStripDeck.ResumeLayout(False)
        Me.GroupPlayerHand.ResumeLayout(False)
        Me.ContextMenuStripHand.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        Me.GroupBoxOpponentPlayCards.ResumeLayout(False)
        Me.groupSearch.ResumeLayout(False)
        Me.groupSearch.PerformLayout()
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        Me.SplitContainer4.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer4.ResumeLayout(False)
        Me.SplitContainer5.Panel1.ResumeLayout(False)
        Me.SplitContainer5.Panel2.ResumeLayout(False)
        Me.SplitContainer5.Panel2.PerformLayout()
        CType(Me.SplitContainer5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupGameStatus As System.Windows.Forms.GroupBox
    Friend WithEvents lblOpponentAbility As System.Windows.Forms.Label
    Friend WithEvents lblOpponentTurnNum As System.Windows.Forms.Label
    Friend WithEvents txtOpponentAbility As System.Windows.Forms.TextBox
    Friend WithEvents txtPlayerAbility As System.Windows.Forms.TextBox
    Friend WithEvents txtOpponentTurnNum As System.Windows.Forms.TextBox
    Friend WithEvents txtPlayerTurnNum As System.Windows.Forms.TextBox
    Friend WithEvents lblPlayerAbility As System.Windows.Forms.Label
    Friend WithEvents lblPlayerTurnNum As System.Windows.Forms.Label
    Friend WithEvents CardsAllCards As Highlander.cardsControl
    Friend WithEvents CurrentCardImage As Highlander.cardImageFB
    Friend WithEvents GroupOpponent As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupPlayer As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer5 As System.Windows.Forms.SplitContainer
    Friend WithEvents txtLogBox As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupPlayerHand As System.Windows.Forms.GroupBox
    Friend WithEvents PictureDeck As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents GroupOpponentPreGame As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents ImageListCards As System.Windows.Forms.ImageList
    Friend WithEvents ListOpponentInPlay As System.Windows.Forms.ListView
    Friend WithEvents ListOpponentDiscard As System.Windows.Forms.ListView
    Friend WithEvents ListOpponentRemovedFromGame As System.Windows.Forms.ListView
    Friend WithEvents ListOpponentPreGame As System.Windows.Forms.ListView
    Friend WithEvents ListPlayerInPlay As System.Windows.Forms.ListView
    Friend WithEvents ListPlayerDiscard As System.Windows.Forms.ListView
    Friend WithEvents ListPlayerRemovedFromGame As System.Windows.Forms.ListView
    Friend WithEvents ListPlayerPreGame As System.Windows.Forms.ListView
    Friend WithEvents ListPlayerHand As System.Windows.Forms.ListView
    Friend WithEvents ButtonCardDetails As System.Windows.Forms.Button
    Friend WithEvents ComboBoxPhase As System.Windows.Forms.ComboBox
    Friend WithEvents txtOpponentName As System.Windows.Forms.TextBox
    Friend WithEvents lblOpponentName As System.Windows.Forms.Label
    Friend WithEvents txtChatText As System.Windows.Forms.TextBox
    Friend WithEvents ButtonSend As System.Windows.Forms.Button
    Friend WithEvents txtPlayerName As System.Windows.Forms.TextBox
    Friend WithEvents lblPlayerName As System.Windows.Forms.Label
    Friend WithEvents groupSearch As System.Windows.Forms.GroupBox
    Friend WithEvents comboPublisher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPublisher As System.Windows.Forms.Label
    Friend WithEvents txtKeywordSearch As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyword As System.Windows.Forms.Label
    Friend WithEvents lblSet As System.Windows.Forms.Label
    Friend WithEvents comboSets As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBoxOpponentPlayCards As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonOpponentAddCards As System.Windows.Forms.Button
    Friend WithEvents ComboBoxAddCardsAction As System.Windows.Forms.ComboBox
    Friend WithEvents ListPlayerDeck As System.Windows.Forms.ListView
    Friend WithEvents ColumnRandomVal As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnID As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnDisplayName As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupPlayerDeck As System.Windows.Forms.GroupBox
    Friend WithEvents ContextMenuStripDeck As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DrawUpToAbilityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShuffleDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShuffleDiscardIntoDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MakeExertionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LookAtTopXCardsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LookAtDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripHand As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents PlayCardsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiscardCardsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveFromGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripPlayerInPlay As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DiscardCardsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsFromGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturnCardsToHandToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiscardToTopOfDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiscardToBottomOfDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripPlayerDiscard As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ReturnCardsToPlayToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturnCardsToHandToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsFromGameToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlaceCardsOnTopOfDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlaceCardsOnBottomOfDeckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripPlayerPreGame As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents PlayCardsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsFromTheGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripPlayerXedFromGame As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ReturnToPlayToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturnToPreGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripOpponentInPlay As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DiscardCardsToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsFromTheGameToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsPermanentlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StealCardsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripOpponentDiscard As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ReturnCardsToPlayToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsFromTheGameToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsPermanentlyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveAllCardsPermanentlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripOpponentXedFromGame As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ReturnCardsToPlayToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturnCardsToPreGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsPermanentlyToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripOpponentPreGame As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents PlayCardsToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsFromTheGameToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveCardsPermanentlyToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StealCardToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlayUnderneathToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
