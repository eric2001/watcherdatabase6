﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.DeckRules.Tests

    Public Class DeckImmortalSpecificRulesTests
        <Theory>
        <InlineData("decks\valid\Amanda1EOnlyDeckSimple.hld")>
        Sub TestImmortalSpecificDeck(ByVal FileName As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Theory>
        <InlineData("decks\valid\GregorPowersType2Deck.hld")>
        <InlineData("decks\valid\MichaelKentType2Deck.hld")>
        <InlineData("decks\valid\ConnorMacLeodType2Deck.hld")>
        <InlineData("decks\valid\SlanQuinceType2Deck.hld")>
        <InlineData("decks\valid\StevenKeaneType2Deck.hld")>
        <InlineData("decks\valid\KassimType2Deck.hld")>
        Sub TestType2ImmortalSpecificDeck(ByVal FileName As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImmortalSpecificRestrictionExceededDeck()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckRestrictionExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Amanda Master's Advice").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificRestrictionExceededAlternateVersionDeck()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckRestrictionExceededAlternateVersion.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Amanda Master's Advice").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckWithWrongImmortal()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckWithAnnie.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Annie Devlin Eye for an Eye").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You are not allowed to use cards belonging to Annie Devlin.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckWithInGameDariusAndWrongImmortal()
            Dim FileName As String = "decks\valid\Amanda1EOnlyDeckWithDariusAndAnnie.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckWithInGameDariusAndWrongImmortalSignature()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckWithDariusAndAnnieSignature.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Annie Devlin Flashing Blade").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You are not allowed to use cards belonging to Annie Devlin.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckMasterCardLimit()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckMasterCardLimitExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Master card limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckRequiredBasicBlock()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckSimpleMissingLowerCenterBlock.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must include at least one Lower Center Block in your deck.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckRequiredBasicAttack()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckSimpleMissingLowerCenterAttack.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must include at least one Lower Center Attack in your deck.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificConnorClanMacLeodDeck()
            Dim FileName As String = "decks\valid\Connor1EClanMacLeodDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDuncanClanMacLeodDeck()
            Dim FileName As String = "decks\valid\Duncan1EClanMacLeodDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDeckClanMacLeod()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckClanMacLeod.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Clan MacLeod Clan Priest").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only use Clan MacLeod cards if you are using the Duncan, Connor, Colin or Kate MacLeod Personas.").Count)
        End Sub

        <Theory>
        <InlineData("decks\invalid\Amanda1EOnlyDeckCeirdwynPremium.hld", "Ceirdwyn Premium (+1)", "This card requires the Ceirdwyn Persona Pre-Game.")>
        Sub TestImmortalSpecificDeckWithOtherImmortalPremium(ByVal FileName As String, ByVal BadCardTitle As String, ByVal CardErrorMessage As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = BadCardTitle).Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = CardErrorMessage).Count)
        End Sub

        <Theory>
        <InlineData("decks\invalid\GregorPowersType2DeckWrongPremium.hld", "Michael Kent Premium", "This card requires the Michael Kent Persona Pre-Game.")>
        <InlineData("decks\invalid\MichaelKentType2DeckWrongPremium.hld", "Gregor Powers Premium", "This card requires the Gregor Powers Persona Pre-Game.")>
        <InlineData("decks\invalid\SlanQuinceType2DeckConnorPremium.hld", "Connor MacLeod Premium", "This card requires the Connor MacLeod Persona Pre-Game.")>
        <InlineData("decks\invalid\ConnorMacLeodType2DeckSlanPremium.hld", "Slan Quince Premium", "This card requires the Slan Quince Persona Pre-Game.")>
        <InlineData("decks\invalid\StevenKeaneType2DecKassimPremium.hld", "Kassim Premium", "This card requires the Kassim Persona Pre-Game.")>
        <InlineData("decks\invalid\KassimType2DeckStevenKeanePremium.hld", "Steven Keane Premium", "This card requires the Steven Keane Persona Pre-Game.")>
        Sub TestType2ImmortalSpecificDeckWithOtherImmortalPremium(ByVal FileName As String, ByVal BadCardTitle As String, ByVal CardErrorMessage As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = BadCardTitle).Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = CardErrorMessage).Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificPremiumLimitExceededAlternateVersionDeck()
            Dim FileName As String = "decks\invalid\ConnorMacLeod1EPremiumLimitExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Connor MacLeod Premium (+1)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificConnorTwoPremiumsDeck()
            Dim FileName As String = "decks\valid\ConnorMacLeod1EWithPremiums.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImmortalSpecificFaceOfDeathDeck()
            Dim FileName As String = "decks\invalid\ImanFasilFaceOfDeath.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Iman Fasil The Face of Death").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "This card is banned.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDuncanMCBCDeck()
            Dim FileName As String = "decks\invalid\Duncan1EPremiumMCBC.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Duncan MacLeod Premium (MCBC / Duncan Collection Misprint)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "This card is banned.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDuncanQuickeningDeck()
            Dim FileName As String = "decks\invalid\ConnorMacLeod1EBannedDuncanQuickingMisprint.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quickening Pre-Game (Duncan Collection Misprint)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "This card is banned.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificAttitudeIsEverythingDeck()
            Dim FileName As String = "decks\invalid\ConnorMacLeod1EAttitudeIsEverything.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Generic Attitude Is Everything").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "This card is banned.").Count)
        End Sub

        <Fact>
        Sub TestImmortalSpecificDonnaLettowDeck()
            Dim FileName As String = "decks\invalid\ConnorMacLeod1EDonnaLettow.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(2, result.Errors.Count)
            Assert.Equal(2, result.Errors.Where(Function(e) e.CardTitle = "Generic Donna Lettow").Count)
            Assert.Equal(2, result.Errors.Where(Function(e) e.Message = "This card is banned.").Count)
        End Sub

        <Theory>
        <InlineData("decks\invalid\GregorPowersType2DeckGregorQuickening.hld", "Quickening Pre-Game (Gregor Powers)")>
        <InlineData("decks\invalid\MichaelKentType2DeckMichaelKentQuickening.hld", "Quickening Pre-Game (Michael Kent)")>
        <InlineData("decks\invalid\SlanQuinceType2DeckSlanQuickening.hld", "Quickening Pre-Game (Slan Quince)")>
        <InlineData("decks\invalid\ConnorMacLeodType2DeckConnorQuickening.hld", "Quickening Pre-Game (Connor MacLeod / Hard Exertion)")>
        <InlineData("decks\invalid\StevenKeaneType2DeckOwnQuickening.hld", "Quickening Pre-Game (Steven Keane)")>
        <InlineData("decks\invalid\KassimType2DeckOwnQuickening.hld", "Quickening Pre-Game (Kassim)")>
        Sub TestType2ImmortalSpecificDeckWithOwnQuickening(ByVal FileName As String, ByVal BadCardTitle As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = BadCardTitle).Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You can't use your own Quickening.").Count)
        End Sub

        <Theory>
        <InlineData("decks\valid\MichaelKentType2DeckImmortalSpecificSlash.hld")>
        <InlineData("decks\valid\MichaelKentType2DeckWithEightSlashCards.hld")>
        Sub TestType2ImmortalSpecificDeckCardsFromOtherImmortals(ByVal FileName As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestType2MichaelKentEightSlashCards()
            Dim FileName As String = "decks\valid\MichaelKentType2DeckWithEightSlashCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestType2ConnorMacLeodSlashUpper()
            Dim FileName As String = "decks\valid\MichaelKentType2DeckNoLowerCenterAttack.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestType2ConnorMacLeodSlashUpperMultipleMissingAttacks()
            Dim FileName As String = "decks\invalid\MichaelKentType2DeckNoLowerCenterOrLowerLeftAttack.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must include at least one Lower Center Attack in your deck.").Count)
        End Sub

        <Fact>
        Sub TestType2KassimBodyCardSixCardLimit()
            Dim FileName As String = "decks\invalid\KassimType2DeckBodyGuardTitleExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Body Guard").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Body Guard in your deck.").Count)
        End Sub
    End Class
End Namespace

