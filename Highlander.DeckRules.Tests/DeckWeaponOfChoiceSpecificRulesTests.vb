﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.DeckRules.Tests

    Public Class DeckWeaponOfChoiceSpecificRulesTests

        <Fact>
        Sub TestGenericImmortalDeckWithWeaponOfChoice()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalWithWeaponOfChoiceDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestPersonaDeckWithWeaponOfChoice()
            Dim FileName As String = "decks\valid\Amanda1EOnlyDeckSimple.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestPersonaDeckWithWeaponOfChoiceShield()
            Dim FileName As String = "decks\valid\Amanda1EOnlyTwoWoCShieldDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestPersonaDeckMultipleWeaponOfChoice()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyTwoWoCDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only use one Weapon of Choice Pre-Game.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckWeaponSpecificCards()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalWeaponSpecificCardDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Broad Bladed Spear Spinning Block (Left)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You are not allowed to use cards belonging to Broad Bladed Spear.").Count)
        End Sub

        <Fact>
        Sub TestPersonaDeckWeaponSpecificCards()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckWithBoardBladeSpearCard.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Broad Bladed Spear Quality Blade").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You are not allowed to use cards belonging to Broad Bladed Spear.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckWeaponSpecificQualityBladeCards()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalWithWeaponSpecificQualityBladeDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quality Blade").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Quality Blade in your deck.").Count)
        End Sub

        <Fact>
        Sub TestPersonaDeckWeaponSpecificQualityBladeCards()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckWithWeaponSpecifcQuailityBlade.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quality Blade").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Quality Blade in your deck.").Count)
        End Sub

        <Fact>
        Sub TestPersonaDeckWeaponSpecificSevenSliceCards()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckWithWeaponSpecifcSevenSlice.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Slice").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Slice in your deck.").Count)
        End Sub

        <Fact>
        Sub TestPersonaDeckWeaponSpecificRestrictionExceeded()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckWithWeaponSpecifcRestrictionExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Rapier Swashbuckler").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Theory>
        <InlineData("decks\valid\GregorPowersType2Deck.hld")>
        <InlineData("decks\valid\MichaelKentType2Deck.hld")>
        <InlineData("decks\valid\ConnorMacLeodType2Deck.hld")>
        <InlineData("decks\valid\SlanQuinceType2Deck.hld")>
        <InlineData("decks\valid\KassimType2Deck.hld")>
        Sub TestType2ImmortalSpecificWeaponOfChoice(ByVal FileName As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Theory>
        <InlineData("decks\invalid\GregorPowersType2DeckMichaelKentWoC.hld", "Katana Pre-Game (Muramasa Katana)", "The Muramasa Katana Weapon of Choice can only be used with the Michael Kent Persona.")>
        <InlineData("decks\invalid\MichaelKentType2DeckGregorPowersWoC.hld", "Single-Handed Broadsword Pre-Game (El Cid Tizona)", "The El Cid Tizona Single-Handed Broadsword Weapon of Choice can only be used with the Gregor Powers Persona.")>
        <InlineData("decks\invalid\ConnorMacLeodType2DeckSlanGreatSword.hld", "Great Sword Pre-Game (Slan Quince Great Sword)", "The Great Sword Weapon of Choice can only be used with the Slan Quince Persona.")>
        <InlineData("decks\invalid\SlanQuinceType2DeckDuncanKatana.hld", "Katana Pre-Game (Rosewood Tachi)", "The Katana Weapon of Choice can only be used with the Connor MacLeod Persona.")>
        <InlineData("decks\invalid\StevenKeaneType2DeckWrongWeaponOfChoice.hld", "Scimitar Pre-Game (Kassim's Damascus Scimitar)", "The Scimitar Weapon of Choice can only be used with the Kassim Persona.")>
        Sub TestType2WrongImmortalSpecificWeaponOfChoice(ByVal FileName As String, ByVal BadCardTitle As String, ByVal CardErrorMessage As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = BadCardTitle).Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = CardErrorMessage).Count)
        End Sub

        <Theory>
        <InlineData("decks\invalid\StevenKeaneType2DeckPrussianCavalrySwordNoWoC.hld", "Steven Keane Prussian Cavalry Sword", "Prussian Cavalry Sword requires the Sabre Weapon of Choice.")>
        Sub TestType2InGameWrongWeaponOfChoicePreGame(ByVal FileName As String, ByVal BadCardTitle As String, ByVal CardErrorMessage As String)
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = BadCardTitle).Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = CardErrorMessage).Count)
        End Sub

        <Fact>
        Sub TestType2ArmsAndTacticsPreGameHilt()
            Dim FileName As String = "decks\valid\StevenKeaneType2DeckHiltPreGame.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestType2ArmsAndTacticsPreGameHiltWithOtherPreGameCards()
            Dim FileName As String = "decks\valid\StevenKeaneType2DeckHiltPreGameWithSixPreGameCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestType2ArmsAndTacticsPreGameHiltWrongWoC()
            Dim FileName As String = "decks\invalid\StevenKeaneType2DeckHiltPreGameWrongHilt.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Steven Keane Prussian Cavalry Sword").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Prussian Cavalry Sword requires the Sabre Weapon of Choice.").Count)
        End Sub

        <Fact>
        Sub TestTypeWalterReinhardtBasketHiltSabre()
            Dim FileName As String = "decks\valid\WalterReinhardtType2Deck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestTypeWalterReinhardtBasketHiltSabreNoWoc()
            Dim FileName As String = "decks\invalid\WalterReinhardtType2DeckBasketHiltSabreNoWoC.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Walter Reinhardt Basket Hilt Sabre").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Basket Hilt Sabre requires the Sabre Weapon of Choice.").Count)
        End Sub

        <Fact>
        Sub TestImanFasilSingleHandedBroardsword()
            Dim FileName As String = "decks\valid\ImanFasilType2Deck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestSingleHandedBroardswordWithoutImanFasil()
            Dim FileName As String = "decks\invalid\WalterReinhardtType2DeckSingleHandedBroardsword.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Single-Handed Broadsword Pre-Game (Iman Fasil Toledo Salamanca)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "The Single-Handed Broadsword Weapon of Choice can only be used with the Iman Fasil Persona.").Count)
        End Sub

        <Fact>
        Sub TestImanFasilSingleHandedBroardswordWithSevenPreGameCards()
            Dim FileName As String = "decks\valid\ImanFasilType2DeckSingleHandedBroadswordSevenPreGameCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImanFasilSingleHandedBroardswordWithExtraToughness()
            Dim FileName As String = "decks\valid\ImanFasilType2DeckSingleHandedBroadswordExtraToughness.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestImanFasilSingleHandedBroardswordWithExtraPreGamesExtraToughness()
            Dim FileName As String = "decks\invalid\ImanFasilType2DeckSingleHandedBroadswordSevenPreGameCardsAndExtraToughness.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Toughness Gem Limit Exceeded.").Count)
        End Sub

        <Fact>
        Sub TestShieldPreGameWithOtherWoC()
            Dim FileName As String = "decks\valid\ImanFasilType2DeckWithShield.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestShieldPreGameWithoutOtherWoC()
            Dim FileName As String = "decks\invalid\ImanFasilType2DeckWithShield.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Shield Pre-Game").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must use two Weapon of Choice.").Count)
        End Sub

        <Fact>
        Sub TestWalterReinhardtSabrePreGame()
            Dim FileName As String = "decks\valid\WalterReinhardtType2Deck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestWalterReinhardtSabrePreGameWithoutWalterPersona()
            Dim FileName As String = "decks\invalid\ImanFasilType2DeckWithWalterReinhardtSabre.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Sabre Pre-Game (Walter Reinhardt)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "The Sabre Weapon of Choice can only be used with the Walter Reinhardt Persona.").Count)
        End Sub

    End Class
End Namespace

