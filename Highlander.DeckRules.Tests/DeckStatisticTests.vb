' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.DeckRules.Tests

    Public Class DeckStatisticTests
        <Fact>
        Sub TestBasicTCGDeckStatistics()
            '@TODO:  The deck stats code has in theory been moved over
            '  It needs to be cleaned up, ex: some of it will likely need to be shared with the rules engine and will need to be moved into a separeate class TBD
            '       I think we're good on splitting stuff up now, everything that's left looks like it would be statstics specific, need to clean up code comments / add ''' comments to beginning of functions / properties / etc
            '  But the main entry point should be set up to run some decks through and see what happens
            '  and should be able to populate all of the info from the summary tab on the analyzer.
            '  Need to determine what kind of tests I want to run

            '@TODO:  I should pull together my in progress lists of cards that require special handling and include the hld(s) that are being used to test this handling at some point to confirm everything is covered by automated testing

            Dim statistics As New DeckStatistics()
            Dim deck As Data.HighlanderDeck = Data.HLD.Load("decks\valid\BasicTCGGenericImmortalDeck.hld")
            Dim summary As DeckSummary = statistics.Analyze("decks\valid\BasicTCGGenericImmortalDeck.hld", deck.Deck, deck.PreGame)

            Assert.Equal("BasicTCGGenericImmortalDeck", summary.DeckTitle)
            Assert.Equal("Generic Immortal", summary.Immortal)
            Assert.Equal(0, summary.PersonaId)
            Assert.Equal(50, summary.DeckSize)
            Assert.Equal(0, summary.PreGameSize)
            Assert.Equal(5, summary.CardsByType.Count)
            Assert.Equal(50, summary.CardsByType.Sum(Function(c) c.Quantity))
            Assert.Equal(1, summary.CardsBySubTitle.Count)
            Assert.Equal(50, summary.CardsBySubTitle.Sum(Function(c) c.Quantity))
        End Sub

        <Fact>
        Sub TestImmortalTCGDeckStatistics()
            Dim statistics As New DeckStatistics()
            Dim deck As Data.HighlanderDeck = Data.HLD.Load("decks\valid\Amanda1EOnlyDeckSimple.hld")
            Dim summary As DeckSummary = statistics.Analyze("decks\valid\Amanda1EOnlyDeckSimple.hld", deck.Deck, deck.PreGame)

            Assert.Equal("Amanda1EOnlyDeckSimple", summary.DeckTitle)
            Assert.Equal("Amanda", summary.Immortal)
            Assert.Equal(1337, summary.PersonaId)
            Assert.Equal(60, summary.DeckSize)
            Assert.Equal(4, summary.PreGameSize)
            Assert.Equal(8, summary.CardsByType.Count)
            Assert.Equal(60, summary.CardsByType.Sum(Function(c) c.Quantity))
            Assert.Equal(3, summary.CardsBySubTitle.Count)
            Assert.Equal(60, summary.CardsBySubTitle.Sum(Function(c) c.Quantity))

            Assert.Equal(5, summary.PersonaAttributes.Master)
            Assert.Equal(3, summary.DeckAttributes.Master)
        End Sub

        <Fact>
        Sub TestImmortalTCGWatcherChronicleDariusPreGame()
            Dim statistics As New DeckStatistics()
            Dim deck As Data.HighlanderDeck = Data.HLD.Load("decks\valid\Amanda1EOnlyDeckWCDariusPreGame.hld")
            Dim summary As DeckSummary = statistics.Analyze("decks\valid\Amanda1EOnlyDeckWCDariusPreGame.hld", deck.Deck, deck.PreGame)

            Assert.Equal(6, summary.PersonaAttributes.Master)
            Assert.Equal(6, summary.DeckAttributes.Master)
        End Sub

        <Fact>
        Sub TestUntitledDeckStatistics()
            Dim statistics As New DeckStatistics()
            Dim deck As Data.HighlanderDeck = Data.HLD.Load("decks\valid\BasicTCGGenericImmortalDeck.hld")
            Dim summary As DeckSummary = statistics.Analyze(String.Empty, deck.Deck, deck.PreGame)

            Assert.Equal("Untitled Deck", summary.DeckTitle)
        End Sub

        <Fact>
        Sub TestBasic2EImmortalDeckStatistics()
            '@TODO:  The deck stats code has in theory been moved over
            '  It needs to be cleaned up, ex: some of it will likely need to be shared with the rules engine and will need to be moved into a separeate class TBD
            '       I think we're good on splitting stuff up now, everything that's left looks like it would be statstics specific, need to clean up code comments / add ''' comments to beginning of functions / properties / etc
            '  But the main entry point should be set up to run some decks through and see what happens
            '  and should be able to populate all of the info from the summary tab on the analyzer.
            '  Need to determine what kind of tests I want to run


            Dim statistics As New DeckStatistics()
            Dim deck As Data.HighlanderDeck = Data.HLD.Load("decks\valid\Amanda2EOnlyDeckSimple.hld")
            Dim summary As DeckSummary = statistics.Analyze("decks\valid\Amanda2EOnlyDeckSimple.hld", deck.Deck, deck.PreGame)

            Assert.Equal("Amanda2EOnlyDeckSimple", summary.DeckTitle)
            Assert.Equal("Amanda", summary.Immortal)
            Assert.Equal(4877, summary.PersonaId)
            Assert.Equal(50, summary.DeckSize)
            Assert.Equal(2, summary.PreGameSize)
            Assert.Equal(8, summary.CardsByType.Count)
            Assert.Equal(50, summary.CardsByType.Sum(Function(c) c.Quantity))
            Assert.Equal(2, summary.CardsBySubTitle.Count)
            Assert.Equal(50, summary.CardsBySubTitle.Sum(Function(c) c.Quantity))

            ' Persona Attributes
            Assert.Equal(6, summary.PersonaAttributes.Master)
            Assert.Equal(27, summary.PersonaAttributes.Agility)
            Assert.Equal(6, summary.PersonaAttributes.Strength)
            Assert.Equal(6, summary.PersonaAttributes.Toughness)
            Assert.Equal(18, summary.PersonaAttributes.Empathy)
            Assert.Equal(18, summary.PersonaAttributes.Reason)

            ' Deck Attributes
            Assert.Equal(5, summary.DeckAttributes.Master)
            Assert.Equal(26, summary.DeckAttributes.Agility)
            Assert.Equal(3, summary.DeckAttributes.Strength)
            Assert.Equal(3, summary.DeckAttributes.Toughness)
            Assert.Equal(14, summary.DeckAttributes.Empathy)
            Assert.Equal(9, summary.DeckAttributes.Reason)

            ' @TODO:  CardsBySubTitle has reserved / signitured counts for each sub title -- need to validate these are populating as expected
        End Sub

        <Fact>
        Sub TestBasic2EWeaponGemsDeckStatistics()
            Dim statistics As New DeckStatistics()
            Dim deck As Data.HighlanderDeck = Data.HLD.Load("decks\valid\Amanda2EOnlyDeckWeaponGems.hld")
            Dim summary As DeckSummary = statistics.Analyze("decks\valid\Amanda2EOnlyDeckWeaponGems.hld", deck.Deck, deck.PreGame)
            Assert.Equal(28, summary.PersonaAttributes.Agility)
            Assert.Equal(26, summary.DeckAttributes.Agility)
        End Sub
    End Class

End Namespace


