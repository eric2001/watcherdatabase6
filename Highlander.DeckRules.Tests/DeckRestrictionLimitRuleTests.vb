﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.DeckRules.Tests
    Public Class DeckRestrictionLimitRuleTests

        <Fact>
        Sub Test6OfEachCardDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortal6CardMaxDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub Test6OfEachCardTitleDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortal6CardMaxAlternateVersionsDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestRestrictionLimit2Deck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortal6CardMaxDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub Test7OfEachCardDeck()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortal7CardMaxDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(2, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Lower Center Attack").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Lower Center Attack in your deck.").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Generic Lower Center Attack").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub Test7OfEachCardAlternateVersionsDeck()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortal7CardMaxAlternateVersionsDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(2, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Lower Center Attack").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Lower Center Attack in your deck.").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Generic Lower Center Attack").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub Test7OfEachCardTitleDifferentVersionsDeck()
            Dim FileName As String = "decks\invalid\AmandaTCG7WatchersDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Watcher").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Watcher in your deck.").Count)
        End Sub

        <Fact>
        Sub TestRestrictionLimit2Exceeded()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalRestrictionLimit2ExceededDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Generic Watcher (Treatment)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestPreGameRestrictionLimit()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckSimplePreGameLimitExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only use up to 6 Pre-Game cards.").Count)
        End Sub

        <Fact>
        Sub TestPreGameCardRestrictionLimit()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckSimplePreGamePremiumLimitExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Amanda Premium (+1)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestMultiplePersonaRestrictionLimit()
            Dim FileName As String = "decks\invalid\AmandaCeirdwyn1EOnlyDeckSimple.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame,
                                                            deck.PreGame.Where(Function(c) c.Immortal = "Amanda" And c.Type = "Persona").FirstOrDefault().Id)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only use one Persona Pre-Game.").Count)
        End Sub

        <Fact>
        Sub TestMultipleThunderCastleGamesRestrictionLimit()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckSimpleTwoThunderCastleGamesAlternateVersions.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Thunder Castle Games Pre-Game").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only include one copy of the Thunder Castle Games Pre-Game.").Count)
        End Sub

        <Fact>
        Sub TestNineCrystalPreGameLimit()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2Deck9Crystals.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestCrystalsPreGameMissingBasicAttacksBlocks()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2Deck9CrystalsBasicAttackBlocksSwappedOut.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestNineCrystalPreGameLimitWithSevenPreGameCards()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2Deck9Crystals7PreGameCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only use up to 6 Pre-Game cards.").Count)
        End Sub

        <Fact>
        Sub TestCrystalsPreGameMissingBasicAttacksBlocksNoReplacement()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2Deck9CrystalsBasicAttackBlocksSwappedOutNotEnoughAttacks.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must include at least one Lower Right Attack in your deck.").Count)
        End Sub

        <Fact>
        Sub TestGenericGuardChooseMissingLowerBlocks()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckGenericGuardChoose.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestGenericGuardChooseMissingExtraLowerBlocks()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckGenericGuardChooseMissingBlocks.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must include at least one Lower Right Block in your deck.").Count)
        End Sub

        <Fact>
        Sub TestThisCardDoesNotCountTowardsYourGemLimits()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckHeadShotGemLimit.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestGenericSmallTalkAgilityLimit()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckSmallTalk.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Generic Small Talk").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may not include this card in your deck if you are using a Persona that has more than Twelve Agility.").Count)
        End Sub

        <Fact>
        Sub TestGenericSmallRestrictionLimit()
            Dim FileName As String = "decks\invalid\SlanQuinceType2DeckSmallTalk.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Generic Small Talk").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may not include any other dodges titled Small Talk in your deck.").Count)
        End Sub

        <Fact>
        Sub TestSlanQuinceGreatSwordSevenPreGameCardsLimit()
            Dim FileName As String = "decks\valid\SlanQuinceType2DeckSevenPreGameCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestSlanQuinceGreatSwordExtraAgilityEmpthyReasonExceededLimit()
            Dim FileName As String = "decks\invalid\SlanQuinceType2DeckExtraAgilityEmpthyReasonExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(3, result.Errors.Count)
            Assert.Equal(3, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Agility Gem Limit Exceeded.").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Empathy Gem Limit Exceeded.").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Reason Gem Limit Exceeded.").Count)
        End Sub

        <Fact>
        Sub TestSlanQuinceGreatSwordExtraAgilityEmpthyReasonLimit()
            Dim FileName As String = "decks\valid\SlanQuinceType2DeckExtraAgilityEmpthyReason.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestConnorMacLeodKatanaSevenPreGameCardsLimit()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckKatanaSevenPreGameCardLimit.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestDarkQuickening()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckTheDarkQuickening.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestDarkQuickeningNoExtraPersona()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckDarkQuickingNoExtraPersona.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quickening Pre-Game (Dark Quickening)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must place one Persona card underneath The Dark Quickening.").Count)
        End Sub

        <Fact>
        Sub TestDarkQuickeningTwoExtraPersonas()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckDarkQuickingTwoExtraPersonas.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quickening Pre-Game (Dark Quickening)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only place one card underneath The Dark Quickening.").Count)
        End Sub

        <Fact>
        Sub TestDarkQuickeningOwnPersona()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckDarkQuickingOwnPersona.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quickening Pre-Game (Dark Quickening)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may may not use the Persona Card you are playing as under The Dark Quickening.").Count)
        End Sub

        <Fact>
        Sub TestDarkQuickeningCordaPersona()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckDarkQuickingCordaPersona.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quickening Pre-Game (Dark Quickening)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "The Dark Quickening cannot be used with the Corda Persona.").Count)
        End Sub

        <Fact>
        Sub TestDarkQuickeningRenoPersona()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckDarkQuickingRenoPersona.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Quickening Pre-Game (Dark Quickening)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "The Dark Quickening cannot be used with the Reno Persona.").Count)
        End Sub

        <Fact>
        Sub TestSlanQuinceWeaponOfABlackSmith()
            Dim FileName As String = "decks\invalid\SlanQuinceType2DeckWeaponOfABlackSmith.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Slan Quince Weapon of a Blacksmith").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only include this card in your Deck if you are using the Great Sword Weapon of Choice.").Count)
        End Sub

        <Fact>
        Sub TestConnorMacLeodSlashUpperMissingBasicAttacks()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckSlashUpperMissingBasicAttacks.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestConnorMacLeodMastersBlockRestriction()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckMastersBlockRestriction.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Connor MacLeod Master's Block (Dodge)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may not include any other Connor MacLeod Specific cards titled Master's Block in your Deck.").Count)
        End Sub

        <Fact>
        Sub TestArmerganBasicAttackPreGameLimit()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckArmerganBasicAttackPreGameLimit.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestArmerganBasicAttackPreGameMissingBasicAttacks()
            Dim FileName As String = "decks\valid\ConnorMacLeodType2DeckArmerganMissingBasicAttacks.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestArmerganBasicAttackPreGameLimitExceeded()
            Dim FileName As String = "decks\invalid\ConnorMacLeodType2DeckArmerganBasicAttackPreGameLimitExceeded.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Amergan Pre-Game (Basic Attack)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Restriction limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestBannedCardTheEyesHaveIt()
            Dim FileName As String = "decks\invalid\Amanda1EOnlyDeckSimpleTheEyesHaveIt.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(3, result.Errors.Count)
            Assert.Equal(3, result.Errors.Where(Function(e) e.CardTitle.StartsWith("Generic The Eyes Have It")).Count)
            Assert.Equal(3, result.Errors.Where(Function(e) e.Message = "This card is banned.").Count)
        End Sub

        <Fact>
        Sub MichaelKentEightSlashLimitTest()
            Dim FileName As String = "decks\invalid\MichaelKentType2DeckWithNineSlashCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Slash").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 8 cards called Slash in your deck.").Count)
        End Sub

        <Fact>
        Sub StevenKeaneCombinationSabreCombinationSixCardTitleLimitTest()
            Dim FileName As String = "decks\invalid\StevenKeaneType2DeckImmortalAndWoCCombinations.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Combination").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Combination in your deck.").Count)
        End Sub

        <Fact>
        Sub TestXavierStCloundDifferentStrokesBasicAttackSubstitution()
            Dim FileName As String = "decks\valid\XavierStCloudDifferentStrokesMissingBasicAttacks.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestXavierStCloundDifferentStrokesBasicAttackSubstitutionMissingAttack()
            Dim FileName As String = "decks\invalid\XavierStCloudDifferentStrokesMissingExtraBasicAttacks.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You must include at least one Lower Right Attack in your deck.").Count)
        End Sub

        <Fact>
        Sub TestWalterReinhardtManipulatorOfManyDuncanMacLeodAllies()
            Dim FileName As String = "decks\valid\WalterReinhardtType2DeckWithDuncanMacLeodAnne.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestWalterReinhardtManipulatorOfManyDuncanMacLeodAlliesExtraAllies()
            Dim FileName As String = "decks\invalid\WalterReinhardtType2DeckWithExtraDuncanMacLeodAnne.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Duncan MacLeod Anne Lindsay").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You are not allowed to use cards belonging to Duncan MacLeod.").Count)
        End Sub

        <Fact>
        Sub TestSlanQuinceBruteStrength()
            Dim FileName As String = "decks\valid\SlanQuinceType2DeckBruteStrength.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestSaifAlRashidMastersDodge()
            Dim FileName As String = "decks\valid\SaifAlRashidType2DeckMastersDodge.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestRolandKantosIllusoryStrike()
            Dim FileName As String = "decks\valid\RolandKantosIllusoryStrike.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestRitaLucePreGameWithMichaelChristianPersona()
            Dim FileName As String = "decks\valid\MichaelChristianType2DeckWithRitaLuicePreGame.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestRitaLucePreGameWithNonMichaelChristianPersona()
            Dim FileName As String = "decks\invalid\ImanFasilType2DeckRitaLuceSevenPreGames.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You may only use up to 6 Pre-Game cards.").Count)
        End Sub

        <Fact>
        Sub TestRebeccaHorneElegantStrikeGems()
            Dim FileName As String = "decks\valid\RebeccaHorneType2DeckElegantStrike.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.Type2, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

    End Class
End Namespace
