﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.DeckRules.Tests

    Public Class DeckGenericImmortalRulesTests
        <Fact>
        Sub TestGenericImmortalDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalDeckWithImmortalSpecifiCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckMultipleContinunity()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalDeckWithMultipleImmortalSpecifiContinunityCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckMultipleImmortalSpecificCards()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckWithMultipleImmortalSpecifiCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Duncan MacLeod Continuity").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Generic Immortals may only use one copy of each Persona Specific Card.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckMultipleImmortalSpecificCardsAlternateVersion()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckWithMultipleImmortalSpecifiCardsAlternateVersion.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Amanda Continuity").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Generic Immortals may only use one copy of each Persona Specific Card.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckSevenContinunity()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckWithSevenImmortalSpecificContinunityCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Continuity").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "You have more then 6 cards called Continuity in your deck.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckReservedImmortalSpecificCards()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckWithReservedImmortalSpecifiCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Duncan MacLeod Battle Rage").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Generic Immortals may not use Reserved or Signatured cards.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckReservedImmortalSpecificCardsAndDarius()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalDeckWithReservedImmortalSpecifiCardsAndDarius.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckSignaturedImmortalSpecificCards()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckWithSignaturedImmortalSpecifiCards.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Hugh Fitzcairn Fast Talk").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Generic Immortals may not use Reserved or Signatured cards.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckSignaturedImmortalSpecificCardsWithDarius()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckWithSignaturedImmortalSpecificCardsAndDarius.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Hugh Fitzcairn Fast Talk").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Generic Immortals may not use Reserved or Signatured cards.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckMasterCard()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckMasterCard.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "*").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "Master card limit exceeded.").Count)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckMasterCardDarius()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalDeckMasterCardDariusPreGame.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestGenericImmortalDeckWithOtherImmortalPremium()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalDeckCeirdwynPremium.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.CardTitle = "Ceirdwyn Premium (+1)").Count)
            Assert.Equal(1, result.Errors.Where(Function(e) e.Message = "This card requires the Ceirdwyn Persona Pre-Game.").Count)
        End Sub

    End Class
End Namespace

