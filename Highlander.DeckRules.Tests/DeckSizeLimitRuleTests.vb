﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.DeckRules.Tests

    Public Class DeckSizeLimitRuleTests
        <Fact>
        Sub TestBasic50CardDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestLeanAndMeanDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalLeanAndMeanDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestBigAndBadDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalBigAndBadDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestBigAndBadLeanAndMeanDeck()
            Dim FileName As String = "decks\valid\BasicTCGGenericImmortalBigAndBadLeanAndMeanDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(True, result.IsDeckValid)
        End Sub

        <Fact>
        Sub TestInvalid49CardDeck()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortal49CardDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal("*", result.Errors(0).CardTitle)
            Assert.Equal("Your deck must contain at least 50 cards.", result.Errors(0).Message)
        End Sub

        <Fact>
        Sub TestInvalidLeanAndMeanDeck()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalLeanAndMeanInvalidDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal("*", result.Errors(0).CardTitle)
            Assert.Equal("Your deck must contain at least 46 cards.", result.Errors(0).Message)
        End Sub

        <Fact>
        Sub TestInvalidBigAndBadDeck()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalBigAndBadInvalidDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal("*", result.Errors(0).CardTitle)
            Assert.Equal("Your deck must contain at least 110 cards.", result.Errors(0).Message)
        End Sub

        <Fact>
        Sub TestInvalidBigAndBadLeanAndMeanDeck()
            Dim FileName As String = "decks\invalid\BasicTCGGenericImmortalBigAndBadLeanAndMeanInvalidDeck.hld"
            Dim deck As Data.HighlanderDeck = Data.HLD.Load(FileName)
            Dim statistics As New DeckStatistics()
            Dim summary As DeckSummary = statistics.Analyze(FileName, deck.Deck, deck.PreGame)
            Dim Engine As New RulesEngine()
            Dim result = Engine.Analyze(deck.Deck, deck.PreGame, DeckFormat.FirstEdition, summary)

            Assert.Equal(False, result.IsDeckValid)
            Assert.Equal(1, result.Errors.Count)
            Assert.Equal("*", result.Errors(0).CardTitle)
            Assert.Equal("Your deck must contain at least 108 cards.", result.Errors(0).Message)
        End Sub

    End Class
End Namespace

