# Highlander Watcher Database
![Highlander Watcher Database](./WDB6%20Documentation/Doc_Images/WDB6_Start.png) 

## Description
This program was designed to allow players of the Highlander Collectible Card Game to track their collection, build and manage decks, and play the game online.  It comes with an XML database of every known card that's been produced by various companies over the years.  Players may associate scans of cards with the individual records in the database, which will then result in those scans being viewable throughout the program.  It is capable of connecting directly with another player over the internet in real time, to play a game as well as tracking a game in progress for "off line" game play (usually via email).

This project was started in 2006 as a replacement to the Visual Basic 6 version of the program.

**License:** [GPL v.3](http://www.gnu.org/copyleft/gpl.html)

## Instructions
Please see the included documentation for additional details and setup / usage instructions.

## History
**Version 6.0.0 Beta 4:**
> - Database:
>   - Database now includes all known cards including Kassim, Steven Keane and Connor MacLeod vs. Slan Quince vs decks and a few previously missing promotional cards.
>   - Database now includes Highlander Pro cards.
> - Software:
>   - Updated Microsoft .Net Framework from 4.0 (no longer supported by Microsoft as of Jan 12, 2016 https://learn.microsoft.com/en-us/lifecycle/products/microsoft-net-framework) to 6.0 (supported until November 12, 2024 https://dotnet.microsoft.com/en-us/platform/support/policy).
>   - Updated development environment from Visual Studio 2010 to Visual Studio 2022.
>   - The previous two bullets broke _everything_.  All screens have been reviewed and fixed.
>   - Numerous behind the scenes architectural changes due to the Framework upgrade and the re-write.
>   - Deck Editor has been updated to support Highlander Pro deck construction rules.
>   - Tools > Settings screen has a Display Mode option -- "1st Edition" will cause the deck editor to use the original card titles for first edition cards (ex: Series Edition's "Amanda Back Away"); "Type 1" will cause the deck editor to use the errataed card title for first edition cards (ex: "Amanda Evade (Back Away)".  This setting is ignored on the Collection screens as I thought it was more useful to use the original card titles there.
>   - Tools > Print MLE Cards screen has been renamed to Print Proxy Cards and now allows printing of cards from any set.
> - Released on 27 August 2023.
>
> Download: [Version 6.0.0 Beta 4 Setup](/uploads/e1e1ec6083b3973e9a2c9ba2940c6a9d/WatcherDatabase6.0.0Beta4Setup.zip) | [Version 6.0.0 Beta 4 Source](/uploads/f80a5bee212df0422bde4f3b0932d268/WatcherDatabase6.0.0Beta4Source.zip)

**Version 6.0.0 Beta 3:**
> - Added ability to search by card number.
> - Improved handling of special characters in keyword search box.
> - Added card numbers into set reports, where available.
> - Updated cards database with all the latest cards, including the Legacy 3 release.
> - Released on 09 October 2022.
>
> Download: [Version 6.0.0 Beta 3 Setup](/uploads/849e1e91f24903a8ec616252c05e8058/WatcherDatabase6.0.0Beta3Setup.zip) | [Version 6.0.0 Beta 3 Source](/uploads/ea0cfb665a628ea545b0cccc89c09be1/WatcherDatabase6.0.0Beta3Source.zip)

**Version 6.0.0 Beta 2:**
> - Database: Includes the latest revision of the cards database, which includes the new 2017 Summer Special spoilers as well as other various improvements.
> - Deck Editor: I've completely re-written all of the deck analyzer logic. I've also been working on improving the support for the various basic deck construction rules that exist across the various rule books.
> - Released on 13 May 2017
>
> Download: [Version 6.0.0 Beta 2 Setup](/uploads/4fd22856aaa3bef6abbf28325e6b59c0/WatcherDatabase6.0.0Beta2Setup.zip) | [Version 6.0.0 Beta 2 Source](/uploads/a61f4f13ca0d8ff65301e0b8e9c8a17c/WatcherDatabase6.0.0Beta2Source.zip)

**Version 6.0.0 Beta 1:**
> - Database:  Includes the latest revision of the cards database, which should have all of the latest second edition cards.
> - Collections:  The View Collection screen should be fully functional.  Please see the online documentation for notes on it's use.  Please report any issues or requests to me via the bug tracker.
> - Trade Lists:  The Create / Edit trade lists screen and the Compare Trade list screen should be fully functional.  Please see the online documentation for notes on it's use.  Please report any issues or requests to me via the bug tracker.
> - Reports:  Watcher Database 6 has new reporting functionality.  These features should be fully functional.  Please see the online documentation for notes on it's use.  Please report any issues or requests to me via the bug tracker.Two options are currently available.
>   - The Overview report provides a list of all sets in watcher database, and shows a break down of how many unique cards you have / don't have for each set/publisher, how many total cards you have for each set/publisher, 
>   - The set report provides a detailed listing of every card in a specific set including how many copies of each card you have, and provides a summary of the set at the end of the report listing unique and total cards owned and unique cards missing.
> - Tools:  Watcher Database 6 has several options under the tools menu for setup, importing old version 5 records and printing MLE cards.  These features should be fully functional.  Please see the online documentation for notes on it's use.  Please report any issues or requests to me via the bug tracker.
> - Deck Editor:  I'm still working on this area, but I believe that creating, viewing, editing and printing decks should all be fully functional.  Watcher Database 6 should be fully compatible with any version 5 deck lists.  The statistics under the Analyze screen should all work correctly, however the rules checker is still a work in progress.  Expect to see improvements here in future releases.  Please report any issues or requests to me via the bug tracker.
> - Play Game:  The play game screens are still under active development and have been disabled in this beta release.  It will be included in a future release once their functionality is more complete.
> - Released on 12 April 2017.
>
> Download: [Version 6.0.0 Beta 1 Setup](/uploads/5073a1c01659aaa66985e50ff9dd9722/WatcherDatabase6.0.0Beta1Setup.zip) | [Version 6.0.0 Beta 1 Source](/uploads/4b1dd6baf0d7f59866b02a80cbf303b2/WatcherDatabase6.0.0Beta1Source.zip)
