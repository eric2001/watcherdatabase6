﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data
Imports Highlander.DeckRules.Rules
Imports System.Data
Imports System.Text

Public Class DeckStatistics
    Public Function Analyze(ByVal DeckFileName As String, ByVal Deck As List(Of Card), ByVal PreGame As List(Of Card), Optional ByVal personaId As Integer = 0) As DeckSummary
        Dim summary As New DeckSummary()

        Dim TempPreGame As List(Of Card) = Clone.CopyDeck(PreGame) ' Copy the pre-game list so the de-duplicating MLE Cards logic doesn't destory the original list
        Dim TempDeck As List(Of Card) = Clone.CopyDeck(Deck)

        ArmsAndTacticsHiltProcessor.Process(TempDeck, TempPreGame)

        summary.DeckTitle = SetDeckTitle(DeckFileName)
        AnalyzePreGame(TempPreGame, summary, personaId)
        AnalyzeDeck(TempDeck, summary)

        Return summary
    End Function

    Private Function SetDeckTitle(ByVal DeckFileName As String) As String
        ' Determine Deck Title (if any) based on the file name.
        If DeckFileName.Length > 0 Then
            Return DeckFileName.Substring(DeckFileName.LastIndexOf("\") + 1, DeckFileName.LastIndexOf(".") - DeckFileName.LastIndexOf("\") - 1)
        Else
            Return "Untitled Deck"
        End If

    End Function

    Private Sub AnalyzePreGame(ByRef PreGame As List(Of Card), ByRef summary As DeckSummary, Optional ByVal personaId As Integer = 0)
        ' MLE has seperate front and back cards for pre-games.  These both count as 1 card.  Remove duplicate card backs, if present to simplify the rest of the logic.
        MissingLinkExpansion.DeDuplicatePreGameCards(PreGame)

        ' Determine who the Persona is for this deck and set the master card limit.
        Dim strImmortalGems As String = IdentifyPersona(PreGame, summary, personaId)
        personaId = summary.PersonaId

        '  Check and see if the persona card has any gem attributes on it, if so, extract the values from the gems string
        If strImmortalGems.Length > 0 Then
            ExtractPersonaGems(strImmortalGems, PreGame.Where(Function(p) p.Id = personaId).FirstOrDefault().ExpansionSet.Edition, summary)
        End If

        For Each oneCard As Card In PreGame
            ' Track total number of pre-game cards in the deck list.
            summary.PreGameSize += oneCard.Quantity

            ' If this pre-game card has gems, and it's not a persona then add it's gems to the total.
            '   (Mostly for certain WoC cards and a few other special pre-games)
            If (oneCard.Type <> "Persona" And Not String.IsNullOrWhiteSpace(oneCard.Gems)) Then
                ExtractPreGameGems(oneCard.Gems, summary)
            End If
            ' End Gem Adjustment.

            ' WC\Darius Pre-Game (Master) -- increases your master card limit by one.
            If oneCard.Id = CardLookup.ThunderCastleGames.TheWatchersChronicle.DariusPreGameMaster Then
                summary.PersonaAttributes.Master += oneCard.Quantity
            End If

        Next
        ' End Pre-Game List loop.

    End Sub

    Private Function IdentifyPersona(ByRef PreGame As List(Of Card), ByRef summary As DeckSummary, Optional ByVal personaId As Integer = 0) As String

        ' Locate the Deck's Persona card, and determine that Persona's Master card limit.
        ' @TODO:  Do I actually need this personaId parameter?  If Four Hoursemen persona or Schizo pre-game then this card is the important one and I take the lower master card limit.
        '     If 2e Corda and Reno personas are both in the deck then I think I probably sum up the gems?  But that doesn't require selecting the card either
        '           I think I can probably get rid of the parameter and if multiple check to see if it's a known exception and handle accordingly, otherwise display an error?
        Dim PersonaCards As List(Of Card)
        If personaId = 0 Then
            PersonaCards = PreGame.Where(Function(p) p.Type = "Persona").ToList()
        Else
            PersonaCards = PreGame.Where(Function(p) p.Id = personaId).ToList()
        End If

        If PersonaCards.Count = 0 Then ' Generic Immortal
            summary.PersonaId = 0
            summary.Immortal = "Generic Immortal"
            Return "" ' Generic Immortal's don't have Gems

        ElseIf PersonaCards.Count = 1 Then
            summary.PersonaId = PersonaCards(0).Id
            summary.PersonaAttributes.Master = Convert.ToInt32(PersonaCards(0).Restricted)
            summary.Immortal = PersonaCards(0).Immortal

            ' Adjust the color of the Toughness gem based on if the  card is 2E or other.
            If PersonaCards(0).ExpansionSet.Edition = Data.Edition.SecondEditionRevised Or
                    PersonaCards(0).ExpansionSet.Edition = Data.Edition.ThirdEdition Then
                summary.PersonaAttributes.Display3EGems = True
            End If

            Return PersonaCards(0).Gems
        Else
            Throw New Exception("Multiple Persona Cards found, unable to proceed")
        End If

    End Function

    Private Sub ExtractPersonaGems(ByVal strImmortalGems As String, ByVal PersonaEdition As Data.Edition, ByRef summary As DeckSummary)
        ' This changes based on the persona.  2e cards the gem count is multiplied by three, for 2e revised, and 3e it is not.  
        '   Default to 2E and update when analyzing the pre-game if needed.
        Dim EditionGemsModifier As Integer = 3

        ' Use the edition to determine if we need to multiply gem values
        '   by 3 (default) or 1 (for newer cards), and to set the thoughness gem color.
        If PersonaEdition = Edition.SecondEditionRevised OrElse
            PersonaEdition = Edition.ThirdEdition Then
            EditionGemsModifier = 1
        End If

        While strImmortalGems.Length > 0

            ' Because gem values can be either 1 or two digits, loop through the string and 
            '   find the next gem letter so we know where to stop.
            Dim gemValueLength As Integer = 1
            While (Not (strImmortalGems.Chars(gemValueLength) = "M") And
                   Not (strImmortalGems.Chars(gemValueLength) = "A") And
                   Not (strImmortalGems.Chars(gemValueLength) = "S") And
                   Not (strImmortalGems.Chars(gemValueLength) = "T") And
                   Not (strImmortalGems.Chars(gemValueLength) = "E") And
                   Not (strImmortalGems.Chars(gemValueLength) = "R") And
                   Not (strImmortalGems.Length - 1 = gemValueLength))
                gemValueLength += 1
            End While
            ' If we hit the end of the string before finding another gem letter, 
            '    then use the string length as the ending value.
            If gemValueLength + 1 = strImmortalGems.Length Then
                gemValueLength += 1
            End If

            '  Store the value for the current gem in the result.
            If strImmortalGems.Chars(0) = "A" Then
                summary.PersonaAttributes.Agility = strImmortalGems.Substring(1, gemValueLength - 1) * EditionGemsModifier
            End If
            If strImmortalGems.Chars(0) = "E" Then
                summary.PersonaAttributes.Empathy = strImmortalGems.Substring(1, gemValueLength - 1) * EditionGemsModifier
            End If
            If strImmortalGems.Chars(0) = "R" Then
                summary.PersonaAttributes.Reason = strImmortalGems.Substring(1, gemValueLength - 1) * EditionGemsModifier
            End If
            If strImmortalGems.Chars(0) = "S" Then
                summary.PersonaAttributes.Strength = strImmortalGems.Substring(1, gemValueLength - 1) * EditionGemsModifier
            End If
            If strImmortalGems.Chars(0) = "T" Then
                summary.PersonaAttributes.Toughness = strImmortalGems.Substring(1, gemValueLength - 1) * EditionGemsModifier
            End If
            strImmortalGems = strImmortalGems.Substring(gemValueLength)
        End While
    End Sub

    Private Sub ExtractPreGameGems(ByVal selectedGems As String, ByRef summary As DeckSummary)
        If selectedGems <> "+1" Then  ' +1 doesn't affect deck construction, ignore it
            While selectedGems.Length > 0

                ' Because gem values can be either 1 or two digits, loop through the string and 
                '   find the next gem letter so we know where to stop.
                Dim gemValueLength As Integer = 1
                While (Not (selectedGems.Chars(gemValueLength) = "M") And
                       Not (selectedGems.Chars(gemValueLength) = "A") And
                       Not (selectedGems.Chars(gemValueLength) = "S") And
                       Not (selectedGems.Chars(gemValueLength) = "T") And
                       Not (selectedGems.Chars(gemValueLength) = "E") And
                       Not (selectedGems.Chars(gemValueLength) = "R") And
                       Not (selectedGems.Length - 1 = gemValueLength))
                    gemValueLength += 1
                End While
                ' If we hit the end of the string before finding another gem letter, 
                '    then use the string length as the ending value.
                If gemValueLength + 1 = selectedGems.Length Then
                    gemValueLength += 1
                End If

                '  Store the value for the current gem in the analysis window.
                If selectedGems.Chars(0) = "M" Then
                    summary.PersonaAttributes.Master += selectedGems.Substring(1, gemValueLength - 1)
                End If
                If selectedGems.Chars(0) = "A" Then
                    summary.PersonaAttributes.Agility += selectedGems.Substring(1, gemValueLength - 1)
                End If
                If selectedGems.Chars(0) = "E" Then
                    summary.PersonaAttributes.Empathy += selectedGems.Substring(1, gemValueLength - 1)
                End If
                If selectedGems.Chars(0) = "R" Then
                    summary.PersonaAttributes.Reason += selectedGems.Substring(1, gemValueLength - 1)
                End If
                If selectedGems.Chars(0) = "S" Then
                    summary.PersonaAttributes.Strength += selectedGems.Substring(1, gemValueLength - 1)
                End If
                If selectedGems.Chars(0) = "T" Then
                    summary.PersonaAttributes.Toughness += selectedGems.Substring(1, gemValueLength - 1)
                End If
                selectedGems = selectedGems.Substring(gemValueLength)
            End While
        End If
    End Sub

    Private Sub AnalyzeDeck(ByRef Deck As List(Of Card), ByRef summary As DeckSummary)

        ' Loop through each card in the deck list, and check for rules violations.
        For Each oneCard As Card In Deck

            ' Track Total Deck Size
            summary.DeckSize += oneCard.Quantity

            ' Maintain a running total of any Gems used by cards in the deck, and get the number of master gems, if any.
            '  This also checks if a card is a master card, for 1st edition cards that lack gems.
            ExtractDeckGems(oneCard, summary)


            ' Deck Statistics -- keep track of how many of each card type and sub title are used in the deck.
            UpdateCardTypeStatistics(oneCard, summary)
            UpdateCardSubTitleStatistics(oneCard, summary)

        Next ' End Loop for individual card checks

    End Sub

    Private Sub ExtractDeckGems(ByVal OneCard As Card, ByRef summary As DeckSummary)
        Dim Gems As String = OneCard.Gems
        If Gems.Length > 0 AndAlso Gems <> "+1" Then ' +1 gem has no effect on deck construction, so ignore it.

            ' Skip cards that say "This card's Attributes do not count towards your deck construction limit."
            If OneCard.Id <> CardLookup.OneShotGames.Legacy3.SlanQuinceBruteStrength AndAlso
                OneCard.Id <> CardLookup.OneShotGames.Legacy3.RebeccaHorneElegantStrike Then

                While Gems.Length > 0
                    Dim strGemValue As Integer = Convert.ToInt32(Gems.Substring(1, 1))
                    If Gems.Chars(0) = "A" Then
                        summary.DeckAttributes.Agility += strGemValue * OneCard.Quantity
                    End If
                    If Gems.Chars(0) = "E" Then
                        summary.DeckAttributes.Empathy += strGemValue * OneCard.Quantity
                    End If
                    If Gems.Chars(0) = "R" Then
                        summary.DeckAttributes.Reason += strGemValue * OneCard.Quantity
                    End If
                    If Gems.Chars(0) = "S" Then
                        summary.DeckAttributes.Strength += strGemValue * OneCard.Quantity
                    End If
                    If Gems.Chars(0) = "T" Then
                        summary.DeckAttributes.Toughness += strGemValue * OneCard.Quantity
                    End If
                    If Gems.Chars(0) = "M" Then
                        summary.DeckAttributes.Master += strGemValue * OneCard.Quantity
                    End If
                    Gems = Gems.Substring(2)
                End While

            End If

        ElseIf OneCard.Title.ToLower.Contains("master") Then
            '  If card does not have Gems, use 1st edition logic and see if card has master in the title, if so, add it's quantity to the master card count.
            summary.DeckAttributes.Master += OneCard.Quantity
        End If
    End Sub

    Private Sub UpdateCardTypeStatistics(ByRef OneCard As Card, ByRef summary As DeckSummary)
        ' Keep track of what types of cards are in the deck and how many of each are used.
        Dim strCardTypes() As String = OneCard.Type.Split("/") ' For Duel Types, split on "/" and track each type seperately.
        For Each strCardType As String In strCardTypes
            Dim TypeExists As TypeStat = summary.CardsByType.Where(Function(t) t.CardType = strCardType.Trim).FirstOrDefault
            If TypeExists IsNot Nothing Then
                TypeExists.Quantity += OneCard.Quantity
            Else
                Dim NewType As New TypeStat With {
                    .CardType = strCardType.Trim,
                    .Quantity = OneCard.Quantity
                }
                summary.CardsByType.Add(NewType)
            End If
        Next
    End Sub

    Private Sub UpdateCardSubTitleStatistics(ByRef OneCard As Card, ByRef summary As DeckSummary)
        ' Keep track of each sub title used in the deck (Immortal specific, Weapon Specific, etc)
        Dim strSubTitles() As String
        If OneCard.Immortal = "Michael Moore / Quentin Barnes" Then
            strSubTitles = New String() {OneCard.Immortal} ' Special handing for Michael Moore / Quentin Barnes -- Not actually a duel immortal, just an insane one so do not spit apart.
        Else
            strSubTitles = OneCard.Immortal.Split("/") ' For duel immortal cards, split on "/", track each sub title seperately
        End If

        For Each strSubTitle As String In strSubTitles
            Dim TitleExists As SubTitleStat = summary.CardsBySubTitle.Where(Function(t) t.Immortal = strSubTitle.Trim).FirstOrDefault
            If TitleExists IsNot Nothing Then
                TitleExists.Quantity += OneCard.Quantity
                If OneCard.Restricted.ToLower.Contains("s") Then
                    TitleExists.Signatured += OneCard.Quantity
                ElseIf OneCard.Restricted.ToLower.Contains("r") Then
                    TitleExists.Reserved += OneCard.Quantity
                End If
            Else
                Dim NewSubTitle As New SubTitleStat With {
                    .Immortal = strSubTitle.Trim,
                    .Quantity = OneCard.Quantity
                }
                If OneCard.Restricted.ToLower.Contains("s") Then
                    NewSubTitle.Signatured += OneCard.Quantity
                ElseIf OneCard.Restricted.ToLower.Contains("r") Then
                    NewSubTitle.Reserved += OneCard.Quantity
                End If
                summary.CardsBySubTitle.Add(NewSubTitle)
            End If
        Next
    End Sub
End Class
