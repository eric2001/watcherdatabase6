﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Public Class ValidationError
    Public CardTitle As String
    Public Message As String
    Public CardId As Integer

    Public Sub New(NameOfCard As String, RuleError As String, Optional ByVal Id As Integer = 0)
        CardId = Id
        CardTitle = NameOfCard
        Message = RuleError
    End Sub

    Public Sub New(Format As DeckFormat, FailedCard As Card, RuleError As String)
        CardId = FailedCard.Id

        If Format = DeckFormat.Type1 Or Format = DeckFormat.Type2 Or Format = DeckFormat.HighlanderPro Then
            CardTitle = String.Format("{0} {1}", FailedCard.Immortal, Card.RemoveUnnecessaryVersion(FailedCard.SecondEditionTitle, FailedCard.Id))
        Else
            CardTitle = String.Format("{0} {1}", FailedCard.Immortal, Card.RemoveUnnecessaryVersion(FailedCard.Title, FailedCard.Id))
        End If

        Message = RuleError
    End Sub
End Class

