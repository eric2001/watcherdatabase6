﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Runtime.InteropServices
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Validates if the Deck / Pre-Game contain cards from any Publishers that are banned in the current format.
    ''' Ex: A valid First Edition deck can only contain cards from Thunder Castle Games and SAEC Games.  It can't use cards from other publishers like Le Montagnard Inc.
    ''' </summary>
    Public Class AllowedPublishersRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim BannedCards As List(Of Card) = GetBannedCards(PreGame, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, String.Format("{0} is banned in this format.", OneCard.ExpansionSet.Name)))
            Next

            For Each OneSubCard As Card In PreGame
                If OneSubCard.SubItems.Count > 0 Then
                    BannedCards = GetBannedCards(OneSubCard.SubItems, Format)
                    For Each OneCard In BannedCards
                        Errors.Add(New ValidationError(Format, OneCard, String.Format("{0} is banned in this format.", OneCard.ExpansionSet.Name)))
                    Next
                End If
            Next

            BannedCards = GetBannedCards(Deck, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, String.Format("{0} is banned in this format.", OneCard.ExpansionSet.Name)))
            Next

            Return Errors
        End Function

        Private Function GetBannedCards(ByVal Cards As List(Of Card), Format As DeckFormat) As List(Of Card)
            Dim BannedCards As New List(Of Card)

            '@TODO:  Technically Type 1 / 2 aren't right as the rulebooks say "Highlander Pro cards with a circle around their set symbol" are allowed in type 1 / 2
            '        and I'm currently flagging all of Highlander Pro as banned.
            '        However none of the cards I have access to have a circle around the set symbol so I'm just banning them all until for now until I see cards with a circle.

            If Format = DeckFormat.FirstEdition Then
                BannedCards = Cards.Where(Function(c) c.Publisher.Id = PublisherLookup.Publisher.MleTeam Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.LeMontagnard Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.OneShotGames Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.ParadoxPublishing Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.WorldOfGameDesign).ToList()

            ElseIf Format = DeckFormat.MissingLinkExpansion Then
                BannedCards = Cards.Where(Function(c) c.Publisher.Id = PublisherLookup.Publisher.LeMontagnard Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.OneShotGames Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.ParadoxPublishing Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.WorldOfGameDesign).ToList()
            ElseIf Format = DeckFormat.Type1 Then
                BannedCards = Cards.Where(Function(c) c.Publisher.Id = PublisherLookup.Publisher.MleTeam Or
                                                      c.Publisher.Id = PublisherLookup.Publisher.WorldOfGameDesign).ToList()
            ElseIf Format = DeckFormat.Type2 Then
                BannedCards = Cards.Where(Function(c) c.Publisher.Id = PublisherLookup.Publisher.MleTeam Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.ThunderCastleGames Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.SaecGames Or
                                                                     c.Publisher.Id = PublisherLookup.Publisher.WorldOfGameDesign).ToList()
            ElseIf Format = DeckFormat.HighlanderPro Then
                BannedCards = Cards.Where(Function(c) c.Publisher.Id = PublisherLookup.Publisher.ThunderCastleGames Or
                                                      c.Publisher.Id = PublisherLookup.Publisher.MleTeam Or
                                                      c.Publisher.Id = PublisherLookup.Publisher.SaecGames Or
                                                      c.Publisher.Id = PublisherLookup.Publisher.LeMontagnard Or
                                                      c.Publisher.Id = PublisherLookup.Publisher.OneShotGames Or
                                                      c.Publisher.Id = PublisherLookup.Publisher.ParadoxPublishing).ToList()
            End If

            Return BannedCards
        End Function

    End Class
End Namespace
