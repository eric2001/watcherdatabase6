﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Reflection.Metadata
Imports System.Runtime.InteropServices
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule looks for Weapon of Choice cards that can only be played in conjunction with specific Immortals, and
    ''' validates that the Persona for that Immortal is being used.
    ''' </summary>
    Public Class WeaponOfChoiceAllowedRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            ' Second Edition Cards
            Dim PreGameCardsToValidate As List(Of Card) = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.FourHorsemen.CutlassPreGameCaspian).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Caspian").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Cutlass Weapon of Choice can only be used with the Caspian Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.FourHorsemen.EnglishLongswordPreGameMethos).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Methos").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The English Longsword Weapon of Choice can only be used with the Methos Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.DuncanVsKanwulf.GreatSwordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal.EndsWith("MacLeod")).Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Great Sword Weapon of Choice can only be used with a MacLeod Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.FourHorsemen.GreatSwordPreGameKronos).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Kronos").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Great Sword Weapon of Choice can only be used with the Kronos Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.FourHorsemen.WarAxePreGameSilas).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Silas").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The War Axe Weapon of Choice can only be used with the Silas Persona."))
                End If
            End If


            ' Second Edition Revised Cards
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.BrianCullenVsNicholasWard.CaneSwordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Nicholas Ward").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Cane Sword Weapon of Choice can only be used with the Nicholas Ward Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.CrystaVanPeltVsKhabulKhan.ChineseDaoPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Crysta Van Pelt").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Chinese Dao Weapon of Choice can only be used with the Crysta Van Pelt Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.UrsaVsGiovani.CortanaPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Cardinal Giovanni").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Cortana of Choice can only be used with the Cardinal Giovanni Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.ReggieWellerVsRaphael.CutlassPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Reggie Weller").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Cutlass Weapon of Choice can only be used with the Reggie Weller Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.CarlRobinsonVsMatthewMcCormick.FalchionPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Carl Robinson").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Falchion Weapon of Choice can only be used with the Carl Robinson Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.ConnorMacLeodVsSlanQuince.GreatSwordPreGame Or
                                                       c.Id = CardLookup.OneShotGames.Legacy3.GreatSwordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Slan Quince").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Great Sword Weapon of Choice can only be used with the Slan Quince Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.ConnorMacLeodVsSlanQuince.KatanaPreGame Or
                                                       c.Id = CardLookup.LeMontagnard.ConnorVsKurgan.KatanaPreGame Or
                                                       c.Id = CardLookup.LeMontagnard.ConnorVsDuncan.KatanaPreGameConnor).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Connor MacLeod").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Katana Weapon of Choice can only be used with the Connor MacLeod Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.ConnorVsDuncan.KatanaPreGameDuncan).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Duncan MacLeod").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Katana Weapon of Choice can only be used with the Duncan MacLeod Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.OldCarlVsThomasSullivan.HandAxePreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Old Carl").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Hand Axe Weapon of Choice can only be used with the Old Carl Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.DarkDuncan.KatanaPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Dark Duncan").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Katana Weapon of Choice can only be used with the Dark Duncan Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.TakNeVsTheBedoin.KatanaPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Tak Ne").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Katana Weapon of Choice can only be used with the Tak Ne Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.KiemSunVsVictorHansen.KrisPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Victor Hansen").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Kris Weapon of Choice can only be used with the Victor Hansen Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.CrystaVanPeltVsKhabulKhan.MongolianBroadswordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Khabul Khan").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Mongolian Broadsword Weapon of Choice can only be used with the Khabul Khan Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.TakNeVsTheBedoin.NimchaPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Saif al-Rashid").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Nimcha Weapon of Choice can only be used with the Saif al-Rashid Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.MichaelMooreQuentinBarnesVsByron.SabrePreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Lord Byron").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Sabre Weapon of Choice can only be used with the Lord Byron Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.SabrePreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Walter Reinhardt").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Sabre Weapon of Choice can only be used with the Walter Reinhardt Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.HamzaElKahirVsMarcusConstantine.ScimitarPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Hamza el Kahir").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Scimitar Weapon of Choice can only be used with the Hamza el Kahir Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.KassimCollection.ScimitarPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Kassim").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Scimitar Weapon of Choice can only be used with the Kassim Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.KatherineVsBartholomew.ShieldPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Bartholomew").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Shield Weapon of Choice can only be used with the Bartholomew Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.HamzaElKahirVsMarcusConstantine.ShortSwordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Marcus Constantine").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Short Sword Weapon of Choice can only be used with the Marcus Constantine Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.SingleHandedBroadswordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Iman Fasil").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Single-Handed Broadsword Weapon of Choice can only be used with the Iman Fasil Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.AlexRavenVsDamonCase.SingleHandedBroadswordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Damon Case").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Single-Handed Broadsword Weapon of Choice can only be used with the Damon Case Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.KatherineVsBartholomew.SwissLongSwordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Katherine").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Swiss Long Sword Weapon of Choice can only be used with the Katherine Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.TwoHandedBroadswordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "The Kurgan").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Two Handed Broadsword Weapon of Choice can only be used with The Kurgan Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.CalebColeVsMarcusKorolus.WarAxePreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Caleb Cole").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The War Axe Weapon of Choice can only be used with the Caleb Cole Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.GregorPowersCollection.SingleHandedBroadswordPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Gregor Powers").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The El Cid Tizona Single-Handed Broadsword Weapon of Choice can only be used with the Gregor Powers Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.MichaelKentCollection.KatanaPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Michael Kent").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Muramasa Katana Weapon of Choice can only be used with the Michael Kent Persona."))
                End If
            End If

            ' Third Edition Cards
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.ParadoxPublishing.TheProphecy.EnglishLongswordPreGameCassandra).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Cassandra").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The English Longsword Weapon of Choice can only be used with the Cassandra Persona."))
                End If
            End If

            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.ParadoxPublishing.TheProphecy.EnglishLongswordPreGameRolandKantos).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Roland Kantos").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The English Longsword Weapon of Choice can only be used with the Roland Kantos Persona."))
                End If
            End If

            Return Errors

        End Function

    End Class
End Namespace
