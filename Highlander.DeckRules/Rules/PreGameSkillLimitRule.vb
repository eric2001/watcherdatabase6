﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Runtime.InteropServices
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This card validates that the decks pre-game only contains at most one Skill Pre-Game card.
    ''' </summary>
    Public Class PreGameSkillLimitRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            ' You may include one Skill Pre-Game card in your Pre-Game.
            Dim SkillPreGames As List(Of Card) = PreGame.Where(Function(c) c.Type.ToLower() = "skill").ToList()
            If SkillPreGames.Count > 1 Then
                For Each OneCard As Card In SkillPreGames
                    Errors.Add(New ValidationError(Format, OneCard, "You may only use one Skill Pre-Game card."))
                Next
            End If

            Return Errors
        End Function

    End Class
End Namespace
