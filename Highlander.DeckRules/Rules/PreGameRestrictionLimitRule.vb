﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule validates that the deck contains only one of each unique pre-game card.
    ''' </summary>
    Public Class PreGameRestrictionLimitRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            ' You may only use 1 of each unique pre-game card.
            ' https://highlandertcg.proboards.com/thread/4540/unique-pre-game-cards-rule -- Title and Text indicates if a pre-game is unique.
            ' Some pre-game cards have been reprinted in different expansions and Ramirez has A/B versions of his premium, 
            '   So we can't just check the individual card quantities to determine if the limit is exceeded or not.
            Dim LocalFormat As DeckFormat = Format
            Dim CardsByGroup = PreGame.GroupBy(Function(c) GetGroup(c, LocalFormat)).Select(Function(g) g.ToList()).ToList()

            For Each OneGroup In CardsByGroup
                If OneGroup.Sum(Function(c) c.Quantity) > 1 Then
                    Errors.Add(New ValidationError(Format, OneGroup.FirstOrDefault(), "Restriction limit exceeded."))
                End If
            Next

            ' 1E Handbook:  Thunder Castle Games Cards - All Thunder Castle Games cards, or "Rips", are considered to be the same card, and are not considered to be unique, 
            '  even though they may have slightly different Text, quotes, or pictures. Being a Pre-Game card, you may only have 1 Thunder Castle Games card in play.
            Dim PreGameCardsToValidate As List(Of Card) = PreGame.Where(Function(c) c.Immortal.ToLower() = "thunder castle games" And
                                                                            c.Type.ToLower() = "pre-game" And
                                                                            c.ExpansionSet.Publisher.Id = PublisherLookup.Publisher.ThunderCastleGames).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Thunder Castle Games Pre-Game", "You may only include one copy of the Thunder Castle Games Pre-Game."))
            End If

            ' MLE Zeist Cards all have the same text, so the Thunder Castle Games RIP rule would apply as well.
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Immortal.ToLower() = "zeist" And
                                                       c.Type.ToLower() = "pre-game" And
                                                       c.ExpansionSet.Publisher.Id = PublisherLookup.Publisher.MleTeam).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Zeist Pre-Game", "You may only include one copy of the Zeist Pre-Game."))
            End If

            ' Swordmaster cards by type

            ' ignore the text on one Special Card or Edge in play. 
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.BetaPromotionalCards.SwordmasterPreGameTemplarShrine Or
                                                               c.Id = CardLookup.LeMontagnard.BetaPromotionalCards.SwordmasterPreGameFallenTree Or
                                                               c.Id = CardLookup.LeMontagnard.BetaPromotionalCards.SwordmasterPreGameMansion Or
                                                               c.Id = CardLookup.LeMontagnard.BetaPromotionalCards.SwordmasterPreGameSulphurPlant Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameCitytop Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameFallenTree Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameMacLeodRuins Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameMansion Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameMossyRocks Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameMountain Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameSnowyHouse Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameSulphurPlant Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameTemplarShrine Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameValley Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameTank).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Ignore Special Or Edge)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' counter any one Special Card played by your opponent
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameSpecialCard Or
                                                               c.Id = CardLookup.LeMontagnard.PromotionalCards.SwordmasterPreGameKyala).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Counter Special Card)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' counter an Illusion as it is being played
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGameRIP1).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Counter Illusion)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' counter any effect from one pre-game card as it is activated
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGameRIP2).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Counter Pre-Game)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' counter any effect from one in-game card as it activated
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGameRIP3).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Counter In-Game)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' nullify the text on one Persona Specific card as it is played.
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGameRIP4).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Nullify Persona Specific)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' nullify the text on one Weapon of Choice Specific card
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGameRIP5).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Nullify Weapon of Choice Specific)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' Your opponent cannot play Edge or Special cards outside of their May Do / Must Do Phase next turn. This effect cannot be duplicated by Divine Intervention. 
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGameRIP8).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Cannot play Edge or Special Cards)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' counter one Edge Card, Special Card, Illusion, or Attack the moment it is played or put into play. 
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.SwordmasterPreGame2021).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Counter Edge, Special, Illusion or Attack)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' remove one Edge, Special, Defense or Attack Card from play. 
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.WorldOfGameDesign.PromotionalCards.SwordmasterPreGame1 Or
                                                               c.Id = CardLookup.WorldOfGameDesign.PromotionalCards.SwordmasterPreGame2).ToList()
            If PreGameCardsToValidate.Count > 1 Then
                Errors.Add(New ValidationError("Swordmaster Pre-Game (Remove edge, Special, Defense or Attack)", "You may only include one copy of the Swordmaster Pre-Game."))
            End If

            ' End Swordmaster cards

            Return Errors
        End Function

        ''' <summary>
        ''' Convert a card into a value that uniquely identifies that version of the card.
        ''' </summary>
        ''' <param name="OneCard">The card being compared</param>
        ''' <param name="Format">The rules format being used</param>
        ''' <returns>A string representing this version of the card.</returns>
        Private Function GetGroup(ByVal OneCard As Card, ByVal Format As DeckFormat) As String
            Dim ReturnValue As String = String.Empty

            ReturnValue = String.Format("{0}|", OneCard.Immortal)

            If Format = DeckFormat.Type1 Or Format = DeckFormat.Type2 Or Format = DeckFormat.HighlanderPro Then
                ReturnValue &= Card.RemoveUnnecessaryVersion(OneCard.SecondEditionTitle, OneCard.Id)
            Else
                ReturnValue &= Card.RemoveUnnecessaryVersion(OneCard.Title, OneCard.Id)
            End If

            ReturnValue &= String.Format("|{0}|", OneCard.Type)

            Return ReturnValue.ToLower()
        End Function

    End Class
End Namespace
