﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule validates that your pre-game does not contain more than one Persona card.
    ''' </summary>
    Public Class PersonaLimitRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim PreGamePersonas As List(Of Card) = PreGame.Where(Function(c) c.Type.ToLower() = "persona").ToList()

            If PreGamePersonas.Count > 0 Then
                If PreGamePersonas.Sum(Function(c) c.Quantity) > 1 Then
                    Errors.Add(New ValidationError("*", "You may only use one Persona Pre-Game."))
                End If
            End If

            ' @TODO:  There are special scenarios like second edition Corda and Reno personas that I'll need to account for.

            Return Errors
        End Function

    End Class
End Namespace
