﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Validate that the total Gems used by all of the cards in the deck doesn't exceed the Persona cards limit (Type 2 Format only).
    ''' This rule also validates Master Card limit for First Edition / Type 1.
    ''' </summary>
    Public Class GemLimitsRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)
            ' Master card limit applies to all formats.
            If deckStats.DeckAttributes.Master > deckStats.PersonaAttributes.Master Then
                Errors.Add(New ValidationError("*", "Master card limit exceeded."))
            End If

            ' The rest of the gems only apply to type 2 format.
            If Format = DeckFormat.Type2 Or
                Format = DeckFormat.HighlanderPro Then
                If deckStats.DeckAttributes.Agility > deckStats.PersonaAttributes.Agility Then
                    Errors.Add(New ValidationError("*", "Agility Gem Limit Exceeded."))
                End If

                If deckStats.DeckAttributes.Strength > deckStats.PersonaAttributes.Strength Then
                    Errors.Add(New ValidationError("*", "Strength Gem Limit Exceeded."))
                End If

                If deckStats.DeckAttributes.Toughness > deckStats.PersonaAttributes.Toughness Then
                    Errors.Add(New ValidationError("*", "Toughness Gem Limit Exceeded."))
                End If

                If deckStats.DeckAttributes.Empathy > deckStats.PersonaAttributes.Empathy Then
                    Errors.Add(New ValidationError("*", "Empathy Gem Limit Exceeded."))
                End If

                If deckStats.DeckAttributes.Reason > deckStats.PersonaAttributes.Reason Then
                    Errors.Add(New ValidationError("*", "Reason Gem Limit Exceeded."))
                End If
            End If

            Return Errors
        End Function

    End Class
End Namespace
