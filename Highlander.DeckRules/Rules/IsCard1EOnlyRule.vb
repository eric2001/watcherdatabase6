﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Some second edition cards indicate they are only playable in Type 1.  This rule checks to make sure those cards aren't in a type 2 deck.
    ''' </summary>
    Public Class IsCard1EOnly : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim BannedCards As List(Of Card) = GetBannedCards(PreGame, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "Card is banned in this format."))
            Next

            For Each OneSubCard As Card In PreGame
                If OneSubCard.SubItems.Count > 0 Then
                    BannedCards = GetBannedCards(OneSubCard.SubItems, Format)
                    For Each OneCard In BannedCards
                        Errors.Add(New ValidationError(Format, OneCard, "Card is banned in this format."))
                    Next
                End If
            Next

            BannedCards = GetBannedCards(Deck, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "Card is banned in this format."))
            Next

            Return Errors
        End Function

        Private Function GetBannedCards(ByVal Cards As List(Of Card), Format As DeckFormat) As List(Of Card)
            Dim BannedCards As New List(Of Card)

            '@TODO:  Redo with a ban cards list instead of this if block

            If Format = DeckFormat.Type2 Then
                BannedCards = Cards.Where(Function(c) c.Id = CardLookup.LeMontagnard.ConnorVsDuncan.ConnorMacLeodPersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnard.ConnorVsDuncan.DuncanMacLeodPersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnard.PromotionalCards.ColinMacLeodPersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnard.PromotionalCards.FeliceMartinPersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnard.PromotionalCards.JinKePersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnard.PromotionalCards.KyalaPersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnard.PromotionalCards.MarcusOctaviusPersona1stEdition Or
                                                      c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.HughFitzcairnPersonaType1 Or
                                                      c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.KiemSunPersonaType1).ToList()
            End If

            Return BannedCards
        End Function

    End Class
End Namespace
