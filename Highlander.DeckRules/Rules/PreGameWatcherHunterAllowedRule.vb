﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Runtime.InteropServices
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Some Personas / Watchers / Hunters have restrictions on which Watcher / Hunter can be used in conjunction with the Persona.
    ''' This rule checks for those restrictions.
    ''' </summary>
    Public Class PreGameWatcherHunterAllowedRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            'The Adam Pierson Watcher Pre-Game can only be used with the Methos Persona
            Dim PreGameCardsToValidate As List(Of Card) = PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.MethosCollection.AdamPiersonPreGame).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Persona" And c.Immortal = "Methos").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, PreGameCardsToValidate(0), "The Adam Pierson Watcher Pre-Game can only be used with the Methos Persona."))
                End If
            End If

            'MC\Methos Persona -- Methos can only use adam as a watcher, any other watchers are banned.  Methos is not required to use a Watcher, so normal rules xml doesn't work for this.
            PreGameCardsToValidate = PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.MethosCollection.MethosPersona).ToList()
            If PreGameCardsToValidate.Count > 0 Then
                Dim PreGameWatchers As List(Of Card) = PreGame.Where(Function(c) (c.Type = "Watcher" Or c.Type = "Hunter") And c.Immortal <> "Adam Pierson").ToList()
                For Each OneCard In PreGameWatchers
                    Errors.Add(New ValidationError(Format, OneCard, "The Methos Persona may only use Adam Pierson as a Watcher."))
                Next
            End If

            Return Errors
        End Function

    End Class
End Namespace
