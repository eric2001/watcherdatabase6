﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Deck construction requirements specifically for Quickening Pre-Game (Dark Quickening) (Q2018-999)
    '''   Place a Persona Card you are not playing as under this card.
    '''   This card cannot be used with Corda and/or Reno.
    '''   This card cannot be used with certain persona cards that specifically don't allow it.
    ''' </summary>
    Public Class TheDarkQuickeningRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim DarkQuickening As List(Of Card) = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.ConnorMacLeodVsSlanQuince.QuickeningPreGameDarkQuickening).ToList()
            If DarkQuickening.Count > 0 Then
                Dim PersonaCards As List(Of Card) = PreGame.Where(Function(c) c.Type.ToLower() = "persona").ToList()

                For Each OneCard As Card In DarkQuickening
                    ' Validate that each The Dark Quickening has exactly one card underneath it
                    If OneCard.SubItems.Sum(Function(c) c.Quantity) > OneCard.Quantity Then
                        Errors.Add(New ValidationError(Format, OneCard, "You may only place one card underneath The Dark Quickening."))
                    End If
                    If OneCard.SubItems.Count = 0 OrElse OneCard.SubItems.Sum(Function(c) c.Quantity) < OneCard.Quantity Then
                        Errors.Add(New ValidationError(Format, OneCard, "You must place one Persona card underneath The Dark Quickening."))
                    End If

                    ' Validate that The Dark Quickening only has Persona cards underneath it.
                    If OneCard.SubItems.Where(Function(c) c.Type.ToLower <> "persona").Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "You may only place Persona cards underneath The Dark Quickening."))
                    End If

                    ' Validate that The Dark Quickening doesn't have the deck's persona card underneath it.
                    For Each PersonaCard As Card In PersonaCards
                        If OneCard.SubItems.Where(Function(c) c.Immortal.ToLower() = PersonaCard.Immortal.ToLower()).Count > 0 Then
                            Errors.Add(New ValidationError(Format, OneCard, "You may not use the Persona Card you are not playing as under The Dark Quickening."))
                        End If
                    Next

                    ' Validate that only valid persona cards are underneath The Dark Quickening
                    If OneCard.SubItems.Where(Function(c) c.Id = CardLookup.LeMontagnard.TheQuickening.CordaPersona).Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "The Dark Quickening cannot be used with the Corda Persona."))
                    End If
                    If OneCard.SubItems.Where(Function(c) c.Id = CardLookup.LeMontagnard.TheQuickening.RenoPersona).Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "The Dark Quickening cannot be used with the Reno Persona."))
                    End If

                    If OneCard.SubItems.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.GenericImmortalPersonaNullifyIfSuccessful).Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "The Dark Quickening cannot be used with the Generic Immortal (Nullify If Successful) Persona."))
                    End If
                    If OneCard.SubItems.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaDavidDetlefs).Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "The Dark Quickening cannot be used with the Generic Immortal (David Detlefs) Persona."))
                    End If
                    If OneCard.SubItems.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJeffSmorey).Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "The Dark Quickening cannot be used with the Generic Immortal (Jeff Smorey) Persona."))
                    End If
                    If OneCard.SubItems.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJimBlack).Count > 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, "The Dark Quickening cannot be used with the Generic Immortal (Jim Black) Persona."))
                    End If

                Next
            End If

            Return Errors
        End Function

    End Class
End Namespace
