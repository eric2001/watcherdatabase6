﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule validates that a Type 1 deck is not using any cards that use the Second Edition Only symbol.
    ''' </summary>
    Public Class IsCard2EOnly : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim BannedCards As List(Of Card) = GetBannedCards(PreGame, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "Card is banned in this format."))
            Next

            For Each OneSubCard As Card In PreGame
                If OneSubCard.SubItems.Count > 0 Then
                    BannedCards = GetBannedCards(OneSubCard.SubItems, Format)
                    For Each OneCard In BannedCards
                        Errors.Add(New ValidationError(Format, OneCard, "Card is banned in this format."))
                    Next
                End If
            Next

            BannedCards = GetBannedCards(Deck, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "Card is banned in this format."))
            Next

            Return Errors
        End Function

        Private Function GetBannedCards(ByVal Cards As List(Of Card), Format As DeckFormat) As List(Of Card)
            Dim BannedCards As New List(Of Card)
            If Format = DeckFormat.Type1 Then
                BannedCards = Cards.Where(Function(c) c.SecondEditionOnly = True).ToList()
            End If

            Return BannedCards
        End Function

    End Class
End Namespace
