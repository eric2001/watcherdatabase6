﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Validates that the deck doesn't contain any cards that the rulebooks say are banned.
    ''' </summary>
    Public Class BannedCardsRule : Implements IDeckConstructionRule

        ' List of ID numbers for Banned cards.
        Private BannedCardIds As New List(Of Integer)

        Public Sub New()
            ' 1E Player's Handbook:  Illegal Cards - Misprinted Premium cards from the Gathering Edition may not be used in tournaments 
            ' Sanctioned by Thunder Castle Games. Also, Thunder Castle Games often releases Promotional cards. These cards may Not be used
            ' in Sanctioned events until they have been officially released by Thunder Castle Games.

            '@TODO:  Not clear on if the above applies to type 1 as well or if I need separate 1E/Type1/Type2 lists?

            ' @TODO:  Replace hard-coded ids with CardLookup references

            BannedCardIds.Add(2436) ' Generic Attitude is Everything
            BannedCardIds.Add(2434) ' Generic Donna Lettow 1
            BannedCardIds.Add(2435) ' Generic Donna Lettow 2
            BannedCardIds.Add(2429) ' Iman Fasil The Face of Death
            BannedCardIds.Add(892) ' Generic Zeist
            BannedCardIds.Add(941) ' The Eyes Have It (Peeping Tom)
            BannedCardIds.Add(942) ' The Eyes Have It (In Your Sights)
            BannedCardIds.Add(943) ' The Eyes Have It (The Eyes Have it)
            BannedCardIds.Add(2433) ' Duncan MacLeod Premium (Misprint)
            BannedCardIds.Add(2432) ' Duncan Collection Quickening Misprint
            BannedCardIds.Add(817) ' Generic Season Six
            BannedCardIds.Add(4292) ' SAEC Games Playtest Card\Amanda Cat Burglar
            BannedCardIds.Add(4293) ' SAEC Games Playtest Card\Amanda Seductress
            BannedCardIds.Add(4294) ' SAEC Games Playtest Card\Clan MacLeod Honor is Satisfied
            BannedCardIds.Add(4295) ' SAEC Games Playtest Card\Generic Enhanced Skill
            BannedCardIds.Add(4258) ' SAEC Games Playtest Card\Generic Priceless Museum
            BannedCardIds.Add(4296) ' SAEC Games Playtest Card\Generic Slimbones Auctions
            BannedCardIds.Add(4297) ' SAEC Games Playtest Card\Generic Vanguard
            BannedCardIds.Add(4298) ' SAEC Games Playtest Card\Generic Waterloo
            BannedCardIds.Add(4262) ' 2E MAO\Generic The Danger Room
            BannedCardIds.Add(7956) ' Jan Stephan Lundquist 1
            BannedCardIds.Add(7957) ' Jan Stephan Lundquist 2
            BannedCardIds.Add(7035) ' Q2E\Quickening (HTGQ-036)
            BannedCardIds.Add(6912) ' PR2\Highlander the Video Game
            BannedCardIds.Add(6903) ' PR2\Golden Turkey
            BannedCardIds.Add(6908) ' PR2\There Can Be Only One (HTVG-002)
        End Sub

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim BannedCards As List(Of Card) = GetBannedCards(PreGame, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "This card is banned."))
            Next

            For Each OneSubCard As Card In PreGame
                If OneSubCard.SubItems.Count > 0 Then
                    BannedCards = GetBannedCards(OneSubCard.SubItems, Format)
                    For Each OneCard In BannedCards
                        Errors.Add(New ValidationError(Format, OneCard, "This card is banned."))
                    Next
                End If
            Next

            BannedCards = GetBannedCards(Deck, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "This card is banned."))
            Next

            ' SAEC Games Time and Trap Cards are banned in Type 1.
            If Format = DeckFormat.Type1 Then
                BannedCards = Deck.Where(Function(c) (c.SubType.ToLower() = "time" Or c.SubType.ToLower() = "trap") And c.Publisher.Id = PublisherLookup.Publisher.SaecGames).ToList()
                For Each OneCard In BannedCards
                    Errors.Add(New ValidationError(Format, OneCard, String.Format("SAEC Games {0} cards are not legal in Type 1.", OneCard.SubType)))
                Next
            End If

            Return Errors
        End Function

        Private Function GetBannedCards(ByVal Cards As List(Of Card), Format As DeckFormat) As List(Of Card)
            Return Cards.Where(Function(c) BannedCardIds.Contains(c.Id)).ToList()
        End Function

    End Class
End Namespace
