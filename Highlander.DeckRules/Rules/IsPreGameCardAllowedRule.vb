﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Runtime.InteropServices
Imports Highlander.Data
Imports System.Data

Namespace Rules

    ''' <summary>
    ''' This rule checks for Pre-Game cards that require specific cards.  
    ''' Ex: A Duncan MacLeod Premium pre-game requires the Duncan MacLeod Persona.
    ''' </summary>
    Public Class IsPreGameCardAllowedRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            ' Check if any pre-game cards are Persona / Weapon of Choice Specific (ex: +1's, MCBCs, etc).
            '  If it is, make sure the corresponding Persona or Weapon of Choice card is in the deck.
            Dim PreGameCardsToValidate As List(Of Card) = PreGame.Where(Function(c) c.Type.ToLower() = "pre-game").ToList()
            For Each OneCard In PreGameCardsToValidate

                Dim RequiredPreGameType As String = String.Empty
                If OneCard.IsImmortalSpecific() Then
                    RequiredPreGameType = "Persona"
                End If
                If OneCard.IsWeaponSpecific() Then
                    RequiredPreGameType = "Weapon of Choice"
                End If

                If Not String.IsNullOrEmpty(RequiredPreGameType) Then
                    Dim ImmortalName As String = OneCard.Immortal
                    If (PreGame.Where(Function(p) p.Immortal = ImmortalName And p.Type = RequiredPreGameType).ToList).Count = 0 Then
                        Errors.Add(New ValidationError(Format, OneCard, String.Format("This card requires the {0} {1} Pre-Game.", ImmortalName, RequiredPreGameType)))
                    End If
                End If
            Next

            Return Errors
        End Function

    End Class
End Namespace
