﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Text.RegularExpressions
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Validates that the none of the cards in the deck exceed the restriction limit of that card (normally 6 copies unless a card says otherwise).
    ''' </summary>
    Public Class DeckRestrictionLimitRule : Implements IDeckConstructionRule
        Private Class UniqueCard
            Public Title As String
            Public Quantity As Integer
            Public MaximumQuantity As Integer
        End Class

        Private DariusRestrictionLimit As Integer = 0
        Private MastersPrizeCount As Integer = 0
        Private SAECRebeccaHornePreGameCount As Integer = 0
        Private UniqueCardTitles As New List(Of UniqueCard)

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            'Identify if there are any cards that affect restriction limits
            CheckPreGameForExceptionCards(PreGame)

            ' Check restriction limits on each card in the deck
            Dim LocalFormat As DeckFormat = Format
            Dim CardsByGroup = Deck.GroupBy(Function(c) GetGroup(c, LocalFormat)).Select(Function(g) g.ToList()).ToList()

            For Each OneGroup In CardsByGroup
                If IsRestrictionLimitExceeded(OneGroup, Format, PreGame) Then
                    Errors.Add(New ValidationError(Format, OneGroup.FirstOrDefault(), "Restriction limit exceeded."))
                End If
            Next

            ' In addition to the individual restriction limit, we're also limited to 6 cards of each title (unless something special happened)
            For Each OneTitle In UniqueCardTitles
                ' Check total number of cards found with this title against the title's limit
                If OneTitle.Quantity > OneTitle.MaximumQuantity Then
                    Errors.Add(New ValidationError(OneTitle.Title, String.Format("You have more then {0} cards called {1} in your deck.", OneTitle.MaximumQuantity, OneTitle.Title)))
                End If
            Next

            Return Errors
        End Function

        ''' <summary>
        ''' Convert a card into a value that uniquely identifies that version of the card.
        ''' </summary>
        ''' <param name="OneCard">The card being compared</param>
        ''' <param name="Format">The rules format being used</param>
        ''' <returns>A string representing this version of the card.</returns>
        Private Function GetGroup(ByVal OneCard As Card, ByVal Format As DeckFormat) As String
            Dim ReturnValue As String = String.Empty

            ReturnValue = String.Format("{0}|", OneCard.Immortal)

            If Format = DeckFormat.Type1 Or Format = DeckFormat.Type2 Or Format = DeckFormat.HighlanderPro Then
                ReturnValue &= Card.RemoveUnnecessaryVersion(OneCard.SecondEditionTitle, OneCard.Id)
            Else
                ReturnValue &= Card.RemoveUnnecessaryVersion(OneCard.Title, OneCard.Id)
            End If

            ReturnValue &= String.Format("|{0}|", OneCard.Type)

            Return ReturnValue.ToLower()
        End Function

        ''' <summary>
        '''Identify cards that affect restriction limits
        ''' </summary>
        ''' <param name="PreGame"></param>
        Private Sub CheckPreGameForExceptionCards(ByRef PreGame As List(Of Card))

            ' TCG PR\Darius Pre-Game (Restriction) -- increases restriction limit on one card.  Track for later.
            Dim cards As List(Of Card) = PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.PromotionalCards.DariusPreGameRestriction).ToList()
            If cards.Count > 0 Then
                DariusRestrictionLimit += cards.Sum(Function(c) c.Quantity)
            End If

            'MAO\Master's Prize Pre-Game -- increases restriction number of one card from 6 to 7.  Must already be allowed 6 copies.
            cards = PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.Misprints.MastersPrizePreGameMisprint1 Or
                                  c.Id = CardLookup.ThunderCastleGames.Misprints.MastersPrizePreGameMisprint2 Or
                                  c.Id = CardLookup.ThunderCastleGames.Misprints.MastersPrizePreGameMisprint3 Or
                                  c.Id = CardLookup.ThunderCastleGames.Misprints.MastersPrizePreGameMisprint4).ToList()
            If cards.Count > 0 Then
                MastersPrizeCount += cards.Sum(Function(c) c.Quantity)
            End If

            ' SAEC BR3\Rebecca Horne Pre-Game (Additional Powers) -- allows one extra dodge card over the restriction.
            cards = PreGame.Where(Function(c) c.Id = CardLookup.SaecGames.BlackRavenVol3.RebeccaHorneAdditionalPowers).ToList()
            If cards.Count > 0 Then
                SAECRebeccaHornePreGameCount += cards.Sum(Function(c) c.Quantity)
            End If

        End Sub

        Private Function IsRestrictionLimitExceeded(ByVal CardsGroup As List(Of Card), Format As DeckFormat, ByRef PreGame As List(Of Card)) As Boolean
            ' Determine the Restriction limit for the current card (default is 6 unless the card says otherwise).
            Dim intRestrictionLimit As Integer = 6 ' Default limit is 6 unless the card says otherwise

            ' Check and see if the card specifies an alternate limit
            Dim OneCard As Card = CardsGroup.FirstOrDefault()
            Dim CardRestrictions As String = OneCard.Restricted
            If CardRestrictions <> "" Then
                Dim strRestrictions As String = CardRestrictions
                Dim strRestrictionCount As String = strRestrictions.ToLower.Replace("s", "").Replace("r", "") ' Remove Reserved or Signatured indicator, if present.  We only care about the quantity here.
                If IsNumeric(strRestrictionCount) Then
                    intRestrictionLimit = Convert.ToInt32(strRestrictionCount)
                End If
            End If

            ' If we're under the restction limit, then the card is good, we don't need to check for any special cases.
            If CardsGroup.Sum(Function(c) c.Quantity) <= intRestrictionLimit Then
                TrackUniqueCardTitles(CardsGroup, Format, PreGame, intRestrictionLimit)
                Return False
            End If

            ' A&T\Generic Immortal Persona WoC -- If you are using a Weapon of Choice, you may increase the Restriction of all Weapon Specific cards for that Weapon of Choice by one
            If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaWoC).Count > 0 Then
                ' Look for a card in the pre-game list of type Weapon of Choice where the immortal line is the same as the current line'
                '   If we find one, then this card is a WoC specific card, increase it's restriction number by one (limit of 6)
                Dim WeaponOfChoice As Integer = PreGame.Where(Function(p) p.Immortal = OneCard.Immortal AndAlso p.Type = "Weapon of Choice").Count()
                If WeaponOfChoice > 0 AndAlso Not CardRestrictions.ToLower.Contains("s") Then ' You generally can't increase the restriction limit of signatured cards (which shouldn't be an issue here as WoC Cards aren't normally signatured anyway)
                    If intRestrictionLimit < 6 Then
                        intRestrictionLimit += 1
                    End If
                End If
            End If

            ' Adjust for TCG PR\Darius Pre-Game (Restriction) -- increase the restriction number on a card you may normally include in your deck. You are allowed 1 extra card (maximum of 6)
            '   Cards from other Personas, whether included in your deck through the use of a Quickening, Persona Power, Darius: Event, Or some other means, are Not "normally" allowed in your deck.
            '@TODO:  Once Darius Event is coded I should circle back around to this and see if I need to adjust anything to stop it from passing a card that you "may not normally play".
            If CardsGroup.Sum(Function(c) c.Quantity) > intRestrictionLimit Then
                If intRestrictionLimit < 6 And DariusRestrictionLimit > 0 Then
                    If Not CardRestrictions.ToLower.Contains("s") Then ' can't increase if signatured
                        ' You are normally allowed generic cards, and cards that are specific to your pre-game cards.  So check and see if that's the case here.
                        If OneCard.IsGeneric Then
                            intRestrictionLimit += 1
                            DariusRestrictionLimit -= 1
                        Else
                            Dim AllowedByPreGame As Integer = PreGame.Where(Function(p) p.Immortal = OneCard.Immortal).Count
                            If AllowedByPreGame > 0 Then
                                intRestrictionLimit += 1
                                DariusRestrictionLimit -= 1
                            End If
                        End If
                    End If
                End If
            End If

            ' Adjust for Master's Prize. -- you may increase one card from 6 to 7.
            If intRestrictionLimit = 6 And CardsGroup.Sum(Function(c) c.Quantity) = 7 Then
                If Not CardRestrictions.ToLower.Contains("s") Then ' Cannot increase limit on signatured cards.
                    If MastersPrizeCount > 0 Then
                        MastersPrizeCount -= 1
                        intRestrictionLimit += 1
                    End If
                End If
            End If

            ' Xavier St. Cloud -- You may include up to twice the normal number of Plot cards
            '  Determine if card is a Plot.  1st edition uses sub type plot, second edition uses type plot
            If OneCard.Type.Contains("Plot") OrElse OneCard.SubType.Contains("Plot") OrElse OneCard.SecondEditionSubType.Contains("Plot") Then
                If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.SeriesEdition.XavierStCloudPersona).Count > 0 OrElse
                PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.TinSet.XavierStCloudPersona).Count > 0 OrElse
                PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.PromotionalCards.QuickeningXavierStCloud).Count > 0 Then
                    If Not CardRestrictions.ToLower.Contains("s") Then ' Cannot increase limit on signatured cards.
                        intRestrictionLimit *= 2
                    End If
                End If
            End If

            ' SAEC BR3\Rebecca Horne Pre-Game (Additional Powers) -- You may add 1 extra non-Master Dodge type card over the restriction (maximum of 6)
            If (OneCard.Type = "Dodge") And (SAECRebeccaHornePreGameCount > 0) Then
                If (intRestrictionLimit < 6) And (CardsGroup.Sum(Function(c) c.Quantity) = intRestrictionLimit + 1) Then
                    If Not OneCard.IsMasterCard Then
                        If Not CardRestrictions.ToLower.Contains("s") Then ' Cannot increase limit on signatured cards.
                            SAECRebeccaHornePreGameCount -= 1
                            intRestrictionLimit += 1
                        End If
                    End If
                End If
            End If

            TrackUniqueCardTitles(CardsGroup, Format, PreGame, intRestrictionLimit)
            ' If we're over the restriction limit, display a warning message.
            If CardsGroup.Sum(Function(c) c.Quantity) > intRestrictionLimit Then
                Return True
            End If

            Return False
        End Function

        Private Sub TrackUniqueCardTitles(ByRef CardsGroup As List(Of Card), Format As DeckFormat, ByRef PreGame As List(Of Card), ByVal intRestrictionLimit As Integer)
            ' @TODO:  Need to update this logic to support duel cards (ex:  Title 1 / Title 2)
            Dim intMaxCopiesOfEachTitle As Integer = 6 ' Normal limit is 6 of each card title.

            ' Figure out the current card title, without any comment / sub title info in ().
            Dim CardTitle As String
            Dim OneCard As Card = CardsGroup.FirstOrDefault()

            If Format = DeckFormat.Type1 Or Format = DeckFormat.Type2 Or Format = DeckFormat.HighlanderPro Then
                CardTitle = OneCard.SecondEditionTitle
            Else
                CardTitle = OneCard.Title
            End If

            If CardTitle.Contains("(") Then
                CardTitle = CardTitle.Substring(0, CardTitle.IndexOf("(")).Trim
            End If

            ' If we're legally allowed more then 6 copies of a card (Ex: Master's Prize, cards with restriction numbers higher then 6, etc)
            ' then set the maximum allowed for this title to the restriction limit.
            Dim intQuantity As Integer = CardsGroup.Sum(Function(c) c.Quantity)
            If intRestrictionLimit > intMaxCopiesOfEachTitle Then
                intMaxCopiesOfEachTitle = intRestrictionLimit
            End If

            ' 1E Xavier Persona / Quickening -- You may include up to twice the normal number of Plot cards.
            If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.SeriesEdition.XavierStCloudPersona).Count > 0 OrElse
                PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.TinSet.XavierStCloudPersona).Count > 0 OrElse
                PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.PromotionalCards.QuickeningXavierStCloud).Count > 0 Then
                If OneCard.Type.Contains("Plot") OrElse OneCard.SubType.Contains("Plot") OrElse OneCard.SecondEditionSubType.Contains("Plot") Then
                    intMaxCopiesOfEachTitle = 12
                End If
            End If

            ' Generic Immortal Persona (Standing Defenses) (2017SS-004) -- You may include up to nine cards titled "Guard" in your deck.
            If CardTitle.ToLower() = "guard" Then
                If PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaStandingDefenses).Count > 0 Then
                    intMaxCopiesOfEachTitle = 9
                End If
            End If

            ' Michael Kent Persona (MKC-001) -- You may include up to 8 cards titled Slash in your deck.
            If CardTitle.ToLower() = "slash" Then
                If PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.MichaelKentCollection.MichaelKentPersona).Count > 0 Then
                    intMaxCopiesOfEachTitle = 8
                End If
            End If

            ' Keep a count of the number of cards with this title that are in the deck 
            '   Ex:  If you have 6 SE\Upper Left Attack and 6 Tin\Upper Left attack, each card is a unique record, 
            '   but you can still only have a total of 6 between them.  This will allow us to track that sort of thing.
            '   Note:  This is probably a bad example, something like Generic Watcher were there are different cards with the same title would be better.
            Dim UniqueTitle As UniqueCard = UniqueCardTitles.Where(Function(t) t.Title = CardTitle).FirstOrDefault()
            If UniqueTitle IsNot Nothing Then
                UniqueTitle.Quantity += intQuantity
            Else
                Dim NewUniqueTitle As New UniqueCard With {
                    .Title = CardTitle,
                    .Quantity = intQuantity,
                    .MaximumQuantity = intMaxCopiesOfEachTitle
                }
                UniqueCardTitles.Add(NewUniqueTitle)
            End If

        End Sub

    End Class
End Namespace
