﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Validate that the deck doesn't contain any Sub Titles that aren't allowed (Immortal Specific, Weapon of Choice Specific, etc)
    ''' </summary>
    Public Class DeckSubTitleRestrictions : Implements IDeckConstructionRule

        Private Class SubTitle
            Public Immortal As String
            Public Quantity As Integer
        End Class

        Private GenericImmortalPersonaResAllowedCards As Integer = 3

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            '  Any in game card that's not a Generic requires a corresponding pre-game card in order to use.
            '    Ex:  Persona's, Weapon of Choice cards, Watcher/Hunter, etc.
            '    This goes through the list of Reserved Types used in the deck, discounts anything that is allowed
            '    by the deck's pregame cards or is generic, then checks the remaining list to see if there are any
            '    "special" cards (ex: SE Darius) that allow other Reserved Types to be included in the deck.

            Dim ImmortalsUsed As New List(Of String) ' Used to track how many different immortals cards are used in the deck, for personas that only allow 1 other immortal.

            ' Used to keep track of how many cards an immortal is allowed to use from another immortal (normally 0)
            Dim AllowedCardsFromOtherImmortals As Integer = 0

            If PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.GenericImmortalPersonaNullifyIfSuccessful Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaStandingDefenses Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaExertions Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaDrawDiscard Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaAD Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaSteveRice Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaShaneRobbins Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaNigelKeates Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJimBlack Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJeffSmorey Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaDavidRobbins Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaDavidDetlefs Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJasonHasis).Count > 0 Then
                AllowedCardsFromOtherImmortals = deckStats.PersonaAttributes.Master
            End If

            Dim DeckSubTitles As List(Of SubTitle) = GetListOfSubTitles(Deck)

            For Each ImmortalType As SubTitle In DeckSubTitles.OrderByDescending(Function(c) c.Quantity)
                ' Eliminate Generics and Anything that matches to a Pre-Game Card.
                If ImmortalType.Immortal <> "Generic" Then ' If Generic then it's automatically allowed.

                    ' Build a list of all cards for this sub title.
                    Dim PersonaSpecificCards As List(Of Card) = Deck.Where(Function(d) d.Immortal = ImmortalType.Immortal OrElse (ImmortalType.Immortal <> "Michael Moore / Quentin Barnes" AndAlso d.Immortal.Contains(ImmortalType.Immortal) AndAlso d.Immortal.Contains("/"))).ToList()

                    ' First, check and see if the card corresponds with something in the pre-game list.
                    If PersonaSpecificCards.Count > 0 Then
                        For Each PreGameCard As Card In PreGame
                            If ImmortalType.Immortal = PreGameCard.Immortal Then
                                ' Filter out matching cards.
                                PersonaSpecificCards.Clear()
                            End If
                        Next
                    End If

                    ' Additional handling for duel persona cards -- make sure we don't display an error if we have one persona but not both.
                    If PersonaSpecificCards.Count > 0 Then
                        Dim DuelCards As List(Of Card) = PersonaSpecificCards.Where(Function(c) c.Immortal.Contains("/")).ToList
                        For Each DuelCard As Card In DuelCards
                            Dim OtherImmortal As String = DuelCard.Immortal.Replace(String.Format("{0} / ", ImmortalType.Immortal), "").Replace(String.Format(" / {0}", ImmortalType.Immortal), "")
                            If PreGame.Where(Function(c) c.Immortal = OtherImmortal).Count > 0 Then
                                PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> DuelCard.Id).ToList()
                            End If
                        Next
                    End If

                    ' Next, check and see if it's a watcher / hunter card.  These go by pre-game type, not pre-game immortal, so the previous check won't find them.
                    ' Also, note that it's using Contains so that if it's a duel immortal or duel type ("Watcher / Hunter"), it'll still pass.
                    If PersonaSpecificCards.Count > 0 AndAlso ImmortalType.Immortal.Contains("Watcher") Then
                        If PreGame.Where(Function(p) p.Type.Contains("Watcher")).Count > 0 Then
                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) Not c.Immortal.Contains("Watcher")).ToList()
                        End If

                        ' Check for Duel cards as well.
                        If PersonaSpecificCards.Count > 0 Then
                            Dim DuelCards As List(Of Card) = PersonaSpecificCards.Where(Function(c) c.Immortal.Contains("/")).ToList
                            For Each DuelCard As Card In DuelCards
                                Dim OtherImmortal As String = DuelCard.Immortal.Replace(String.Format("{0} / ", ImmortalType.Immortal), "").Replace(String.Format(" / {0}", ImmortalType.Immortal), "")
                                If PreGame.Where(Function(c) c.Type.Contains(OtherImmortal)).Count > 0 Then
                                    PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> DuelCard.Id).ToList()
                                End If
                            Next
                        End If
                    End If

                    If PersonaSpecificCards.Count > 0 AndAlso ImmortalType.Immortal.Contains("Hunter") Then
                        If PreGame.Where(Function(p) p.Type.Contains("Hunter")).Count > 0 Then
                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) Not c.Immortal.Contains("Hunter")).ToList()
                        End If

                        ' Check for Duel cards as well.
                        If PersonaSpecificCards.Count > 0 Then
                            Dim DuelCards As List(Of Card) = PersonaSpecificCards.Where(Function(c) c.Immortal.Contains("/")).ToList
                            For Each DuelCard As Card In DuelCards
                                Dim OtherImmortal As String = DuelCard.Immortal.Replace(String.Format("{0} / ", ImmortalType.Immortal), "").Replace(String.Format(" / {0}", ImmortalType.Immortal), "")
                                If PreGame.Where(Function(c) c.Type.Contains(OtherImmortal)).Count > 0 Then
                                    PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> DuelCard.Id).ToList()
                                End If
                            Next
                        End If
                    End If
                    ' End Watcher / Hunter logic

                    ' Faction Card Logic -- If it's a faction, and we're using an immortal that belongs to that faction, then filter out these cards, else display a warning.
                    If PersonaSpecificCards.Count > 0 AndAlso ImmortalType.Immortal.ToLower = "clan macleod" Then
                        If PreGame.Where(Function(c) c.Immortal.ToLower = "duncan macleod" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "connor macleod" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "colin macleod" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "kate macleod" And c.Type = "Persona").Count > 0 Then
                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Immortal <> ImmortalType.Immortal).ToList()
                        Else
                            For Each OneCard As Card In PersonaSpecificCards
                                Errors.Add(New ValidationError(Format, OneCard, "You may only use Clan MacLeod cards if you are using the Duncan, Connor, Colin or Kate MacLeod Personas."))
                                PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                            Next
                        End If
                    End If

                    If PersonaSpecificCards.Count > 0 AndAlso ImmortalType.Immortal.ToLower = "the four horsemen" Then
                        If PreGame.Where(Function(c) c.Immortal.ToLower = "methos" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "kronos" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "silas" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "caspian" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "war" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "death" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "famine" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "pestilence" And c.Type = "Persona").Count > 0 Then
                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Immortal <> ImmortalType.Immortal).ToList()
                        Else
                            For Each OneCard As Card In PersonaSpecificCards
                                Errors.Add(New ValidationError(Format, OneCard, "You may only use The Four Horsemen cards if you are using the Methos, Silas, Caspian, Kronos, War, Death, Famine or Pestilence Personas."))
                                PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                            Next
                        End If
                    End If

                    If PersonaSpecificCards.Count > 0 AndAlso ImmortalType.Immortal.ToLower = "romans" Then
                        If PreGame.Where(Function(c) c.Immortal.ToLower = "marcus octavius" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "marcus constantine" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "brother paul" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "kalas" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "cardinal giovanni" And c.Type = "Persona").Count > 0 OrElse
                            PreGame.Where(Function(c) c.Immortal.ToLower = "marcus korolus" And c.Type = "Persona").Count > 0 Then
                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Immortal <> ImmortalType.Immortal).ToList()
                        Else
                            For Each OneCard As Card In PersonaSpecificCards
                                Errors.Add(New ValidationError(Format, OneCard, "You may only use The Romans cards if you are using the Marcus Octavius, Marcus Constantine, Brother Paul, Kalas, Cardinal Giovanni or Marcus Korolus Personas."))
                                PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                            Next
                        End If
                    End If
                    ' End Faction logic

                    ' Methos / Methos Quickening "You may use Master cards from any Persona, even if Reserved"
                    If PersonaSpecificCards.Count > 0 AndAlso (PreGame.Where(Function(p) p.Id = CardLookup.ThunderCastleGames.PromotionalCards.QuickeningMethos).Count > 0 OrElse
                                                                PreGame.Where(Function(p) p.Id = CardLookup.ThunderCastleGames.MethosCollection.MethosPersona).Count > 0) Then
                        Dim PersonaSpecificMasterCards As List(Of Card) = PersonaSpecificCards
                        For Each OneCard As Card In PersonaSpecificMasterCards
                            If OneCard.IsMasterCard Then
                                If Not OneCard.Restricted.ToLower().Contains("s") Then
                                    PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList() ' remove all non-signatured master cards from the list
                                End If
                            End If
                        Next
                    End If

                    ' SAEC BR2\Father Liam Riley Pre-Game -- allows one non-sigature card from another immortal in your deck
                    Dim SAECFatherLiamRiley As Integer = 0
                    Dim cards As List(Of Card) = PreGame.Where(Function(c) c.Id = CardLookup.SaecGames.BlackRavenVol2.FatherLiamRiley).ToList()
                    If cards.Count > 0 Then
                        SAECFatherLiamRiley = cards.Sum(Function(c) c.Quantity)
                    End If

                    ' Father Liam Riley Pre-Game -- You may include one Non-Signature Master card from another Immortal in your deck
                    If SAECFatherLiamRiley > 0 Then
                        Dim PersonaSpecificMasterCards As List(Of Card) = PersonaSpecificCards
                        For Each OneCard As Card In PersonaSpecificMasterCards
                            If OneCard.IsMasterCard Then
                                If Not OneCard.Restricted.ToLower.Contains("s") Then
                                    If SAECFatherLiamRiley > OneCard.Quantity Then
                                        SAECFatherLiamRiley -= OneCard.Quantity
                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList() ' remove cards from the list.
                                    End If
                                End If
                            End If
                        Next
                    End If

                    ' SE and Tin Set In Game Darius -- You may include one card from another Persona in your deck -- count for later.
                    Dim InGameDariusCount As Integer = 0
                    cards = Deck.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.SeriesEdition.GenericDarius Or c.Id = CardLookup.ThunderCastleGames.TinSet.GenericDarius).ToList()
                    If cards.Count > 0 Then
                        InGameDariusCount = cards.Sum(Function(c) c.Quantity)
                    End If

                    ' 1E Promo -- Quickening Bob -- You may use one of any non-Reserved card from each Persona. 
                    If PersonaSpecificCards.Count > 0 Then
                        If PersonaSpecificCards.First.IsImmortalSpecific AndAlso Not PersonaSpecificCards.First.IsFactionSpecific Then
                            If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.PromotionalCards.QuickeningBob).Count > 0 Then
                                Dim CardsUsedForThisImmortal As Integer = 0
                                Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                                For Each OneCard As Card In ImmortalSpecificCards
                                    If (Not OneCard.Restricted.ToLower().Contains("s")) AndAlso (Not OneCard.Restricted.ToLower().Contains("r")) Then
                                        If OneCard.Quantity = 1 And CardsUsedForThisImmortal = 0 Then ' If one copy, and if this is the only card used for this immortal, allow it.
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                            CardsUsedForThisImmortal += 1
                                        ElseIf (CardsUsedForThisImmortal = 0) AndAlso (InGameDariusCount >= (ImmortalType.Quantity - 1)) Then ' If we can use one copy, and cover the rest with Darius, allow it.
                                            InGameDariusCount -= ImmortalType.Quantity - 1
                                            CardsUsedForThisImmortal += 1
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Immortal <> ImmortalType.Immortal).ToList()
                                        ElseIf (CardsUsedForThisImmortal > 0) AndAlso (InGameDariusCount >= ImmortalType.Quantity) Then ' If we can't use it at all, but we have in game darius, allow it.
                                            InGameDariusCount -= ImmortalType.Quantity
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Immortal <> ImmortalType.Immortal).ToList()
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    End If

                    ' A&T\Generic Immortal Persona Immortal Specific
                    ' Instead of using the Generic Immortal deck construction rules, you may include any number of Immortal Specific cards from one Persona that are not Reserved or Signature in your deck.
                    If PersonaSpecificCards.Count > 0 AndAlso PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaImmortalSpecific).Count > 0 Then
                        ' If we have in game darius cards, and if we've already assigned one immortal to this persona, then see if we can assign these cards to in game darius.

                        If ImmortalsUsed.Count > 0 And InGameDariusCount > 0 Then
                            Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                            For Each OneCard As Card In ImmortalSpecificCards
                                If (Not OneCard.Restricted.ToLower().Contains("s")) AndAlso (Not OneCard.Restricted.ToLower().Contains("r")) Then
                                    If InGameDariusCount >= OneCard.Quantity Then
                                        InGameDariusCount -= ImmortalType.Quantity
                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                    End If
                                End If
                            Next
                        End If

                        If PersonaSpecificCards.Count > 0 AndAlso (PersonaSpecificCards.First.IsImmortalSpecific And (Not PersonaSpecificCards.First.IsFactionSpecific)) Then
                            Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                            For Each OneCard As Card In ImmortalSpecificCards
                                If (Not OneCard.Restricted.ToLower.Contains("s")) AndAlso (Not OneCard.Restricted.ToLower.Contains("r")) Then
                                    PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList() ' remove all non-signatured master cards from the list
                                    If ImmortalsUsed.Where(Function(f) f = ImmortalType.Immortal).Count = 0 Then
                                        ImmortalsUsed.Add(ImmortalType.Immortal)
                                    End If
                                End If
                            Next
                        End If
                    End If

                    ' A&T Generic Immortal Persona (Res) -- You may include up to 3 of any Immortal Specific Reserved cards in your deck. For each Reserved card you include, your Master card limitation is reduced by one. 
                    If PersonaSpecificCards.Count > 0 AndAlso PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaRes).Count > 0 Then
                        If PersonaSpecificCards.First.IsImmortalSpecific AndAlso Not PersonaSpecificCards.First.IsFactionSpecific Then
                            Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                            For Each OneCard As Card In ImmortalSpecificCards
                                If OneCard.Restricted.ToLower.Contains("r") Then
                                    If (GenericImmortalPersonaResAllowedCards >= OneCard.Quantity) Then
                                        GenericImmortalPersonaResAllowedCards -= OneCard.Quantity

                                        ' Only decrease this for first edition so we don't have to re-set it after checking each individual format
                                        If Format = DeckFormat.FirstEdition Then
                                            deckStats.DeckAttributes.Master -= OneCard.Quantity
                                        End If

                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList() ' Persona power allows the card, remove card from the list
                                    End If
                                End If
                            Next
                        End If
                    End If

                    ' 2E Generic Immortal Persona
                    '   You may include and use one non-Reserved, non-Signature card from another Persona in your deck 
                    '   for each Master Card you are allowed. 
                    If PersonaSpecificCards.Count > 0 Then
                        If PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.GenericImmortalPersonaNullifyIfSuccessful Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaStandingDefenses Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaExertions Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaDrawDiscard Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaAD Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaSteveRice Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaShaneRobbins Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaNigelKeates Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJimBlack Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJeffSmorey Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaDavidRobbins Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaDavidDetlefs Or
                                 c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJasonHasis).Count > 0 Then
                            If PersonaSpecificCards.First.IsImmortalSpecific AndAlso Not PersonaSpecificCards.First.IsFactionSpecific Then
                                Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                                For Each OneCard As Card In ImmortalSpecificCards
                                    If (Not OneCard.Restricted.ToLower().Contains("s")) AndAlso (Not OneCard.Restricted.ToLower().Contains("r")) Then
                                        If (AllowedCardsFromOtherImmortals >= OneCard.Quantity) Then
                                            AllowedCardsFromOtherImmortals -= OneCard.Quantity
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList() ' Persona power allows the card, remove card from the list
                                        End If
                                    Else
                                        Errors.Add(New ValidationError(Format, OneCard, "Generic Immortals may not use Reserved or Signatured cards."))
                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                    End If
                                Next
                            End If
                        End If
                    End If

                    ' 0 = Generic Immortal Rules, 1457 = A&T\Generic Immortal Persona Res, 1724 = ME\Generic Immortal Persona, 1459 = A&T\Generic Immortal A&D, 1460 = A&T\Generic Immortal WoC Persona
                    If PersonaSpecificCards.Count > 0 Then
                        If PersonaSpecificCards.First.IsImmortalSpecific AndAlso Not PersonaSpecificCards.First.IsFactionSpecific Then
                            If PreGame.Where(Function(c) c.Type.ToLower() = "persona").Count = 0 OrElse
                            PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaRes).Count > 0 OrElse
                            PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.MovieEdition.GenericImmortalPersona).Count > 0 OrElse
                            PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaAD).Count > 0 OrElse
                            PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaWoC).Count > 0 Then
                                Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                                For Each OneCard As Card In ImmortalSpecificCards
                                    ' Generic Immortals may only use one of each Persona Specific Card, and none of their Reserved or Signature Cards.
                                    If (Not OneCard.Restricted.ToLower().Contains("s")) AndAlso (Not OneCard.Restricted.ToLower().Contains("r")) Then
                                        If OneCard.Quantity = 1 Then
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList() ' Allowed by generic immortal rules, remove from list.
                                        ElseIf InGameDariusCount >= (ImmortalType.Quantity - 1) Then
                                            ' If more then one copy, and if in game darius, see if we can apply the extra copies to darius.
                                            InGameDariusCount -= (ImmortalType.Quantity - 1)
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Immortal <> ImmortalType.Immortal).ToList()
                                        Else
                                            Errors.Add(New ValidationError(Format, OneCard, "Generic Immortals may only use one copy of each Persona Specific Card."))
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                        End If
                                    Else
                                        Errors.Add(New ValidationError(Format, OneCard, "Generic Immortals may not use Reserved or Signatured cards."))
                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                    End If
                                Next
                            End If
                        End If
                    End If

                    ' Richie Ryan -- You may use one non-Signature Immortal Specific card from each Persona. 
                    If PersonaSpecificCards.Count > 0 Then
                        If PersonaSpecificCards.First.IsImmortalSpecific AndAlso Not PersonaSpecificCards.First.IsFactionSpecific Then
                            If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.SeriesEdition.RichieRyanPersona Or c.Id = CardLookup.ThunderCastleGames.TinSet.RichieRyanPersona).Count > 0 Then
                                Dim CardsUsedForThisImmortal As Integer = 0
                                Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                                For Each OneCard As Card In ImmortalSpecificCards
                                    If Not OneCard.Restricted.ToLower().Contains("s") Then
                                        If OneCard.Quantity = 1 And CardsUsedForThisImmortal = 0 Then ' If one copy, and if this is the only card used for this immortal, allow it.
                                            Dim IdToRemove As Integer = OneCard.Id
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> IdToRemove).ToList()
                                            CardsUsedForThisImmortal += 1
                                        ElseIf (Not OneCard.Restricted.ToLower().Contains("r")) AndAlso (CardsUsedForThisImmortal = 0) AndAlso (InGameDariusCount >= (OneCard.Quantity - 1)) Then ' If we can use one copy, and cover the rest with Darius, allow it.
                                            InGameDariusCount -= OneCard.Quantity - 1
                                            CardsUsedForThisImmortal += 1
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                        ElseIf (Not OneCard.Restricted.ToLower().Contains("r")) AndAlso (CardsUsedForThisImmortal > 0) AndAlso (InGameDariusCount >= OneCard.Quantity) Then ' If we can't use it at all, but we have in game darius, allow it.
                                            InGameDariusCount -= OneCard.Quantity
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                        Else
                                            Errors.Add(New ValidationError(Format, OneCard, "Richie Ryan may only use one non-Signature Immortal Specific card from each Persona."))
                                            PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                        End If
                                    Else
                                        Errors.Add(New ValidationError(Format, OneCard, "Richie Ryan may only use one non-Signature Immortal Specific card from each Persona."))
                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                    End If
                                Next
                            End If
                        End If
                    End If


                    ' Darius -- You may include one card from another Persona in your deck. 
                    If PersonaSpecificCards.Count > 0 And InGameDariusCount > 0 Then
                        If PersonaSpecificCards.First.IsImmortalSpecific AndAlso Not PersonaSpecificCards.First.IsFactionSpecific Then
                            Dim ImmortalSpecificCards As List(Of Card) = PersonaSpecificCards
                            For Each OneCard As Card In ImmortalSpecificCards
                                If (Not OneCard.Restricted.ToLower().Contains("s")) AndAlso (Not OneCard.Restricted.ToLower().Contains("r")) Then
                                    If (InGameDariusCount >= OneCard.Quantity) Then
                                        InGameDariusCount -= OneCard.Quantity
                                        PersonaSpecificCards = PersonaSpecificCards.Where(Function(c) c.Id <> OneCard.Id).ToList()
                                    End If
                                End If
                            Next
                        End If
                    End If

                    ' Display a warning for anything left on the list.
                    For Each OneCard As Card In PersonaSpecificCards
                        Errors.Add(New ValidationError(Format, OneCard, String.Format("You are not allowed to use cards belonging to {0}.", ImmortalType.Immortal)))
                    Next
                End If
            Next

            ' A&T\Generic Immortal Persona Immortal Specific
            If ImmortalsUsed.Count > 1 And PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaImmortalSpecific).Count > 0 Then
                Errors.Add(New ValidationError("*", "You may only use Immortal Specific cards from a single Immortal."))
            End If

            Return Errors
        End Function

        Private Function GetListOfSubTitles(Deck As List(Of Card)) As List(Of SubTitle)
            Dim DeckSubTitles As List(Of SubTitle) = Deck.Where(Function(c) Not c.Immortal.Contains("/")).ToList().GroupBy(Function(c) c.Immortal).Select(Function(s) New SubTitle With {.Immortal = s.First.Immortal, .Quantity = s.Sum(Function(d) d.Quantity)}).ToList()
            Dim DuelSubTitles As List(Of SubTitle) = Deck.Where(Function(c) c.Immortal.Contains("/")).ToList().GroupBy(Function(c) c.Immortal).Select(Function(s) New SubTitle With {.Immortal = s.First.Immortal, .Quantity = s.Sum(Function(d) d.Quantity)}).ToList()

            For Each DuelSubTitle As SubTitle In DuelSubTitles
                If DuelSubTitle.Immortal = "Michael Moore / Quentin Barnes" Then ' not actually a sub-title
                    DeckSubTitles.Add(New SubTitle With {.Immortal = DuelSubTitle.Immortal, .Quantity = DuelSubTitle.Quantity})
                Else
                    Dim SubTitles() As String = DuelSubTitle.Immortal.Split("/")
                    For Each SubTitle In SubTitles
                        Dim MatchedTitle As SubTitle = DeckSubTitles.Where(Function(c) c.Immortal = SubTitle).FirstOrDefault()
                        If MatchedTitle Is Nothing Then
                            DeckSubTitles.Add(New SubTitle With {.Immortal = SubTitle, .Quantity = DuelSubTitle.Quantity})
                        Else
                            MatchedTitle.Quantity += DuelSubTitle.Quantity
                        End If
                    Next
                End If
            Next

            Return DeckSubTitles
        End Function

    End Class
End Namespace
