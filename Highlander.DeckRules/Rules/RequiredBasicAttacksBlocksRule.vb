﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule validates that your deck contains one of each of the required basic attacks and blocks.
    ''' </summary>
    Public Class RequiredBasicAttacksBlocksRule : Implements IDeckConstructionRule
        Private Class RequiredCard
            Public Title As String
            Public Quantity As Integer
        End Class

        Private BasicAttacksAndBlocks As New List(Of RequiredCard)

        Public Sub New()

            ' Initialize List of required basic attacks / blocks
            For counter As Integer = 0 To 14
                Dim OneCard As New RequiredCard
                OneCard.Quantity = 0
                BasicAttacksAndBlocks.Add(OneCard)
            Next

            ' Set Titles for each attack and block on the list.
            BasicAttacksAndBlocks(0).Title = "Upper Left Attack"
            BasicAttacksAndBlocks(1).Title = "Upper Center Attack"
            BasicAttacksAndBlocks(2).Title = "Upper Right Attack"
            BasicAttacksAndBlocks(3).Title = "Middle Left Attack"
            BasicAttacksAndBlocks(4).Title = "Thrust"
            BasicAttacksAndBlocks(5).Title = "Middle Right Attack"
            BasicAttacksAndBlocks(6).Title = "Lower Left Attack"
            BasicAttacksAndBlocks(7).Title = "Lower Center Attack"
            BasicAttacksAndBlocks(8).Title = "Lower Right Attack"
            BasicAttacksAndBlocks(9).Title = "Upper Left Block"
            BasicAttacksAndBlocks(10).Title = "Upper Center Block"
            BasicAttacksAndBlocks(11).Title = "Upper Right Block"
            BasicAttacksAndBlocks(12).Title = "Lower Left Block"
            BasicAttacksAndBlocks(13).Title = "Lower Center Block"
            BasicAttacksAndBlocks(14).Title = "Lower Right Block"
        End Sub

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)
            Dim UsedAttackBlockIds As New List(Of Integer) ' Track Ids to prevent using the same card for both the attack and the block (for cards that are both attacks and blocks)

            ' Total up the quantities of each basic attack / block that's in the deck
            For Each RequiredAttackBlock As RequiredCard In BasicAttacksAndBlocks
                Dim MatchedAttacksBlocks As List(Of Card) = Deck.Where(Function(c) c.Title.Contains(RequiredAttackBlock.Title)).ToList()
                RequiredAttackBlock.Quantity = MatchedAttacksBlocks.Sum(Function(c) c.Quantity)
            Next

            ' Check for any special conditions that affect the requirements
            ApplySpecialAttackCards(Deck, PreGame, UsedAttackBlockIds)
            ApplySpecialDefenseCards(Deck, PreGame, UsedAttackBlockIds)
            ApplyPreGameCrystals(Deck, PreGame, UsedAttackBlockIds)
            ApplyGenericImmortalPersonaAD(Deck, PreGame, UsedAttackBlockIds)

            ' Display a warning for each basic attack / block that's still flagged as missing.
            Dim MissingAttacksBlocks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0).ToList
            For Each MissingCard As RequiredCard In MissingAttacksBlocks
                Errors.Add(New ValidationError("*", String.Format("You must include at least one {0} in your deck.", MissingCard.Title)))
            Next

            Return Errors
        End Function

        Private Sub ApplySpecialAttackCards(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef UsedAttackBlockIds As List(Of Integer))
            ' Some special attacks contain the text "You may include this card in your deck in place of a Basic Attack."
            '  Check for any of these and if found, apply in place of any missing basic attacks.

            Dim MissingAttacks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0 AndAlso b.Title.Contains("Attack") OrElse b.Title.Contains("Thrust")).ToList()
            If MissingAttacks.Count > 0 Then

                Dim SpecialAttacks As List(Of Card) = Deck.Where(Function(c) c.Id = CardLookup.OneShotGames.MichaelKentCollection.ConnorMacLeodSlashUpper OrElse
                                                                     c.Id = CardLookup.LeMontagnard.TheGathering.ConnorMacLeodSlashUpper OrElse
                                                                     c.Id = CardLookup.LeMontagnard.ConnorVsKurgan.ConnorMacLeodSlashUpper OrElse
                                                                     c.Id = CardLookup.OneShotGames.ConnorMacLeodVsSlanQuince.ConnorMacLeodSlashUpper OrElse
                                                                     c.Id = CardLookup.LeMontagnard.SeasonFour.JacobGalatiSniperShot OrElse
                                                                     c.Id = CardLookup.OneShotGames.Legacy3.RolandKantosIllusoryStrike OrElse
                                                                     c.Id = CardLookup.OneShotGames.Legacy3.XavierStCloudDifferentStrokesAdditionalDamage OrElse
                                                                     c.Id = CardLookup.OneShotGames.Legacy3.XavierStCloudDifferentStrokesPlot).ToList()
                If SpecialAttacks.Count > 0 Then
                    For Each OneAttack As Card In SpecialAttacks

                        Dim AttackQuantity As Integer = OneAttack.Quantity

                        For Each MissingAttack As RequiredCard In MissingAttacks
                            If AttackQuantity > 0 AndAlso MissingAttack.Quantity = 0 Then
                                MissingAttack.Quantity = 1
                                AttackQuantity -= 1

                                If Not UsedAttackBlockIds.Contains(OneAttack.Id) Then
                                    UsedAttackBlockIds.Add(OneAttack.Id)
                                End If
                            End If
                        Next
                    Next
                End If

            End If

        End Sub

        Private Sub ApplySpecialDefenseCards(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef UsedAttackBlockIds As List(Of Integer))
            ' Some special attacks contain the text "You may include this card in your deck in place of a Basic Block."
            '  Check for any of these and if found, apply in place of any missing basic attacks.

            Dim MissingBlocks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0 AndAlso b.Title.Contains("Block")).ToList()
            If MissingBlocks.Count > 0 Then

                Dim SpecialDefenses As List(Of Card) = Deck.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.SaifAlRashidMastersDodge).ToList()
                If SpecialDefenses.Count > 0 Then
                    For Each OneDefense As Card In SpecialDefenses

                        Dim DefenseQuantity As Integer = OneDefense.Quantity

                        For Each MissingBlock As RequiredCard In MissingBlocks
                            If DefenseQuantity > 0 AndAlso MissingBlock.Quantity = 0 Then
                                MissingBlock.Quantity = 1
                                DefenseQuantity -= 1

                                If Not UsedAttackBlockIds.Contains(OneDefense.Id) Then
                                    UsedAttackBlockIds.Add(OneDefense.Id)
                                End If
                            End If
                        Next
                    Next
                End If

            End If

        End Sub

        Private Sub ApplyPreGameCrystals(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef UsedAttackBlockIds As List(Of Integer))

            ' Check and see if there are any Crystals in the pr-game, as this will affect how many attacks / blocks are required
            Dim CrystalCount As Integer = 0
            Dim Crystals As List(Of Card) = PreGame.Where(Function(c) c.Immortal.ToLower() = "crystal").ToList()
            If Crystals.Count > 0 Then
                CrystalCount += Crystals.Sum(Function(c) c.Quantity)
            End If

            ' Crystal's allow you to get around the "You must have one of each basic attack and block" rule.
            '   Go through each required Attack / Block in the required cards list with a 0 quantity.
            '   For each one that's missing, swap it out with a crystal, if possible.
            'During deck construction, you may either substitute any 1 non-Special Attack for 1 Basic Attack or substitute any 1 Defense for 1 Basic Block for each Crystal associated with your deck.

            If CrystalCount > 0 Then
                ' For each basic attack we're missing, attempt to locate an alternate attack and substitute it in
                Dim MissingAttacks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0 AndAlso b.Title.Contains("Attack") OrElse b.Title.Contains("Thrust")).ToList()
                If MissingAttacks.Count > 0 Then
                    Dim DeckAttacks As List(Of Card) = Deck.Where(Function(d) d.Type.Contains("Attack") AndAlso (Not d.Type.Contains("Special") AndAlso Not d.Type.Contains("Ranged"))).ToList
                    For Each OneAttack As Card In DeckAttacks
                        Dim AttackTitle As String = OneAttack.Title
                        Dim AttackId As Integer = OneAttack.Id
                        If BasicAttacksAndBlocks.Where(Function(b) b.Title = AttackTitle).Count = 0 AndAlso UsedAttackBlockIds.Where(Function(b) b = AttackId).Count = 0 Then
                            Dim CurrentQuantity As Integer = OneAttack.Quantity
                            For Each MissingAttack As RequiredCard In MissingAttacks
                                If CurrentQuantity > 0 AndAlso CrystalCount > 0 AndAlso MissingAttack.Quantity = 0 Then
                                    MissingAttack.Quantity = 1
                                    CurrentQuantity -= 1
                                    CrystalCount -= 1
                                    UsedAttackBlockIds.Add(OneAttack.Id)
                                End If
                            Next
                        End If
                    Next
                End If

                ' For each Basic Block we're missing, attempt to locate an alternate block and substitute it in.
                Dim MissingBlocks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0 AndAlso b.Title.Contains("Block")).ToList()
                If MissingBlocks.Count > 0 Then
                    Dim DeckBlocks As List(Of Card) = Deck.Where(Function(d) d.Type.Contains("Block") OrElse d.Type.Contains("Dodge")).ToList
                    For Each OneBlock As Card In DeckBlocks
                        Dim BlockTitle As String = OneBlock.Title
                        Dim BlockId As Integer = OneBlock.Id
                        If BasicAttacksAndBlocks.Where(Function(b) b.Title = BlockTitle).Count = 0 AndAlso UsedAttackBlockIds.Where(Function(b) b = BlockId).Count = 0 Then
                            Dim CurrentQuantity As Integer = OneBlock.Quantity
                            For Each MissingBlock As RequiredCard In MissingBlocks
                                If CurrentQuantity > 0 AndAlso CrystalCount > 0 AndAlso MissingBlock.Quantity = 0 Then
                                    MissingBlock.Quantity = 1
                                    CurrentQuantity -= 1
                                    CrystalCount -= 1
                                    UsedAttackBlockIds.Add(OneBlock.Id)
                                End If
                            Next
                        End If
                    Next
                End If
            End If
        End Sub

        Private Sub ApplyGenericImmortalPersonaAD(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef UsedAttackBlockIds As List(Of Integer))

            ' Generic Immortal (A&D) Persona -- You do not have to include the Basic Attacks and Defenses, but you must include at least 9 attacks and 6 blocks in your deck. 
            If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaAD Or
                                 c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaAD).Count > 0 Then

                ' For each basic attack we're missing, attempt to locate an alternate attack and substitute it in
                Dim MissingAttacks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0 AndAlso b.Title.Contains("Attack") OrElse b.Title.Contains("Thrust")).ToList()
                If MissingAttacks.Count > 0 Then

                    Dim DeckAttacks As List(Of Card) = New List(Of Card)
                    If PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericImmortalPersonaAD).Count > 0 Then
                        ' You do not have to include the Basic Attacks and Defenses, but you must include at least 9 attacks and 6 blocks in your deck. 
                        DeckAttacks = Deck.Where(Function(d) d.Type.Contains("Attack")).ToList()

                    ElseIf PreGame.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.SummerSpecial2017.GenericImmortalPersonaAD).Count > 0 Then
                        ' You are not required to include the 9 basic attacks and 6 basic blocks during deck construction, as long as you have 9 non-special attacks and 6 blocks.
                        DeckAttacks = Deck.Where(Function(d) d.Type.Contains("Attack") And Not (d.Type.Contains("Special") Or d.Type.Contains("Ranged"))).ToList()
                    End If

                    For Each OneAttack As Card In DeckAttacks
                            Dim AttackTitle As String = OneAttack.Title
                            Dim AttackId As Integer = OneAttack.Id
                            If BasicAttacksAndBlocks.Where(Function(b) b.Title = AttackTitle).Count = 0 AndAlso UsedAttackBlockIds.Where(Function(b) b = AttackId).Count = 0 Then
                                Dim CurrentQuantity As Integer = OneAttack.Quantity
                                For Each MissingAttack As RequiredCard In MissingAttacks
                                    If CurrentQuantity > 0 And MissingAttack.Quantity = 0 Then
                                        MissingAttack.Quantity = 1
                                        CurrentQuantity -= 1
                                        UsedAttackBlockIds.Add(OneAttack.Id)
                                    End If
                                Next
                            End If
                        Next
                    End If

                    ' For each Basic Block we're missing, attempt to locate an alternate block and substitute it in.
                    Dim MissingBlocks As List(Of RequiredCard) = BasicAttacksAndBlocks.Where(Function(b) b.Quantity = 0 AndAlso b.Title.Contains("Block")).ToList()
                If MissingBlocks.Count > 0 Then
                    Dim DeckBlocks As List(Of Card) = Deck.Where(Function(d) d.Type.Contains("Block")).ToList
                    For Each OneBlock As Card In DeckBlocks
                        Dim BlockTitle As String = OneBlock.Title
                        Dim BlockId As Integer = OneBlock.Id
                        If BasicAttacksAndBlocks.Where(Function(b) b.Title = BlockTitle).Count = 0 AndAlso UsedAttackBlockIds.Where(Function(b) b = BlockId).Count = 0 Then
                            Dim CurrentQuantity As Integer = OneBlock.Quantity
                            For Each MissingBlock As RequiredCard In MissingBlocks
                                If CurrentQuantity > 0 And MissingBlock.Quantity = 0 Then
                                    MissingBlock.Quantity = 1
                                    CurrentQuantity -= 1
                                    UsedAttackBlockIds.Add(OneBlock.Id)
                                End If
                            Next
                        End If
                    Next
                End If
            End If
        End Sub

    End Class
End Namespace
