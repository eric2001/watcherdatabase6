﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Type 2 rules require that all decks contain a Persona Card in their pre-game.
    ''' Other formats allow for generic immortal decks without a persona, so this rule doesn't apply.
    ''' </summary>
    Public Class IsPersonaRequired : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            If Format = DeckFormat.Type2 Or
                Format = DeckFormat.HighlanderPro Then
                If PreGame.Where(Function(c) c.Type = "Persona").Count = 0 Then
                    Errors.Add(New ValidationError("*", "You must include a Persona in your Pre-Game list."))
                End If
            End If

            Return Errors
        End Function

    End Class
End Namespace
