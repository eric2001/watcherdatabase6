﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule checks any pre-game quickening cards to determine if they can be included in the deck.  
    ''' Deck construction rules do not allow you to play your Persona's corresponding quickening.
    ''' </summary>
    Public Class IsQuickeningAllowedRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim PersonaCards As List(Of Card) = PreGame.Where(Function(c) c.Type = "Persona").ToList()
            If PersonaCards.Count > 0 Then
                For Each OneCard In PersonaCards
                    Errors.AddRange(IsQuickeningAllowed(OneCard, PreGame, Format))
                Next
            Else
                ' Generic Immortals cannot use their own quickenings
                Dim GenericQuickenings As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Generic Immortal")).ToList()
                For Each OneCard As Card In GenericQuickenings
                    Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                Next
            End If

            Return Errors
        End Function

        Private Function IsQuickeningAllowed(ByRef CurrentCard As Card, ByRef PreGame As List(Of Card), Format As DeckFormat) As List(Of ValidationError)
            Dim Errors As New List(Of ValidationError)
            ' Check if the current card is a persona.  If it is, check for any quicking cards that belong to this persona and display a warning if found.
            '  Doing this in a loop in case multiple personas.

            ' Note:  In addition to the below logic, there's a check in the Generic Immortal section of IdentifyPersona to warn on Generic Immortal / Generic Immortal quickenings.

            If CurrentCard.Type.Contains("Persona") Then
                Dim DefaultQuickingTitle As String = String.Format("Pre-Game ({0}", CurrentCard.Immortal)
                Dim PreGameQuickenings As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith(DefaultQuickingTitle)).ToList()
                For Each OneCard As Card In PreGameQuickenings
                    Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                Next

                ' Exception case -- Corda and Reno -- 2e has a seperate persona for each, and a seperate quickening for each.  however all in game cards are joint, so immortal line is also joint.
                If CurrentCard.Immortal = "Corda and Reno" Then
                    If CurrentCard.Title = "Corda Persona" Then
                        Dim CordaQuickening As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Corda")).ToList()
                        For Each OneCard As Card In CordaQuickening
                            Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                        Next
                    End If
                    If CurrentCard.Title = "Reno Persona" Then
                        Dim RenoQuickening As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Reno")).ToList()
                        For Each OneCard As Card In RenoQuickening
                            Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                        Next
                    End If
                End If

                ' Exception case -- Michael Moore / Quentin Barnes -- has two quickings for one persona.
                If CurrentCard.Immortal = "Michael Moore / Quentin Barnes" Then
                    Dim MmqbQuickenings As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso (c.Title.StartsWith("Pre-Game (Quentin Barnes") OrElse c.Title.StartsWith("Pre-Game (Michael Moore"))).ToList()
                    For Each OneCard As Card In MmqbQuickenings
                        Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                    Next
                End If

                ' Exception case -- The Dark Quickening corresponds to Dark Duncan
                If CurrentCard.Immortal = "Dark Duncan" Then
                    Dim DarkQuickening As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (The Dark Quickening")).ToList()
                    For Each OneCard As Card In DarkQuickening
                        Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                    Next
                End If

                ' Exception case -- The Flock -- multiple personas for The Flock, so quickening names don't match the default pattern
                If CurrentCard.Immortal = "The Flock" Then
                    If CurrentCard.Title.StartsWith("Persona (Cracker Bob") Then
                        Dim FlockQuickenings As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Cracker Bob")).ToList()
                        For Each OneCard As Card In FlockQuickenings
                            Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                        Next
                    End If
                    If CurrentCard.Title.StartsWith("Persona (Manny") Then
                        Dim FlockQuickenings As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Manny")).ToList()
                        For Each OneCard As Card In FlockQuickenings
                            Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                        Next
                    End If
                    If CurrentCard.Title.StartsWith("Persona (Winston") Then
                        Dim FlockQuickenings As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Winston")).ToList()
                        For Each OneCard As Card In FlockQuickenings
                            Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                        Next
                    End If
                End If
            End If

            ' Exception Case -- for some reason there's a quickening for a watcher
            If CurrentCard.Type = "Watcher" AndAlso CurrentCard.Immortal = "Ian Bancroft" AndAlso CurrentCard.Title.StartsWith("Pre-Game") Then
                Dim WatcherQuickening As List(Of Card) = PreGame.Where(Function(c) c.Immortal = "Quickening" AndAlso c.Type = "Quickening" AndAlso c.Title.StartsWith("Pre-Game (Ian Bancroft")).ToList()
                For Each OneCard As Card In WatcherQuickening
                    Errors.Add(New ValidationError(Format, OneCard, "You can't use your own Quickening."))
                Next
            End If

            Return Errors
        End Function

    End Class
End Namespace
