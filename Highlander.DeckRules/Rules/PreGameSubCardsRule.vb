﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Some pre-game cards (ex: Quickening Pre-Game (Dark Quickening) (Q2018-999)) allow you to place cards underneath them.
    ''' Validate that the only cards with other cards underneath them are the ones that allow it.
    ''' </summary>
    Public Class PreGameSubCardsRule : Implements IDeckConstructionRule

        ' List of ID numbers for cards that other cards can be placed underneath.
        Private CardIds As New List(Of Integer)

        Public Sub New()
            CardIds.Add(CardLookup.OneShotGames.ConnorMacLeodVsSlanQuince.QuickeningPreGameDarkQuickening)
        End Sub

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            For Each OneCard As Card In PreGame
                If OneCard.SubItems.Count > 0 Then
                    If Not CardIds.Contains(OneCard.Id) Then
                        Errors.Add(New ValidationError(Format, OneCard, "You may not play cards underneath this card."))
                    End If
                End If
            Next

            Return Errors
        End Function

    End Class
End Namespace
