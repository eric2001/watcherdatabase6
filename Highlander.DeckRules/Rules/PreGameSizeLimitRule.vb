﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This deck validates that the total number of pre-game cards in your deck doesn't exceed the limit (normally 6 cards)
    ''' </summary>
    Public Class PreGameSizeLimitRule : Implements IDeckConstructionRule

        ' List of card ID numbers that are pre-game cards but do not count twoards your Pre-Game limit.
        Private DoesNotCountTwoardsYourPreGameLimitIds As New List(Of Integer)

        Public Sub New()
            DoesNotCountTwoardsYourPreGameLimitIds.Add(CardLookup.WorldOfGameDesign.PromotionalCards.PrincesOfTheUniversePreGame)
        End Sub

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)
            Dim PreGameCardsLimit As Integer = 6 ' Maximum allowed number of pre-game cards.  Default is 6 unless something special changes the limit
            Dim TotalPreGameCards As Integer = PreGame.Sum(Function(c) c.Quantity) ' The total number of pre-game cards that count towards the above limit.  Note that certain cards (Crystals) do not count, so will not be tracked here.

            ' Crystals do not count towards you pre-game limit.
            Dim Cards As List(Of Card) = PreGame.Where(Function(c) c.Immortal.ToLower() = "crystal").ToList()
            If Cards.Count > 0 Then
                TotalPreGameCards -= Cards.Sum(Function(c) c.Quantity)
            End If

            ' Places do not count towards your pre-game limit.
            Cards = PreGame.Where(Function(c) c.Type = "Place").ToList()
            If Cards.Count > 0 Then
                TotalPreGameCards -= Cards.Sum(Function(c) c.Quantity)
            End If

            ' Rita Luce Pre-Game (LG3-103) -- If you are using the Michael Christian Persona, this card does not count towards your Pre-Game limit.
            Cards = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.RitaLucePreGame).ToList()
            If Cards.Count > 0 Then
                If PreGame.Where(Function(c) c.Immortal.ToLower() = "michael christian" And c.Type.ToLower = "persona").Count > 0 Then
                    TotalPreGameCards -= Cards.Sum(Function(c) c.Quantity)
                End If
            End If

            ' Generic Immortal Persona (Jason Hasis) (LG3G1-008)
            '   If you are using the Cutlass Weapon of Choice, it does not count towards your Pre-Game limit.
            If PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.GenericImmortalPersonaJasonHasis).Count > 0 Then
                Cards = PreGame.Where(Function(c) c.Type.ToLower() = "weapon of choice" And c.Immortal.ToLower = "cutlass").ToList()
                If Cards.Count > 0 Then
                    TotalPreGameCards -= Cards.Sum(Function(c) c.Quantity)
                End If
            End If

            ' Other cards that do not count towards your pre-game limit.
            Cards = PreGame.Where(Function(c) DoesNotCountTwoardsYourPreGameLimitIds.Contains(c.Id)).ToList()
            If Cards.Count > 0 Then
                TotalPreGameCards -= Cards.Sum(Function(c) c.Quantity)
            End If

            ' DC\Duncan MacLeod Premium (2 additional Pre-Game Cards) -- increases pre-game card limit
            Cards = PreGame.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.DuncanCollection.DuncanMacLeodPremiumAdditionalCards).ToList()
            If Cards.Count > 0 Then
                PreGameCardsLimit += (Cards.Sum(Function(c) c.Quantity) * 2)
            End If

            ' Display a warning if the deck has too many pre-game cards.
            If TotalPreGameCards > PreGameCardsLimit Then
                Errors.Add(New ValidationError("*", String.Format("You may only use up to {0} Pre-Game cards.", PreGameCardsLimit)))
            End If

            Return Errors
        End Function

    End Class
End Namespace
