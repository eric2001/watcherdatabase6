﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule checks for cards that require a specific pre-game card in order to be included in the deck.
    ''' Ex: Some Hilt cards can only be used if you're using a specific Weapon of Choice pre-game.
    ''' </summary>
    Public Class IsPreGameCardRequiredRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Errors.AddRange(DoesHiltRequireWeaponOfChoice(Deck, PreGame, Format, deckStats))

            Return Errors
        End Function

        Public Shared Function DoesHiltRequireWeaponOfChoice(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError)
            Dim Errors As New List(Of ValidationError)

            ' Second Edition Cards

            ' Katherine Scorned Blade (PP2024-002) -- Requires Swiss Long Sword Weapon of Choice
            Dim CardToValidate As List(Of Card) = Deck.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.KatherineScornedBlade).ToList()
            If CardToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Weapon of Choice" And c.Immortal = "Swiss Long Sword").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, CardToValidate(0), "Scorned Blade requires the Swiss Long Sword Weapon of Choice."))
                End If
            End If

            ' Kamir Talwar (PP2024-003) -- Requires Sabre Weapon of Choice
            CardToValidate = Deck.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.KamirTalwar).ToList()
            If CardToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Weapon of Choice" And c.Immortal = "Sabre").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, CardToValidate(0), "Talwar requires the Sabre Weapon of Choice."))
                End If
            End If

            ' Steven Keane Talwar Prussian Cavalry Sword (SKC-018) -- Requires Sabre Weapon of Choice
            CardToValidate = Deck.Where(Function(c) c.Id = CardLookup.OneShotGames.StevenKeaneCollection.StevenKeanePrussianCavalrySword).ToList()
            If CardToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Weapon of Choice" And c.Immortal = "Sabre").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, CardToValidate(0), "Prussian Cavalry Sword requires the Sabre Weapon of Choice."))
                End If
            End If

            ' Walter Reinhardt Basket Hilt Sabre (LG3-088) -- Requires the Sabre Weapon of Choice
            CardToValidate = Deck.Where(Function(c) c.Id = CardLookup.OneShotGames.Legacy3.WalterReinhardtBasketHiltSabre).ToList()
            If CardToValidate.Count > 0 Then
                If PreGame.Where(Function(c) c.Type = "Weapon of Choice" And c.Immortal = "Sabre").Count() = 0 Then
                    Errors.Add(New ValidationError(Format, CardToValidate(0), "Basket Hilt Sabre requires the Sabre Weapon of Choice."))
                End If
            End If

            Return Errors

        End Function

    End Class
End Namespace
