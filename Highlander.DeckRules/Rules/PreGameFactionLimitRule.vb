﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Runtime.InteropServices
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This card validates that the decks pre-game only contains at most one Faction Pre-Game card.
    ''' </summary>
    Public Class PreGameFactionLimitRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            ' You may only include one Faction pre-game card.
            Dim FactionPreGames As List(Of Card) = PreGame.Where(Function(c) c.Type.ToLower() = "faction").ToList()
            If FactionPreGames.Count > 1 Then
                For Each OneCard As Card In FactionPreGames
                    Errors.Add(New ValidationError(Format, OneCard, "You may only use one Faction Pre-Game card."))
                Next
            End If

            Return Errors
        End Function

    End Class
End Namespace
