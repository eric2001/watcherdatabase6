﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule validates that your pre-game doesn't exceed the maximum number of Weapon of Choice cards allowed (normally limited to one unless a card says you can use two).
    ''' </summary>
    Public Class WeaponOfChoiceLimitRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim PreGameWeapons As List(Of Card) = PreGame.Where(Function(c) c.Type.ToLower() = "weapon of choice").ToList()

            ' You can normally only have up to 1 Weapon of Choice card.
            Dim PreGameWeaponsCount As Integer = PreGameWeapons.Count
            Dim OffHandWeaponsCount As Integer = 0
            Dim HandsUsed As Integer = 0

            For Each OneCard In PreGameWeapons
                ' Unless it's one of these cards, in which case you can have two Weapon of Choice cards.
                If OneCard.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.ParryingBladePreGame OrElse
                        OneCard.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.ShieldPreGame OrElse
                        OneCard.Id = CardLookup.SaecGames.BlackRavenVol4.BlackPhoenixFanPreGame OrElse
                        OneCard.Id = CardLookup.Mle.MacLeodChronicles.EscutcheonPreGameBack OrElse
                        OneCard.Id = CardLookup.Mle.MacLeodChronicles.EscutcheonPreGameFront OrElse
                        OneCard.Id = CardLookup.Mle.SoldiersOfImmortality.HandAxePreGameFront OrElse
                        OneCard.Id = CardLookup.Mle.SoldiersOfImmortality.HandAxePreGameBack OrElse
                        OneCard.Id = CardLookup.Mle.SoldiersOfImmortality.KatarPreGameBack OrElse
                        OneCard.Id = CardLookup.Mle.SoldiersOfImmortality.KatarPreGameFront OrElse
                        OneCard.Id = CardLookup.Mle.PromotionalCards.KatarPreGameAlternateFront OrElse
                        OneCard.Hands.ToLower().Contains("o") Then ' Or unless the weapon has an off-hand symbol on it
                    OffHandWeaponsCount += 1
                    HandsUsed += 1
                Else
                    If OneCard.Hands = "1" Then
                        HandsUsed += 1
                    Else
                        ' If it's not one-handed or off-handed then it's either two handed (2) or one or two handed (1 / 2)
                        '   either way treat as two-handed.
                        HandsUsed += 2

                        '@TODO:  There's a pre-game to let you use a 2handed weapon one handed that should be accounted for here.

                    End If
                End If

                ' Broad Bladed Spear -- The only Off-Hand Weapon of Choice you can include or use is the Shield
                If OneCard.Id = CardLookup.LeMontagnardRevised.PromotionalCards.BroadBladedSpearPreGame Then
                    Dim OtherNonShildWeapons = PreGameWeapons.Where(Function(c) c.Id <> CardLookup.LeMontagnardRevised.PromotionalCards.BroadBladedSpearPreGame And
                                              c.Immortal <> "Shield").ToList()
                    If OtherNonShildWeapons.Count > 0 Then
                        For Each OneWoC As Card In OtherNonShildWeapons
                            Errors.Add(New ValidationError(Format, OneWoC, "The only Off-Hand Weapon of Choice you can include with Broad Bladed Spear is Shield."))
                        Next
                    End If
                End If

            Next

            ' If they have two Weapon of Choice cards and none of the cards are off-hand or allow you to use an additional Weapon of Choice,
            '   Or if the deck has 3 or more weapon of choice cards, then the deck is invalid
            If (PreGameWeaponsCount = 2 And OffHandWeaponsCount = 0) OrElse PreGameWeaponsCount > 2 Then
                Errors.Add(New ValidationError("*", "You may only use one Weapon of Choice Pre-Game."))
            End If

            ' You only have two hands, so if the hand-count is too high, that's stll invalid.
            If PreGameWeaponsCount = 2 And OffHandWeaponsCount = 1 Then
                If HandsUsed > 2 Then
                    Errors.Add(New ValidationError("*", "You cannot combine a 1-handed Weapon Of Choice with a 2-handed Weapon of Choice."))
                End If
            End If

            Return Errors
        End Function

    End Class
End Namespace
