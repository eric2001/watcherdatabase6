﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' This rule validates that the deck doesn't contain any un-playable cards.
    ''' Ex:  You can't have pre-game cards in your deck, you can't use in-game cards in your pre-game, you can't use rule and checklist cards, etc.
    ''' </summary>
    Public Class IsCardTypePlayable : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            Dim BannedCards As List(Of Card) = GetBannedCards(PreGame, Format)
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, String.Format("{0}s are not playable.", OneCard.Type)))
            Next

            For Each OneSubCard As Card In PreGame
                If OneSubCard.SubItems.Count > 0 Then
                    BannedCards = GetBannedCards(OneSubCard.SubItems, Format)
                    For Each OneCard In BannedCards
                        Errors.Add(New ValidationError(Format, OneCard, String.Format("{0}s are not playable.", OneCard.Type)))
                    Next
                End If
            Next

            ' @TODO:  I believe there's some special conditions where this isn't always true.
            '         Alternately I made need to add a way to play cards underneath a pre-game, so they're not on this list.  Maybe turn pre-game list into a tab where you select a pre-game card on the first tab and then the second tab filters to show all cards underneath it?  Along with an add card underneath selected pre-game button?
            BannedCards = PreGame.Where(Function(c) Not c.IsPreGame).ToList()


            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "You can not include In Game cards in your Pre-Game."))
            Next

            BannedCards = GetBannedCards(Deck, Format)
            For Each OneCard In BannedCards
                If OneCard.Type = "Rules" Then ' Don't add an "s" to the end of Rules in the error message.
                    Errors.Add(New ValidationError(Format, OneCard, String.Format("{0} are not playable.", OneCard.Type)))
                Else
                    Errors.Add(New ValidationError(Format, OneCard, String.Format("{0}s are not playable.", OneCard.Type)))
                End If
            Next

            BannedCards = Deck.Where(Function(c) c.IsPreGame).ToList()
            For Each OneCard In BannedCards
                Errors.Add(New ValidationError(Format, OneCard, "You can not include Pre-Game cards in your Deck."))
            Next

            Return Errors
        End Function

        Private Function GetBannedCards(ByVal Cards As List(Of Card), Format As DeckFormat) As List(Of Card)

            ' Rules cards, form cards and checklists are not playable
            Dim BannedCards As List(Of Card) = Cards.Where(Function(c) c.Type = "Rules" Or c.Type = "Form" Or c.Type = "Checklist" Or c.Type = "Insert").ToList()

            Return BannedCards
        End Function

    End Class
End Namespace
