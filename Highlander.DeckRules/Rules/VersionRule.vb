﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Runtime.InteropServices
Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' When a card (Pre-game or In-game) shares the same title, grid, and text as a card from another edition of the game, 
    ''' you must choose which version you wish to include In your deck. You may not mix versions of the card chosen when
    ''' constructing your deck.
    ''' </summary>
    Public Class VersionRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)

            If Format = DeckFormat.Type1 Or Format = DeckFormat.Type2 Then
                Dim LocalFormat As DeckFormat = Format
                Dim CardsByGroup = Deck.GroupBy(Function(c) GetGroup(c, LocalFormat)).Select(Function(g) g.ToList()).ToList()

                For Each OneGroup In CardsByGroup
                    If OneGroup.DistinctBy(Function(c) c.ExpansionSet.Edition).Count > 1 Then
                        Errors.Add(New ValidationError(Format, OneGroup.FirstOrDefault(), "You may not mix versions of the card from different editions."))
                    End If
                Next
            End If

            Return Errors
        End Function

        ''' <summary>
        ''' Convert a card into a value that uniquely identifies that version of the card.
        ''' </summary>
        ''' <param name="OneCard">The card being compared</param>
        ''' <param name="Format">The rules format being used</param>
        ''' <returns>A string representing this version of the card.</returns>
        Private Function GetGroup(ByVal OneCard As Card, ByVal Format As DeckFormat) As String
            Dim ReturnValue As String = String.Empty

            ReturnValue = String.Format("{0}|", OneCard.Immortal)

            If Format = DeckFormat.Type1 Or Format = DeckFormat.Type2 Or Format = DeckFormat.HighlanderPro Then
                ReturnValue &= Card.RemoveUnnecessaryVersion(OneCard.SecondEditionTitle, OneCard.Id)
            Else
                ReturnValue &= Card.RemoveUnnecessaryVersion(OneCard.Title, OneCard.Id)
            End If

            ReturnValue &= String.Format("|{0}|", OneCard.Type)

            Return ReturnValue.ToLower()
        End Function

    End Class
End Namespace
