﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Namespace Rules

    ''' <summary>
    ''' Validates that the deck contains at least the minimum number of cards required (normally 50 cards unless something says otherwise).
    ''' </summary>
    Public Class DeckSizeLimitRule : Implements IDeckConstructionRule

        Public Function Validate(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card), ByRef Format As DeckFormat, ByRef deckStats As DeckSummary) As List(Of ValidationError) Implements IDeckConstructionRule.Validate
            Dim Errors As New List(Of ValidationError)
            Dim MinimumDeckSize As Integer = 50 ' Minimum allowed number of cards for a vaild deck.  Default is 50 unless something special changes the minimum
            Dim MaximumDeckSize As Integer = 75 ' Maximum allowed number of cards in a valid deck.  Default is 75 unless something changes it.
            Dim TotalDeckSize As Integer = Deck.Sum(Function(c) c.Quantity) ' The total number of cards in the deck.

            ' 1st Edition Big and Bad -- your minimum deck size is 80 cards. For each Big and Bad in your deck, this minimum increases by 10 cards
            Dim Cards As List(Of Card) = Deck.Where(Function(c) c.Id = CardLookup.ThunderCastleGames.ArmsAndTactics.GenericBigAndBad).ToList()
            If Cards.Count > 0 Then
                MinimumDeckSize = 80
                MinimumDeckSize += (10 * Cards.Sum(Function(c) c.Quantity))
            End If

            ' 2nd Edition Big and Bad -- You may only include this card in your deck if you have 75 or more cards in your deck
            Cards = Deck.Where(Function(c) c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.GenericBigAndBad Or
                                c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.GenericBigAndBadDiscard Or
                                c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.GenericBigAndBadEndurance).ToList()
            If Cards.Count > 0 Then
                MinimumDeckSize = 75
            End If

            ' Adjust minimum deck size for Lean and Mean -- your minimum deck size is reduced by one.
            '   Except for Paradox Publishing Lean and Mean, which does nothing.
            Cards = Deck.Where(Function(c) (c.Immortal.ToLower() = "generic" And c.Title.ToLower.StartsWith("lean and mean")) And
                                   c.Id <> CardLookup.ParadoxPublishing.PromotionalCards.LeanAndMeanCantPlay).ToList()
            If Cards.Count > 0 Then
                MinimumDeckSize -= Cards.Sum(Function(c) c.Quantity)
            End If

            ' 4th Edition Lean and Mean
            Cards = Deck.Where(Function(c) c.Id = CardLookup.WorldOfGameDesign.PromotionalCards.PrincesOfTheUniverseLeanAndMean).ToList()
            If Cards.Count > 0 Then
                MinimumDeckSize -= Cards.Sum(Function(c) c.Quantity)
            End If

            ' Display a warning if the deck does not have enough cards in it.
            If TotalDeckSize < MinimumDeckSize Then
                Errors.Add(New ValidationError("*", String.Format("Your deck must contain at least {0} cards.", MinimumDeckSize)))
            End If

            ' The highlander Pro rulebook is the only one that mentions a maximum deck size limit.
            If Format = DeckFormat.HighlanderPro Then
                If TotalDeckSize > MaximumDeckSize Then
                    Errors.Add(New ValidationError("*", String.Format("Your deck must contain a maximum of {0} cards.", MaximumDeckSize)))
                End If
            End If

            Return Errors
        End Function

    End Class
End Namespace
