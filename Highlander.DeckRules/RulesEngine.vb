﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Public Class RulesEngine
    Private Rules As New List(Of Rules.IDeckConstructionRule)

    Public Sub New()

        ' Pre-Game Rules
        Rules.Add(New Rules.PreGameSizeLimitRule)
        Rules.Add(New Rules.PreGameRestrictionLimitRule)
        Rules.Add(New Rules.IsPreGameCardAllowedRule)
        Rules.Add(New Rules.IsQuickeningAllowedRule)
        Rules.Add(New Rules.IsPersonaRequired)
        Rules.Add(New Rules.WeaponOfChoiceLimitRule)
        Rules.Add(New Rules.WeaponOfChoiceAllowedRule)
        Rules.Add(New Rules.PreGameWatcherHunterLimitRule)
        Rules.Add(New Rules.PreGameWatcherHunterAllowedRule)
        Rules.Add(New Rules.PreGameSkillLimitRule)
        Rules.Add(New Rules.PreGameFactionLimitRule)
        Rules.Add(New Rules.IsPreGameCardRequiredRule)
        Rules.Add(New Rules.PersonaLimitRule)

        ' Sub-Card Rules
        Rules.Add(New Rules.PreGameSubCardsRule)
        Rules.Add(New Rules.TheDarkQuickeningRule)

        ' Deck Rules
        Rules.Add(New Rules.DeckSizeLimitRule)
        Rules.Add(New Rules.DeckRestrictionLimitRule)
        Rules.Add(New Rules.DeckSubTitleRestrictions)
        Rules.Add(New Rules.RequiredBasicAttacksBlocksRule)
        Rules.Add(New Rules.GemLimitsRule)
        Rules.Add(New Rules.VersionRule)

        ' General Rules
        Rules.Add(New Rules.BannedCardsRule)
        Rules.Add(New Rules.AllowedPublishersRule)
        Rules.Add(New Rules.IsCard1EOnly)
        Rules.Add(New Rules.IsCard2EOnly)
        Rules.Add(New Rules.IsCardTypePlayable)

        '@TODO:  STATUS -- In theory all of the VS 2010 Deck code should now be in here.
        '       Next Steps -- Go through the 1E rulebook and adandumns and create test cases for each deck consturction rule
        '           Next Go through list of 1E cards with special deck construction rules and start building test cases for those
        '               Plus test cases to validate that the standard rules are all working (ex: One of each basic attack / block pass / fail, immortal specific cards from other personas pass / fail, etc).
        '   There's probably bugs, but hopefully if I can get a solid set of TCG format tests in place then I'll be at a good starting point
        '  Try to get all TCG/MLE/SAEC cards / rules covered and working as throughly as possible and make sure automated tests exist for all 1e scenarios in the rule engine
        '  That should get me to a solid starting point to expand into type 1 / type 2
    End Sub

    Public Function Analyze(ByVal Deck As List(Of Card), ByVal PreGame As List(Of Card), ByVal Format As DeckFormat, ByVal deckStats As DeckSummary) As Result
        Dim result As New Result()

        Dim TempPreGame As List(Of Card) = Clone.CopyDeck(PreGame)
        Dim TempDeck As List(Of Card) = Clone.CopyDeck(Deck)

        ArmsAndTacticsHiltProcessor.Process(TempDeck, TempPreGame)

        For Each rule As Rules.IDeckConstructionRule In Me.Rules
            result.Errors.AddRange(rule.Validate(TempDeck, TempPreGame, Format, deckStats))
        Next

        Return result
    End Function

End Class


