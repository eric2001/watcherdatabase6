﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data
Imports System.Data

Public Class Clone

    ''' <summary>
    ''' Create a copy of the passed in pre-game or deck list. 
    ''' </summary>
    ''' <param name="Deck">The List of cards to copy</param>
    ''' <returns>The new copied list</returns>
    Public Shared Function CopyDeck(ByVal Deck As List(Of Card)) As List(Of Card)
        Dim NewDeck As New List(Of Card)

        For Each OneCard In Deck
            Dim NewCard As Card = Card.LoadCard(OneCard.Id, OneCard.Quantity)
            NewDeck.Add(NewCard)
        Next

        Return NewDeck
    End Function
End Class
