﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data

Public Class MissingLinkExpansion

    Public Shared Sub DeDuplicatePreGameCards(ByRef PreGame As List(Of Card))
        Dim CardBacks As List(Of Card) = PreGame.Where(Function(p) p.Title.Contains(" Back")).ToList()
        For Each CardBack As Card In CardBacks
            Dim CardImmortal As String = CardBack.Immortal, CardTitle As String = CardBack.Title.Replace(" Back", " Front")
            Dim CardFront As Card = PreGame.Where(Function(p) p.Immortal = CardImmortal And p.Title = CardTitle).FirstOrDefault
            If CardFront IsNot Nothing Then
                PreGame.Remove(CardFront)
            End If

            ' Scimitar has two Front's -- French and US, but only back for both, which unique titles for each, which throws off auto-detection.
            If CardBack.Id = CardLookup.Mle.MissingLinkExpansion.ScimitarPreGameBack Then
                Dim ScimitarCardFront As Card = PreGame.Where(Function(p) _
                                                                  p.Id = CardLookup.Mle.MissingLinkExpansion.ScimitarPreGameFrontUs Or
                                                                  p.Id = CardLookup.Mle.MissingLinkExpansion.ScimitarPreGameFrontFrench).FirstOrDefault
                If ScimitarCardFront IsNot Nothing Then
                    PreGame.Remove(ScimitarCardFront)
                End If
            End If

            ' Jacob Kell has three backs and two fronts, all of which have completely different titles, which makes not counting it twice a pain in the ass.
            If CardBack.Id = CardLookup.Mle.MissingLinkExpansion.JacobKellPersonaBackEnglish OrElse
                CardBack.Id = CardLookup.Mle.MissingLinkExpansion.JacobKellPersonaBackUs OrElse
                CardBack.Id = CardLookup.Mle.MissingLinkExpansion.JacobKellPersonaBackFrench Then
                Dim JacobKellPersonaFront As Card = PreGame.Where(Function(p) _
                                                                      p.Id = CardLookup.Mle.MissingLinkExpansion.JacobKellPersonaFront Or
                                                                      p.Id = CardLookup.Mle.PromotionalCards.JacobKellPersonaFront).FirstOrDefault ' If for some weird reason they have multiple jacobs, we are only removing one for each front that we find.
                If JacobKellPersonaFront IsNot Nothing Then
                    PreGame.Remove(JacobKellPersonaFront)
                End If
            End If
        Next
    End Sub
End Class
