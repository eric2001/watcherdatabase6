﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports Highlander.Data
Imports System.Data

Public Class ArmsAndTacticsHiltProcessor
    ''' <summary>
    ''' Arms & Tactics (PP-2021)
    ''' Before the Game begins, place a compatible OBJECT:HILT from your constructed deck with a Weapon of Choice in your Pre-Game.
    ''' 
    ''' Allow Hilt's to be placed in the pre-game, but move them to the main deck before running analysis / checks.
    ''' If there are more hilts in the pre-game then Arms And Tactics Hilt Cards allow, then leave them and let standard validation flag them as illegal.
    ''' </summary>
    ''' <param name="Deck">The deck to process</param>
    ''' <param name="PreGame">The pre-game to process</param>
    Public Shared Sub Process(ByRef Deck As List(Of Card), ByRef PreGame As List(Of Card))
        Dim InGameHiltCards As List(Of Card) = PreGame.Where(Function(c) Not c.IsPreGame And c.SubType.ToLower() = "hilt").ToList()
        If InGameHiltCards.Count > 0 Then

            Dim ArmsAndTacticsHiltCount As Integer = 0
            Dim ArmsAndTacticsHilts As List(Of Card) = PreGame.Where(Function(c) c.Id = CardLookup.OneShotGames.StevenKeaneCollection.ArmsAndTacticsPreGameHilt Or
                                                                             c.Id = CardLookup.LeMontagnardRevised.PromotionalCards.ArmsAndTacticsPreGameHilt).ToList()
            If ArmsAndTacticsHilts.Count > 0 Then
                ArmsAndTacticsHiltCount += ArmsAndTacticsHilts.Sum(Function(c) c.Quantity)
            End If

            If ArmsAndTacticsHiltCount > 0 Then
                For Each OneCard In InGameHiltCards
                    If ArmsAndTacticsHiltCount > 0 Then
                        Dim NewCard As Card = Card.LoadCard(OneCard.Id, 0)
                        While OneCard.Quantity > 0 AndAlso ArmsAndTacticsHiltCount > 0
                            NewCard.Quantity += 1
                            OneCard.Quantity -= 1
                            ArmsAndTacticsHiltCount -= 1
                        End While

                        Deck.Add(NewCard)
                        If OneCard.Quantity = 0 Then
                            PreGame.Remove(OneCard)
                        End If
                    End If
                Next
            End If
        End If
    End Sub

End Class
