﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Public Class DeckSummary
    Public DeckTitle As String = ""
    Public Immortal As String = ""
    Public DeckSize As Integer = 0
    Public PreGameSize As Integer = 0
    Public PersonaAttributes As New Gems
    Public DeckAttributes As New Gems
    Public CardsByType As New List(Of TypeStat) ' Attack, Block, etc.
    Public CardsBySubTitle As New List(Of SubTitleStat) ' Immortal, WoC, etc
    Public PersonaId As Integer = 0
End Class

Public Class Gems
    Public Master As Integer = 0
    Public Agility As Integer = 0
    Public Strength As Integer = 0
    Public Toughness As Integer = 0
    Public Empathy As Integer = 0
    Public Reason As Integer = 0
    Public Display3EGems As Boolean = False
End Class

Public Class TypeStat
    Public CardType As String = String.Empty
    Public Quantity As Integer = 0
End Class

Public Class SubTitleStat
    Public Immortal As String = String.Empty
    Public Quantity As Integer = 0
    Public Reserved As Integer = 0
    Public Signatured As Integer = 0
End Class