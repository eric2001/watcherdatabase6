' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.Data.Tests

    Public Class HLDTests

        ''' <summary>
        ''' Test loading an hld file and confirm that the deck and pre-game card counts match the expected sizes.
        ''' </summary>
        ''' <param name="FileName">The .hld file to load.</param>
        ''' <param name="ExpectedDeckSize">The expected number of cards in the deck.</param>
        ''' <param name="ExpectedPreGameSize">The expected number of cards in the pre-game.</param>
        <Theory>
        <InlineData("TestData\BasicTCGGenericImmortalDeck.hld", 50, 0, 0)>
        <InlineData("TestData\TCGAmandaDeck.hld", 50, 5, 0)>
        <InlineData("TestData\EmptyDeck.hld", 0, 0, 0)>
        <InlineData("TestData\DarkQuickeningDeck.hld", 50, 5, 7)>
        Sub TestDeckLoad(ByVal FileName As String, ByVal ExpectedDeckSize As Integer, ByVal ExpectedPreGameSize As Integer, ByVal ExpectedPreGameSubCardSize As Integer)
            Dim deck As HighlanderDeck = HLD.Load(FileName)

            Dim SubItemQuantity As Integer = 0
            For Each CurrentCard As Card In deck.PreGame
                SubItemQuantity += CurrentCard.SubItems.Sum(Function(c) c.Quantity)
            Next

            Assert.Equal(ExpectedDeckSize, deck.DeckSize)
            Assert.Equal(ExpectedPreGameSize, deck.PreGameSize)
            Assert.Equal(ExpectedPreGameSubCardSize, SubItemQuantity)
        End Sub

        ''' <summary>
        ''' Test loading a .hld with a bad Card Id to test error handling.
        ''' </summary>
        ''' <param name="FileName">Name and path of .hld file to test loading.</param>
        <Theory>
        <InlineData("TestData\DeckWithInvalidCardId.hld")>
        Sub TestDeckLoadWithInvalidCardId(ByVal FileName As String)
            Dim ex = Assert.Throws(Of Exception)(Function() HLD.Load(FileName))
            Assert.Equal("Unable to Load Deck:  Card ID 0 Not Found.", ex.Message)
        End Sub

        ''' <summary>
        ''' Test saving and re-loading an existing hld to confirm the save created an identical copy.
        ''' </summary>
        <Fact>
        Sub TestDeckSave()
            Dim deck As HighlanderDeck = HLD.Load("TestData\TCGAmandaDeck.hld")
            Dim result As Boolean = HLD.Save("TestData\temp.hld", deck)
            Dim duplicateDeck As HighlanderDeck = HLD.Load("TestData\temp.hld")

            Dim originalDeckText As String = System.IO.File.ReadAllText("TestData\TCGAmandaDeck.hld")
            Dim duplicateDeckText As String = System.IO.File.ReadAllText("TestData\temp.hld")

            System.IO.File.Delete("TestData\temp.hld")

            Assert.Equal(True, result)

            Assert.Equal(originalDeckText, duplicateDeckText)

            Assert.Equal(deck.DeckSize, duplicateDeck.DeckSize)
            Assert.Equal(deck.Deck.Count, duplicateDeck.Deck.Count)
            Assert.Equal(deck.PreGameSize, duplicateDeck.PreGameSize)
            Assert.Equal(deck.PreGame.Count, duplicateDeck.PreGame.Count)
            Assert.Equal(deck.PreGame.Sum(Function(c) c.SubItems.Count), duplicateDeck.PreGame.Sum(Function(c) c.SubItems.Count))
        End Sub

        ''' <summary>
        ''' Test saving and re-loading an existing hld with sub-cards to confirm the save created an identical copy.
        ''' </summary>
        <Fact>
        Sub TestDeckSubCardSave()
            Dim deck As HighlanderDeck = HLD.Load("TestData\DarkQuickeningDeck.hld")
            Dim result As Boolean = HLD.Save("TestData\temp.hld", deck)
            Dim duplicateDeck As HighlanderDeck = HLD.Load("TestData\temp.hld")

            Dim originalDeckText As String = System.IO.File.ReadAllText("TestData\DarkQuickeningDeck.hld")
            Dim duplicateDeckText As String = System.IO.File.ReadAllText("TestData\temp.hld")

            System.IO.File.Delete("TestData\temp.hld")

            Assert.Equal(True, result)

            Assert.Equal(originalDeckText, duplicateDeckText)

            Assert.Equal(deck.DeckSize, duplicateDeck.DeckSize)
            Assert.Equal(deck.Deck.Count, duplicateDeck.Deck.Count)
            Assert.Equal(deck.PreGameSize, duplicateDeck.PreGameSize)
            Assert.Equal(deck.PreGame.Count, duplicateDeck.PreGame.Count)
            Assert.Equal(deck.PreGame.Sum(Function(c) c.SubItems.Count), duplicateDeck.PreGame.Sum(Function(c) c.SubItems.Count))
        End Sub

    End Class
End Namespace

