﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Linq
Imports Xunit

Namespace Highlander.Data.Tests

    ''' <summary>
    ''' InventoryDB is a shared object, so these need to run sequentially.
    ''' </summary>
    <Collection("Sequential")>
    Public Class InventoryDBTests

        ''' <summary>
        ''' Test loading a .hlt file and confirm that the expected number of cards and expected quantities were loaded.
        ''' </summary>
        ''' <param name="FileName">The name of the .hli file to load.</param>
        ''' <param name="ExpectedUniqueCards">The total number of unique cards in the .hlt file.</param>
        ''' <param name="ExpectedTotalCards">The total number of cards in the .hli file.</param>
        <Theory>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", 22, 88)>
        Sub TestInventoryLoad(ByVal FileName As String, ByVal ExpectedUniqueCards As Integer, ByVal ExpectedTotalCards As Integer)
            InventoryDB.LoadFile(FileName)

            Assert.Equal(ExpectedUniqueCards, InventoryDB.Data.Tables("Inventory").Rows.Count)
            Assert.Equal(ExpectedTotalCards, InventoryDB.Data.Tables("Inventory").Compute("SUM(Quantity)", String.Empty))
            Assert.Equal(FileName, InventoryDB.FileName)
        End Sub

        ''' <summary>
        ''' Test saving and re-loading an existing .hli file to confirm the save creates an identical copy.
        ''' </summary>
        <Fact>
        Sub TestInventorySave()
            InventoryDB.LoadFile("TestData\MethosCollectionOnlyInventory.hli")
            Dim ExpectedUniqueCards As Integer = InventoryDB.GetInventoryRecordCount()
            Dim ExpectedTotalCards As Integer = InventoryDB.Data.Tables("Inventory").Compute("SUM(Quantity)", String.Empty)

            InventoryDB.SaveFile("TestData\temp.hli")
            InventoryDB.LoadFile("TestData\temp.hli")
            Dim UniqueCardsLoaded As Integer = InventoryDB.Data.Tables("Inventory").Rows.Count
            Dim TotalCardsLoaded As Integer = InventoryDB.Data.Tables("Inventory").Compute("SUM(Quantity)", String.Empty)

            Dim originalInventoryText As String = System.IO.File.ReadAllText("TestData\MethosCollectionOnlyInventory.hli")
            Dim copiedInventoryText As String = System.IO.File.ReadAllText("TestData\temp.hli")

            System.IO.File.Delete("TestData\temp.hli")

            Assert.Equal(originalInventoryText, copiedInventoryText)
            Assert.Equal(ExpectedUniqueCards, UniqueCardsLoaded)
            Assert.Equal(ExpectedTotalCards, TotalCardsLoaded)
        End Sub

        ''' <summary>
        ''' Test saving a new .hli file without specifying a file name to test error handling.
        ''' </summary>
        ''' <param name="FileName">Name and path of .hli file to test saving.</param>
        <Theory>
        <InlineData("")>
        Sub TestInventorySaveNoFileName(ByVal FileName As String)
            InventoryDB.ClearFile()
            Dim ex = Assert.Throws(Of Exception)(Sub() InventoryDB.SaveFile(FileName))
            Assert.Equal("Unable to Save Inventory:  No File Name", ex.Message)
        End Sub

        ''' <summary>
        ''' Test loading and then clearing an inventory to confirm everything is cleared out of memory.
        ''' </summary>
        <Fact>
        Sub TestInventoryClear()
            InventoryDB.LoadFile("TestData\MethosCollectionOnlyInventory.hli")
            InventoryDB.ClearFile()
            InventoryDB.SaveFile("TestData\temp.hli")

            Dim RawInventoryData As String = System.IO.File.ReadAllText("TestData\temp.hli")
            System.IO.File.Delete("TestData\temp.hli")

            Assert.Equal("<?xml version=""1.0"" standalone=""yes""?>" & vbCrLf & "<Inventory xmlns=""Inventory.xsd"" />", RawInventoryData)
            Assert.Equal(0, InventoryDB.GetInventoryRecordCount())
            Assert.Equal(DBNull.Value, InventoryDB.Data.Tables("Inventory").Compute("SUM(Quantity)", String.Empty))
        End Sub

        ''' <summary>
        ''' Test loading a specific card from an inventory file.
        ''' </summary>
        ''' <param name="FileName">The path to the inventory file.</param>
        ''' <param name="CardId">The Id of the card to test.</param>
        ''' <param name="Quantity">The expected quantity for the card id.</param>
        ''' <param name="FileNameFront">The expected file name front for the card id.</param>
        ''' <param name="FileNameBack">The expected file name back for the card id.</param>
        ''' <param name="MleUrl">The expected URL for the card id.</param>
        ''' <param name="Notes">The expected notes for the card id.</param>
        <Theory>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", 1211, 2, "SecretIdentity.jpg", "1stEditionBack.jpg", "http://www.watcherdatabase.tk/Cards/Card/1211.html", "This card has a quantity of 2")>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", 500, 0, "", "", "", "")> ' This card is not in the inventory
        Sub TestInventoryRowLoad(ByVal FileName As String, ByVal CardId As Integer, ByVal Quantity As Integer, ByVal FileNameFront As String, ByVal FileNameBack As String, ByVal MleUrl As String, ByVal Notes As String)
            InventoryDB.LoadFile(FileName)
            Dim CurrentCard As Inventory = Inventory.GetInventoryForCard(CardId)

            Assert.Equal(CardId, CurrentCard.Id)
            Assert.Equal(Quantity, CurrentCard.Quantity)
            Assert.Equal(FileNameFront, CurrentCard.FileNameFront)
            Assert.Equal(FileNameBack, CurrentCard.FileNameBack)
            Assert.Equal(MleUrl, CurrentCard.MleUrl)
            Assert.Equal(Notes, CurrentCard.Notes)
        End Sub

        ''' <summary>
        ''' Test loading the quantity of a specific card from an inventory file.
        ''' </summary>
        ''' <param name="FileName">The path to the inventory file.</param>
        ''' <param name="CardId">The Id of the card to test.</param>
        ''' <param name="Quantity">The expected quantity for the Card Id.</param>
        <Theory>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", 885, 1)>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", 890, 6)>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", 500, 0)> ' 500 isn't in the file, the other two are.
        Sub TestInventoryQuantity(ByVal FileName As String, ByVal CardId As Integer, ByVal Quantity As Integer)
            InventoryDB.LoadFile(FileName)

            Assert.Equal(Quantity, Inventory.GetCardQuantity(CardId))
        End Sub

        ''' <summary>
        ''' Test modifying an inventory
        ''' </summary>
        ''' <param name="FileName">The path to the inventory file.</param>
        ''' <param name="CardId">The Id of the card to test.</param>
        ''' <param name="Quantity">The new quantity for the card id.</param>
        ''' <param name="FileNameFront">The new file name front for the card id.</param>
        ''' <param name="FileNameBack">The new file name back for the card id.</param>
        ''' <param name="MleUrl">The new URL for the card id.</param>
        ''' <param name="Notes">The new notes for the card id.</param>
        ''' <param name="ExpectedUniqueCards">The total number of unique cards in the .hlt file.</param>
        ''' <param name="ExpectedTotalCards">The total number of cards in the .hli file.</param>
        <Theory>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", "TestData\MethosCollectionOnlyInventorySecretIdentityIncrease.hli", 1211, 3, "SecretIdentity1.jpg", "1stEditionBack1.jpg", "https://www.watcherdatabase.tk/Cards/Card/1211.html", "This card has a quantity of 3", 22, 89)>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", "TestData\MethosCollectionOnlyInventoryYungDolKimUpdate.hli", 500, 2, "Front.jpg", "Back.jpg", "www.watcherdatabase.tk", "This is a note.", 23, 90)> ' This card is not in the inventory
        Sub TestInventoryRowUpdate(ByVal FileName As String, ByVal UpdatedFileName As String, ByVal CardId As Integer, ByVal Quantity As Integer, ByVal FileNameFront As String, ByVal FileNameBack As String, ByVal MleUrl As String, ByVal Notes As String, ByVal ExpectedUniqueCards As Integer, ByVal ExpectedTotalCards As Integer)
            InventoryDB.LoadFile(FileName)

            ' Add / Update a card in the inventory
            Inventory.SaveInventoryForCard(CardId, Quantity, FileNameFront, FileNameBack, MleUrl, Notes)

            ' Confirm counts match expected value
            Assert.Equal(ExpectedUniqueCards, InventoryDB.Data.Tables("Inventory").Rows.Count)
            Assert.Equal(ExpectedTotalCards, InventoryDB.Data.Tables("Inventory").Compute("SUM(Quantity)", String.Empty))

            ' Test reading the updated data back out of the inventory
            Dim CurrentCard As Inventory = Inventory.GetInventoryForCard(CardId)
            Assert.Equal(CardId, CurrentCard.Id)
            Assert.Equal(Quantity, CurrentCard.Quantity)
            Assert.Equal(FileNameFront, CurrentCard.FileNameFront)
            Assert.Equal(FileNameBack, CurrentCard.FileNameBack)
            Assert.Equal(MleUrl, CurrentCard.MleUrl)
            Assert.Equal(Notes, CurrentCard.Notes)

            ' Test saving changes
            InventoryDB.SaveFile("TestData\temp.hli")
            Dim ExpectedRawInventoryData As String = System.IO.File.ReadAllText(UpdatedFileName)
            Dim ActualRawInventoryData As String = System.IO.File.ReadAllText("TestData\temp.hli")
            System.IO.File.Delete("TestData\temp.hli")
            Assert.Equal(ExpectedRawInventoryData, ActualRawInventoryData)
        End Sub

        ''' <summary>
        ''' Test modifying an inventory
        ''' </summary>
        ''' <param name="FileName">The path to the inventory file.</param>
        ''' <param name="CardId">The Id of the card to test.</param>
        ''' <param name="Quantity">The new quantity for the card id.</param>
        ''' <param name="FileNameFront">The new file name front for the card id.</param>
        ''' <param name="FileNameBack">The new file name back for the card id.</param>
        ''' <param name="MleUrl">The new URL for the card id.</param>
        ''' <param name="Notes">The new notes for the card id.</param>
        ''' <param name="ExpectedUniqueCards">The total number of unique cards in the .hlt file.</param>
        ''' <param name="ExpectedTotalCards">The total number of cards in the .hli file.</param>
        <Theory>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", "TestData\MethosCollectionOnlyInventorySecretIdentityRemoved.hli", 1211, 0, "", "", "", "", 21, 86)>
        <InlineData("TestData\MethosCollectionOnlyInventory.hli", "TestData\MethosCollectionOnlyInventory.hli", 500, 0, "", "", "", "", 22, 88)> ' This card is not in the inventory
        Sub TestInventoryRowDelete(ByVal FileName As String, ByVal UpdatedFileName As String, ByVal CardId As Integer, ByVal Quantity As Integer, ByVal FileNameFront As String, ByVal FileNameBack As String, ByVal MleUrl As String, ByVal Notes As String, ByVal ExpectedUniqueCards As Integer, ByVal ExpectedTotalCards As Integer)
            InventoryDB.LoadFile(FileName)

            ' Remove a card from the inventory
            Inventory.SaveInventoryForCard(CardId, Quantity, FileNameFront, FileNameBack, MleUrl, Notes)

            ' Confirm counts match expected value
            Assert.Equal(ExpectedUniqueCards, InventoryDB.Data.Tables("Inventory").Rows.Count)
            Assert.Equal(ExpectedTotalCards, InventoryDB.Data.Tables("Inventory").Compute("SUM(Quantity)", String.Empty))

            ' Test reading the updated data back out of the inventory
            Dim CurrentCard As Inventory = Inventory.GetInventoryForCard(CardId)
            Assert.Equal(CardId, CurrentCard.Id)
            Assert.Equal(Quantity, CurrentCard.Quantity)
            Assert.Equal(FileNameFront, CurrentCard.FileNameFront)
            Assert.Equal(FileNameBack, CurrentCard.FileNameBack)
            Assert.Equal(MleUrl, CurrentCard.MleUrl)
            Assert.Equal(Notes, CurrentCard.Notes)

            ' Test saving changes
            InventoryDB.SaveFile("TestData\temp.hli")
            Dim ExpectedRawInventoryData As String = System.IO.File.ReadAllText(UpdatedFileName)
            Dim ActualRawInventoryData As String = System.IO.File.ReadAllText("TestData\temp.hli")
            System.IO.File.Delete("TestData\temp.hli")
            Assert.Equal(ExpectedRawInventoryData, ActualRawInventoryData)
        End Sub

    End Class
End Namespace
