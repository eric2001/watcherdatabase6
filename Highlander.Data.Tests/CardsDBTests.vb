﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports System.Data
Imports Xunit

Namespace Highlander.Data.Tests

    ''' <summary>
    ''' Some of these tests involve changing global settings, so this needs to be tested Sequentially.
    ''' </summary>
    <Collection("Sequential")>
    Public Class CardsDBTests

        ''' <summary>
        ''' Load a specific card, confirm it's title matches the expected title.
        ''' </summary>
        ''' <param name="CardId">Unique Id of the card to load.</param>
        ''' <param name="CardImmortal">Expected Immortal value for the Card Id.</param>
        ''' <param name="CardTitle">Expected Title for the Card Id.</param>
        <Theory>
        <InlineData(1, "Dark Duncan", "Cleave (ML,UC)")>
        <InlineData(4213, "Single-Handed Broadsword", "Weapon Lock")>
        <InlineData(9112, "Michael Moore / Quentin Barnes", "Master Slash")>
        Sub TestCardData(ByVal CardId As Integer, ByVal CardImmortal As String, ByVal CardTitle As String)
            Dim SelectedCard() As DataRow = CardsDB.Data.Tables("Card").Select("ID = '" & CardId.ToString() & "'")
            Assert.Equal(CardTitle, SelectedCard(0).Item("Title").ToString())
            Assert.Equal(CardImmortal, SelectedCard(0).Item("Immortal").ToString())

            Dim CurrentCard As Card = Card.DataRowToCard(SelectedCard(0))
            Assert.Equal(CardTitle, CurrentCard.Title)
            Assert.Equal(CardImmortal, CurrentCard.Immortal)
        End Sub

        ''' <summary>
        ''' Test determining a card's file name.
        ''' </summary>
        ''' <param name="CardId">Unique Id of the card to test.</param>
        ''' <param name="InventoryFileName">Location of inventory file to load (optional, for testing overrides).</param>
        ''' <param name="FileNameFront">Expected filename for the front of the card.</param>
        ''' <param name="FileNameBack">Expected filename for the back of the card.</param>
        <Theory>
        <InlineData(1338, "", "TestData\Images\Thunder Castle Games\Series Edition\Amanda_BackAway.jpg", "1eback.jpg")> ' Front from database exists in test data, back is blank in database and should be first edition default
        <InlineData(1338, "TestData\AmandaBackAwayFileNameOverride.hli", "TestData\Images\Thunder Castle Games\Series Edition\Amanda_PersonaFront.jpg", "1eback.jpg")> ' Front from database exists in test data, back is blank in database and should be first edition default
        <InlineData(1337, "", "TestData\Images\Thunder Castle Games\Series Edition\Amanda_PersonaFront.jpg", "TestData\Images\Thunder Castle Games\Series Edition\Amanda_PersonaBack.jpg")> ' Front and back both exist in database and test data
        <InlineData(1358, "", "1eback.jpg", "1eback.jpg")> ' Front from database does not exists in test data, back is blank in database and should be first edition default
        <InlineData(1357, "", "1eback.jpg", "1eback.jpg")> ' Front and back both defaulted in database but do not exist in test data
        <InlineData(4755, "", "TestData\Images\Le Montagnard Inc\Season 1\HS1-115_Amanda_Counter(Jump).jpg", "2eback.jpg")> ' Front from database exists in test data, back is blank in database and should be first edition default
        <InlineData(4877, "", "TestData\Images\Le Montagnard Inc\Season 1\HS1-237_Amanda_Persona(Front).jpg", "TestData\Images\Le Montagnard Inc\Season 1\HS1-237_Amanda_Persona(Back).jpg")> ' Front and back both exist in database and test data
        <InlineData(4772, "", "2eback.jpg", "2eback.jpg")> ' Front from database does not exists in test data, back is blank in database and should be first edition default
        <InlineData(4878, "", "2eback.jpg", "2eback.jpg")> ' Front and back both defaulted in database but do not exist in test data
        <InlineData(6835, "", "TestData\Images\Paradox Publishing\Duncan MacLeod Chronicles, Vol 1\P03-011_DuncanMacLeod_Darius.jpg", "3eback.jpg")> ' Front from database exists in test data, back is blank in database and should be first edition default
        <InlineData(6826, "", "TestData\Images\Paradox Publishing\Duncan MacLeod Chronicles, Vol 1\P03-002_DuncanMacLeod_PremiumFront.jpg", "TestData\Images\Paradox Publishing\Duncan MacLeod Chronicles, Vol 1\P03-002_DuncanMacLeod_PremiumBack.jpg")> ' Front and back both exist in database and test data
        <InlineData(6851, "", "3eback.jpg", "3eback.jpg")> ' Front from database does not exists in test data, back is blank in database and should be first edition default
        <InlineData(6827, "", "3eback.jpg", "3eback.jpg")> ' Front and back both defaulted in database but do not exist in test data
        <InlineData(10020, "", "4eback.jpg", "4eback.jpg")> ' Front from database does not exists in test data, back is blank in database and should be first edition default
        <InlineData(9989, "", "4eback.jpg", "4eback.jpg")> ' Front and back both defaulted in database but do not exist in test data
        <InlineData(7217, "", "TestData\Images\Misprints and Oddities\AmandaPersonaFront.jpg", "1eback.jpg")> ' Front from database exists in test data, back is blank in database and should be first edition default
        <InlineData(3705, "", "TestData\Images\Misprints and Oddities\MastersPrizeFront.jpg", "TestData\Images\Misprints and Oddities\YungDolKimMisprintPremiumBack.jpg")> ' Front and back both exist in database and test data
        Sub TestCardFileName(ByVal CardId As Integer, ByVal InventoryFileName As String, ByVal FileNameFront As String, ByVal FileNameBack As String)

            ' Override default location for testing purposes
            CardsDB.HighlanderImagePath = IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestData\Images\")

            ' Clear any previously loaded inventory data as this can override the default location for a card image.
            InventoryDB.ClearFile()

            ' If an inventory file was specified for this test, load it
            If Not String.IsNullOrEmpty(InventoryFileName) Then
                InventoryDB.LoadFile(InventoryFileName)
            End If

            ' Load the specified card
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            ' Test the data
            Assert.Equal(CardId, CurrentCard.Id)
            Assert.Equal(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileNameFront), CurrentCard.FilePathFront)
            Assert.Equal(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileNameBack), CurrentCard.FilePathBack)
        End Sub

        ''' <summary>
        ''' Test saving the CardsDB.
        ''' </summary>
        <Fact>
        Sub TestCardDataSave()
            Dim RawCardData As String = System.IO.File.ReadAllText(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\cards.xml"))

            CardsDB.Save("TestData\tempCards.xml")
            Dim RawCardsDataCopy As String = System.IO.File.ReadAllText("TestData\tempCards.xml")
            System.IO.File.Delete("TestData\tempCards.xml")

            Assert.Equal(RawCardData, RawCardsDataCopy)
        End Sub

        ''' <summary>
        ''' Test loading multiple cards by query.
        ''' </summary>
        ''' <param name="QueryString">Search query to use.</param>
        ''' <param name="CardCount">Expected number of cards.</param>
        <Theory>
        <InlineData("Set = 'DC'", 23)>
        <InlineData("PublisherID = 3", 148)>
        Sub TestLoadCards(ByVal QueryString As String, ByVal CardCount As Integer)
            Dim Cards As List(Of Card) = Card.LoadCards(QueryString)

            Assert.Equal(CardCount, Cards.Count)
        End Sub

        <Theory>
        <InlineData(1338, "Series Edition")>
        <InlineData(3203, "Black Raven, Volume 1")>
        <InlineData(551, "MacLeod Chronicles")>
        <InlineData(5040, "Season 2")>
        <InlineData(6766, "Generic Chronicles, Vol 1")>
        <InlineData(10012, "Gencon 2023 Dawn of Time Preview Decks")>
        Sub TestCardExpansionSet(ByVal CardId As Integer, ByVal ExpansionSetName As String)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(ExpansionSetName, CurrentCard.ExpansionSet.Name)
        End Sub

        <Theory>
        <InlineData(1338, "Thunder Castle Games")>
        <InlineData(3203, "SAEC Games")>
        <InlineData(551, "MLE Team")>
        <InlineData(5040, "Le Montagnard Inc")>
        <InlineData(6766, "Paradox Publishing")>
        <InlineData(10012, "World of Game Design")>
        Sub TestCardPublisher(ByVal CardId As Integer, ByVal PublisherName As String)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(PublisherName, CurrentCard.Publisher.Name)
        End Sub

        <Theory>
        <InlineData(1338, "Amanda Back Away", "Back Away", "Amanda Evade (Back Away)", "Evade (Back Away)", "Back Away")>
        <InlineData(1341, "Amanda Jump", "Jump", "Amanda Counter (Jump)", "Counter (Jump)", "Jump")>
        <InlineData(1354, "Connor MacLeod Dodge", "Dodge", "Connor MacLeod Evade (Escape)", "Evade (Escape)", "Escape")>
        <InlineData(1342, "Amanda Left Side Step", "Left Side Step", "Amanda Counter (Left Sidestep)", "Counter (Left Sidestep)", "Side Step")>
        <InlineData(1345, "Amanda Right Side Step", "Right Side Step", "Amanda Counter (Right Sidestep)", "Counter (Right Sidestep)", "Side Step")>
        <InlineData(2140, "Annie Devlin Duck", "Duck", "Annie Devlin Counter (Duck)", "Counter (Duck)", "Duck")>
        <InlineData(1329, "Generic Left Guard", "Left Guard", "Generic Guard (Left)", "Guard (Left)", "")>
        <InlineData(1330, "Generic Right Guard", "Right Guard", "Generic Guard (Right)", "Guard (Right)", "")>
        <InlineData(1455, "Generic Upper Guard", "Upper Guard", "Generic Guard (Upper)", "Guard (Upper)", "Guard")>
        <InlineData(1423, "Generic Lower Guard", "Lower Guard", "Generic Guard (Lower)", "Guard (Lower)", "Guard")>
        <InlineData(2290, "Amanda Acrobat", "Acrobat", "Amanda Acrobat", "Acrobat", "")>
        <InlineData(1299, "Generic Carl", "Carl", "Generic Carl", "Carl", "")>
        <InlineData(3602, "Talia Bauer Back Away", "Back Away", "Talia Bauer Evade (Back Away)", "Evade (Back Away)", "Back Away")>
        <InlineData(159, "Ramirez Back Away", "Back Away", "Ramirez Evade (Back Away)", "Evade (Back Away)", "Back Away")>
        <InlineData(1538, "Paul Kinman Side Step (Left)", "Side Step (Left)", "Paul Kinman Counter (Left Sidestep)", "Counter (Left Sidestep)", "Side Step")>
        <InlineData(1539, "Paul Kinman Side Step (Right)", "Side Step (Right)", "Paul Kinman Counter (Right Sidestep)", "Counter (Right Sidestep)", "Side Step")>
        <InlineData(292, "Jin Ke Side Step (Left)", "Side Step (Left)", "Jin Ke Counter (Left Sidestep)", "Counter (Left Sidestep)", "Side Step")>
        <InlineData(293, "Jin Ke Side Step (Right)", "Side Step (Right)", "Jin Ke Counter (Right Sidestep)", "Counter (Right Sidestep)", "Side Step")>
        <InlineData(305, "Nick Wolfe Left Side Step", "Left Side Step", "Nick Wolfe Counter (Left Sidestep)", "Counter (Left Sidestep)", "Side Step")>
        <InlineData(309, "Nick Wolfe Right Side Step", "Right Side Step", "Nick Wolfe Counter (Right Sidestep)", "Counter (Right Sidestep)", "Side Step")>
        <InlineData(2851, "Colonel Bellian Side Step (Left)", "Side Step (Left)", "Colonel Bellian Counter (Left Sidestep)", "Counter (Left Sidestep)", "Side Step")>
        <InlineData(2852, "Colonel Bellian Side Step (Right)", "Side Step (Right)", "Colonel Bellian Counter (Right Sidestep)", "Counter (Right Sidestep)", "Side Step")>
        Sub TestCardDisplayName(ByVal CardId As Integer, ByVal FirstEditionName As String, ByVal FirstEditionTitle As String, ByVal TypeOneName As String, ByVal TypeOneTitle As String, ByVal TypeOneSubType As String)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(FirstEditionName, CurrentCard.OriginalDisplayName)

            CardsDB.CardDisplayMode = "first_edition"
            Assert.Equal(FirstEditionName, CurrentCard.DisplayName)
            Assert.Equal(FirstEditionTitle, CurrentCard.DisplayTitle)

            CurrentCard.DisplayName = String.Empty
            CardsDB.CardDisplayMode = "type_one"
            Assert.Equal(TypeOneName, CurrentCard.DisplayName)
            Assert.Equal(TypeOneTitle, CurrentCard.DisplayTitle)
            Assert.Equal(TypeOneTitle, CurrentCard.SecondEditionTitle)
            Assert.Equal(TypeOneSubType, CurrentCard.SecondEditionSubType)
        End Sub

        <Theory>
        <InlineData(1299, False)> ' 1E Ingame non-master card
        <InlineData(1475, True)> ' 1E Ingame master card
        <InlineData(1465, False)> ' 1E Pre-Game Persona
        <InlineData(4793, True)> ' 2E Ingame master card
        <InlineData(4785, False)> ' 2E Ingame non-master card
        Sub TestIsMasterCard(ByVal CardId As Integer, ByVal IsMasterCard As Boolean)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(IsMasterCard, CurrentCard.IsMasterCard)
        End Sub

        <Theory>
        <InlineData(1299, False)> ' 1E Ingame
        <InlineData(1465, True)> ' 1E Pre-Game Persona
        <InlineData(2929, True)> ' MLE Place
        <InlineData(2133, True)> ' 1E Watcher
        <InlineData(3684, True)> ' 1E Pre-Game
        <InlineData(988, True)> ' 1E Quickening
        <InlineData(4883, True)> ' 2E Weapon of Choice
        <InlineData(4892, True)> ' 2E Pre-Game
        <InlineData(4897, True)> ' 2E Premium
        <InlineData(4893, True)> ' 2E Crystal
        <InlineData(9991, True)> ' 4E Skill
        <InlineData(9992, True)> ' 4E Faction
        <InlineData(5185, True)> ' 2E Watcher
        <InlineData(5184, True)> ' 2E Hunter
        Sub TestIsPreGameCard(ByVal CardId As Integer, ByVal IsPreGameCard As Boolean)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(IsPreGameCard, CurrentCard.IsPreGame)
        End Sub

        <Theory>
        <InlineData(1299, False)> ' 1E Ingame generic
        <InlineData(1465, False)> ' 1E Pre-Game Persona
        <InlineData(4883, False)> ' 2E Weapon of Choice
        <InlineData(4897, True)> ' 2E Premium
        <InlineData(1871, False)> ' 1E Pre-Game
        <InlineData(2309, False)> ' 1E Pre-Game
        <InlineData(3684, False)> ' 1E Pre-Game
        <InlineData(3653, False)> ' 1E Pre-Game
        <InlineData(3621, False)> ' 1E Pre-Game
        <InlineData(1338, True)> ' 1E Amanda card
        <InlineData(1547, False)> ' 1E WoC InGame Card
        <InlineData(5060, True)> ' 2E Annie Devlin Card
        <InlineData(5166, False)> ' 2E WoC Card
        <InlineData(10018, False)> ' 4E Generic
        <InlineData(10020, True)> ' 4E Kurgan
        <InlineData(10037, False)> ' 4E Faction
        Sub TestIsImmortalSpecific(ByVal CardId As Integer, ByVal IsImmortalSpecific As Boolean)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(IsImmortalSpecific, CurrentCard.IsImmortalSpecific)
        End Sub

        <Theory>
        <InlineData(10020, False)> ' 4E Kurgan
        <InlineData(10037, True)> ' 4E Faction
        <InlineData(1299, False)> ' 1E Ingame generic
        <InlineData(1338, False)> ' 1E Amanda card
        <InlineData(5060, False)> ' 2E Annie Devlin Card
        <InlineData(2162, True)> ' 1E Clan MacLeod
        <InlineData(250, True)> ' MLE Clan MacLeod
        <InlineData(7244, True)> ' 2E Clan MacLeod
        Sub TestIsFactionSpecific(ByVal CardId As Integer, ByVal IsFactionSpecific As Boolean)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(IsFactionSpecific, CurrentCard.IsFactionSpecific)
        End Sub

        <Theory>
        <InlineData(1299, False)> ' 1E Ingame generic
        <InlineData(1465, False)> ' 1E Pre-Game Persona
        <InlineData(4883, False)> ' 2E Weapon of Choice
        <InlineData(1547, True)> ' 1E WoC InGame Card
        <InlineData(5060, False)> ' 2E Annie Devlin Card
        <InlineData(5166, True)> ' 2E WoC Card
        <InlineData(10018, False)> ' 4E Generic
        <InlineData(10020, False)> ' 4E Kurgan
        <InlineData(9990, False)> ' 4E WoC
        <InlineData(10031, True)> ' 4E WoC InGame
        Sub TestIsWeaponSpecific(ByVal CardId As Integer, ByVal IsWeaponSpecific As Boolean)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(IsWeaponSpecific, CurrentCard.IsWeaponSpecific)
        End Sub

        <Theory>
        <InlineData(1299, False)> ' 1E Ingame generic
        <InlineData(1465, False)> ' 1E Pre-Game Persona
        <InlineData(4883, False)> ' 2E Weapon of Choice
        <InlineData(1547, False)> ' 1E WoC InGame Card
        <InlineData(5060, False)> ' 2E Annie Devlin Card
        <InlineData(5166, False)> ' 2E WoC Card
        <InlineData(10018, False)> ' 4E Generic
        <InlineData(10020, False)> ' 4E Kurgan
        <InlineData(9990, False)> ' 4E WoC
        <InlineData(10031, False)> ' 4E WoC InGame
        <InlineData(9991, False)> ' 4E Skill PreGame
        <InlineData(10035, True)> ' 4E Skill InGame
        Sub TestIsSkillSpecific(ByVal CardId As Integer, ByVal IsSkillSpecific As Boolean)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(IsSkillSpecific, CurrentCard.IsSkillSpecific)
        End Sub

        <Theory>
        <InlineData(1687, "Destruction (Defeat)")>
        <InlineData(1847, "Lower Center Block")>
        <InlineData(1605, "Lower Center Attack")>
        Sub TestRemoveUnnecessaryVersion(ByVal CardId As Integer, ByVal ExpectedTitle As String)
            Dim CurrentCard As Card = Card.LoadCard(CardId)

            Assert.Equal(ExpectedTitle, Card.RemoveUnnecessaryVersion(CurrentCard.Title, CurrentCard.Id))
        End Sub

    End Class
End Namespace

