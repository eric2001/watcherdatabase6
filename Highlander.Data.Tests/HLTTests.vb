﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports Xunit

Namespace Highlander.Data.Tests

    Public Class HLTTests

        ''' <summary>
        ''' Test loading an .hlt file and confirm that the haves and wants list counts match the expected sizes.
        ''' </summary>
        ''' <param name="FileName">The .hlt file to load.</param>
        ''' <param name="ExpectedHavesSize">The expected number of cards on the Haves list.</param>
        ''' <param name="ExpectedWantsSize">The expected number of cards on the Wants list.</param>
        <Theory>
        <InlineData("TestData\TradeListWantsOnly.hlt", 0, 10)>
        <InlineData("TestData\TradeListHavesOnly.hlt", 11, 0)>
        <InlineData("TestData\TradeListHavesWants.hlt", 9, 8)>
        <InlineData("TestData\EmptyTradeList.hlt", 0, 0)>
        Sub TestTradeListLoad(ByVal FileName As String, ByVal ExpectedHavesSize As Integer, ByVal ExpectedWantsSize As Integer)
            Dim list As TradeList = HLT.Load(FileName)

            Assert.Equal(ExpectedHavesSize, list.TotalHaves)
            Assert.Equal(ExpectedWantsSize, list.TotalWants)
        End Sub

        ''' <summary>
        ''' Test loading a .hlt file with an invalid Card Id to test error handling.
        ''' </summary>
        ''' <param name="FileName">Name and path of .hlt file to test loading</param>
        <Theory>
        <InlineData("TestData\TradeListWithInvalidCardId.hlt")>
        Sub TestTradeListLoadWithInvalidCardId(ByVal FileName As String)
            Dim ex = Assert.Throws(Of Exception)(Function() HLT.Load(FileName))
            Assert.Equal("Unable to Load Deck:  Card ID 0 Not Found.", ex.Message)
        End Sub

        ''' <summary>
        ''' Test saving and re-loading an existing .hlt file to confirm the save creates an identical copy.
        ''' </summary>
        <Fact>
        Sub TestTradeListSave()
            Dim list As TradeList = HLT.Load("TestData\TradeListHavesWants.hlt")
            Dim result As Boolean = HLT.Save("TestData\temp.hlt", list)
            Dim duplicateList As TradeList = HLT.Load("TestData\temp.hlt")

            Dim originalListText As String = System.IO.File.ReadAllText("TestData\TradeListHavesWants.hlt")
            Dim duplicateListText As String = System.IO.File.ReadAllText("TestData\temp.hlt")

            System.IO.File.Delete("TestData\temp.hlt")

            Assert.Equal(True, result)

            Assert.Equal(originalListText, duplicateListText)

            Assert.Equal(list.TotalHaves, duplicateList.TotalHaves)
            Assert.Equal(list.TotalWants, duplicateList.TotalWants)
            Assert.Equal(list.Haves.Count, duplicateList.Haves.Count)
            Assert.Equal(list.Wants.Count, duplicateList.Wants.Count)
        End Sub

    End Class
End Namespace

