﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System
Imports System.Data
Imports Xunit

Namespace Highlander.Data.Tests
    Public Class SetsDBTests

        ''' <summary>
        ''' Load a specific Set, confirm it's name and code match the expected values.
        ''' </summary>
        ''' <param name="SetId">The Unique Id of the set to load.</param>
        ''' <param name="SetName">The expected name for the Set Id.</param>
        ''' <param name="SetCode">The expected code for the Set Id.</param>
        <Theory>
        <InlineData(0, "(All)", "(All)", 0, "(All)")>
        <InlineData(1, "Series Edition", "SE", 1, "Thunder Castle Games")>
        <InlineData(14, "Soldiers of Immortality", "SOI", 1, "MLE Team")>
        <InlineData(59, "Otavio Consone Chronicles, Vol 1", "P04", 3, "Paradox Publishing")>
        <InlineData(90, "Michael Moore / Quentin Barnes vs. Byron", "BVM", 4, "Le Montagnard Inc")>
        <InlineData(97, "Gencon 2023 Dawn of Time Preview Decks", "G23", 5, "World of Game Design")>
        Sub TestSetDataById(ByVal SetId As Integer, ByVal SetName As String, ByVal SetCode As String, ByVal SetEdition As Integer, ByVal PublisherName As String)
            Dim CurrentSet As ExpansionSet = ExpansionSet.LoadSet(SetId)
            Assert.Equal(SetName, CurrentSet.Name)
            Assert.Equal(SetCode, CurrentSet.Code)
            Assert.Equal(SetEdition, CurrentSet.Edition)
            Assert.Equal(PublisherName, CurrentSet.Publisher.Name)
        End Sub

        ''' <summary>
        ''' Load a specific Set, confirm it's name and code match the expected values.
        ''' </summary>
        ''' <param name="SetId">The Unique Id of the set to load.</param>
        ''' <param name="SetName">The expected name for the Set Id.</param>
        ''' <param name="SetCode">The expected code for the Set Id.</param>
        <Theory>
        <InlineData(0, "(All)", "(All)", 0, "(All)")>
        <InlineData(1, "Series Edition", "SE", 1, "Thunder Castle Games")>
        <InlineData(14, "Soldiers of Immortality", "SOI", 1, "MLE Team")>
        <InlineData(59, "Otavio Consone Chronicles, Vol 1", "P04", 3, "Paradox Publishing")>
        <InlineData(90, "Michael Moore / Quentin Barnes vs. Byron", "BVM", 4, "Le Montagnard Inc")>
        <InlineData(97, "Gencon 2023 Dawn of Time Preview Decks", "G23", 5, "World of Game Design")>
        Sub TestSetDataByCode(ByVal SetId As Integer, ByVal SetName As String, ByVal SetCode As String, ByVal SetEdition As Integer, ByVal PublisherName As String)
            Dim CurrentSet As ExpansionSet = ExpansionSet.LoadSet(SetCode)
            Assert.Equal(SetId, CurrentSet.Id)
            Assert.Equal(SetName, CurrentSet.Name)
            Assert.Equal(SetEdition, CurrentSet.Edition)
            Assert.Equal(PublisherName, CurrentSet.Publisher.Name)
        End Sub

        ''' <summary>
        ''' Test loading a bad Set Id.
        ''' </summary>
        <Fact>
        Sub TestBadSetId()
            Dim CurrentSet As ExpansionSet = ExpansionSet.LoadSet(9999)
            Assert.Equal(Nothing, CurrentSet)
        End Sub

        ''' <summary>
        ''' Test loading a bad Set code.
        ''' </summary>
        <Fact>
        Sub TestBadSetCode()
            Dim CurrentSet As ExpansionSet = ExpansionSet.LoadSet("ASDF")
            Assert.Equal(Nothing, CurrentSet)
        End Sub

        ''' <summary>
        ''' Test loading all sets from Paradox Publishing to test multiple set loads and sorting.
        ''' </summary>
        <Fact>
        Sub TestLoadParadoxPublishingSets()
            Dim SetsDesc As List(Of ExpansionSet) = ExpansionSet.LoadSets("set_Publisher = 5", "set_long DESC")
            Dim SetsAsc As List(Of ExpansionSet) = ExpansionSet.LoadSets("set_Publisher = 5", "set_long ASC")

            Assert.Equal(8, SetsAsc.Count)
            Assert.Equal(8, SetsDesc.Count)

            Dim i As Integer = 0, j As Integer = SetsDesc.Count - 1

            While i < SetsAsc.Count
                Assert.Equal(SetsAsc(i).Id, SetsDesc(j).Id)
                Assert.Equal(SetsAsc(i).Name, SetsDesc(j).Name)
                Assert.Equal(SetsAsc(i).Code, SetsDesc(j).Code)
                i += 1
                j -= 1
            End While
        End Sub

        ''' <summary>
        ''' Test loading all sets from SAEC Games to test multiple set loads.
        ''' </summary>
        <Fact>
        Sub TestLoadSaecGamesSets()
            Dim Sets As List(Of ExpansionSet) = ExpansionSet.LoadSets("set_Publisher = 3")

            Assert.Equal(5, Sets.Count)
        End Sub
        ''' <summary>
        ''' Load a specific Publisher, confirm it's name and code match the expected values.
        ''' </summary>
        ''' <param name="PublisherId">The Unique Id of the publisher to load.</param>
        ''' <param name="PublisherName">The expected name for the Publisher Id.</param>
        ''' <param name="PublisherCode">The expected code for the Publisher Id.</param>
        <Theory>
        <InlineData(0, "(All)", "ALL")>
        <InlineData(1, "Thunder Castle Games", "TCG")>
        <InlineData(2, "MLE Team", "MLE")>
        Sub TestPublisherDataById(ByVal PublisherId As Integer, ByVal PublisherName As String, ByVal PublisherCode As String)
            Dim CurrentPublisher As Publisher = Publisher.LoadPublisher(PublisherId)
            Assert.Equal(PublisherName, CurrentPublisher.Name)
            Assert.Equal(PublisherCode, CurrentPublisher.Code)
        End Sub

        ''' <summary>
        ''' Load a specific Publisher, confirm it's name and code match the expected values.
        ''' </summary>
        ''' <param name="PublisherId">The Unique Id of the publisher to load.</param>
        ''' <param name="PublisherName">The expected name for the Publisher Id.</param>
        ''' <param name="PublisherCode">The expected code for the Publisher Id.</param>
        <Theory>
        <InlineData(0, "(All)", "ALL")>
        <InlineData(1, "Thunder Castle Games", "TCG")>
        <InlineData(2, "MLE Team", "MLE")>
        Sub TestPublisherDataByName(ByVal PublisherId As Integer, ByVal PublisherName As String, ByVal PublisherCode As String)
            Dim CurrentPublisher As Publisher = Publisher.LoadPublisher(PublisherName)
            Assert.Equal(PublisherId, CurrentPublisher.Id)
            Assert.Equal(PublisherCode, CurrentPublisher.Code)
        End Sub

        ''' <summary>
        ''' Test loading all publishers.
        ''' </summary>
        <Fact>
        Sub TestLoadAllPublishers()
            Dim Publishers As List(Of Publisher) = Publisher.LoadPublishers()

            Assert.Equal(8, Publishers.Count)
        End Sub

        ''' <summary>
        ''' Test loading all publishers with "game" in their name.
        ''' </summary>
        <Fact>
        Sub TestLoadGamePublishers()
            Dim Publishers As List(Of Publisher) = Publisher.LoadPublishers("Long_Name LIKE '%Game%'")

            Assert.Equal(4, Publishers.Count)
        End Sub

        ''' <summary>
        ''' Test loading a bad Publisher Id.
        ''' </summary>
        <Fact>
        Sub TestBadPublisherId()
            Dim CurrentPublisher As Publisher = Publisher.LoadPublisher(9999)
            Assert.Equal(Nothing, CurrentPublisher)
        End Sub

        ''' <summary>
        ''' Test loading a bad Publisher Name.
        ''' </summary>
        <Fact>
        Sub TestBadPublisherName()
            Dim CurrentPublisher As Publisher = Publisher.LoadPublisher("ASDF")
            Assert.Equal(Nothing, CurrentPublisher)
        End Sub

        ''' <summary>
        ''' Test saving the SetsDB.
        ''' </summary>
        <Fact>
        Sub TestSetsDataSave()
            Dim RawSetData As String = System.IO.File.ReadAllText(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\HLSets.xml"))
            SetsDB.Save("TestData\tempSets.xml")
            Dim RawSetDataCopy As String = System.IO.File.ReadAllText("TestData\tempSets.xml")
            System.IO.File.Delete("TestData\tempSets.xml")

            Assert.Equal(RawSetData, RawSetDataCopy)
        End Sub

    End Class
End Namespace

