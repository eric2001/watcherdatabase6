﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Highlander.Data

Public Class CollectionSetData

    ''' <summary>
    ''' Current progress in data set generation.
    ''' </summary>
    ''' <returns></returns>
    Public Property Progress As Integer = 0

    ''' <summary>
    ''' Total number of records to process to create the data set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Maximum As Integer = 0

    ''' <summary>
    ''' The data to use to generate a report with.
    ''' </summary>
    Public Data As DataSet = Nothing

    ''' <summary>
    ''' Generates a DataSet based on the cards.xml and the open .hli file to use with the Collection Set Report.
    ''' </summary>
    Public Sub RefreshData(ByVal SetName As String)
        ' Initialize
        Progress = 0
        Data = Nothing

        Dim TempCardsDB As New DataSet("Card")
        TempCardsDB.ReadXmlSchema(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\ReportData.xsd"))

        Dim results As New List(Of Card)

        Dim SetAbvr As String = ""
        If SetName <> "" Then
            Dim resultsSet() As DataRow = SetsDB.Data.Tables("set").Select("set_long = '" & SetName.Replace("'", "''") & "'")
            If resultsSet.Length > 0 Then
                SetAbvr = "Set = '" & resultsSet(0).Item("set_short") & "'"
                results = Card.LoadCards(SetAbvr)
            End If
        End If

        If results.Count = 0 Then ' If no results then nothing to do, exit
            Data = TempCardsDB
            Maximum = 1
            Progress = Maximum
            Exit Sub
        End If

        Maximum = results.Count + 1

        For Each oneCard As Card In results
            Dim NewRow As DataRow = TempCardsDB.Tables("Card").NewRow
            NewRow.Item("ID") = oneCard.Id
            NewRow.Item("Rarity") = oneCard.Rarity
            NewRow.Item("Type") = oneCard.Type
            NewRow.Item("Restricted") = oneCard.Restricted
            NewRow.Item("Hands") = oneCard.Hands
            NewRow.Item("Text") = oneCard.Text
            NewRow.Item("Grid") = oneCard.Grid
            NewRow.Item("Gems") = oneCard.Gems
            NewRow.Item("PublisherID") = oneCard.Publisher.Id
            NewRow.Item("CardNumber") = oneCard.CardNumber
            NewRow.Item("UniquenessMarker") = oneCard.UniquenessMarker
            NewRow.Item("DoubleDiamond") = oneCard.DoubleDiamond
            NewRow.Item("Quantity") = Inventory.GetCardQuantity(oneCard.Id)
            NewRow.Item("PublisherName") = oneCard.Publisher.Name

            ' Display the card number in the title field, if card number is available
            If Not String.IsNullOrEmpty(oneCard.CardNumber) Then
                NewRow.Item("Title") &= String.Format("{0} ({1})", oneCard.Title, oneCard.CardNumber)
            Else
                NewRow.Item("Title") = oneCard.Title
            End If

            ' Change the "Immortal" field for non-playable inserts to "Inserts" to group them all together.
            If oneCard.Type = "Rules" Or
                    oneCard.Type = "Form" Or
                    oneCard.Type = "Checklist" Then
                NewRow.Item("Immortal") = "Inserts"
            Else
                NewRow.Item("Immortal") = oneCard.Immortal
            End If

            ' Repurpose the Set field to order results as we're not displaying it on the report
            '  Display Immortal Specific / Weapon Specific cards first, then Generic cards, then Inserts (if any)
            'NewRow.Item("CardSet") = oneCard.ExpansionSet.Name
            If oneCard.IsGeneric Then
                NewRow.Item("CardSet") = "Y"
            ElseIf NewRow.Item("Immortal") = "Inserts" Then
                NewRow.Item("CardSet") = "Z"
            Else
                NewRow.Item("CardSet") = "A"
            End If

            TempCardsDB.Tables("Card").Rows.Add(NewRow)
            Progress += 1
        Next

        TempCardsDB.AcceptChanges()

        Data = TempCardsDB
        Progress = Maximum

    End Sub

End Class
