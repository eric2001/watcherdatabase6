﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Highlander.Data

Public Class TradeListData

    ''' <summary>
    ''' Current progress in data set generation.
    ''' </summary>
    ''' <returns></returns>
    Public Property Progress As Integer = 0

    ''' <summary>
    ''' Total number of records to process to create the data set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Maximum As Integer = 0

    ''' <summary>
    ''' The data to use to generate a report with.
    ''' </summary>
    Public Data As DataSet = Nothing

    ''' <summary>
    ''' Generates a DataSet based on the based on the passed in Trade List
    ''' </summary>
    Public Sub RefreshData(ByRef tradeList As TradeList, ByVal listToPrint As String)
        ' Initialize
        Progress = 0
        Maximum = tradeList.Haves.Count + tradeList.Wants.Count + 1
        Data = Nothing

        Dim TempCardsDB As New DataSet("Card")
        TempCardsDB.ReadXmlSchema(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\ReportData.xsd"))

        If listToPrint = "H" Or listToPrint = "B" Then
            For Each oneCard As Card In tradeList.Haves
                Dim NewRow As DataRow = TempCardsDB.Tables("Card").NewRow
                NewRow.Item("ID") = oneCard.Id
                NewRow.Item("Rarity") = oneCard.Rarity
                NewRow.Item("Type") = oneCard.Type
                NewRow.Item("Restricted") = oneCard.Restricted
                NewRow.Item("Hands") = oneCard.Hands
                NewRow.Item("Text") = oneCard.Text
                NewRow.Item("Grid") = oneCard.Grid
                NewRow.Item("Gems") = oneCard.Gems
                NewRow.Item("PublisherID") = oneCard.Publisher.Id
                NewRow.Item("CardNumber") = oneCard.CardNumber
                NewRow.Item("UniquenessMarker") = oneCard.UniquenessMarker
                NewRow.Item("DoubleDiamond") = oneCard.DoubleDiamond
                NewRow.Item("Quantity") = oneCard.Quantity
                NewRow.Item("PublisherName") = oneCard.Publisher.Name
                NewRow.Item("CardSet") = oneCard.ExpansionSet.Code

                ' Display the card number in the title field, if card number is available
                '   Otherwise just display "[Immortal] [Title]" as the card number
                If Not String.IsNullOrEmpty(oneCard.CardNumber) Then
                    NewRow.Item("Title") = String.Format("{0} ({1})", oneCard.OriginalDisplayName, oneCard.CardNumber)
                Else
                    NewRow.Item("Title") = oneCard.OriginalDisplayName
                End If

                ' We're displaying Immortal as part of the title field so
                '   re-purpose this field to use for grouping in the report and display as the section header
                NewRow.Item("Immortal") = "Haves (" & tradeList.TotalHaves.ToString() & ")"

                TempCardsDB.Tables("Card").Rows.Add(NewRow)

                Progress += 1
            Next
        End If

        If listToPrint = "W" Or listToPrint = "B" Then
            For Each oneCard As Card In tradeList.Wants
                Dim NewRow As DataRow = TempCardsDB.Tables("Card").NewRow
                NewRow.Item("ID") = oneCard.Id
                NewRow.Item("Rarity") = oneCard.Rarity
                NewRow.Item("Type") = oneCard.Type
                NewRow.Item("Restricted") = oneCard.Restricted
                NewRow.Item("Hands") = oneCard.Hands
                NewRow.Item("Text") = oneCard.Text
                NewRow.Item("Grid") = oneCard.Grid
                NewRow.Item("Gems") = oneCard.Gems
                NewRow.Item("PublisherID") = oneCard.Publisher.Id
                NewRow.Item("CardNumber") = oneCard.CardNumber
                NewRow.Item("UniquenessMarker") = oneCard.UniquenessMarker
                NewRow.Item("DoubleDiamond") = oneCard.DoubleDiamond
                NewRow.Item("Quantity") = oneCard.Quantity
                NewRow.Item("PublisherName") = oneCard.Publisher.Name
                NewRow.Item("CardSet") = oneCard.ExpansionSet.Code

                ' Display the card number in the title field, if card number is available
                '   Otherwise just display "[Immortal] [Title]" as the card number
                If Not String.IsNullOrEmpty(oneCard.CardNumber) Then
                    NewRow.Item("Title") &= String.Format("{0} ({1})", oneCard.OriginalDisplayName, oneCard.CardNumber)
                Else
                    NewRow.Item("Title") = oneCard.OriginalDisplayName
                End If

                ' We're displaying Immortal as part of the title field so
                '   re-purpose this field to use for grouping in the report and display as the section header
                NewRow.Item("Immortal") = "Wants (" & tradeList.TotalWants.ToString() & ")"

                TempCardsDB.Tables("Card").Rows.Add(NewRow)

                Progress += 1
            Next
        End If

        TempCardsDB.AcceptChanges()

        Data = TempCardsDB
        Progress = Maximum
    End Sub

End Class
