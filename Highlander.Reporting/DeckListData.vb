﻿' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Highlander.Data

Public Class DeckListData

    ''' <summary>
    ''' Current progress in data set generation.
    ''' </summary>
    ''' <returns></returns>
    Public Property Progress As Integer = 0

    ''' <summary>
    ''' Total number of records to process to create the data set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Maximum As Integer = 0

    ''' <summary>
    ''' The data to use to generate a report with.
    ''' </summary>
    Public Data As DataSet = Nothing

    ''' <summary>
    ''' Generates a DataSet based on the based on the passed in Deck.
    ''' </summary>
    Public Sub RefreshData(ByRef deck As HighlanderDeck)
        ' Initialize
        Progress = 0
        Maximum = deck.Deck.Count + deck.PreGame.Count + deck.PreGame.Sum(Function(c) c.SubItems.Count) + 1
        Data = Nothing

        Dim TempCardsDB As New DataSet("Card")
        TempCardsDB.ReadXmlSchema(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\ReportData.xsd"))

        For Each oneCard As Card In deck.Deck
            Dim newRow As DataRow = TempCardsDB.Tables("Card").NewRow
            With newRow
                .Item("ID") = oneCard.Id
                .Item("Quantity") = oneCard.Quantity
                .Item("Immortal") = "Deck (" & deck.DeckSize.ToString() & ")"
                .Item("Rarity") = oneCard.Rarity
                .Item("Type") = oneCard.Type
                .Item("CardSet") = oneCard.ExpansionSet.Code

                ' Display the card number in the title field, if card number is available
                '   Otherwise just display "[Immortal] [Title]" as the card number
                Dim title As String = oneCard.Immortal.Trim()
                If Not String.IsNullOrEmpty(title) Then
                    title &= " "
                End If
                If Not String.IsNullOrEmpty(oneCard.CardNumber) Then
                    .Item("Title") &= String.Format("{0}{1} ({2})", title, oneCard.DisplayTitle, oneCard.CardNumber).Trim
                Else
                    .Item("Title") &= String.Format("{0}{1}", title, oneCard.DisplayTitle).Trim
                End If
            End With

            TempCardsDB.Tables("Card").Rows.Add(newRow)

            Progress += 1
        Next
        TempCardsDB.AcceptChanges()

        For Each oneCard As Card In deck.PreGame
            Dim newRow As DataRow = TempCardsDB.Tables("Card").NewRow
            With newRow
                .Item("ID") = oneCard.Id
                .Item("Quantity") = oneCard.Quantity
                .Item("Immortal") = "Pre-Game (" & deck.PreGameSize.ToString() & ")"
                .Item("Rarity") = oneCard.Rarity
                .Item("Type") = oneCard.Type
                .Item("CardSet") = oneCard.ExpansionSet.Code

                ' Display the card number in the title field, if card number is available
                '   Otherwise just display "[Immortal] [Title]" as the card number
                Dim title As String = oneCard.Immortal.Trim()
                If Not String.IsNullOrEmpty(title) Then
                    title &= " "
                End If
                If Not String.IsNullOrEmpty(oneCard.CardNumber) Then
                    .Item("Title") &= String.Format("{0}{1} ({2})", title, oneCard.DisplayTitle, oneCard.CardNumber).Trim
                Else
                    .Item("Title") &= String.Format("{0}{1}", title, oneCard.DisplayTitle).Trim
                End If
            End With

            TempCardsDB.Tables("Card").Rows.Add(newRow)

            Progress += 1
        Next
        TempCardsDB.AcceptChanges()

        For Each oneCard As Card In deck.PreGame
            If oneCard.SubItems.Count > 0 Then
                Dim CategoryTitle As String = oneCard.Immortal.Trim()
                If Not String.IsNullOrEmpty(CategoryTitle) Then
                    CategoryTitle &= " "
                End If
                CategoryTitle = String.Format("Pre-Game:  {0}{1} ({2})", CategoryTitle, oneCard.DisplayTitle, oneCard.SubItems.Sum(Function(c) c.Quantity)).Trim

                For Each SubCard As Card In oneCard.SubItems
                    Dim newRow As DataRow = TempCardsDB.Tables("Card").NewRow
                    With newRow
                        .Item("ID") = SubCard.Id
                        .Item("Quantity") = SubCard.Quantity
                        .Item("Immortal") = CategoryTitle
                        .Item("Rarity") = SubCard.Rarity
                        .Item("Type") = SubCard.Type
                        .Item("CardSet") = SubCard.ExpansionSet.Code

                        ' Display the card number in the title field, if card number is available
                        '   Otherwise just display "[Immortal] [Title]" as the card number
                        Dim title As String = SubCard.Immortal.Trim()
                        If Not String.IsNullOrEmpty(title) Then
                            title &= " "
                        End If
                        If Not String.IsNullOrEmpty(SubCard.CardNumber) Then
                            .Item("Title") &= String.Format("{0}{1} ({2})", title, SubCard.DisplayTitle, SubCard.CardNumber).Trim
                        Else
                            .Item("Title") &= String.Format("{0}{1}", title, SubCard.DisplayTitle).Trim
                        End If
                    End With

                    TempCardsDB.Tables("Card").Rows.Add(newRow)

                    Progress += 1
                Next
            End If
        Next
        TempCardsDB.AcceptChanges()

        Data = TempCardsDB
        Progress = Maximum
    End Sub

End Class
