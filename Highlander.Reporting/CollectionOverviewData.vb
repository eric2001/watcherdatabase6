' Project Name:  Watcher Database 6
' Copyright 2017 - 2025 Eric Cavaliere
' License:  AGPL Version 3

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU Affero General Public License as
' published by the Free Software Foundation, either version 3 of the
' License, or (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU Affero General Public License for more details.

' You should have received a copy of the GNU Affero General Public License
' along with this program.  If not, see <https://www.gnu.org/licenses/>.

Imports System.Data
Imports Highlander.Data

Public Class CollectionOverviewData

    ''' <summary>
    ''' Current progress in data set generation.
    ''' </summary>
    ''' <returns></returns>
    Public Property Progress As Integer = 0

    ''' <summary>
    ''' Total number of records to process to create the data set.
    ''' </summary>
    ''' <returns></returns>
    Public Property Maximum As Integer = 0

    ''' <summary>
    ''' The data to use to generate a report with.
    ''' </summary>
    Public Data As DataSet = Nothing

    ''' <summary>
    ''' Generates a DataSet based on the cards.xml and the open .hli file to use with the Collection Overview Report.
    ''' </summary>
    Public Sub RefreshData()
        ' Initialize
        Progress = 0
        Maximum = CardsDB.Data.Tables("Card").Rows.Count + 1
        Data = Nothing

        Dim TempCardsDB As New DataSet("Card")
        TempCardsDB.ReadXmlSchema(IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\ReportData.xsd"))

        For Each OneCard As Card In Card.LoadCards("PublisherID > 0")
            If OneCard.ExpansionSet.Name.Length > 0 And OneCard.Publisher.Id > 0 Then
                Dim NewRow As DataRow = TempCardsDB.Tables("Card").NewRow
                NewRow.Item("ID") = OneCard.Id
                NewRow.Item("CardSet") = OneCard.ExpansionSet.Name
                NewRow.Item("Rarity") = OneCard.Rarity
                NewRow.Item("Type") = OneCard.Type
                NewRow.Item("Restricted") = OneCard.Restricted
                NewRow.Item("Hands") = OneCard.Hands
                NewRow.Item("Immortal") = OneCard.Immortal
                NewRow.Item("Title") = OneCard.Title
                NewRow.Item("Text") = OneCard.Text
                NewRow.Item("Grid") = OneCard.Grid
                NewRow.Item("Gems") = OneCard.Gems
                NewRow.Item("PublisherID") = OneCard.Publisher.Id
                NewRow.Item("CardNumber") = OneCard.CardNumber
                NewRow.Item("UniquenessMarker") = OneCard.UniquenessMarker
                NewRow.Item("DoubleDiamond") = OneCard.DoubleDiamond
                NewRow.Item("Quantity") = Inventory.GetCardQuantity(OneCard.Id)
                NewRow.Item("PublisherName") = OneCard.Publisher.Name

                TempCardsDB.Tables("Card").Rows.Add(NewRow)
            End If
            Progress += 1
        Next

        TempCardsDB.AcceptChanges()

        Data = TempCardsDB
        Progress = Maximum
    End Sub
End Class
